declare module "@salesforce/label/c.AFYP" {
    var AFYP: string;
    export default AFYP;
}
declare module "@salesforce/label/c.AIP_Created_Channel" {
    var AIP_Created_Channel: string;
    export default AIP_Created_Channel;
}
declare module "@salesforce/label/c.AIP_Plan_Info" {
    var AIP_Plan_Info: string;
    export default AIP_Plan_Info;
}
declare module "@salesforce/label/c.AIP_Since_Date" {
    var AIP_Since_Date: string;
    export default AIP_Since_Date;
}
declare module "@salesforce/label/c.AIP_balance" {
    var AIP_balance: string;
    export default AIP_balance;
}
declare module "@salesforce/label/c.AIP_date" {
    var AIP_date: string;
    export default AIP_date;
}
declare module "@salesforce/label/c.AIP_frequency" {
    var AIP_frequency: string;
    export default AIP_frequency;
}
declare module "@salesforce/label/c.AIP_instruction" {
    var AIP_instruction: string;
    export default AIP_instruction;
}
declare module "@salesforce/label/c.Account_Edit" {
    var Account_Edit: string;
    export default Account_Edit;
}
declare module "@salesforce/label/c.Account_Name" {
    var Account_Name: string;
    export default Account_Name;
}
declare module "@salesforce/label/c.Account_Number" {
    var Account_Number: string;
    export default Account_Number;
}
declare module "@salesforce/label/c.Account_Status" {
    var Account_Status: string;
    export default Account_Status;
}
declare module "@salesforce/label/c.Action" {
    var Action: string;
    export default Action;
}
declare module "@salesforce/label/c.Action_History" {
    var Action_History: string;
    export default Action_History;
}
declare module "@salesforce/label/c.Activated_Date" {
    var Activated_Date: string;
    export default Activated_Date;
}
declare module "@salesforce/label/c.Additional" {
    var Additional: string;
    export default Additional;
}
declare module "@salesforce/label/c.Additional_Information" {
    var Additional_Information: string;
    export default Additional_Information;
}
declare module "@salesforce/label/c.Address" {
    var Address: string;
    export default Address;
}
declare module "@salesforce/label/c.Address_Information" {
    var Address_Information: string;
    export default Address_Information;
}
declare module "@salesforce/label/c.Address_and_Contact_Number" {
    var Address_and_Contact_Number: string;
    export default Address_and_Contact_Number;
}
declare module "@salesforce/label/c.Alternate_Contact_Information" {
    var Alternate_Contact_Information: string;
    export default Alternate_Contact_Information;
}
declare module "@salesforce/label/c.Amount_of_Interest_Charged" {
    var Amount_of_Interest_Charged: string;
    export default Amount_of_Interest_Charged;
}
declare module "@salesforce/label/c.Amount_of_Next_Cash_Back" {
    var Amount_of_Next_Cash_Back: string;
    export default Amount_of_Next_Cash_Back;
}
declare module "@salesforce/label/c.Amount_of_Non_Interest_Charged" {
    var Amount_of_Non_Interest_Charged: string;
    export default Amount_of_Non_Interest_Charged;
}
declare module "@salesforce/label/c.Approved_Amount" {
    var Approved_Amount: string;
    export default Approved_Amount;
}
declare module "@salesforce/label/c.Approved_Stage" {
    var Approved_Stage: string;
    export default Approved_Stage;
}
declare module "@salesforce/label/c.Asset_Class" {
    var Asset_Class: string;
    export default Asset_Class;
}
declare module "@salesforce/label/c.Assign_To" {
    var Assign_To: string;
    export default Assign_To;
}
declare module "@salesforce/label/c.Authen_Result" {
    var Authen_Result: string;
    export default Authen_Result;
}
declare module "@salesforce/label/c.Auto_Investment_Plan" {
    var Auto_Investment_Plan: string;
    export default Auto_Investment_Plan;
}
declare module "@salesforce/label/c.Automobile_Detail" {
    var Automobile_Detail: string;
    export default Automobile_Detail;
}
declare module "@salesforce/label/c.Automobile_Type_Section" {
    var Automobile_Type_Section: string;
    export default Automobile_Type_Section;
}
declare module "@salesforce/label/c.Available_Balance_Deposit" {
    var Available_Balance_Deposit: string;
    export default Available_Balance_Deposit;
}
declare module "@salesforce/label/c.Average_cost_per_unit" {
    var Average_cost_per_unit: string;
    export default Average_cost_per_unit;
}
declare module "@salesforce/label/c.Average_cost_per_unit_summary" {
    var Average_cost_per_unit_summary: string;
    export default Average_cost_per_unit_summary;
}
declare module "@salesforce/label/c.Avg_Outstanding_MTD" {
    var Avg_Outstanding_MTD: string;
    export default Avg_Outstanding_MTD;
}
declare module "@salesforce/label/c.BA_Plan" {
    var BA_Plan: string;
    export default BA_Plan;
}
declare module "@salesforce/label/c.Back" {
    var Back: string;
    export default Back;
}
declare module "@salesforce/label/c.Bancassurance_Details" {
    var Bancassurance_Details: string;
    export default Bancassurance_Details;
}
declare module "@salesforce/label/c.Bancassurance_Info" {
    var Bancassurance_Info: string;
    export default Bancassurance_Info;
}
declare module "@salesforce/label/c.Bancassurance_Product" {
    var Bancassurance_Product: string;
    export default Bancassurance_Product;
}
declare module "@salesforce/label/c.Bancassurance_Product_Details" {
    var Bancassurance_Product_Details: string;
    export default Bancassurance_Product_Details;
}
declare module "@salesforce/label/c.Beneficiary_Info" {
    var Beneficiary_Info: string;
    export default Beneficiary_Info;
}
declare module "@salesforce/label/c.Beneficiary_Name" {
    var Beneficiary_Name: string;
    export default Beneficiary_Name;
}
declare module "@salesforce/label/c.Benefit" {
    var Benefit: string;
    export default Benefit;
}
declare module "@salesforce/label/c.Block_Code" {
    var Block_Code: string;
    export default Block_Code;
}
declare module "@salesforce/label/c.Brand" {
    var Brand: string;
    export default Brand;
}
declare module "@salesforce/label/c.Business_information_title_Mini_CSV" {
    var Business_information_title_Mini_CSV: string;
    export default Business_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.CCPIN_Verification_CustPhoneNumber" {
    var CCPIN_Verification_CustPhoneNumber: string;
    export default CCPIN_Verification_CustPhoneNumber;
}
declare module "@salesforce/label/c.CCPIN_Verification_Datetime" {
    var CCPIN_Verification_Datetime: string;
    export default CCPIN_Verification_Datetime;
}
declare module "@salesforce/label/c.CCPIN_Verification_EmployeeID" {
    var CCPIN_Verification_EmployeeID: string;
    export default CCPIN_Verification_EmployeeID;
}
declare module "@salesforce/label/c.CCPIN_Verification_FailReason" {
    var CCPIN_Verification_FailReason: string;
    export default CCPIN_Verification_FailReason;
}
declare module "@salesforce/label/c.CCPIN_Verification_Name" {
    var CCPIN_Verification_Name: string;
    export default CCPIN_Verification_Name;
}
declare module "@salesforce/label/c.CCPIN_Verification_Result" {
    var CCPIN_Verification_Result: string;
    export default CCPIN_Verification_Result;
}
declare module "@salesforce/label/c.CCPIN_Verification_Section_Title" {
    var CCPIN_Verification_Section_Title: string;
    export default CCPIN_Verification_Section_Title;
}
declare module "@salesforce/label/c.CCPIN_Verification_Status" {
    var CCPIN_Verification_Status: string;
    export default CCPIN_Verification_Status;
}
declare module "@salesforce/label/c.CCPIN_Verification_Title" {
    var CCPIN_Verification_Title: string;
    export default CCPIN_Verification_Title;
}
declare module "@salesforce/label/c.CCPIN_Verification_Type" {
    var CCPIN_Verification_Type: string;
    export default CCPIN_Verification_Type;
}
declare module "@salesforce/label/c.CCPIN_Verification_Xfer" {
    var CCPIN_Verification_Xfer: string;
    export default CCPIN_Verification_Xfer;
}
declare module "@salesforce/label/c.CCPIN_Verification_Xfer_Label" {
    var CCPIN_Verification_Xfer_Label: string;
    export default CCPIN_Verification_Xfer_Label;
}
declare module "@salesforce/label/c.Camp_Hist_Btn" {
    var Camp_Hist_Btn: string;
    export default Camp_Hist_Btn;
}
declare module "@salesforce/label/c.Campaign_Name" {
    var Campaign_Name: string;
    export default Campaign_Name;
}
declare module "@salesforce/label/c.Campaign_Related" {
    var Campaign_Related: string;
    export default Campaign_Related;
}
declare module "@salesforce/label/c.Cancel" {
    var Cancel: string;
    export default Cancel;
}
declare module "@salesforce/label/c.Card_Activated_Stage" {
    var Card_Activated_Stage: string;
    export default Card_Activated_Stage;
}
declare module "@salesforce/label/c.Card_Number" {
    var Card_Number: string;
    export default Card_Number;
}
declare module "@salesforce/label/c.Cardholder_Name" {
    var Cardholder_Name: string;
    export default Cardholder_Name;
}
declare module "@salesforce/label/c.Case_Choose_Button" {
    var Case_Choose_Button: string;
    export default Case_Choose_Button;
}
declare module "@salesforce/label/c.Case_Close_Invalid_Close_By_BU" {
    var Case_Close_Invalid_Close_By_BU: string;
    export default Case_Close_Invalid_Close_By_BU;
}
declare module "@salesforce/label/c.Case_ERR001" {
    var Case_ERR001: string;
    export default Case_ERR001;
}
declare module "@salesforce/label/c.Case_ERR002" {
    var Case_ERR002: string;
    export default Case_ERR002;
}
declare module "@salesforce/label/c.Case_ERR003" {
    var Case_ERR003: string;
    export default Case_ERR003;
}
declare module "@salesforce/label/c.Case_ERR003_Save_With_New" {
    var Case_ERR003_Save_With_New: string;
    export default Case_ERR003_Save_With_New;
}
declare module "@salesforce/label/c.Case_ERR004" {
    var Case_ERR004: string;
    export default Case_ERR004;
}
declare module "@salesforce/label/c.Case_ERR005" {
    var Case_ERR005: string;
    export default Case_ERR005;
}
declare module "@salesforce/label/c.Case_ERR006" {
    var Case_ERR006: string;
    export default Case_ERR006;
}
declare module "@salesforce/label/c.Case_Edit_Invalid_Problem_Type" {
    var Case_Edit_Invalid_Problem_Type: string;
    export default Case_Edit_Invalid_Problem_Type;
}
declare module "@salesforce/label/c.Case_Edit_Invalid_Resolve_Info" {
    var Case_Edit_Invalid_Resolve_Info: string;
    export default Case_Edit_Invalid_Resolve_Info;
}
declare module "@salesforce/label/c.Case_Edit_Invalid_Service_Request" {
    var Case_Edit_Invalid_Service_Request: string;
    export default Case_Edit_Invalid_Service_Request;
}
declare module "@salesforce/label/c.Case_Edit_NotAuthorizedMsg" {
    var Case_Edit_NotAuthorizedMsg: string;
    export default Case_Edit_NotAuthorizedMsg;
}
declare module "@salesforce/label/c.Case_INFO001" {
    var Case_INFO001: string;
    export default Case_INFO001;
}
declare module "@salesforce/label/c.Case_Section_ORFT" {
    var Case_Section_ORFT: string;
    export default Case_Section_ORFT;
}
declare module "@salesforce/label/c.Case_Section_Third_Party" {
    var Case_Section_Third_Party: string;
    export default Case_Section_Third_Party;
}
declare module "@salesforce/label/c.Case_Select_Product_Number" {
    var Case_Select_Product_Number: string;
    export default Case_Select_Product_Number;
}
declare module "@salesforce/label/c.Case_Tab_Bancassurance" {
    var Case_Tab_Bancassurance: string;
    export default Case_Tab_Bancassurance;
}
declare module "@salesforce/label/c.Case_Tab_Deposit" {
    var Case_Tab_Deposit: string;
    export default Case_Tab_Deposit;
}
declare module "@salesforce/label/c.Case_Tab_Investment" {
    var Case_Tab_Investment: string;
    export default Case_Tab_Investment;
}
declare module "@salesforce/label/c.Case_Table_Header_Account_Number" {
    var Case_Table_Header_Account_Number: string;
    export default Case_Table_Header_Account_Number;
}
declare module "@salesforce/label/c.Case_Table_Header_Product_Name" {
    var Case_Table_Header_Product_Name: string;
    export default Case_Table_Header_Product_Name;
}
declare module "@salesforce/label/c.Case_Table_Header_Product_Sub" {
    var Case_Table_Header_Product_Sub: string;
    export default Case_Table_Header_Product_Sub;
}
declare module "@salesforce/label/c.Case_Table_Header_Suffix" {
    var Case_Table_Header_Suffix: string;
    export default Case_Table_Header_Suffix;
}
declare module "@salesforce/label/c.Case_Tmb_Cus_Error" {
    var Case_Tmb_Cus_Error: string;
    export default Case_Tmb_Cus_Error;
}
declare module "@salesforce/label/c.Cash_Chill_Chill" {
    var Cash_Chill_Chill: string;
    export default Cash_Chill_Chill;
}
declare module "@salesforce/label/c.Cash_Chill_Chill_Status" {
    var Cash_Chill_Chill_Status: string;
    export default Cash_Chill_Chill_Status;
}
declare module "@salesforce/label/c.Cash_Withdrawal_Account_number" {
    var Cash_Withdrawal_Account_number: string;
    export default Cash_Withdrawal_Account_number;
}
declare module "@salesforce/label/c.Changed" {
    var Changed: string;
    export default Changed;
}
declare module "@salesforce/label/c.Claim_Type" {
    var Claim_Type: string;
    export default Claim_Type;
}
declare module "@salesforce/label/c.Closed_Lost_Stage" {
    var Closed_Lost_Stage: string;
    export default Closed_Lost_Stage;
}
declare module "@salesforce/label/c.Closed_Won_Stage" {
    var Closed_Won_Stage: string;
    export default Closed_Won_Stage;
}
declare module "@salesforce/label/c.Co_Borrower_Info" {
    var Co_Borrower_Info: string;
    export default Co_Borrower_Info;
}
declare module "@salesforce/label/c.Co_Borrower_Name" {
    var Co_Borrower_Name: string;
    export default Co_Borrower_Name;
}
declare module "@salesforce/label/c.Collateral_information_title_Mini_CSV" {
    var Collateral_information_title_Mini_CSV: string;
    export default Collateral_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Collateral_owner_header" {
    var Collateral_owner_header: string;
    export default Collateral_owner_header;
}
declare module "@salesforce/label/c.Collateral_type_header" {
    var Collateral_type_header: string;
    export default Collateral_type_header;
}
declare module "@salesforce/label/c.Commercial_Referral" {
    var Commercial_Referral: string;
    export default Commercial_Referral;
}
declare module "@salesforce/label/c.Complete_Stage" {
    var Complete_Stage: string;
    export default Complete_Stage;
}
declare module "@salesforce/label/c.Contact_Number_and_Email_Address" {
    var Contact_Number_and_Email_Address: string;
    export default Contact_Number_and_Email_Address;
}
declare module "@salesforce/label/c.Contact_information_title_Mini_CSV" {
    var Contact_information_title_Mini_CSV: string;
    export default Contact_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Contract_End_Date" {
    var Contract_End_Date: string;
    export default Contract_End_Date;
}
declare module "@salesforce/label/c.Convert_Lead_Title" {
    var Convert_Lead_Title: string;
    export default Convert_Lead_Title;
}
declare module "@salesforce/label/c.Cost_of_Investment" {
    var Cost_of_Investment: string;
    export default Cost_of_Investment;
}
declare module "@salesforce/label/c.Create_By_Note" {
    var Create_By_Note: string;
    export default Create_By_Note;
}
declare module "@salesforce/label/c.CreatedBy" {
    var CreatedBy: string;
    export default CreatedBy;
}
declare module "@salesforce/label/c.Credit_Card" {
    var Credit_Card: string;
    export default Credit_Card;
}
declare module "@salesforce/label/c.Credit_Card_RDC_Information" {
    var Credit_Card_RDC_Information: string;
    export default Credit_Card_RDC_Information;
}
declare module "@salesforce/label/c.Credit_Card_RDC_Product_Details" {
    var Credit_Card_RDC_Product_Details: string;
    export default Credit_Card_RDC_Product_Details;
}
declare module "@salesforce/label/c.Credit_Limit" {
    var Credit_Limit: string;
    export default Credit_Limit;
}
declare module "@salesforce/label/c.Credit_information_title_Mini_CSV" {
    var Credit_information_title_Mini_CSV: string;
    export default Credit_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Current_Offer" {
    var Current_Offer: string;
    export default Current_Offer;
}
declare module "@salesforce/label/c.Current_Tenor" {
    var Current_Tenor: string;
    export default Current_Tenor;
}
declare module "@salesforce/label/c.Customer_Authen_Type" {
    var Customer_Authen_Type: string;
    export default Customer_Authen_Type;
}
declare module "@salesforce/label/c.Customer_Demographic" {
    var Customer_Demographic: string;
    export default Customer_Demographic;
}
declare module "@salesforce/label/c.Customer_Edit" {
    var Customer_Edit: string;
    export default Customer_Edit;
}
declare module "@salesforce/label/c.Customer_Information_navigator" {
    var Customer_Information_navigator: string;
    export default Customer_Information_navigator;
}
declare module "@salesforce/label/c.Customer_Product_ERR001" {
    var Customer_Product_ERR001: string;
    export default Customer_Product_ERR001;
}
declare module "@salesforce/label/c.Customer_Product_ERR002" {
    var Customer_Product_ERR002: string;
    export default Customer_Product_ERR002;
}
declare module "@salesforce/label/c.Customer_Product_ERR003" {
    var Customer_Product_ERR003: string;
    export default Customer_Product_ERR003;
}
declare module "@salesforce/label/c.Customer_Product_Holding_Summary" {
    var Customer_Product_Holding_Summary: string;
    export default Customer_Product_Holding_Summary;
}
declare module "@salesforce/label/c.Customer_Product_Not_Found" {
    var Customer_Product_Not_Found: string;
    export default Customer_Product_Not_Found;
}
declare module "@salesforce/label/c.Customer_Product_Not_Found_OSC14" {
    var Customer_Product_Not_Found_OSC14: string;
    export default Customer_Product_Not_Found_OSC14;
}
declare module "@salesforce/label/c.Customer_Product_Not_Found_OSC15" {
    var Customer_Product_Not_Found_OSC15: string;
    export default Customer_Product_Not_Found_OSC15;
}
declare module "@salesforce/label/c.Customer_Product_Not_Found_OSC16" {
    var Customer_Product_Not_Found_OSC16: string;
    export default Customer_Product_Not_Found_OSC16;
}
declare module "@salesforce/label/c.Customer_Product_ReRequest" {
    var Customer_Product_ReRequest: string;
    export default Customer_Product_ReRequest;
}
declare module "@salesforce/label/c.Customer_Product_Timeout" {
    var Customer_Product_Timeout: string;
    export default Customer_Product_Timeout;
}
declare module "@salesforce/label/c.Customer_Relationship_Information" {
    var Customer_Relationship_Information: string;
    export default Customer_Relationship_Information;
}
declare module "@salesforce/label/c.Customer_information_title_Mini_CSV" {
    var Customer_information_title_Mini_CSV: string;
    export default Customer_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Cycle_Cut" {
    var Cycle_Cut: string;
    export default Cycle_Cut;
}
declare module "@salesforce/label/c.Data_Condition_Hidden_Text" {
    var Data_Condition_Hidden_Text: string;
    export default Data_Condition_Hidden_Text;
}
declare module "@salesforce/label/c.Data_Condition_NotAuthorizedMsg" {
    var Data_Condition_NotAuthorizedMsg: string;
    export default Data_Condition_NotAuthorizedMsg;
}
declare module "@salesforce/label/c.Data_Quality_Details" {
    var Data_Quality_Details: string;
    export default Data_Quality_Details;
}
declare module "@salesforce/label/c.Date" {
    var Date: string;
    export default Date;
}
declare module "@salesforce/label/c.Date_History" {
    var Date_History: string;
    export default Date_History;
}
declare module "@salesforce/label/c.Delete" {
    var Delete: string;
    export default Delete;
}
declare module "@salesforce/label/c.Deposit_Account_Info" {
    var Deposit_Account_Info: string;
    export default Deposit_Account_Info;
}
declare module "@salesforce/label/c.Deposit_Product" {
    var Deposit_Product: string;
    export default Deposit_Product;
}
declare module "@salesforce/label/c.Deposit_Product_Details" {
    var Deposit_Product_Details: string;
    export default Deposit_Product_Details;
}
declare module "@salesforce/label/c.Deposit_information_title_Mini_CSV" {
    var Deposit_information_title_Mini_CSV: string;
    export default Deposit_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Description" {
    var Description: string;
    export default Description;
}
declare module "@salesforce/label/c.Details_of_Bancassurance_Product" {
    var Details_of_Bancassurance_Product: string;
    export default Details_of_Bancassurance_Product;
}
declare module "@salesforce/label/c.Details_of_Credit_Card" {
    var Details_of_Credit_Card: string;
    export default Details_of_Credit_Card;
}
declare module "@salesforce/label/c.Details_of_Investment_Product" {
    var Details_of_Investment_Product: string;
    export default Details_of_Investment_Product;
}
declare module "@salesforce/label/c.Details_of_Product_Holdings" {
    var Details_of_Product_Holdings: string;
    export default Details_of_Product_Holdings;
}
declare module "@salesforce/label/c.Direct_Debit_Account_Number" {
    var Direct_Debit_Account_Number: string;
    export default Direct_Debit_Account_Number;
}
declare module "@salesforce/label/c.Draw_Down_Stage" {
    var Draw_Down_Stage: string;
    export default Draw_Down_Stage;
}
declare module "@salesforce/label/c.ECM_Permission_Error_Msg" {
    var ECM_Permission_Error_Msg: string;
    export default ECM_Permission_Error_Msg;
}
declare module "@salesforce/label/c.ECM_SelectFile_Error_Msg" {
    var ECM_SelectFile_Error_Msg: string;
    export default ECM_SelectFile_Error_Msg;
}
declare module "@salesforce/label/c.ECM_SelectFile_LargeFile_Error_Msg" {
    var ECM_SelectFile_LargeFile_Error_Msg: string;
    export default ECM_SelectFile_LargeFile_Error_Msg;
}
declare module "@salesforce/label/c.ECM_SelectFile_LongName_Error_Msg" {
    var ECM_SelectFile_LongName_Error_Msg: string;
    export default ECM_SelectFile_LongName_Error_Msg;
}
declare module "@salesforce/label/c.ECM_SelectFile_OnlyPDF_Error_Msg" {
    var ECM_SelectFile_OnlyPDF_Error_Msg: string;
    export default ECM_SelectFile_OnlyPDF_Error_Msg;
}
declare module "@salesforce/label/c.ECM_SelectFile_SpecialCharacter_Error_Msg" {
    var ECM_SelectFile_SpecialCharacter_Error_Msg: string;
    export default ECM_SelectFile_SpecialCharacter_Error_Msg;
}
declare module "@salesforce/label/c.ECM_Upload_Fail_Msg" {
    var ECM_Upload_Fail_Msg: string;
    export default ECM_Upload_Fail_Msg;
}
declare module "@salesforce/label/c.ECM_Upload_Success_Msg" {
    var ECM_Upload_Success_Msg: string;
    export default ECM_Upload_Success_Msg;
}
declare module "@salesforce/label/c.ECM_Upload_Timeout_Msg" {
    var ECM_Upload_Timeout_Msg: string;
    export default ECM_Upload_Timeout_Msg;
}
declare module "@salesforce/label/c.ECM_View_Error_Msg" {
    var ECM_View_Error_Msg: string;
    export default ECM_View_Error_Msg;
}
declare module "@salesforce/label/c.ERR001" {
    var ERR001: string;
    export default ERR001;
}
declare module "@salesforce/label/c.ERR002" {
    var ERR002: string;
    export default ERR002;
}
declare module "@salesforce/label/c.ERR003" {
    var ERR003: string;
    export default ERR003;
}
declare module "@salesforce/label/c.ERR004" {
    var ERR004: string;
    export default ERR004;
}
declare module "@salesforce/label/c.ERR005" {
    var ERR005: string;
    export default ERR005;
}
declare module "@salesforce/label/c.ERR006" {
    var ERR006: string;
    export default ERR006;
}
declare module "@salesforce/label/c.ERR007" {
    var ERR007: string;
    export default ERR007;
}
declare module "@salesforce/label/c.ERR008" {
    var ERR008: string;
    export default ERR008;
}
declare module "@salesforce/label/c.E_Client_CSCancelEClientController_GetEclient_Error_Msg" {
    var E_Client_CSCancelEClientController_GetEclient_Error_Msg: string;
    export default E_Client_CSCancelEClientController_GetEclient_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_CSEClientSuitabilityController_GetSurvey_Error_Msg" {
    var E_Client_CSEClientSuitabilityController_GetSurvey_Error_Msg: string;
    export default E_Client_CSEClientSuitabilityController_GetSurvey_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_CSReviewEclientSuitListViewCtrl_NavigateToEclientPage_Success_Msg" {
    var E_Client_CSReviewEclientSuitListViewCtrl_NavigateToEclientPage_Success_Msg: string;
    export default E_Client_CSReviewEclientSuitListViewCtrl_NavigateToEclientPage_Success_Msg;
}
declare module "@salesforce/label/c.E_Client_CSReviewEclientSuitListViewCtrl_Select_Error_Msg" {
    var E_Client_CSReviewEclientSuitListViewCtrl_Select_Error_Msg: string;
    export default E_Client_CSReviewEclientSuitListViewCtrl_Select_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_CSReviewEclientSuitListView_Cancel_Button_Text" {
    var E_Client_CSReviewEclientSuitListView_Cancel_Button_Text: string;
    export default E_Client_CSReviewEclientSuitListView_Cancel_Button_Text;
}
declare module "@salesforce/label/c.E_Client_CSReviewEclientSuitListView_Error_Text" {
    var E_Client_CSReviewEclientSuitListView_Error_Text: string;
    export default E_Client_CSReviewEclientSuitListView_Error_Text;
}
declare module "@salesforce/label/c.E_Client_CSReviewEclientSuitListView_Title_Text" {
    var E_Client_CSReviewEclientSuitListView_Title_Text: string;
    export default E_Client_CSReviewEclientSuitListView_Title_Text;
}
declare module "@salesforce/label/c.E_Client_CS_Cancel_E_Client_Suitability_Cancel_Button_Text" {
    var E_Client_CS_Cancel_E_Client_Suitability_Cancel_Button_Text: string;
    export default E_Client_CS_Cancel_E_Client_Suitability_Cancel_Button_Text;
}
declare module "@salesforce/label/c.E_Client_CS_Cancel_E_Client_Suitability_Confirm_Button_Text" {
    var E_Client_CS_Cancel_E_Client_Suitability_Confirm_Button_Text: string;
    export default E_Client_CS_Cancel_E_Client_Suitability_Confirm_Button_Text;
}
declare module "@salesforce/label/c.E_Client_CS_Cancel_E_Client_Suitability_Title_Text" {
    var E_Client_CS_Cancel_E_Client_Suitability_Title_Text: string;
    export default E_Client_CS_Cancel_E_Client_Suitability_Title_Text;
}
declare module "@salesforce/label/c.E_Client_CS_Change_RM_Sales_Cancel_Button_Text" {
    var E_Client_CS_Change_RM_Sales_Cancel_Button_Text: string;
    export default E_Client_CS_Change_RM_Sales_Cancel_Button_Text;
}
declare module "@salesforce/label/c.E_Client_CS_Change_RM_Sales_Error_Msg" {
    var E_Client_CS_Change_RM_Sales_Error_Msg: string;
    export default E_Client_CS_Change_RM_Sales_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_CS_Change_RM_Sales_Title_Text" {
    var E_Client_CS_Change_RM_Sales_Title_Text: string;
    export default E_Client_CS_Change_RM_Sales_Title_Text;
}
declare module "@salesforce/label/c.E_Client_CS_EClientSuitabilityTriggerHandler_BeforeInsert_Error_Msg" {
    var E_Client_CS_EClientSuitabilityTriggerHandler_BeforeInsert_Error_Msg: string;
    export default E_Client_CS_EClientSuitabilityTriggerHandler_BeforeInsert_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_CS_E_ClientCancelExtension_Error_Msg" {
    var E_Client_CS_E_ClientCancelExtension_Error_Msg: string;
    export default E_Client_CS_E_ClientCancelExtension_Error_Msg;
}
declare module "@salesforce/label/c.E_Client_Cancel_Button_Text" {
    var E_Client_Cancel_Button_Text: string;
    export default E_Client_Cancel_Button_Text;
}
declare module "@salesforce/label/c.E_Client_Cancel_Failed_Msg" {
    var E_Client_Cancel_Failed_Msg: string;
    export default E_Client_Cancel_Failed_Msg;
}
declare module "@salesforce/label/c.E_Client_Cancel_Invalid_Msg" {
    var E_Client_Cancel_Invalid_Msg: string;
    export default E_Client_Cancel_Invalid_Msg;
}
declare module "@salesforce/label/c.E_Client_Cancel_Success_Msg" {
    var E_Client_Cancel_Success_Msg: string;
    export default E_Client_Cancel_Success_Msg;
}
declare module "@salesforce/label/c.E_Client_Cancel_Title" {
    var E_Client_Cancel_Title: string;
    export default E_Client_Cancel_Title;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_FXSale_Text" {
    var E_Client_ChangeRM_FXSale_Text: string;
    export default E_Client_ChangeRM_FXSale_Text;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_Failed_ChangeRM_Msg" {
    var E_Client_ChangeRM_Failed_ChangeRM_Msg: string;
    export default E_Client_ChangeRM_Failed_ChangeRM_Msg;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_Incorrect_ChangeRM_Msg" {
    var E_Client_ChangeRM_Incorrect_ChangeRM_Msg: string;
    export default E_Client_ChangeRM_Incorrect_ChangeRM_Msg;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_Invalid_ChangeRM_Msg" {
    var E_Client_ChangeRM_Invalid_ChangeRM_Msg: string;
    export default E_Client_ChangeRM_Invalid_ChangeRM_Msg;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_RM_Text" {
    var E_Client_ChangeRM_RM_Text: string;
    export default E_Client_ChangeRM_RM_Text;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_Succes_ChangeRM_Msg" {
    var E_Client_ChangeRM_Succes_ChangeRM_Msg: string;
    export default E_Client_ChangeRM_Succes_ChangeRM_Msg;
}
declare module "@salesforce/label/c.E_Client_ChangeRM_Title" {
    var E_Client_ChangeRM_Title: string;
    export default E_Client_ChangeRM_Title;
}
declare module "@salesforce/label/c.E_Client_Confirm_Button_Text" {
    var E_Client_Confirm_Button_Text: string;
    export default E_Client_Confirm_Button_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_Action_Text" {
    var E_Client_FormList_Action_Text: string;
    export default E_Client_FormList_Action_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_ApprovedBy_Text" {
    var E_Client_FormList_ApprovedBy_Text: string;
    export default E_Client_FormList_ApprovedBy_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_CreateDate_Text" {
    var E_Client_FormList_CreateDate_Text: string;
    export default E_Client_FormList_CreateDate_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_Edit_Text" {
    var E_Client_FormList_Edit_Text: string;
    export default E_Client_FormList_Edit_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_FormA_Text" {
    var E_Client_FormList_FormA_Text: string;
    export default E_Client_FormList_FormA_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_FormB_Text" {
    var E_Client_FormList_FormB_Text: string;
    export default E_Client_FormList_FormB_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_FormName_Text" {
    var E_Client_FormList_FormName_Text: string;
    export default E_Client_FormList_FormName_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_LastModifiedBy_Text" {
    var E_Client_FormList_LastModifiedBy_Text: string;
    export default E_Client_FormList_LastModifiedBy_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_NavigateToEdit_Failed_Msg" {
    var E_Client_FormList_NavigateToEdit_Failed_Msg: string;
    export default E_Client_FormList_NavigateToEdit_Failed_Msg;
}
declare module "@salesforce/label/c.E_Client_FormList_NotFoundSurvey_Text" {
    var E_Client_FormList_NotFoundSurvey_Text: string;
    export default E_Client_FormList_NotFoundSurvey_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_Owner_Text" {
    var E_Client_FormList_Owner_Text: string;
    export default E_Client_FormList_Owner_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_ReverseReason_Text" {
    var E_Client_FormList_ReverseReason_Text: string;
    export default E_Client_FormList_ReverseReason_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_Status_Text" {
    var E_Client_FormList_Status_Text: string;
    export default E_Client_FormList_Status_Text;
}
declare module "@salesforce/label/c.E_Client_FormList_View_Text" {
    var E_Client_FormList_View_Text: string;
    export default E_Client_FormList_View_Text;
}
declare module "@salesforce/label/c.E_Client_GeneratePDF_Generate_Failed_Msg" {
    var E_Client_GeneratePDF_Generate_Failed_Msg: string;
    export default E_Client_GeneratePDF_Generate_Failed_Msg;
}
declare module "@salesforce/label/c.E_Client_GeneratePDF_Invalid_Text" {
    var E_Client_GeneratePDF_Invalid_Text: string;
    export default E_Client_GeneratePDF_Invalid_Text;
}
declare module "@salesforce/label/c.E_Client_GeneratePDF_Title_Text" {
    var E_Client_GeneratePDF_Title_Text: string;
    export default E_Client_GeneratePDF_Title_Text;
}
declare module "@salesforce/label/c.E_Client_Header_Customer_Text" {
    var E_Client_Header_Customer_Text: string;
    export default E_Client_Header_Customer_Text;
}
declare module "@salesforce/label/c.E_Client_Header_Name_Text" {
    var E_Client_Header_Name_Text: string;
    export default E_Client_Header_Name_Text;
}
declare module "@salesforce/label/c.E_Client_Header_RM_Text" {
    var E_Client_Header_RM_Text: string;
    export default E_Client_Header_RM_Text;
}
declare module "@salesforce/label/c.E_Client_Header_Sale_Text" {
    var E_Client_Header_Sale_Text: string;
    export default E_Client_Header_Sale_Text;
}
declare module "@salesforce/label/c.E_Client_Header_Status_Text" {
    var E_Client_Header_Status_Text: string;
    export default E_Client_Header_Status_Text;
}
declare module "@salesforce/label/c.E_Client_Lookup_NoResult_Text" {
    var E_Client_Lookup_NoResult_Text: string;
    export default E_Client_Lookup_NoResult_Text;
}
declare module "@salesforce/label/c.E_Client_Lookup_SearchResult_Text" {
    var E_Client_Lookup_SearchResult_Text: string;
    export default E_Client_Lookup_SearchResult_Text;
}
declare module "@salesforce/label/c.E_Client_ReleatedSurveyQuestionList_QuestionNumber_Text" {
    var E_Client_ReleatedSurveyQuestionList_QuestionNumber_Text: string;
    export default E_Client_ReleatedSurveyQuestionList_QuestionNumber_Text;
}
declare module "@salesforce/label/c.E_Client_ReleatedSurveyQuestionList_QuestionTitle_Text" {
    var E_Client_ReleatedSurveyQuestionList_QuestionTitle_Text: string;
    export default E_Client_ReleatedSurveyQuestionList_QuestionTitle_Text;
}
declare module "@salesforce/label/c.E_Client_ReleatedSurveyQuestionList_RequireIput_Text" {
    var E_Client_ReleatedSurveyQuestionList_RequireIput_Text: string;
    export default E_Client_ReleatedSurveyQuestionList_RequireIput_Text;
}
declare module "@salesforce/label/c.E_Client_ReleatedSurveyQuestionList_SurveyQuestionName_Text" {
    var E_Client_ReleatedSurveyQuestionList_SurveyQuestionName_Text: string;
    export default E_Client_ReleatedSurveyQuestionList_SurveyQuestionName_Text;
}
declare module "@salesforce/label/c.E_Client_Remove_Button_Text" {
    var E_Client_Remove_Button_Text: string;
    export default E_Client_Remove_Button_Text;
}
declare module "@salesforce/label/c.E_Client_Reverse_CannotReverse_Msg" {
    var E_Client_Reverse_CannotReverse_Msg: string;
    export default E_Client_Reverse_CannotReverse_Msg;
}
declare module "@salesforce/label/c.E_Client_Reverse_Description_Text" {
    var E_Client_Reverse_Description_Text: string;
    export default E_Client_Reverse_Description_Text;
}
declare module "@salesforce/label/c.E_Client_Reverse_Fail_Msg" {
    var E_Client_Reverse_Fail_Msg: string;
    export default E_Client_Reverse_Fail_Msg;
}
declare module "@salesforce/label/c.E_Client_Reverse_No_Button" {
    var E_Client_Reverse_No_Button: string;
    export default E_Client_Reverse_No_Button;
}
declare module "@salesforce/label/c.E_Client_Reverse_Success_Msg" {
    var E_Client_Reverse_Success_Msg: string;
    export default E_Client_Reverse_Success_Msg;
}
declare module "@salesforce/label/c.E_Client_Reverse_Title_Text" {
    var E_Client_Reverse_Title_Text: string;
    export default E_Client_Reverse_Title_Text;
}
declare module "@salesforce/label/c.E_Client_Reverse_Yes_Button" {
    var E_Client_Reverse_Yes_Button: string;
    export default E_Client_Reverse_Yes_Button;
}
declare module "@salesforce/label/c.E_Client_SurveyFormA_SaveDraft_Button_Text" {
    var E_Client_SurveyFormA_SaveDraft_Button_Text: string;
    export default E_Client_SurveyFormA_SaveDraft_Button_Text;
}
declare module "@salesforce/label/c.E_Client_SurveyFormB_SaveDraft_Button_Text" {
    var E_Client_SurveyFormB_SaveDraft_Button_Text: string;
    export default E_Client_SurveyFormB_SaveDraft_Button_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Cancel_Button_Text" {
    var E_Client_VerifyCustomer_Cancel_Button_Text: string;
    export default E_Client_VerifyCustomer_Cancel_Button_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_CannotEdit_Text" {
    var E_Client_VerifyCustomer_CannotEdit_Text: string;
    export default E_Client_VerifyCustomer_CannotEdit_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Close_Button_Text" {
    var E_Client_VerifyCustomer_Close_Button_Text: string;
    export default E_Client_VerifyCustomer_Close_Button_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Confirm_Button_Text" {
    var E_Client_VerifyCustomer_Confirm_Button_Text: string;
    export default E_Client_VerifyCustomer_Confirm_Button_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_E_Client_Review_Data_Invalid" {
    var E_Client_VerifyCustomer_E_Client_Review_Data_Invalid: string;
    export default E_Client_VerifyCustomer_E_Client_Review_Data_Invalid;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_E_Client_Review_Failed" {
    var E_Client_VerifyCustomer_E_Client_Review_Failed: string;
    export default E_Client_VerifyCustomer_E_Client_Review_Failed;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_E_Client_Review_Success" {
    var E_Client_VerifyCustomer_E_Client_Review_Success: string;
    export default E_Client_VerifyCustomer_E_Client_Review_Success;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Error_Text" {
    var E_Client_VerifyCustomer_Error_Text: string;
    export default E_Client_VerifyCustomer_Error_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_FxSale_Text" {
    var E_Client_VerifyCustomer_FxSale_Text: string;
    export default E_Client_VerifyCustomer_FxSale_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Invalid_E_Client" {
    var E_Client_VerifyCustomer_Invalid_E_Client: string;
    export default E_Client_VerifyCustomer_Invalid_E_Client;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_NewCustomer_Text" {
    var E_Client_VerifyCustomer_NewCustomer_Text: string;
    export default E_Client_VerifyCustomer_NewCustomer_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_No_Button_Text" {
    var E_Client_VerifyCustomer_No_Button_Text: string;
    export default E_Client_VerifyCustomer_No_Button_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Not_Pending" {
    var E_Client_VerifyCustomer_Not_Pending: string;
    export default E_Client_VerifyCustomer_Not_Pending;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_RM_Text" {
    var E_Client_VerifyCustomer_RM_Text: string;
    export default E_Client_VerifyCustomer_RM_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_RMandSale_Title_Text" {
    var E_Client_VerifyCustomer_RMandSale_Title_Text: string;
    export default E_Client_VerifyCustomer_RMandSale_Title_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Title_Text" {
    var E_Client_VerifyCustomer_Title_Text: string;
    export default E_Client_VerifyCustomer_Title_Text;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Update_To_Basic" {
    var E_Client_VerifyCustomer_Update_To_Basic: string;
    export default E_Client_VerifyCustomer_Update_To_Basic;
}
declare module "@salesforce/label/c.E_Client_VerifyCustomer_Yes_Button_Text" {
    var E_Client_VerifyCustomer_Yes_Button_Text: string;
    export default E_Client_VerifyCustomer_Yes_Button_Text;
}
declare module "@salesforce/label/c.E_Statement" {
    var E_Statement: string;
    export default E_Statement;
}
declare module "@salesforce/label/c.Edit" {
    var Edit: string;
    export default Edit;
}
declare module "@salesforce/label/c.Education_Level" {
    var Education_Level: string;
    export default Education_Level;
}
declare module "@salesforce/label/c.Effective_Date" {
    var Effective_Date: string;
    export default Effective_Date;
}
declare module "@salesforce/label/c.End_date" {
    var End_date: string;
    export default End_date;
}
declare module "@salesforce/label/c.Event_Status" {
    var Event_Status: string;
    export default Event_Status;
}
declare module "@salesforce/label/c.Expiry_Date" {
    var Expiry_Date: string;
    export default Expiry_Date;
}
declare module "@salesforce/label/c.FXO_Acknowledge_Failed_Text" {
    var FXO_Acknowledge_Failed_Text: string;
    export default FXO_Acknowledge_Failed_Text;
}
declare module "@salesforce/label/c.FXO_Acknowledge_Success_Text" {
    var FXO_Acknowledge_Success_Text: string;
    export default FXO_Acknowledge_Success_Text;
}
declare module "@salesforce/label/c.FXO_Cancel_Failed_Text" {
    var FXO_Cancel_Failed_Text: string;
    export default FXO_Cancel_Failed_Text;
}
declare module "@salesforce/label/c.FXO_Cancel_Success_Text" {
    var FXO_Cancel_Success_Text: string;
    export default FXO_Cancel_Success_Text;
}
declare module "@salesforce/label/c.FXO_Clone_Error_Text" {
    var FXO_Clone_Error_Text: string;
    export default FXO_Clone_Error_Text;
}
declare module "@salesforce/label/c.FXO_Customer_Rate" {
    var FXO_Customer_Rate: string;
    export default FXO_Customer_Rate;
}
declare module "@salesforce/label/c.FXO_Interbank_Rate" {
    var FXO_Interbank_Rate: string;
    export default FXO_Interbank_Rate;
}
declare module "@salesforce/label/c.FXO_Invalid_order_Acknowledge" {
    var FXO_Invalid_order_Acknowledge: string;
    export default FXO_Invalid_order_Acknowledge;
}
declare module "@salesforce/label/c.FXO_Invalid_order_Cancel" {
    var FXO_Invalid_order_Cancel: string;
    export default FXO_Invalid_order_Cancel;
}
declare module "@salesforce/label/c.FXO_Invalid_order_Submit" {
    var FXO_Invalid_order_Submit: string;
    export default FXO_Invalid_order_Submit;
}
declare module "@salesforce/label/c.FXO_Submit_Fail_Text" {
    var FXO_Submit_Fail_Text: string;
    export default FXO_Submit_Fail_Text;
}
declare module "@salesforce/label/c.FXO_Submit_Fail_Traffic_Text" {
    var FXO_Submit_Fail_Traffic_Text: string;
    export default FXO_Submit_Fail_Traffic_Text;
}
declare module "@salesforce/label/c.FXO_Submit_Success_Text" {
    var FXO_Submit_Success_Text: string;
    export default FXO_Submit_Success_Text;
}
declare module "@salesforce/label/c.FXS_Cancel_Fail" {
    var FXS_Cancel_Fail: string;
    export default FXS_Cancel_Fail;
}
declare module "@salesforce/label/c.FXS_Cancel_Success" {
    var FXS_Cancel_Success: string;
    export default FXS_Cancel_Success;
}
declare module "@salesforce/label/c.FXS_Cancel_Summary_error_text" {
    var FXS_Cancel_Summary_error_text: string;
    export default FXS_Cancel_Summary_error_text;
}
declare module "@salesforce/label/c.FXS_Dashborad_Offline" {
    var FXS_Dashborad_Offline: string;
    export default FXS_Dashborad_Offline;
}
declare module "@salesforce/label/c.FXS_Dashborad_Offline_warning" {
    var FXS_Dashborad_Offline_warning: string;
    export default FXS_Dashborad_Offline_warning;
}
declare module "@salesforce/label/c.FXS_Dashborad_Online" {
    var FXS_Dashborad_Online: string;
    export default FXS_Dashborad_Online;
}
declare module "@salesforce/label/c.FXS_Display_Toast_Fill_Order_Failed" {
    var FXS_Display_Toast_Fill_Order_Failed: string;
    export default FXS_Display_Toast_Fill_Order_Failed;
}
declare module "@salesforce/label/c.FXS_Display_Toast_Update_Fill_Amount" {
    var FXS_Display_Toast_Update_Fill_Amount: string;
    export default FXS_Display_Toast_Update_Fill_Amount;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Back" {
    var FXS_Fill_Amount_Back: string;
    export default FXS_Fill_Amount_Back;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Confirm" {
    var FXS_Fill_Amount_Confirm: string;
    export default FXS_Fill_Amount_Confirm;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Message_Allow_edit" {
    var FXS_Fill_Amount_Message_Allow_edit: string;
    export default FXS_Fill_Amount_Message_Allow_edit;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Message_Invalid_Input" {
    var FXS_Fill_Amount_Message_Invalid_Input: string;
    export default FXS_Fill_Amount_Message_Invalid_Input;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Message_Validation" {
    var FXS_Fill_Amount_Message_Validation: string;
    export default FXS_Fill_Amount_Message_Validation;
}
declare module "@salesforce/label/c.FXS_Fill_Amount_Save" {
    var FXS_Fill_Amount_Save: string;
    export default FXS_Fill_Amount_Save;
}
declare module "@salesforce/label/c.FXS_Fill_Total_Allocate_Amount" {
    var FXS_Fill_Total_Allocate_Amount: string;
    export default FXS_Fill_Total_Allocate_Amount;
}
declare module "@salesforce/label/c.FXS_Form_Cancel_Order_Summary" {
    var FXS_Form_Cancel_Order_Summary: string;
    export default FXS_Form_Cancel_Order_Summary;
}
declare module "@salesforce/label/c.FXS_Form_Edit_Fill_amount" {
    var FXS_Form_Edit_Fill_amount: string;
    export default FXS_Form_Edit_Fill_amount;
}
declare module "@salesforce/label/c.FXS_Invalid_Staus_not_New" {
    var FXS_Invalid_Staus_not_New: string;
    export default FXS_Invalid_Staus_not_New;
}
declare module "@salesforce/label/c.FXS_LastModified_By" {
    var FXS_LastModified_By: string;
    export default FXS_LastModified_By;
}
declare module "@salesforce/label/c.FXS_Short_Total_Request_Amount" {
    var FXS_Short_Total_Request_Amount: string;
    export default FXS_Short_Total_Request_Amount;
}
declare module "@salesforce/label/c.FXS_TotaRequestAmount_has_been_updated" {
    var FXS_TotaRequestAmount_has_been_updated: string;
    export default FXS_TotaRequestAmount_has_been_updated;
}
declare module "@salesforce/label/c.FXS_TotalAllocateAmount_has_been_updated" {
    var FXS_TotalAllocateAmount_has_been_updated: string;
    export default FXS_TotalAllocateAmount_has_been_updated;
}
declare module "@salesforce/label/c.FX_Dashboard_Action_Header" {
    var FX_Dashboard_Action_Header: string;
    export default FX_Dashboard_Action_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Amount_Header" {
    var FX_Dashboard_Amount_Header: string;
    export default FX_Dashboard_Amount_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Buy_Header" {
    var FX_Dashboard_Buy_Header: string;
    export default FX_Dashboard_Buy_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Cancel_Text" {
    var FX_Dashboard_Cancel_Text: string;
    export default FX_Dashboard_Cancel_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Close_Text" {
    var FX_Dashboard_Close_Text: string;
    export default FX_Dashboard_Close_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Confirm_Cancel_Text" {
    var FX_Dashboard_Confirm_Cancel_Text: string;
    export default FX_Dashboard_Confirm_Cancel_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Confirm_Fill_Order_Text" {
    var FX_Dashboard_Confirm_Fill_Order_Text: string;
    export default FX_Dashboard_Confirm_Fill_Order_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Confirm_Text" {
    var FX_Dashboard_Confirm_Text: string;
    export default FX_Dashboard_Confirm_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Cuureny_Exchange_Header" {
    var FX_Dashboard_Cuureny_Exchange_Header: string;
    export default FX_Dashboard_Cuureny_Exchange_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Exchange_Rate_Header" {
    var FX_Dashboard_Exchange_Rate_Header: string;
    export default FX_Dashboard_Exchange_Rate_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Fill_Fail_Notification" {
    var FX_Dashboard_Fill_Fail_Notification: string;
    export default FX_Dashboard_Fill_Fail_Notification;
}
declare module "@salesforce/label/c.FX_Dashboard_Fill_Header" {
    var FX_Dashboard_Fill_Header: string;
    export default FX_Dashboard_Fill_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Fill_Invalid_Text" {
    var FX_Dashboard_Fill_Invalid_Text: string;
    export default FX_Dashboard_Fill_Invalid_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Fill_Success_Notification" {
    var FX_Dashboard_Fill_Success_Notification: string;
    export default FX_Dashboard_Fill_Success_Notification;
}
declare module "@salesforce/label/c.FX_Dashboard_Fill_Text" {
    var FX_Dashboard_Fill_Text: string;
    export default FX_Dashboard_Fill_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Full_Fill_Confirm_Text" {
    var FX_Dashboard_Full_Fill_Confirm_Text: string;
    export default FX_Dashboard_Full_Fill_Confirm_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Init_Text" {
    var FX_Dashboard_Init_Text: string;
    export default FX_Dashboard_Init_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Negative_Fill_Text" {
    var FX_Dashboard_Negative_Fill_Text: string;
    export default FX_Dashboard_Negative_Fill_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Of_Text" {
    var FX_Dashboard_Of_Text: string;
    export default FX_Dashboard_Of_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Over_Fill_Text" {
    var FX_Dashboard_Over_Fill_Text: string;
    export default FX_Dashboard_Over_Fill_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Page_Text" {
    var FX_Dashboard_Page_Text: string;
    export default FX_Dashboard_Page_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Partial_Fill_Confirm_Text" {
    var FX_Dashboard_Partial_Fill_Confirm_Text: string;
    export default FX_Dashboard_Partial_Fill_Confirm_Text;
}
declare module "@salesforce/label/c.FX_Dashboard_Search_Label" {
    var FX_Dashboard_Search_Label: string;
    export default FX_Dashboard_Search_Label;
}
declare module "@salesforce/label/c.FX_Dashboard_Sell_Header" {
    var FX_Dashboard_Sell_Header: string;
    export default FX_Dashboard_Sell_Header;
}
declare module "@salesforce/label/c.FX_Dashboard_Title" {
    var FX_Dashboard_Title: string;
    export default FX_Dashboard_Title;
}
declare module "@salesforce/label/c.FX_Dashboard_Update_Notification" {
    var FX_Dashboard_Update_Notification: string;
    export default FX_Dashboard_Update_Notification;
}
declare module "@salesforce/label/c.FX_Order_Acknowledge_After_Validate_Message" {
    var FX_Order_Acknowledge_After_Validate_Message: string;
    export default FX_Order_Acknowledge_After_Validate_Message;
}
declare module "@salesforce/label/c.FX_Order_Acknowledge_Confirm" {
    var FX_Order_Acknowledge_Confirm: string;
    export default FX_Order_Acknowledge_Confirm;
}
declare module "@salesforce/label/c.FX_Order_Acknowledge_Title" {
    var FX_Order_Acknowledge_Title: string;
    export default FX_Order_Acknowledge_Title;
}
declare module "@salesforce/label/c.FX_Order_Acknowledge_Validate_Message" {
    var FX_Order_Acknowledge_Validate_Message: string;
    export default FX_Order_Acknowledge_Validate_Message;
}
declare module "@salesforce/label/c.FX_Order_Cancel_Confirm" {
    var FX_Order_Cancel_Confirm: string;
    export default FX_Order_Cancel_Confirm;
}
declare module "@salesforce/label/c.FX_Order_Cancel_Title" {
    var FX_Order_Cancel_Title: string;
    export default FX_Order_Cancel_Title;
}
declare module "@salesforce/label/c.FX_Order_Cancel_Validate_Message" {
    var FX_Order_Cancel_Validate_Message: string;
    export default FX_Order_Cancel_Validate_Message;
}
declare module "@salesforce/label/c.FX_Order_Last_Modified" {
    var FX_Order_Last_Modified: string;
    export default FX_Order_Last_Modified;
}
declare module "@salesforce/label/c.FX_Order_List_Clear_Button" {
    var FX_Order_List_Clear_Button: string;
    export default FX_Order_List_Clear_Button;
}
declare module "@salesforce/label/c.FX_Order_List_Title" {
    var FX_Order_List_Title: string;
    export default FX_Order_List_Title;
}
declare module "@salesforce/label/c.FX_Order_Not_Allowed_Cancel" {
    var FX_Order_Not_Allowed_Cancel: string;
    export default FX_Order_Not_Allowed_Cancel;
}
declare module "@salesforce/label/c.FX_Order_Submit_Account_Name" {
    var FX_Order_Submit_Account_Name: string;
    export default FX_Order_Submit_Account_Name;
}
declare module "@salesforce/label/c.FX_Order_Submit_Buy_or_Sell" {
    var FX_Order_Submit_Buy_or_Sell: string;
    export default FX_Order_Submit_Buy_or_Sell;
}
declare module "@salesforce/label/c.FX_Order_Submit_Cancel_Button" {
    var FX_Order_Submit_Cancel_Button: string;
    export default FX_Order_Submit_Cancel_Button;
}
declare module "@salesforce/label/c.FX_Order_Submit_Clone_Button" {
    var FX_Order_Submit_Clone_Button: string;
    export default FX_Order_Submit_Clone_Button;
}
declare module "@salesforce/label/c.FX_Order_Submit_Confirm_Button" {
    var FX_Order_Submit_Confirm_Button: string;
    export default FX_Order_Submit_Confirm_Button;
}
declare module "@salesforce/label/c.FX_Order_Submit_Customer_Rate" {
    var FX_Order_Submit_Customer_Rate: string;
    export default FX_Order_Submit_Customer_Rate;
}
declare module "@salesforce/label/c.FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_faild" {
    var FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_faild: string;
    export default FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_faild;
}
declare module "@salesforce/label/c.FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_success_message" {
    var FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_success_message: string;
    export default FX_Order_Submit_Display_Toast_Cancel_remaining_amount_order_success_message;
}
declare module "@salesforce/label/c.FX_Order_Submit_Display_Toast_Clear_Message" {
    var FX_Order_Submit_Display_Toast_Clear_Message: string;
    export default FX_Order_Submit_Display_Toast_Clear_Message;
}
declare module "@salesforce/label/c.FX_Order_Submit_Expiration_Date" {
    var FX_Order_Submit_Expiration_Date: string;
    export default FX_Order_Submit_Expiration_Date;
}
declare module "@salesforce/label/c.FX_Order_Submit_Fill_Amount" {
    var FX_Order_Submit_Fill_Amount: string;
    export default FX_Order_Submit_Fill_Amount;
}
declare module "@salesforce/label/c.FX_Order_Submit_GTC" {
    var FX_Order_Submit_GTC: string;
    export default FX_Order_Submit_GTC;
}
declare module "@salesforce/label/c.FX_Order_Submit_Invalid_Request_Amount_less_than_Fill_Amount_Message" {
    var FX_Order_Submit_Invalid_Request_Amount_less_than_Fill_Amount_Message: string;
    export default FX_Order_Submit_Invalid_Request_Amount_less_than_Fill_Amount_Message;
}
declare module "@salesforce/label/c.FX_Order_Submit_Name" {
    var FX_Order_Submit_Name: string;
    export default FX_Order_Submit_Name;
}
declare module "@salesforce/label/c.FX_Order_Submit_Owner_Name" {
    var FX_Order_Submit_Owner_Name: string;
    export default FX_Order_Submit_Owner_Name;
}
declare module "@salesforce/label/c.FX_Order_Submit_Remaining_Amount" {
    var FX_Order_Submit_Remaining_Amount: string;
    export default FX_Order_Submit_Remaining_Amount;
}
declare module "@salesforce/label/c.FX_Order_Submit_Request_Amount" {
    var FX_Order_Submit_Request_Amount: string;
    export default FX_Order_Submit_Request_Amount;
}
declare module "@salesforce/label/c.FX_Order_Submit_Spread_Order_Failed_Message" {
    var FX_Order_Submit_Spread_Order_Failed_Message: string;
    export default FX_Order_Submit_Spread_Order_Failed_Message;
}
declare module "@salesforce/label/c.FX_Order_Submit_Spread_Order_Success_Message" {
    var FX_Order_Submit_Spread_Order_Success_Message: string;
    export default FX_Order_Submit_Spread_Order_Success_Message;
}
declare module "@salesforce/label/c.FX_Order_Submit_To_Trader_Message_Validate" {
    var FX_Order_Submit_To_Trader_Message_Validate: string;
    export default FX_Order_Submit_To_Trader_Message_Validate;
}
declare module "@salesforce/label/c.FX_Order_Submit_To_Trader_Title" {
    var FX_Order_Submit_To_Trader_Title: string;
    export default FX_Order_Submit_To_Trader_Title;
}
declare module "@salesforce/label/c.FX_Order_Submit_Total_Fill_Amount_Equal_Total_Allocate_Amount_Message" {
    var FX_Order_Submit_Total_Fill_Amount_Equal_Total_Allocate_Amount_Message: string;
    export default FX_Order_Submit_Total_Fill_Amount_Equal_Total_Allocate_Amount_Message;
}
declare module "@salesforce/label/c.FX_Order_Submit_Total_Fill_Amount_Greater_than_Zero_Message" {
    var FX_Order_Submit_Total_Fill_Amount_Greater_than_Zero_Message: string;
    export default FX_Order_Submit_Total_Fill_Amount_Greater_than_Zero_Message;
}
declare module "@salesforce/label/c.FX_Order_Summary_Invalid_Order" {
    var FX_Order_Summary_Invalid_Order: string;
    export default FX_Order_Summary_Invalid_Order;
}
declare module "@salesforce/label/c.FX_Order_Summary_Not_Allowed_Cancel" {
    var FX_Order_Summary_Not_Allowed_Cancel: string;
    export default FX_Order_Summary_Not_Allowed_Cancel;
}
declare module "@salesforce/label/c.FX_Refresh_Text" {
    var FX_Refresh_Text: string;
    export default FX_Refresh_Text;
}
declare module "@salesforce/label/c.Fax" {
    var Fax: string;
    export default Fax;
}
declare module "@salesforce/label/c.Fetch_NBO_Btn" {
    var Fetch_NBO_Btn: string;
    export default Fetch_NBO_Btn;
}
declare module "@salesforce/label/c.For_Daily_Batch_Desc" {
    var For_Daily_Batch_Desc: string;
    export default For_Daily_Batch_Desc;
}
declare module "@salesforce/label/c.For_Daily_Batch_Header" {
    var For_Daily_Batch_Header: string;
    export default For_Daily_Batch_Header;
}
declare module "@salesforce/label/c.Frequency" {
    var Frequency: string;
    export default Frequency;
}
declare module "@salesforce/label/c.From" {
    var From: string;
    export default From;
}
declare module "@salesforce/label/c.Fund_Name" {
    var Fund_Name: string;
    export default Fund_Name;
}
declare module "@salesforce/label/c.Fund_type" {
    var Fund_type: string;
    export default Fund_type;
}
declare module "@salesforce/label/c.Go_To_List" {
    var Go_To_List: string;
    export default Go_To_List;
}
declare module "@salesforce/label/c.Has_Co_borrower" {
    var Has_Co_borrower: string;
    export default Has_Co_borrower;
}
declare module "@salesforce/label/c.Has_Joint" {
    var Has_Joint: string;
    export default Has_Joint;
}
declare module "@salesforce/label/c.Holding" {
    var Holding: string;
    export default Holding;
}
declare module "@salesforce/label/c.INT_Investment_Incomplete_Info" {
    var INT_Investment_Incomplete_Info: string;
    export default INT_Investment_Incomplete_Info;
}
declare module "@salesforce/label/c.INT_Investment_Record_Not_Found" {
    var INT_Investment_Record_Not_Found: string;
    export default INT_Investment_Record_Not_Found;
}
declare module "@salesforce/label/c.INT_No_Active_Product" {
    var INT_No_Active_Product: string;
    export default INT_No_Active_Product;
}
declare module "@salesforce/label/c.Initial_Value_Cost" {
    var Initial_Value_Cost: string;
    export default Initial_Value_Cost;
}
declare module "@salesforce/label/c.Installment_balance" {
    var Installment_balance: string;
    export default Installment_balance;
}
declare module "@salesforce/label/c.Insurance_Company" {
    var Insurance_Company: string;
    export default Insurance_Company;
}
declare module "@salesforce/label/c.Insurance_Plan" {
    var Insurance_Plan: string;
    export default Insurance_Plan;
}
declare module "@salesforce/label/c.Insured_Claimed_Record" {
    var Insured_Claimed_Record: string;
    export default Insured_Claimed_Record;
}
declare module "@salesforce/label/c.Insurer" {
    var Insurer: string;
    export default Insurer;
}
declare module "@salesforce/label/c.Insurer_contact_number_1" {
    var Insurer_contact_number_1: string;
    export default Insurer_contact_number_1;
}
declare module "@salesforce/label/c.Insurer_contact_number_2" {
    var Insurer_contact_number_2: string;
    export default Insurer_contact_number_2;
}
declare module "@salesforce/label/c.Interact_Channel" {
    var Interact_Channel: string;
    export default Interact_Channel;
}
declare module "@salesforce/label/c.Interest_Earned_YTD" {
    var Interest_Earned_YTD: string;
    export default Interest_Earned_YTD;
}
declare module "@salesforce/label/c.Interest_Plan" {
    var Interest_Plan: string;
    export default Interest_Plan;
}
declare module "@salesforce/label/c.Interest_Rate" {
    var Interest_Rate: string;
    export default Interest_Rate;
}
declare module "@salesforce/label/c.Interested_Products_Title" {
    var Interested_Products_Title: string;
    export default Interested_Products_Title;
}
declare module "@salesforce/label/c.Interested_YTD_Help_Text" {
    var Interested_YTD_Help_Text: string;
    export default Interested_YTD_Help_Text;
}
declare module "@salesforce/label/c.Investment_Info" {
    var Investment_Info: string;
    export default Investment_Info;
}
declare module "@salesforce/label/c.Investment_Product" {
    var Investment_Product: string;
    export default Investment_Product;
}
declare module "@salesforce/label/c.Investment_Product_Details" {
    var Investment_Product_Details: string;
    export default Investment_Product_Details;
}
declare module "@salesforce/label/c.Investment_Transaction" {
    var Investment_Transaction: string;
    export default Investment_Transaction;
}
declare module "@salesforce/label/c.Issued_Stage" {
    var Issued_Stage: string;
    export default Issued_Stage;
}
declare module "@salesforce/label/c.Issuer_Fund_House" {
    var Issuer_Fund_House: string;
    export default Issuer_Fund_House;
}
declare module "@salesforce/label/c.Item_No" {
    var Item_No: string;
    export default Item_No;
}
declare module "@salesforce/label/c.Join_Account_Owner_Name" {
    var Join_Account_Owner_Name: string;
    export default Join_Account_Owner_Name;
}
declare module "@salesforce/label/c.Joint_Account_Info" {
    var Joint_Account_Info: string;
    export default Joint_Account_Info;
}
declare module "@salesforce/label/c.Last_6_Months_Transactor_Revolver" {
    var Last_6_Months_Transactor_Revolver: string;
    export default Last_6_Months_Transactor_Revolver;
}
declare module "@salesforce/label/c.Last_Amount" {
    var Last_Amount: string;
    export default Last_Amount;
}
declare module "@salesforce/label/c.Last_Month_Transaction_Summary" {
    var Last_Month_Transaction_Summary: string;
    export default Last_Month_Transaction_Summary;
}
declare module "@salesforce/label/c.Last_Offer" {
    var Last_Offer: string;
    export default Last_Offer;
}
declare module "@salesforce/label/c.Last_Payment_Date" {
    var Last_Payment_Date: string;
    export default Last_Payment_Date;
}
declare module "@salesforce/label/c.Last_Trx_Date" {
    var Last_Trx_Date: string;
    export default Last_Trx_Date;
}
declare module "@salesforce/label/c.Last_Updated_Date" {
    var Last_Updated_Date: string;
    export default Last_Updated_Date;
}
declare module "@salesforce/label/c.LeadDesc_ProductNameNotFoundAsInterestedProduct" {
    var LeadDesc_ProductNameNotFoundAsInterestedProduct: string;
    export default LeadDesc_ProductNameNotFoundAsInterestedProduct;
}
declare module "@salesforce/label/c.LeadError_CannotConvertToExistingCustomer" {
    var LeadError_CannotConvertToExistingCustomer: string;
    export default LeadError_CannotConvertToExistingCustomer;
}
declare module "@salesforce/label/c.LeadError_DupCustomerBeforeConversion" {
    var LeadError_DupCustomerBeforeConversion: string;
    export default LeadError_DupCustomerBeforeConversion;
}
declare module "@salesforce/label/c.LeadError_NoMoreThanOneInterestedProduct" {
    var LeadError_NoMoreThanOneInterestedProduct: string;
    export default LeadError_NoMoreThanOneInterestedProduct;
}
declare module "@salesforce/label/c.LeadError_OnePrimaryProductBeforeConversion" {
    var LeadError_OnePrimaryProductBeforeConversion: string;
    export default LeadError_OnePrimaryProductBeforeConversion;
}
declare module "@salesforce/label/c.LeadError_QualifiedBeforeConversion" {
    var LeadError_QualifiedBeforeConversion: string;
    export default LeadError_QualifiedBeforeConversion;
}
declare module "@salesforce/label/c.Lead_Create" {
    var Lead_Create: string;
    export default Lead_Create;
}
declare module "@salesforce/label/c.Lead_Detail" {
    var Lead_Detail: string;
    export default Lead_Detail;
}
declare module "@salesforce/label/c.Lead_Edit" {
    var Lead_Edit: string;
    export default Lead_Edit;
}
declare module "@salesforce/label/c.Lead_Information" {
    var Lead_Information: string;
    export default Lead_Information;
}
declare module "@salesforce/label/c.Lead_information_title_Mini_CSV" {
    var Lead_information_title_Mini_CSV: string;
    export default Lead_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Lead_is_disqualified_error_message" {
    var Lead_is_disqualified_error_message: string;
    export default Lead_is_disqualified_error_message;
}
declare module "@salesforce/label/c.Ledger_Balance_Deposit" {
    var Ledger_Balance_Deposit: string;
    export default Ledger_Balance_Deposit;
}
declare module "@salesforce/label/c.Limit" {
    var Limit: string;
    export default Limit;
}
declare module "@salesforce/label/c.Limit_ODLimit" {
    var Limit_ODLimit: string;
    export default Limit_ODLimit;
}
declare module "@salesforce/label/c.Loan_Credit_Limit" {
    var Loan_Credit_Limit: string;
    export default Loan_Credit_Limit;
}
declare module "@salesforce/label/c.Loan_Info" {
    var Loan_Info: string;
    export default Loan_Info;
}
declare module "@salesforce/label/c.Loan_Payment_Info" {
    var Loan_Payment_Info: string;
    export default Loan_Payment_Info;
}
declare module "@salesforce/label/c.Loan_Product" {
    var Loan_Product: string;
    export default Loan_Product;
}
declare module "@salesforce/label/c.Loan_Product_Detail" {
    var Loan_Product_Detail: string;
    export default Loan_Product_Detail;
}
declare module "@salesforce/label/c.Loan_Product_Details" {
    var Loan_Product_Details: string;
    export default Loan_Product_Details;
}
declare module "@salesforce/label/c.Log_a_Call" {
    var Log_a_Call: string;
    export default Log_a_Call;
}
declare module "@salesforce/label/c.ME_Account_Bundled_Bank" {
    var ME_Account_Bundled_Bank: string;
    export default ME_Account_Bundled_Bank;
}
declare module "@salesforce/label/c.Marital_Status" {
    var Marital_Status: string;
    export default Marital_Status;
}
declare module "@salesforce/label/c.Market_Consent_Consent_to_disclosure_N" {
    var Market_Consent_Consent_to_disclosure_N: string;
    export default Market_Consent_Consent_to_disclosure_N;
}
declare module "@salesforce/label/c.Market_Consent_Consent_to_disclosure_Y" {
    var Market_Consent_Consent_to_disclosure_Y: string;
    export default Market_Consent_Consent_to_disclosure_Y;
}
declare module "@salesforce/label/c.Market_Value" {
    var Market_Value: string;
    export default Market_Value;
}
declare module "@salesforce/label/c.Maturity_Date" {
    var Maturity_Date: string;
    export default Maturity_Date;
}
declare module "@salesforce/label/c.Mobile_No_HelpText" {
    var Mobile_No_HelpText: string;
    export default Mobile_No_HelpText;
}
declare module "@salesforce/label/c.Model" {
    var Model: string;
    export default Model;
}
declare module "@salesforce/label/c.My_Customers" {
    var My_Customers: string;
    export default My_Customers;
}
declare module "@salesforce/label/c.My_Leads" {
    var My_Leads: string;
    export default My_Leads;
}
declare module "@salesforce/label/c.NAV_Unit" {
    var NAV_Unit: string;
    export default NAV_Unit;
}
declare module "@salesforce/label/c.Nationality_c" {
    var Nationality_c: string;
    export default Nationality_c;
}
declare module "@salesforce/label/c.New_Attach" {
    var New_Attach: string;
    export default New_Attach;
}
declare module "@salesforce/label/c.New_Case" {
    var New_Case: string;
    export default New_Case;
}
declare module "@salesforce/label/c.New_Customer_Complaint" {
    var New_Customer_Complaint: string;
    export default New_Customer_Complaint;
}
declare module "@salesforce/label/c.New_Customer_Title" {
    var New_Customer_Title: string;
    export default New_Customer_Title;
}
declare module "@salesforce/label/c.New_Do_Not_Contact" {
    var New_Do_Not_Contact: string;
    export default New_Do_Not_Contact;
}
declare module "@salesforce/label/c.New_Event" {
    var New_Event: string;
    export default New_Event;
}
declare module "@salesforce/label/c.New_Lead" {
    var New_Lead: string;
    export default New_Lead;
}
declare module "@salesforce/label/c.New_Note" {
    var New_Note: string;
    export default New_Note;
}
declare module "@salesforce/label/c.New_Opportunity" {
    var New_Opportunity: string;
    export default New_Opportunity;
}
declare module "@salesforce/label/c.New_Referral" {
    var New_Referral: string;
    export default New_Referral;
}
declare module "@salesforce/label/c.New_Task" {
    var New_Task: string;
    export default New_Task;
}
declare module "@salesforce/label/c.Next_Cash_Back_Info" {
    var Next_Cash_Back_Info: string;
    export default Next_Cash_Back_Info;
}
declare module "@salesforce/label/c.Next_Cash_Back_Payment_Date" {
    var Next_Cash_Back_Payment_Date: string;
    export default Next_Cash_Back_Payment_Date;
}
declare module "@salesforce/label/c.Next_Expired_Point_On" {
    var Next_Expired_Point_On: string;
    export default Next_Expired_Point_On;
}
declare module "@salesforce/label/c.Next_Expired_Points" {
    var Next_Expired_Points: string;
    export default Next_Expired_Points;
}
declare module "@salesforce/label/c.Next_due_date" {
    var Next_due_date: string;
    export default Next_due_date;
}
declare module "@salesforce/label/c.No_of_Time_Premium_Paid_This_Year" {
    var No_of_Time_Premium_Paid_This_Year: string;
    export default No_of_Time_Premium_Paid_This_Year;
}
declare module "@salesforce/label/c.Number_of_Account" {
    var Number_of_Account: string;
    export default Number_of_Account;
}
declare module "@salesforce/label/c.Number_of_Active_Debit_Card_Bundling" {
    var Number_of_Active_Debit_Card_Bundling: string;
    export default Number_of_Active_Debit_Card_Bundling;
}
declare module "@salesforce/label/c.Number_of_Product" {
    var Number_of_Product: string;
    export default Number_of_Product;
}
declare module "@salesforce/label/c.Number_of_unit" {
    var Number_of_unit: string;
    export default Number_of_unit;
}
declare module "@salesforce/label/c.OD_Balance_Deposit" {
    var OD_Balance_Deposit: string;
    export default OD_Balance_Deposit;
}
declare module "@salesforce/label/c.OTC_ATM_ADM_IB_MIB" {
    var OTC_ATM_ADM_IB_MIB: string;
    export default OTC_ATM_ADM_IB_MIB;
}
declare module "@salesforce/label/c.Occupation" {
    var Occupation: string;
    export default Occupation;
}
declare module "@salesforce/label/c.Open_Date" {
    var Open_Date: string;
    export default Open_Date;
}
declare module "@salesforce/label/c.Opened_Date" {
    var Opened_Date: string;
    export default Opened_Date;
}
declare module "@salesforce/label/c.Opportunity_Detail" {
    var Opportunity_Detail: string;
    export default Opportunity_Detail;
}
declare module "@salesforce/label/c.Order_Not_Found" {
    var Order_Not_Found: string;
    export default Order_Not_Found;
}
declare module "@salesforce/label/c.Order_Type_Buy" {
    var Order_Type_Buy: string;
    export default Order_Type_Buy;
}
declare module "@salesforce/label/c.Order_Type_Sell" {
    var Order_Type_Sell: string;
    export default Order_Type_Sell;
}
declare module "@salesforce/label/c.Order_Type_Switch" {
    var Order_Type_Switch: string;
    export default Order_Type_Switch;
}
declare module "@salesforce/label/c.Other" {
    var Other: string;
    export default Other;
}
declare module "@salesforce/label/c.Other_Conditions" {
    var Other_Conditions: string;
    export default Other_Conditions;
}
declare module "@salesforce/label/c.Outstanding" {
    var Outstanding: string;
    export default Outstanding;
}
declare module "@salesforce/label/c.Outstanding_Balance" {
    var Outstanding_Balance: string;
    export default Outstanding_Balance;
}
declare module "@salesforce/label/c.Outstanding_Credit" {
    var Outstanding_Credit: string;
    export default Outstanding_Credit;
}
declare module "@salesforce/label/c.Outstanding_Deposit" {
    var Outstanding_Deposit: string;
    export default Outstanding_Deposit;
}
declare module "@salesforce/label/c.Outstanding_Deposit_Help_Text" {
    var Outstanding_Deposit_Help_Text: string;
    export default Outstanding_Deposit_Help_Text;
}
declare module "@salesforce/label/c.Owner_Full_Name" {
    var Owner_Full_Name: string;
    export default Owner_Full_Name;
}
declare module "@salesforce/label/c.Payment_Behavior" {
    var Payment_Behavior: string;
    export default Payment_Behavior;
}
declare module "@salesforce/label/c.Payment_Cash_Credit" {
    var Payment_Cash_Credit: string;
    export default Payment_Cash_Credit;
}
declare module "@salesforce/label/c.Payment_Due" {
    var Payment_Due: string;
    export default Payment_Due;
}
declare module "@salesforce/label/c.Payment_Due_Date" {
    var Payment_Due_Date: string;
    export default Payment_Due_Date;
}
declare module "@salesforce/label/c.Payment_Info" {
    var Payment_Info: string;
    export default Payment_Info;
}
declare module "@salesforce/label/c.Payment_Method_BA" {
    var Payment_Method_BA: string;
    export default Payment_Method_BA;
}
declare module "@salesforce/label/c.Payment_Method_Loan" {
    var Payment_Method_Loan: string;
    export default Payment_Method_Loan;
}
declare module "@salesforce/label/c.Payment_Mode" {
    var Payment_Mode: string;
    export default Payment_Mode;
}
declare module "@salesforce/label/c.Payment_method" {
    var Payment_method: string;
    export default Payment_method;
}
declare module "@salesforce/label/c.Payroll_deduction_unit" {
    var Payroll_deduction_unit: string;
    export default Payroll_deduction_unit;
}
declare module "@salesforce/label/c.Pending_Task" {
    var Pending_Task: string;
    export default Pending_Task;
}
declare module "@salesforce/label/c.Period" {
    var Period: string;
    export default Period;
}
declare module "@salesforce/label/c.Personalized_Information" {
    var Personalized_Information: string;
    export default Personalized_Information;
}
declare module "@salesforce/label/c.Plate_Number" {
    var Plate_Number: string;
    export default Plate_Number;
}
declare module "@salesforce/label/c.Policy_No" {
    var Policy_No: string;
    export default Policy_No;
}
declare module "@salesforce/label/c.Policy_number" {
    var Policy_number: string;
    export default Policy_number;
}
declare module "@salesforce/label/c.Portfolio_Review" {
    var Portfolio_Review: string;
    export default Portfolio_Review;
}
declare module "@salesforce/label/c.Premium_Amount" {
    var Premium_Amount: string;
    export default Premium_Amount;
}
declare module "@salesforce/label/c.Privilege_Start_End_Date" {
    var Privilege_Start_End_Date: string;
    export default Privilege_Start_End_Date;
}
declare module "@salesforce/label/c.Product" {
    var Product: string;
    export default Product;
}
declare module "@salesforce/label/c.Product_Group" {
    var Product_Group: string;
    export default Product_Group;
}
declare module "@salesforce/label/c.Product_Holding" {
    var Product_Holding: string;
    export default Product_Holding;
}
declare module "@salesforce/label/c.Product_Holding_Btn" {
    var Product_Holding_Btn: string;
    export default Product_Holding_Btn;
}
declare module "@salesforce/label/c.Product_Holding_ReRequest" {
    var Product_Holding_ReRequest: string;
    export default Product_Holding_ReRequest;
}
declare module "@salesforce/label/c.Product_Holding_Timeout" {
    var Product_Holding_Timeout: string;
    export default Product_Holding_Timeout;
}
declare module "@salesforce/label/c.Product_Holding_and_NBO" {
    var Product_Holding_and_NBO: string;
    export default Product_Holding_and_NBO;
}
declare module "@salesforce/label/c.Product_Holding_information_title_Mini_CSV" {
    var Product_Holding_information_title_Mini_CSV: string;
    export default Product_Holding_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.Product_Name" {
    var Product_Name: string;
    export default Product_Name;
}
declare module "@salesforce/label/c.Product_Sub_Group" {
    var Product_Sub_Group: string;
    export default Product_Sub_Group;
}
declare module "@salesforce/label/c.Product_Type" {
    var Product_Type: string;
    export default Product_Type;
}
declare module "@salesforce/label/c.Production_Description_header" {
    var Production_Description_header: string;
    export default Production_Description_header;
}
declare module "@salesforce/label/c.Production_Name_header" {
    var Production_Name_header: string;
    export default Production_Name_header;
}
declare module "@salesforce/label/c.Property_Detail" {
    var Property_Detail: string;
    export default Property_Detail;
}
declare module "@salesforce/label/c.Property_Type_Section" {
    var Property_Type_Section: string;
    export default Property_Type_Section;
}
declare module "@salesforce/label/c.Prospect_Stage" {
    var Prospect_Stage: string;
    export default Prospect_Stage;
}
declare module "@salesforce/label/c.Qualified_Lead_Validation_Error_Message" {
    var Qualified_Lead_Validation_Error_Message: string;
    export default Qualified_Lead_Validation_Error_Message;
}
declare module "@salesforce/label/c.Quick_CA" {
    var Quick_CA: string;
    export default Quick_CA;
}
declare module "@salesforce/label/c.RTL_Age_22_To_26" {
    var RTL_Age_22_To_26: string;
    export default RTL_Age_22_To_26;
}
declare module "@salesforce/label/c.RTL_Age_27_To_40" {
    var RTL_Age_27_To_40: string;
    export default RTL_Age_27_To_40;
}
declare module "@salesforce/label/c.RTL_Age_41_To_60" {
    var RTL_Age_41_To_60: string;
    export default RTL_Age_41_To_60;
}
declare module "@salesforce/label/c.RTL_Age_Low_21" {
    var RTL_Age_Low_21: string;
    export default RTL_Age_Low_21;
}
declare module "@salesforce/label/c.RTL_Age_Up_60" {
    var RTL_Age_Up_60: string;
    export default RTL_Age_Up_60;
}
declare module "@salesforce/label/c.RTL_Approval" {
    var RTL_Approval: string;
    export default RTL_Approval;
}
declare module "@salesforce/label/c.RTL_ApprovalCondition_01" {
    var RTL_ApprovalCondition_01: string;
    export default RTL_ApprovalCondition_01;
}
declare module "@salesforce/label/c.RTL_ApprovalCondition_02" {
    var RTL_ApprovalCondition_02: string;
    export default RTL_ApprovalCondition_02;
}
declare module "@salesforce/label/c.RTL_ApprovalCondition_03" {
    var RTL_ApprovalCondition_03: string;
    export default RTL_ApprovalCondition_03;
}
declare module "@salesforce/label/c.RTL_Approval_Details" {
    var RTL_Approval_Details: string;
    export default RTL_Approval_Details;
}
declare module "@salesforce/label/c.RTL_Approve_All" {
    var RTL_Approve_All: string;
    export default RTL_Approve_All;
}
declare module "@salesforce/label/c.RTL_Back_Button_Label" {
    var RTL_Back_Button_Label: string;
    export default RTL_Back_Button_Label;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_TitlePage" {
    var RTL_CampaignMemberCreateOpportunity_TitlePage: string;
    export default RTL_CampaignMemberCreateOpportunity_TitlePage;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_colAmount" {
    var RTL_CampaignMemberCreateOpportunity_colAmount: string;
    export default RTL_CampaignMemberCreateOpportunity_colAmount;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_colStage" {
    var RTL_CampaignMemberCreateOpportunity_colStage: string;
    export default RTL_CampaignMemberCreateOpportunity_colStage;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_colStatus" {
    var RTL_CampaignMemberCreateOpportunity_colStatus: string;
    export default RTL_CampaignMemberCreateOpportunity_colStatus;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_secCreateOpportunity" {
    var RTL_CampaignMemberCreateOpportunity_secCreateOpportunity: string;
    export default RTL_CampaignMemberCreateOpportunity_secCreateOpportunity;
}
declare module "@salesforce/label/c.RTL_CampaignMemberCreateOpportunity_secLeadConversion" {
    var RTL_CampaignMemberCreateOpportunity_secLeadConversion: string;
    export default RTL_CampaignMemberCreateOpportunity_secLeadConversion;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_AccountOrLead" {
    var RTL_CampaignMemberEdit_AccountOrLead: string;
    export default RTL_CampaignMemberEdit_AccountOrLead;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColNo" {
    var RTL_CampaignMemberEdit_ColNo: string;
    export default RTL_CampaignMemberEdit_ColNo;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColOfferResult" {
    var RTL_CampaignMemberEdit_ColOfferResult: string;
    export default RTL_CampaignMemberEdit_ColOfferResult;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColOpportunity" {
    var RTL_CampaignMemberEdit_ColOpportunity: string;
    export default RTL_CampaignMemberEdit_ColOpportunity;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColProductGroup" {
    var RTL_CampaignMemberEdit_ColProductGroup: string;
    export default RTL_CampaignMemberEdit_ColProductGroup;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColProductName" {
    var RTL_CampaignMemberEdit_ColProductName: string;
    export default RTL_CampaignMemberEdit_ColProductName;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColProductSubGroup" {
    var RTL_CampaignMemberEdit_ColProductSubGroup: string;
    export default RTL_CampaignMemberEdit_ColProductSubGroup;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ColReason" {
    var RTL_CampaignMemberEdit_ColReason: string;
    export default RTL_CampaignMemberEdit_ColReason;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ContactStatusAndOfferResult_ErrMsg" {
    var RTL_CampaignMemberEdit_ContactStatusAndOfferResult_ErrMsg: string;
    export default RTL_CampaignMemberEdit_ContactStatusAndOfferResult_ErrMsg;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ContactStatus_ErrMsg" {
    var RTL_CampaignMemberEdit_ContactStatus_ErrMsg: string;
    export default RTL_CampaignMemberEdit_ContactStatus_ErrMsg;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_Convert_Lead_API" {
    var RTL_CampaignMemberEdit_Convert_Lead_API: string;
    export default RTL_CampaignMemberEdit_Convert_Lead_API;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ERR001" {
    var RTL_CampaignMemberEdit_ERR001: string;
    export default RTL_CampaignMemberEdit_ERR001;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ERR002" {
    var RTL_CampaignMemberEdit_ERR002: string;
    export default RTL_CampaignMemberEdit_ERR002;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ERR003" {
    var RTL_CampaignMemberEdit_ERR003: string;
    export default RTL_CampaignMemberEdit_ERR003;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ERR004" {
    var RTL_CampaignMemberEdit_ERR004: string;
    export default RTL_CampaignMemberEdit_ERR004;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_OfferStatus_ErrMsg" {
    var RTL_CampaignMemberEdit_OfferStatus_ErrMsg: string;
    export default RTL_CampaignMemberEdit_OfferStatus_ErrMsg;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_OfferStatus_Interested_ErrMsg" {
    var RTL_CampaignMemberEdit_OfferStatus_Interested_ErrMsg: string;
    export default RTL_CampaignMemberEdit_OfferStatus_Interested_ErrMsg;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_OpportunityList" {
    var RTL_CampaignMemberEdit_OpportunityList: string;
    export default RTL_CampaignMemberEdit_OpportunityList;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_SecCampaignProduct" {
    var RTL_CampaignMemberEdit_SecCampaignProduct: string;
    export default RTL_CampaignMemberEdit_SecCampaignProduct;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_SecInformation" {
    var RTL_CampaignMemberEdit_SecInformation: string;
    export default RTL_CampaignMemberEdit_SecInformation;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_SecInvitation" {
    var RTL_CampaignMemberEdit_SecInvitation: string;
    export default RTL_CampaignMemberEdit_SecInvitation;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_SecMemo" {
    var RTL_CampaignMemberEdit_SecMemo: string;
    export default RTL_CampaignMemberEdit_SecMemo;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_SecSegmentation" {
    var RTL_CampaignMemberEdit_SecSegmentation: string;
    export default RTL_CampaignMemberEdit_SecSegmentation;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_TitlePage" {
    var RTL_CampaignMemberEdit_TitlePage: string;
    export default RTL_CampaignMemberEdit_TitlePage;
}
declare module "@salesforce/label/c.RTL_CampaignMemberEdit_ViewHistory" {
    var RTL_CampaignMemberEdit_ViewHistory: string;
    export default RTL_CampaignMemberEdit_ViewHistory;
}
declare module "@salesforce/label/c.RTL_CampaignMemberMassCampaign_ERR001" {
    var RTL_CampaignMemberMassCampaign_ERR001: string;
    export default RTL_CampaignMemberMassCampaign_ERR001;
}
declare module "@salesforce/label/c.RTL_CampaignMemberMassCampaign_ERR002" {
    var RTL_CampaignMemberMassCampaign_ERR002: string;
    export default RTL_CampaignMemberMassCampaign_ERR002;
}
declare module "@salesforce/label/c.RTL_CampaignMemberMassCampaign_ERR003" {
    var RTL_CampaignMemberMassCampaign_ERR003: string;
    export default RTL_CampaignMemberMassCampaign_ERR003;
}
declare module "@salesforce/label/c.RTL_CampaignMemberMassCampaign_ERR004" {
    var RTL_CampaignMemberMassCampaign_ERR004: string;
    export default RTL_CampaignMemberMassCampaign_ERR004;
}
declare module "@salesforce/label/c.RTL_CampaignMemberViewOpportunity_OwnerName" {
    var RTL_CampaignMemberViewOpportunity_OwnerName: string;
    export default RTL_CampaignMemberViewOpportunity_OwnerName;
}
declare module "@salesforce/label/c.RTL_CampaignMemberViewOpportunity_TitlePage" {
    var RTL_CampaignMemberViewOpportunity_TitlePage: string;
    export default RTL_CampaignMemberViewOpportunity_TitlePage;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteEmail_Content" {
    var RTL_CampaignMember_DeleteEmail_Content: string;
    export default RTL_CampaignMember_DeleteEmail_Content;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteEmail_Subject" {
    var RTL_CampaignMember_DeleteEmail_Subject: string;
    export default RTL_CampaignMember_DeleteEmail_Subject;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteObject" {
    var RTL_CampaignMember_DeleteObject: string;
    export default RTL_CampaignMember_DeleteObject;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteObject_Description_Text" {
    var RTL_CampaignMember_DeleteObject_Description_Text: string;
    export default RTL_CampaignMember_DeleteObject_Description_Text;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteObject_Subject_Text" {
    var RTL_CampaignMember_DeleteObject_Subject_Text: string;
    export default RTL_CampaignMember_DeleteObject_Subject_Text;
}
declare module "@salesforce/label/c.RTL_CampaignMember_DeleteObject_Text" {
    var RTL_CampaignMember_DeleteObject_Text: string;
    export default RTL_CampaignMember_DeleteObject_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Edit_Text" {
    var RTL_Campaign_CallList_Edit_Text: string;
    export default RTL_Campaign_CallList_Edit_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Filter_All" {
    var RTL_Campaign_CallList_Filter_All: string;
    export default RTL_Campaign_CallList_Filter_All;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Filter_Campaign" {
    var RTL_Campaign_CallList_Filter_Campaign: string;
    export default RTL_Campaign_CallList_Filter_Campaign;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Filter_Owner" {
    var RTL_Campaign_CallList_Filter_Owner: string;
    export default RTL_Campaign_CallList_Filter_Owner;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Filter_Section" {
    var RTL_Campaign_CallList_Filter_Section: string;
    export default RTL_Campaign_CallList_Filter_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Head_Action" {
    var RTL_Campaign_CallList_Head_Action: string;
    export default RTL_Campaign_CallList_Head_Action;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Head_CampaignName" {
    var RTL_Campaign_CallList_Head_CampaignName: string;
    export default RTL_Campaign_CallList_Head_CampaignName;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Head_LeadName" {
    var RTL_Campaign_CallList_Head_LeadName: string;
    export default RTL_Campaign_CallList_Head_LeadName;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Head_No" {
    var RTL_Campaign_CallList_Head_No: string;
    export default RTL_Campaign_CallList_Head_No;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_List_Section" {
    var RTL_Campaign_CallList_List_Section: string;
    export default RTL_Campaign_CallList_List_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Loading_Text" {
    var RTL_Campaign_CallList_Loading_Text: string;
    export default RTL_Campaign_CallList_Loading_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_MouseOver_Tooltip" {
    var RTL_Campaign_CallList_MouseOver_Tooltip: string;
    export default RTL_Campaign_CallList_MouseOver_Tooltip;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_MyList_Option" {
    var RTL_Campaign_CallList_MyList_Option: string;
    export default RTL_Campaign_CallList_MyList_Option;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_MyTeamList_Option" {
    var RTL_Campaign_CallList_MyTeamList_Option: string;
    export default RTL_Campaign_CallList_MyTeamList_Option;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Next_Text" {
    var RTL_Campaign_CallList_Next_Text: string;
    export default RTL_Campaign_CallList_Next_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Of_Text" {
    var RTL_Campaign_CallList_Of_Text: string;
    export default RTL_Campaign_CallList_Of_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Prev_Text" {
    var RTL_Campaign_CallList_Prev_Text: string;
    export default RTL_Campaign_CallList_Prev_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Searh_Btn" {
    var RTL_Campaign_CallList_Searh_Btn: string;
    export default RTL_Campaign_CallList_Searh_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Subtitle" {
    var RTL_Campaign_CallList_Subtitle: string;
    export default RTL_Campaign_CallList_Subtitle;
}
declare module "@salesforce/label/c.RTL_Campaign_CallList_Title" {
    var RTL_Campaign_CallList_Title: string;
    export default RTL_Campaign_CallList_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Btn" {
    var RTL_Campaign_History_Btn: string;
    export default RTL_Campaign_History_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignChannelHeader" {
    var RTL_Campaign_History_CampaignChannelHeader: string;
    export default RTL_Campaign_History_CampaignChannelHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignCodeHeader" {
    var RTL_Campaign_History_CampaignCodeHeader: string;
    export default RTL_Campaign_History_CampaignCodeHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignEndDateHeader" {
    var RTL_Campaign_History_CampaignEndDateHeader: string;
    export default RTL_Campaign_History_CampaignEndDateHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignMemberLinkHeader" {
    var RTL_Campaign_History_CampaignMemberLinkHeader: string;
    export default RTL_Campaign_History_CampaignMemberLinkHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignNameHeader" {
    var RTL_Campaign_History_CampaignNameHeader: string;
    export default RTL_Campaign_History_CampaignNameHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignOfferResultCRMHeader" {
    var RTL_Campaign_History_CampaignOfferResultCRMHeader: string;
    export default RTL_Campaign_History_CampaignOfferResultCRMHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignOfferResultCTHeader" {
    var RTL_Campaign_History_CampaignOfferResultCTHeader: string;
    export default RTL_Campaign_History_CampaignOfferResultCTHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignOfferResultHeader" {
    var RTL_Campaign_History_CampaignOfferResultHeader: string;
    export default RTL_Campaign_History_CampaignOfferResultHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignStartDateHeader" {
    var RTL_Campaign_History_CampaignStartDateHeader: string;
    export default RTL_Campaign_History_CampaignStartDateHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_CampaignStatusHeader" {
    var RTL_Campaign_History_CampaignStatusHeader: string;
    export default RTL_Campaign_History_CampaignStatusHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_CustReq" {
    var RTL_Campaign_History_Campaing_CustReq: string;
    export default RTL_Campaign_History_Campaing_CustReq;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_Exclusive" {
    var RTL_Campaign_History_Campaing_Exclusive: string;
    export default RTL_Campaign_History_Campaing_Exclusive;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_Lite_Link" {
    var RTL_Campaign_History_Campaing_Lite_Link: string;
    export default RTL_Campaign_History_Campaing_Lite_Link;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_LocalEx" {
    var RTL_Campaign_History_Campaing_LocalEx: string;
    export default RTL_Campaign_History_Campaing_LocalEx;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_Mass" {
    var RTL_Campaign_History_Campaing_Mass: string;
    export default RTL_Campaign_History_Campaing_Mass;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_NewerTab" {
    var RTL_Campaign_History_Campaing_NewerTab: string;
    export default RTL_Campaign_History_Campaing_NewerTab;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Campaing_OlderTab" {
    var RTL_Campaign_History_Campaing_OlderTab: string;
    export default RTL_Campaign_History_Campaing_OlderTab;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ContentTitle" {
    var RTL_Campaign_History_ContentTitle: string;
    export default RTL_Campaign_History_ContentTitle;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ErrorMessage1" {
    var RTL_Campaign_History_ErrorMessage1: string;
    export default RTL_Campaign_History_ErrorMessage1;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ErrorMessage2" {
    var RTL_Campaign_History_ErrorMessage2: string;
    export default RTL_Campaign_History_ErrorMessage2;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ErrorMessage3" {
    var RTL_Campaign_History_ErrorMessage3: string;
    export default RTL_Campaign_History_ErrorMessage3;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ErrorMessage4" {
    var RTL_Campaign_History_ErrorMessage4: string;
    export default RTL_Campaign_History_ErrorMessage4;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Loading" {
    var RTL_Campaign_History_Loading: string;
    export default RTL_Campaign_History_Loading;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Next" {
    var RTL_Campaign_History_Next: string;
    export default RTL_Campaign_History_Next;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Of" {
    var RTL_Campaign_History_Of: string;
    export default RTL_Campaign_History_Of;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Page" {
    var RTL_Campaign_History_Page: string;
    export default RTL_Campaign_History_Page;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Previous" {
    var RTL_Campaign_History_Previous: string;
    export default RTL_Campaign_History_Previous;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_ApplyChannelHeader" {
    var RTL_Campaign_History_SMSCampaign_ApplyChannelHeader: string;
    export default RTL_Campaign_History_SMSCampaign_ApplyChannelHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_CampCodeHeader" {
    var RTL_Campaign_History_SMSCampaign_CampCodeHeader: string;
    export default RTL_Campaign_History_SMSCampaign_CampCodeHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_CampNameHeader" {
    var RTL_Campaign_History_SMSCampaign_CampNameHeader: string;
    export default RTL_Campaign_History_SMSCampaign_CampNameHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_ContentHeader" {
    var RTL_Campaign_History_SMSCampaign_ContentHeader: string;
    export default RTL_Campaign_History_SMSCampaign_ContentHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_MoblieHeader" {
    var RTL_Campaign_History_SMSCampaign_MoblieHeader: string;
    export default RTL_Campaign_History_SMSCampaign_MoblieHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_MsgHeader" {
    var RTL_Campaign_History_SMSCampaign_MsgHeader: string;
    export default RTL_Campaign_History_SMSCampaign_MsgHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_SMSCampaign_RegisHeader" {
    var RTL_Campaign_History_SMSCampaign_RegisHeader: string;
    export default RTL_Campaign_History_SMSCampaign_RegisHeader;
}
declare module "@salesforce/label/c.RTL_Campaign_History_TabCampaign" {
    var RTL_Campaign_History_TabCampaign: string;
    export default RTL_Campaign_History_TabCampaign;
}
declare module "@salesforce/label/c.RTL_Campaign_History_TabFulfillment" {
    var RTL_Campaign_History_TabFulfillment: string;
    export default RTL_Campaign_History_TabFulfillment;
}
declare module "@salesforce/label/c.RTL_Campaign_History_TabSMS" {
    var RTL_Campaign_History_TabSMS: string;
    export default RTL_Campaign_History_TabSMS;
}
declare module "@salesforce/label/c.RTL_Campaign_History_Title" {
    var RTL_Campaign_History_Title: string;
    export default RTL_Campaign_History_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_History_ViewDetail" {
    var RTL_Campaign_History_ViewDetail: string;
    export default RTL_Campaign_History_ViewDetail;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Calllist" {
    var RTL_Campaign_Home_Calllist: string;
    export default RTL_Campaign_Home_Calllist;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Calllist_Table" {
    var RTL_Campaign_Home_Calllist_Table: string;
    export default RTL_Campaign_Home_Calllist_Table;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Campaign_Name" {
    var RTL_Campaign_Home_Campaign_Name: string;
    export default RTL_Campaign_Home_Campaign_Name;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Hotlink_header" {
    var RTL_Campaign_Home_Hotlink_header: string;
    export default RTL_Campaign_Home_Hotlink_header;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Hotlink_link1" {
    var RTL_Campaign_Home_Hotlink_link1: string;
    export default RTL_Campaign_Home_Hotlink_link1;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Hotlink_link2" {
    var RTL_Campaign_Home_Hotlink_link2: string;
    export default RTL_Campaign_Home_Hotlink_link2;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Hotlink_link3" {
    var RTL_Campaign_Home_Hotlink_link3: string;
    export default RTL_Campaign_Home_Hotlink_link3;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Hotlink_link4" {
    var RTL_Campaign_Home_Hotlink_link4: string;
    export default RTL_Campaign_Home_Hotlink_link4;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_ItemToApprove" {
    var RTL_Campaign_Home_ItemToApprove: string;
    export default RTL_Campaign_Home_ItemToApprove;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Reassign" {
    var RTL_Campaign_Home_Reassign: string;
    export default RTL_Campaign_Home_Reassign;
}
declare module "@salesforce/label/c.RTL_Campaign_Home_Waiting" {
    var RTL_Campaign_Home_Waiting: string;
    export default RTL_Campaign_Home_Waiting;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Delete_Notice" {
    var RTL_Campaign_Member_Delete_Notice: string;
    export default RTL_Campaign_Member_Delete_Notice;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Filter_Offer_Interest" {
    var RTL_Campaign_Member_Filter_Offer_Interest: string;
    export default RTL_Campaign_Member_Filter_Offer_Interest;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Filter_Offer_NotInterest" {
    var RTL_Campaign_Member_Filter_Offer_NotInterest: string;
    export default RTL_Campaign_Member_Filter_Offer_NotInterest;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Filter_Offer_Pending" {
    var RTL_Campaign_Member_Filter_Offer_Pending: string;
    export default RTL_Campaign_Member_Filter_Offer_Pending;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_From_Text" {
    var RTL_Campaign_Member_From_Text: string;
    export default RTL_Campaign_Member_From_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Close_Btn" {
    var RTL_Campaign_Member_History_Close_Btn: string;
    export default RTL_Campaign_Member_History_Close_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Header_Date" {
    var RTL_Campaign_Member_History_Header_Date: string;
    export default RTL_Campaign_Member_History_Header_Date;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Header_Field" {
    var RTL_Campaign_Member_History_Header_Field: string;
    export default RTL_Campaign_Member_History_Header_Field;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Header_History" {
    var RTL_Campaign_Member_History_Header_History: string;
    export default RTL_Campaign_Member_History_Header_History;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Header_ModBy" {
    var RTL_Campaign_Member_History_Header_ModBy: string;
    export default RTL_Campaign_Member_History_Header_ModBy;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Header_No" {
    var RTL_Campaign_Member_History_Header_No: string;
    export default RTL_Campaign_Member_History_Header_No;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Loading" {
    var RTL_Campaign_Member_History_Loading: string;
    export default RTL_Campaign_Member_History_Loading;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Section_Title" {
    var RTL_Campaign_Member_History_Section_Title: string;
    export default RTL_Campaign_Member_History_Section_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Subtitle" {
    var RTL_Campaign_Member_History_Subtitle: string;
    export default RTL_Campaign_Member_History_Subtitle;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_History_Title" {
    var RTL_Campaign_Member_History_Title: string;
    export default RTL_Campaign_Member_History_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_Info1" {
    var RTL_Campaign_Member_Remove_Info1: string;
    export default RTL_Campaign_Member_Remove_Info1;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_Info2" {
    var RTL_Campaign_Member_Remove_Info2: string;
    export default RTL_Campaign_Member_Remove_Info2;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_Info3" {
    var RTL_Campaign_Member_Remove_Info3: string;
    export default RTL_Campaign_Member_Remove_Info3;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_Selected" {
    var RTL_Campaign_Member_Remove_Selected: string;
    export default RTL_Campaign_Member_Remove_Selected;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_Title" {
    var RTL_Campaign_Member_Remove_Title: string;
    export default RTL_Campaign_Member_Remove_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_all" {
    var RTL_Campaign_Member_Remove_all: string;
    export default RTL_Campaign_Member_Remove_all;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Remove_unused" {
    var RTL_Campaign_Member_Remove_unused: string;
    export default RTL_Campaign_Member_Remove_unused;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_To_Text" {
    var RTL_Campaign_Member_To_Text: string;
    export default RTL_Campaign_Member_To_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Updated_Text" {
    var RTL_Campaign_Member_Updated_Text: string;
    export default RTL_Campaign_Member_Updated_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Upload_File_Size" {
    var RTL_Campaign_Member_Upload_File_Size: string;
    export default RTL_Campaign_Member_Upload_File_Size;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Upload_Invalid_Column" {
    var RTL_Campaign_Member_Upload_Invalid_Column: string;
    export default RTL_Campaign_Member_Upload_Invalid_Column;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Upload_Invalid_File" {
    var RTL_Campaign_Member_Upload_Invalid_File: string;
    export default RTL_Campaign_Member_Upload_Invalid_File;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Upload_Invalid_Row" {
    var RTL_Campaign_Member_Upload_Invalid_Row: string;
    export default RTL_Campaign_Member_Upload_Invalid_Row;
}
declare module "@salesforce/label/c.RTL_Campaign_Member_Upload_Notice" {
    var RTL_Campaign_Member_Upload_Notice: string;
    export default RTL_Campaign_Member_Upload_Notice;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Campaign" {
    var RTL_Campaign_Reassign_Filter_Campaign: string;
    export default RTL_Campaign_Reassign_Filter_Campaign;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Cancel_Btn" {
    var RTL_Campaign_Reassign_Filter_Cancel_Btn: string;
    export default RTL_Campaign_Reassign_Filter_Cancel_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Chanel" {
    var RTL_Campaign_Reassign_Filter_Chanel: string;
    export default RTL_Campaign_Reassign_Filter_Chanel;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Contact" {
    var RTL_Campaign_Reassign_Filter_Contact: string;
    export default RTL_Campaign_Reassign_Filter_Contact;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_OfferResult" {
    var RTL_Campaign_Reassign_Filter_OfferResult: string;
    export default RTL_Campaign_Reassign_Filter_OfferResult;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Search_Btn" {
    var RTL_Campaign_Reassign_Filter_Search_Btn: string;
    export default RTL_Campaign_Reassign_Filter_Search_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Section" {
    var RTL_Campaign_Reassign_Filter_Section: string;
    export default RTL_Campaign_Reassign_Filter_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Filter_Unassigned" {
    var RTL_Campaign_Reassign_Filter_Unassigned: string;
    export default RTL_Campaign_Reassign_Filter_Unassigned;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Head_Campaign" {
    var RTL_Campaign_Reassign_Head_Campaign: string;
    export default RTL_Campaign_Reassign_Head_Campaign;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Head_Name" {
    var RTL_Campaign_Reassign_Head_Name: string;
    export default RTL_Campaign_Reassign_Head_Name;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Head_No" {
    var RTL_Campaign_Reassign_Head_No: string;
    export default RTL_Campaign_Reassign_Head_No;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Head_Selection" {
    var RTL_Campaign_Reassign_Head_Selection: string;
    export default RTL_Campaign_Reassign_Head_Selection;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Invalid_Agent" {
    var RTL_Campaign_Reassign_Invalid_Agent: string;
    export default RTL_Campaign_Reassign_Invalid_Agent;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Invalid_Campaign" {
    var RTL_Campaign_Reassign_Invalid_Campaign: string;
    export default RTL_Campaign_Reassign_Invalid_Campaign;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Invalid_Input" {
    var RTL_Campaign_Reassign_Invalid_Input: string;
    export default RTL_Campaign_Reassign_Invalid_Input;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Loading" {
    var RTL_Campaign_Reassign_Loading: string;
    export default RTL_Campaign_Reassign_Loading;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Next_Text" {
    var RTL_Campaign_Reassign_Next_Text: string;
    export default RTL_Campaign_Reassign_Next_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Of_Text" {
    var RTL_Campaign_Reassign_Of_Text: string;
    export default RTL_Campaign_Reassign_Of_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Prev_Text" {
    var RTL_Campaign_Reassign_Prev_Text: string;
    export default RTL_Campaign_Reassign_Prev_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Reassign_Back_Btn" {
    var RTL_Campaign_Reassign_Reassign_Back_Btn: string;
    export default RTL_Campaign_Reassign_Reassign_Back_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Reassign_Btn" {
    var RTL_Campaign_Reassign_Reassign_Btn: string;
    export default RTL_Campaign_Reassign_Reassign_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Reassign_Section" {
    var RTL_Campaign_Reassign_Reassign_Section: string;
    export default RTL_Campaign_Reassign_Reassign_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Reassign_Select_Btn" {
    var RTL_Campaign_Reassign_Reassign_Select_Btn: string;
    export default RTL_Campaign_Reassign_Reassign_Select_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Result_Section" {
    var RTL_Campaign_Reassign_Result_Section: string;
    export default RTL_Campaign_Reassign_Result_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_SelectMem_Text" {
    var RTL_Campaign_Reassign_SelectMem_Text: string;
    export default RTL_Campaign_Reassign_SelectMem_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Select_Section" {
    var RTL_Campaign_Reassign_Select_Section: string;
    export default RTL_Campaign_Reassign_Select_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Subtitle" {
    var RTL_Campaign_Reassign_Subtitle: string;
    export default RTL_Campaign_Reassign_Subtitle;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Success" {
    var RTL_Campaign_Reassign_Success: string;
    export default RTL_Campaign_Reassign_Success;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Target_Agent_Text" {
    var RTL_Campaign_Reassign_Target_Agent_Text: string;
    export default RTL_Campaign_Reassign_Target_Agent_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Target_Branch_Text" {
    var RTL_Campaign_Reassign_Target_Branch_Text: string;
    export default RTL_Campaign_Reassign_Target_Branch_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Target_Section" {
    var RTL_Campaign_Reassign_Target_Section: string;
    export default RTL_Campaign_Reassign_Target_Section;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Title" {
    var RTL_Campaign_Reassign_Title: string;
    export default RTL_Campaign_Reassign_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Reassign_Warning" {
    var RTL_Campaign_Reassign_Warning: string;
    export default RTL_Campaign_Reassign_Warning;
}
declare module "@salesforce/label/c.RTL_Campaign_Remove_ERR001" {
    var RTL_Campaign_Remove_ERR001: string;
    export default RTL_Campaign_Remove_ERR001;
}
declare module "@salesforce/label/c.RTL_Campaign_Remove_ERR002" {
    var RTL_Campaign_Remove_ERR002: string;
    export default RTL_Campaign_Remove_ERR002;
}
declare module "@salesforce/label/c.RTL_Campaign_Remove_ERR003" {
    var RTL_Campaign_Remove_ERR003: string;
    export default RTL_Campaign_Remove_ERR003;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_Header_Status" {
    var RTL_Campaign_Resubmit_All_Header_Status: string;
    export default RTL_Campaign_Resubmit_All_Header_Status;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_Header_name" {
    var RTL_Campaign_Resubmit_All_Header_name: string;
    export default RTL_Campaign_Resubmit_All_Header_name;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_ListCampaing" {
    var RTL_Campaign_Resubmit_All_ListCampaing: string;
    export default RTL_Campaign_Resubmit_All_ListCampaing;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_Notice" {
    var RTL_Campaign_Resubmit_All_Notice: string;
    export default RTL_Campaign_Resubmit_All_Notice;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_Title" {
    var RTL_Campaign_Resubmit_All_Title: string;
    export default RTL_Campaign_Resubmit_All_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_All_Warning" {
    var RTL_Campaign_Resubmit_All_Warning: string;
    export default RTL_Campaign_Resubmit_All_Warning;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Cancel_Btn" {
    var RTL_Campaign_Resubmit_Cancel_Btn: string;
    export default RTL_Campaign_Resubmit_Cancel_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Desc_Text" {
    var RTL_Campaign_Resubmit_Desc_Text: string;
    export default RTL_Campaign_Resubmit_Desc_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Error_Email_Body" {
    var RTL_Campaign_Resubmit_Error_Email_Body: string;
    export default RTL_Campaign_Resubmit_Error_Email_Body;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Error_Email_Subject" {
    var RTL_Campaign_Resubmit_Error_Email_Subject: string;
    export default RTL_Campaign_Resubmit_Error_Email_Subject;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Head_ErrorCode" {
    var RTL_Campaign_Resubmit_Head_ErrorCode: string;
    export default RTL_Campaign_Resubmit_Head_ErrorCode;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Head_ErrorMsg" {
    var RTL_Campaign_Resubmit_Head_ErrorMsg: string;
    export default RTL_Campaign_Resubmit_Head_ErrorMsg;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Head_Remark" {
    var RTL_Campaign_Resubmit_Head_Remark: string;
    export default RTL_Campaign_Resubmit_Head_Remark;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Head_Status" {
    var RTL_Campaign_Resubmit_Head_Status: string;
    export default RTL_Campaign_Resubmit_Head_Status;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Head_SubmitDate" {
    var RTL_Campaign_Resubmit_Head_SubmitDate: string;
    export default RTL_Campaign_Resubmit_Head_SubmitDate;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Not_Available" {
    var RTL_Campaign_Resubmit_Not_Available: string;
    export default RTL_Campaign_Resubmit_Not_Available;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Notice" {
    var RTL_Campaign_Resubmit_Notice: string;
    export default RTL_Campaign_Resubmit_Notice;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Resubmit_Btn" {
    var RTL_Campaign_Resubmit_Resubmit_Btn: string;
    export default RTL_Campaign_Resubmit_Resubmit_Btn;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Status_Failed" {
    var RTL_Campaign_Resubmit_Status_Failed: string;
    export default RTL_Campaign_Resubmit_Status_Failed;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Status_Processing" {
    var RTL_Campaign_Resubmit_Status_Processing: string;
    export default RTL_Campaign_Resubmit_Status_Processing;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Status_Success" {
    var RTL_Campaign_Resubmit_Status_Success: string;
    export default RTL_Campaign_Resubmit_Status_Success;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Status_Text" {
    var RTL_Campaign_Resubmit_Status_Text: string;
    export default RTL_Campaign_Resubmit_Status_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Status_Unknown" {
    var RTL_Campaign_Resubmit_Status_Unknown: string;
    export default RTL_Campaign_Resubmit_Status_Unknown;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Subtitle" {
    var RTL_Campaign_Resubmit_Subtitle: string;
    export default RTL_Campaign_Resubmit_Subtitle;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Subtitle_Info" {
    var RTL_Campaign_Resubmit_Subtitle_Info: string;
    export default RTL_Campaign_Resubmit_Subtitle_Info;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_TItle" {
    var RTL_Campaign_Resubmit_TItle: string;
    export default RTL_Campaign_Resubmit_TItle;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_Trans_Title" {
    var RTL_Campaign_Resubmit_Trans_Title: string;
    export default RTL_Campaign_Resubmit_Trans_Title;
}
declare module "@salesforce/label/c.RTL_Campaign_Resubmit_UnexpectError_Text" {
    var RTL_Campaign_Resubmit_UnexpectError_Text: string;
    export default RTL_Campaign_Resubmit_UnexpectError_Text;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_BtnCancel" {
    var RTL_Campaign_Status_BtnCancel: string;
    export default RTL_Campaign_Status_BtnCancel;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_BtnConfirm" {
    var RTL_Campaign_Status_BtnConfirm: string;
    export default RTL_Campaign_Status_BtnConfirm;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_BtnReturn" {
    var RTL_Campaign_Status_BtnReturn: string;
    export default RTL_Campaign_Status_BtnReturn;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR001" {
    var RTL_Campaign_Status_ERR001: string;
    export default RTL_Campaign_Status_ERR001;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR002" {
    var RTL_Campaign_Status_ERR002: string;
    export default RTL_Campaign_Status_ERR002;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR003" {
    var RTL_Campaign_Status_ERR003: string;
    export default RTL_Campaign_Status_ERR003;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR004" {
    var RTL_Campaign_Status_ERR004: string;
    export default RTL_Campaign_Status_ERR004;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR005" {
    var RTL_Campaign_Status_ERR005: string;
    export default RTL_Campaign_Status_ERR005;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR006" {
    var RTL_Campaign_Status_ERR006: string;
    export default RTL_Campaign_Status_ERR006;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_ERR007" {
    var RTL_Campaign_Status_ERR007: string;
    export default RTL_Campaign_Status_ERR007;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_cf_cancel" {
    var RTL_Campaign_Status_cf_cancel: string;
    export default RTL_Campaign_Status_cf_cancel;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_cf_hold" {
    var RTL_Campaign_Status_cf_hold: string;
    export default RTL_Campaign_Status_cf_hold;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_cf_unhold" {
    var RTL_Campaign_Status_cf_unhold: string;
    export default RTL_Campaign_Status_cf_unhold;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_end_cancel" {
    var RTL_Campaign_Status_end_cancel: string;
    export default RTL_Campaign_Status_end_cancel;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_end_hold" {
    var RTL_Campaign_Status_end_hold: string;
    export default RTL_Campaign_Status_end_hold;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_end_unhold" {
    var RTL_Campaign_Status_end_unhold: string;
    export default RTL_Campaign_Status_end_unhold;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_remark" {
    var RTL_Campaign_Status_remark: string;
    export default RTL_Campaign_Status_remark;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_title_cancel" {
    var RTL_Campaign_Status_title_cancel: string;
    export default RTL_Campaign_Status_title_cancel;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_title_hold" {
    var RTL_Campaign_Status_title_hold: string;
    export default RTL_Campaign_Status_title_hold;
}
declare module "@salesforce/label/c.RTL_Campaign_Status_title_unhold" {
    var RTL_Campaign_Status_title_unhold: string;
    export default RTL_Campaign_Status_title_unhold;
}
declare module "@salesforce/label/c.RTL_Changed" {
    var RTL_Changed: string;
    export default RTL_Changed;
}
declare module "@salesforce/label/c.RTL_Click_Here" {
    var RTL_Click_Here: string;
    export default RTL_Click_Here;
}
declare module "@salesforce/label/c.RTL_Col_Label_Choose" {
    var RTL_Col_Label_Choose: string;
    export default RTL_Col_Label_Choose;
}
declare module "@salesforce/label/c.RTL_Col_Label_CustomerName" {
    var RTL_Col_Label_CustomerName: string;
    export default RTL_Col_Label_CustomerName;
}
declare module "@salesforce/label/c.RTL_Col_Label_Customer_Segment" {
    var RTL_Col_Label_Customer_Segment: string;
    export default RTL_Col_Label_Customer_Segment;
}
declare module "@salesforce/label/c.RTL_Col_Label_ID_Type" {
    var RTL_Col_Label_ID_Type: string;
    export default RTL_Col_Label_ID_Type;
}
declare module "@salesforce/label/c.RTL_Col_Label_MobileNO" {
    var RTL_Col_Label_MobileNO: string;
    export default RTL_Col_Label_MobileNO;
}
declare module "@salesforce/label/c.RTL_Col_Label_NID" {
    var RTL_Col_Label_NID: string;
    export default RTL_Col_Label_NID;
}
declare module "@salesforce/label/c.RTL_Col_Label_No" {
    var RTL_Col_Label_No: string;
    export default RTL_Col_Label_No;
}
declare module "@salesforce/label/c.RTL_Col_Label_OwnerName" {
    var RTL_Col_Label_OwnerName: string;
    export default RTL_Col_Label_OwnerName;
}
declare module "@salesforce/label/c.RTL_Col_Label_TMB_Cust_ID" {
    var RTL_Col_Label_TMB_Cust_ID: string;
    export default RTL_Col_Label_TMB_Cust_ID;
}
declare module "@salesforce/label/c.RTL_Communication_Label" {
    var RTL_Communication_Label: string;
    export default RTL_Communication_Label;
}
declare module "@salesforce/label/c.RTL_Confirm_Approve_All" {
    var RTL_Confirm_Approve_All: string;
    export default RTL_Confirm_Approve_All;
}
declare module "@salesforce/label/c.RTL_Confirm_Cancel_Create_Household" {
    var RTL_Confirm_Cancel_Create_Household: string;
    export default RTL_Confirm_Cancel_Create_Household;
}
declare module "@salesforce/label/c.RTL_Confirm_Delete" {
    var RTL_Confirm_Delete: string;
    export default RTL_Confirm_Delete;
}
declare module "@salesforce/label/c.RTL_Confirm_Reject_All" {
    var RTL_Confirm_Reject_All: string;
    export default RTL_Confirm_Reject_All;
}
declare module "@salesforce/label/c.RTL_Confirm_Request_Delete_Household" {
    var RTL_Confirm_Request_Delete_Household: string;
    export default RTL_Confirm_Request_Delete_Household;
}
declare module "@salesforce/label/c.RTL_Confirm_Request_Delete_Household_Member" {
    var RTL_Confirm_Request_Delete_Household_Member: string;
    export default RTL_Confirm_Request_Delete_Household_Member;
}
declare module "@salesforce/label/c.RTL_Confirm_Send_Email" {
    var RTL_Confirm_Send_Email: string;
    export default RTL_Confirm_Send_Email;
}
declare module "@salesforce/label/c.RTL_Confirm_Submit_For_Approval" {
    var RTL_Confirm_Submit_For_Approval: string;
    export default RTL_Confirm_Submit_For_Approval;
}
declare module "@salesforce/label/c.RTL_Contact_Center_Interaction_Information" {
    var RTL_Contact_Center_Interaction_Information: string;
    export default RTL_Contact_Center_Interaction_Information;
}
declare module "@salesforce/label/c.RTL_CreateAcc_Button_Label" {
    var RTL_CreateAcc_Button_Label: string;
    export default RTL_CreateAcc_Button_Label;
}
declare module "@salesforce/label/c.RTL_Create_Account_Error_Message" {
    var RTL_Create_Account_Error_Message: string;
    export default RTL_Create_Account_Error_Message;
}
declare module "@salesforce/label/c.RTL_Created" {
    var RTL_Created: string;
    export default RTL_Created;
}
declare module "@salesforce/label/c.RTL_DQ_OZip" {
    var RTL_DQ_OZip: string;
    export default RTL_DQ_OZip;
}
declare module "@salesforce/label/c.RTL_DQ_Office" {
    var RTL_DQ_Office: string;
    export default RTL_DQ_Office;
}
declare module "@salesforce/label/c.RTL_DQ_PZip" {
    var RTL_DQ_PZip: string;
    export default RTL_DQ_PZip;
}
declare module "@salesforce/label/c.RTL_DQ_Primary" {
    var RTL_DQ_Primary: string;
    export default RTL_DQ_Primary;
}
declare module "@salesforce/label/c.RTL_DQ_RZip" {
    var RTL_DQ_RZip: string;
    export default RTL_DQ_RZip;
}
declare module "@salesforce/label/c.RTL_DQ_Registered" {
    var RTL_DQ_Registered: string;
    export default RTL_DQ_Registered;
}
declare module "@salesforce/label/c.RTL_Data_Age" {
    var RTL_Data_Age: string;
    export default RTL_Data_Age;
}
declare module "@salesforce/label/c.RTL_Data_Quality" {
    var RTL_Data_Quality: string;
    export default RTL_Data_Quality;
}
declare module "@salesforce/label/c.RTL_Data_Quality_Marketing" {
    var RTL_Data_Quality_Marketing: string;
    export default RTL_Data_Quality_Marketing;
}
declare module "@salesforce/label/c.RTL_Default_Deposit" {
    var RTL_Default_Deposit: string;
    export default RTL_Default_Deposit;
}
declare module "@salesforce/label/c.RTL_Del_HH" {
    var RTL_Del_HH: string;
    export default RTL_Del_HH;
}
declare module "@salesforce/label/c.RTL_Del_HHM" {
    var RTL_Del_HHM: string;
    export default RTL_Del_HHM;
}
declare module "@salesforce/label/c.RTL_Deleted" {
    var RTL_Deleted: string;
    export default RTL_Deleted;
}
declare module "@salesforce/label/c.RTL_Details_of_Next_Best_Offers" {
    var RTL_Details_of_Next_Best_Offers: string;
    export default RTL_Details_of_Next_Best_Offers;
}
declare module "@salesforce/label/c.RTL_ERR001" {
    var RTL_ERR001: string;
    export default RTL_ERR001;
}
declare module "@salesforce/label/c.RTL_ERR002" {
    var RTL_ERR002: string;
    export default RTL_ERR002;
}
declare module "@salesforce/label/c.RTL_ERR003" {
    var RTL_ERR003: string;
    export default RTL_ERR003;
}
declare module "@salesforce/label/c.RTL_ERR004" {
    var RTL_ERR004: string;
    export default RTL_ERR004;
}
declare module "@salesforce/label/c.RTL_ERR005" {
    var RTL_ERR005: string;
    export default RTL_ERR005;
}
declare module "@salesforce/label/c.RTL_ERR006" {
    var RTL_ERR006: string;
    export default RTL_ERR006;
}
declare module "@salesforce/label/c.RTL_ERR007" {
    var RTL_ERR007: string;
    export default RTL_ERR007;
}
declare module "@salesforce/label/c.RTL_ERR008" {
    var RTL_ERR008: string;
    export default RTL_ERR008;
}
declare module "@salesforce/label/c.RTL_ERR009" {
    var RTL_ERR009: string;
    export default RTL_ERR009;
}
declare module "@salesforce/label/c.RTL_ERR010" {
    var RTL_ERR010: string;
    export default RTL_ERR010;
}
declare module "@salesforce/label/c.RTL_ERR011" {
    var RTL_ERR011: string;
    export default RTL_ERR011;
}
declare module "@salesforce/label/c.RTL_ERR012" {
    var RTL_ERR012: string;
    export default RTL_ERR012;
}
declare module "@salesforce/label/c.RTL_ERR013" {
    var RTL_ERR013: string;
    export default RTL_ERR013;
}
declare module "@salesforce/label/c.RTL_Edit_Condition_01" {
    var RTL_Edit_Condition_01: string;
    export default RTL_Edit_Condition_01;
}
declare module "@salesforce/label/c.RTL_Edit_Condition_02" {
    var RTL_Edit_Condition_02: string;
    export default RTL_Edit_Condition_02;
}
declare module "@salesforce/label/c.RTL_Edit_HHM_Benefit" {
    var RTL_Edit_HHM_Benefit: string;
    export default RTL_Edit_HHM_Benefit;
}
declare module "@salesforce/label/c.RTL_Edit_HHM_Relationship" {
    var RTL_Edit_HHM_Relationship: string;
    export default RTL_Edit_HHM_Relationship;
}
declare module "@salesforce/label/c.RTL_Edit_HH_Benefit" {
    var RTL_Edit_HH_Benefit: string;
    export default RTL_Edit_HH_Benefit;
}
declare module "@salesforce/label/c.RTL_Edit_HH_Remarks" {
    var RTL_Edit_HH_Remarks: string;
    export default RTL_Edit_HH_Remarks;
}
declare module "@salesforce/label/c.RTL_Fail" {
    var RTL_Fail: string;
    export default RTL_Fail;
}
declare module "@salesforce/label/c.RTL_Field" {
    var RTL_Field: string;
    export default RTL_Field;
}
declare module "@salesforce/label/c.RTL_First" {
    var RTL_First: string;
    export default RTL_First;
}
declare module "@salesforce/label/c.RTL_From" {
    var RTL_From: string;
    export default RTL_From;
}
declare module "@salesforce/label/c.RTL_Fulfillment_Completed" {
    var RTL_Fulfillment_Completed: string;
    export default RTL_Fulfillment_Completed;
}
declare module "@salesforce/label/c.RTL_Fulfillment_NA" {
    var RTL_Fulfillment_NA: string;
    export default RTL_Fulfillment_NA;
}
declare module "@salesforce/label/c.RTL_Fulfillment_Not_Eligible" {
    var RTL_Fulfillment_Not_Eligible: string;
    export default RTL_Fulfillment_Not_Eligible;
}
declare module "@salesforce/label/c.RTL_Fulfillment_Pending" {
    var RTL_Fulfillment_Pending: string;
    export default RTL_Fulfillment_Pending;
}
declare module "@salesforce/label/c.RTL_Fulfillment_Processing" {
    var RTL_Fulfillment_Processing: string;
    export default RTL_Fulfillment_Processing;
}
declare module "@salesforce/label/c.RTL_HasCampaignFulfillment" {
    var RTL_HasCampaignFulfillment: string;
    export default RTL_HasCampaignFulfillment;
}
declare module "@salesforce/label/c.RTL_Header_Label_Lead_Detail" {
    var RTL_Header_Label_Lead_Detail: string;
    export default RTL_Header_Label_Lead_Detail;
}
declare module "@salesforce/label/c.RTL_History" {
    var RTL_History: string;
    export default RTL_History;
}
declare module "@salesforce/label/c.RTL_History_Information" {
    var RTL_History_Information: string;
    export default RTL_History_Information;
}
declare module "@salesforce/label/c.RTL_Household" {
    var RTL_Household: string;
    export default RTL_Household;
}
declare module "@salesforce/label/c.RTL_Household_And_Member_Information" {
    var RTL_Household_And_Member_Information: string;
    export default RTL_Household_And_Member_Information;
}
declare module "@salesforce/label/c.RTL_Household_ERR01" {
    var RTL_Household_ERR01: string;
    export default RTL_Household_ERR01;
}
declare module "@salesforce/label/c.RTL_Household_ERR02" {
    var RTL_Household_ERR02: string;
    export default RTL_Household_ERR02;
}
declare module "@salesforce/label/c.RTL_Household_ERR03" {
    var RTL_Household_ERR03: string;
    export default RTL_Household_ERR03;
}
declare module "@salesforce/label/c.RTL_Household_ERR04" {
    var RTL_Household_ERR04: string;
    export default RTL_Household_ERR04;
}
declare module "@salesforce/label/c.RTL_Household_ERR05" {
    var RTL_Household_ERR05: string;
    export default RTL_Household_ERR05;
}
declare module "@salesforce/label/c.RTL_Household_ERR06" {
    var RTL_Household_ERR06: string;
    export default RTL_Household_ERR06;
}
declare module "@salesforce/label/c.RTL_Household_ERR07" {
    var RTL_Household_ERR07: string;
    export default RTL_Household_ERR07;
}
declare module "@salesforce/label/c.RTL_Household_ERR08" {
    var RTL_Household_ERR08: string;
    export default RTL_Household_ERR08;
}
declare module "@salesforce/label/c.RTL_Household_Information" {
    var RTL_Household_Information: string;
    export default RTL_Household_Information;
}
declare module "@salesforce/label/c.RTL_Household_Member" {
    var RTL_Household_Member: string;
    export default RTL_Household_Member;
}
declare module "@salesforce/label/c.RTL_Household_Member_Information" {
    var RTL_Household_Member_Information: string;
    export default RTL_Household_Member_Information;
}
declare module "@salesforce/label/c.RTL_Household_Requested_Type" {
    var RTL_Household_Requested_Type: string;
    export default RTL_Household_Requested_Type;
}
declare module "@salesforce/label/c.RTL_In" {
    var RTL_In: string;
    export default RTL_In;
}
declare module "@salesforce/label/c.RTL_Information_Update_Label" {
    var RTL_Information_Update_Label: string;
    export default RTL_Information_Update_Label;
}
declare module "@salesforce/label/c.RTL_InvalidNBONoProduct" {
    var RTL_InvalidNBONoProduct: string;
    export default RTL_InvalidNBONoProduct;
}
declare module "@salesforce/label/c.RTL_Invitation_Label" {
    var RTL_Invitation_Label: string;
    export default RTL_Invitation_Label;
}
declare module "@salesforce/label/c.RTL_Last" {
    var RTL_Last: string;
    export default RTL_Last;
}
declare module "@salesforce/label/c.RTL_Last_Customer_Data_Update" {
    var RTL_Last_Customer_Data_Update: string;
    export default RTL_Last_Customer_Data_Update;
}
declare module "@salesforce/label/c.RTL_Manage_Duplicate_Customer_Label" {
    var RTL_Manage_Duplicate_Customer_Label: string;
    export default RTL_Manage_Duplicate_Customer_Label;
}
declare module "@salesforce/label/c.RTL_Manage_Duplicate_HelpText_Label" {
    var RTL_Manage_Duplicate_HelpText_Label: string;
    export default RTL_Manage_Duplicate_HelpText_Label;
}
declare module "@salesforce/label/c.RTL_Marketing" {
    var RTL_Marketing: string;
    export default RTL_Marketing;
}
declare module "@salesforce/label/c.RTL_Marketing_List" {
    var RTL_Marketing_List: string;
    export default RTL_Marketing_List;
}
declare module "@salesforce/label/c.RTL_Member" {
    var RTL_Member: string;
    export default RTL_Member;
}
declare module "@salesforce/label/c.RTL_Merge_Account_Error_Message" {
    var RTL_Merge_Account_Error_Message: string;
    export default RTL_Merge_Account_Error_Message;
}
declare module "@salesforce/label/c.RTL_Merge_Button_Label" {
    var RTL_Merge_Button_Label: string;
    export default RTL_Merge_Button_Label;
}
declare module "@salesforce/label/c.RTL_Messenger_Area" {
    var RTL_Messenger_Area: string;
    export default RTL_Messenger_Area;
}
declare module "@salesforce/label/c.RTL_My_Activity" {
    var RTL_My_Activity: string;
    export default RTL_My_Activity;
}
declare module "@salesforce/label/c.RTL_NBO" {
    var RTL_NBO: string;
    export default RTL_NBO;
}
declare module "@salesforce/label/c.RTL_NBO_Campaign_ERR001" {
    var RTL_NBO_Campaign_ERR001: string;
    export default RTL_NBO_Campaign_ERR001;
}
declare module "@salesforce/label/c.RTL_NBO_Detail_Info" {
    var RTL_NBO_Detail_Info: string;
    export default RTL_NBO_Detail_Info;
}
declare module "@salesforce/label/c.RTL_NBO_ERR001" {
    var RTL_NBO_ERR001: string;
    export default RTL_NBO_ERR001;
}
declare module "@salesforce/label/c.RTL_NBO_ERR002" {
    var RTL_NBO_ERR002: string;
    export default RTL_NBO_ERR002;
}
declare module "@salesforce/label/c.RTL_NBO_ERR003" {
    var RTL_NBO_ERR003: string;
    export default RTL_NBO_ERR003;
}
declare module "@salesforce/label/c.RTL_NBO_ERR004" {
    var RTL_NBO_ERR004: string;
    export default RTL_NBO_ERR004;
}
declare module "@salesforce/label/c.RTL_NBO_ERR005" {
    var RTL_NBO_ERR005: string;
    export default RTL_NBO_ERR005;
}
declare module "@salesforce/label/c.RTL_NBO_ERR006" {
    var RTL_NBO_ERR006: string;
    export default RTL_NBO_ERR006;
}
declare module "@salesforce/label/c.RTL_NBO_ERR007" {
    var RTL_NBO_ERR007: string;
    export default RTL_NBO_ERR007;
}
declare module "@salesforce/label/c.RTL_NBO_ERR008" {
    var RTL_NBO_ERR008: string;
    export default RTL_NBO_ERR008;
}
declare module "@salesforce/label/c.RTL_NBO_ERR009" {
    var RTL_NBO_ERR009: string;
    export default RTL_NBO_ERR009;
}
declare module "@salesforce/label/c.RTL_NBO_ERR01" {
    var RTL_NBO_ERR01: string;
    export default RTL_NBO_ERR01;
}
declare module "@salesforce/label/c.RTL_NBO_History" {
    var RTL_NBO_History: string;
    export default RTL_NBO_History;
}
declare module "@salesforce/label/c.RTL_NBO_Information" {
    var RTL_NBO_Information: string;
    export default RTL_NBO_Information;
}
declare module "@salesforce/label/c.RTL_NBO_Insertion_ERR000" {
    var RTL_NBO_Insertion_ERR000: string;
    export default RTL_NBO_Insertion_ERR000;
}
declare module "@salesforce/label/c.RTL_NBO_Insertion_ERR001" {
    var RTL_NBO_Insertion_ERR001: string;
    export default RTL_NBO_Insertion_ERR001;
}
declare module "@salesforce/label/c.RTL_NBO_Insertion_ERR002" {
    var RTL_NBO_Insertion_ERR002: string;
    export default RTL_NBO_Insertion_ERR002;
}
declare module "@salesforce/label/c.RTL_NBO_Name" {
    var RTL_NBO_Name: string;
    export default RTL_NBO_Name;
}
declare module "@salesforce/label/c.RTL_NBO_Offering_Channel_Branch" {
    var RTL_NBO_Offering_Channel_Branch: string;
    export default RTL_NBO_Offering_Channel_Branch;
}
declare module "@salesforce/label/c.RTL_NBO_Oppt_Error" {
    var RTL_NBO_Oppt_Error: string;
    export default RTL_NBO_Oppt_Error;
}
declare module "@salesforce/label/c.RTL_NBO_Product" {
    var RTL_NBO_Product: string;
    export default RTL_NBO_Product;
}
declare module "@salesforce/label/c.RTL_NBO_Product_History" {
    var RTL_NBO_Product_History: string;
    export default RTL_NBO_Product_History;
}
declare module "@salesforce/label/c.RTL_NBO_Product_History_Gotolist" {
    var RTL_NBO_Product_History_Gotolist: string;
    export default RTL_NBO_Product_History_Gotolist;
}
declare module "@salesforce/label/c.RTL_NBO_Product_History_User" {
    var RTL_NBO_Product_History_User: string;
    export default RTL_NBO_Product_History_User;
}
declare module "@salesforce/label/c.RTL_NBO_Product_Information" {
    var RTL_NBO_Product_Information: string;
    export default RTL_NBO_Product_Information;
}
declare module "@salesforce/label/c.RTL_NBO_Product_Status_Accepted" {
    var RTL_NBO_Product_Status_Accepted: string;
    export default RTL_NBO_Product_Status_Accepted;
}
declare module "@salesforce/label/c.RTL_NBO_Product_Status_Rejected" {
    var RTL_NBO_Product_Status_Rejected: string;
    export default RTL_NBO_Product_Status_Rejected;
}
declare module "@salesforce/label/c.RTL_NBO_Refresh" {
    var RTL_NBO_Refresh: string;
    export default RTL_NBO_Refresh;
}
declare module "@salesforce/label/c.RTL_NBO_RejectReason" {
    var RTL_NBO_RejectReason: string;
    export default RTL_NBO_RejectReason;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Accepted_All" {
    var RTL_NBO_Status_Accepted_All: string;
    export default RTL_NBO_Status_Accepted_All;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Accepted_Some" {
    var RTL_NBO_Status_Accepted_Some: string;
    export default RTL_NBO_Status_Accepted_Some;
}
declare module "@salesforce/label/c.RTL_NBO_Status_ContactCenter" {
    var RTL_NBO_Status_ContactCenter: string;
    export default RTL_NBO_Status_ContactCenter;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Invalid" {
    var RTL_NBO_Status_Invalid: string;
    export default RTL_NBO_Status_Invalid;
}
declare module "@salesforce/label/c.RTL_NBO_Status_New" {
    var RTL_NBO_Status_New: string;
    export default RTL_NBO_Status_New;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Pending" {
    var RTL_NBO_Status_Pending: string;
    export default RTL_NBO_Status_Pending;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Rejected_All" {
    var RTL_NBO_Status_Rejected_All: string;
    export default RTL_NBO_Status_Rejected_All;
}
declare module "@salesforce/label/c.RTL_NBO_Status_Rejected_Some" {
    var RTL_NBO_Status_Rejected_Some: string;
    export default RTL_NBO_Status_Rejected_Some;
}
declare module "@salesforce/label/c.RTL_New_HH" {
    var RTL_New_HH: string;
    export default RTL_New_HH;
}
declare module "@salesforce/label/c.RTL_New_HHM" {
    var RTL_New_HHM: string;
    export default RTL_New_HHM;
}
declare module "@salesforce/label/c.RTL_New_Household_Member" {
    var RTL_New_Household_Member: string;
    export default RTL_New_Household_Member;
}
declare module "@salesforce/label/c.RTL_New_Member" {
    var RTL_New_Member: string;
    export default RTL_New_Member;
}
declare module "@salesforce/label/c.RTL_New_Referral" {
    var RTL_New_Referral: string;
    export default RTL_New_Referral;
}
declare module "@salesforce/label/c.RTL_Next" {
    var RTL_Next: string;
    export default RTL_Next;
}
declare module "@salesforce/label/c.RTL_NoCampaignFulfillment" {
    var RTL_NoCampaignFulfillment: string;
    export default RTL_NoCampaignFulfillment;
}
declare module "@salesforce/label/c.RTL_No_Record_Display" {
    var RTL_No_Record_Display: string;
    export default RTL_No_Record_Display;
}
declare module "@salesforce/label/c.RTL_Normal" {
    var RTL_Normal: string;
    export default RTL_Normal;
}
declare module "@salesforce/label/c.RTL_Occupation1_Government" {
    var RTL_Occupation1_Government: string;
    export default RTL_Occupation1_Government;
}
declare module "@salesforce/label/c.RTL_Occupation2_Business_Owner" {
    var RTL_Occupation2_Business_Owner: string;
    export default RTL_Occupation2_Business_Owner;
}
declare module "@salesforce/label/c.RTL_Occupation3_Police" {
    var RTL_Occupation3_Police: string;
    export default RTL_Occupation3_Police;
}
declare module "@salesforce/label/c.RTL_Occupation4_Military" {
    var RTL_Occupation4_Military: string;
    export default RTL_Occupation4_Military;
}
declare module "@salesforce/label/c.RTL_Occupation5_Students" {
    var RTL_Occupation5_Students: string;
    export default RTL_Occupation5_Students;
}
declare module "@salesforce/label/c.RTL_Occupation6_Employee" {
    var RTL_Occupation6_Employee: string;
    export default RTL_Occupation6_Employee;
}
declare module "@salesforce/label/c.RTL_Occupation7_Housewives" {
    var RTL_Occupation7_Housewives: string;
    export default RTL_Occupation7_Housewives;
}
declare module "@salesforce/label/c.RTL_Occupation8_Professionals" {
    var RTL_Occupation8_Professionals: string;
    export default RTL_Occupation8_Professionals;
}
declare module "@salesforce/label/c.RTL_Occupation9_Others" {
    var RTL_Occupation9_Others: string;
    export default RTL_Occupation9_Others;
}
declare module "@salesforce/label/c.RTL_Offerring_User_Information" {
    var RTL_Offerring_User_Information: string;
    export default RTL_Offerring_User_Information;
}
declare module "@salesforce/label/c.RTL_Overall_Data_Quality" {
    var RTL_Overall_Data_Quality: string;
    export default RTL_Overall_Data_Quality;
}
declare module "@salesforce/label/c.RTL_Page" {
    var RTL_Page: string;
    export default RTL_Page;
}
declare module "@salesforce/label/c.RTL_Pass" {
    var RTL_Pass: string;
    export default RTL_Pass;
}
declare module "@salesforce/label/c.RTL_Previous" {
    var RTL_Previous: string;
    export default RTL_Previous;
}
declare module "@salesforce/label/c.RTL_Purge_Email_001" {
    var RTL_Purge_Email_001: string;
    export default RTL_Purge_Email_001;
}
declare module "@salesforce/label/c.RTL_Purge_Email_002" {
    var RTL_Purge_Email_002: string;
    export default RTL_Purge_Email_002;
}
declare module "@salesforce/label/c.RTL_Purge_Email_003" {
    var RTL_Purge_Email_003: string;
    export default RTL_Purge_Email_003;
}
declare module "@salesforce/label/c.RTL_Purge_Email_004" {
    var RTL_Purge_Email_004: string;
    export default RTL_Purge_Email_004;
}
declare module "@salesforce/label/c.RTL_Purge_Email_005" {
    var RTL_Purge_Email_005: string;
    export default RTL_Purge_Email_005;
}
declare module "@salesforce/label/c.RTL_Purge_Email_006" {
    var RTL_Purge_Email_006: string;
    export default RTL_Purge_Email_006;
}
declare module "@salesforce/label/c.RTL_Purge_Email_007" {
    var RTL_Purge_Email_007: string;
    export default RTL_Purge_Email_007;
}
declare module "@salesforce/label/c.RTL_Purge_Email_008" {
    var RTL_Purge_Email_008: string;
    export default RTL_Purge_Email_008;
}
declare module "@salesforce/label/c.RTL_Purge_Email_009" {
    var RTL_Purge_Email_009: string;
    export default RTL_Purge_Email_009;
}
declare module "@salesforce/label/c.RTL_Purge_Email_010" {
    var RTL_Purge_Email_010: string;
    export default RTL_Purge_Email_010;
}
declare module "@salesforce/label/c.RTL_Purge_Email_011" {
    var RTL_Purge_Email_011: string;
    export default RTL_Purge_Email_011;
}
declare module "@salesforce/label/c.RTL_Purge_Email_012" {
    var RTL_Purge_Email_012: string;
    export default RTL_Purge_Email_012;
}
declare module "@salesforce/label/c.RTL_Purge_Email_013" {
    var RTL_Purge_Email_013: string;
    export default RTL_Purge_Email_013;
}
declare module "@salesforce/label/c.RTL_Purge_Email_014" {
    var RTL_Purge_Email_014: string;
    export default RTL_Purge_Email_014;
}
declare module "@salesforce/label/c.RTL_Purge_Email_015" {
    var RTL_Purge_Email_015: string;
    export default RTL_Purge_Email_015;
}
declare module "@salesforce/label/c.RTL_Purge_Email_footer" {
    var RTL_Purge_Email_footer: string;
    export default RTL_Purge_Email_footer;
}
declare module "@salesforce/label/c.RTL_Purge_MSG001" {
    var RTL_Purge_MSG001: string;
    export default RTL_Purge_MSG001;
}
declare module "@salesforce/label/c.RTL_Purge_MSG002" {
    var RTL_Purge_MSG002: string;
    export default RTL_Purge_MSG002;
}
declare module "@salesforce/label/c.RTL_Purge_MSG003" {
    var RTL_Purge_MSG003: string;
    export default RTL_Purge_MSG003;
}
declare module "@salesforce/label/c.RTL_Quality" {
    var RTL_Quality: string;
    export default RTL_Quality;
}
declare module "@salesforce/label/c.RTL_Referral_ERR001" {
    var RTL_Referral_ERR001: string;
    export default RTL_Referral_ERR001;
}
declare module "@salesforce/label/c.RTL_Referral_ERR002" {
    var RTL_Referral_ERR002: string;
    export default RTL_Referral_ERR002;
}
declare module "@salesforce/label/c.RTL_Referral_ERR003" {
    var RTL_Referral_ERR003: string;
    export default RTL_Referral_ERR003;
}
declare module "@salesforce/label/c.RTL_Referral_ERR004" {
    var RTL_Referral_ERR004: string;
    export default RTL_Referral_ERR004;
}
declare module "@salesforce/label/c.RTL_Referral_ERR005" {
    var RTL_Referral_ERR005: string;
    export default RTL_Referral_ERR005;
}
declare module "@salesforce/label/c.RTL_Referral_ERR006" {
    var RTL_Referral_ERR006: string;
    export default RTL_Referral_ERR006;
}
declare module "@salesforce/label/c.RTL_Referral_ERR007" {
    var RTL_Referral_ERR007: string;
    export default RTL_Referral_ERR007;
}
declare module "@salesforce/label/c.RTL_Referral_ERR008" {
    var RTL_Referral_ERR008: string;
    export default RTL_Referral_ERR008;
}
declare module "@salesforce/label/c.RTL_Referral_ERR009" {
    var RTL_Referral_ERR009: string;
    export default RTL_Referral_ERR009;
}
declare module "@salesforce/label/c.RTL_Referral_ERR010" {
    var RTL_Referral_ERR010: string;
    export default RTL_Referral_ERR010;
}
declare module "@salesforce/label/c.RTL_Referral_ERR011" {
    var RTL_Referral_ERR011: string;
    export default RTL_Referral_ERR011;
}
declare module "@salesforce/label/c.RTL_Referral_ERR012" {
    var RTL_Referral_ERR012: string;
    export default RTL_Referral_ERR012;
}
declare module "@salesforce/label/c.RTL_Referral_ERR013" {
    var RTL_Referral_ERR013: string;
    export default RTL_Referral_ERR013;
}
declare module "@salesforce/label/c.RTL_Referral_ERR014" {
    var RTL_Referral_ERR014: string;
    export default RTL_Referral_ERR014;
}
declare module "@salesforce/label/c.RTL_Referral_ERR015" {
    var RTL_Referral_ERR015: string;
    export default RTL_Referral_ERR015;
}
declare module "@salesforce/label/c.RTL_Referral_ERR016" {
    var RTL_Referral_ERR016: string;
    export default RTL_Referral_ERR016;
}
declare module "@salesforce/label/c.RTL_Referral_ERR017" {
    var RTL_Referral_ERR017: string;
    export default RTL_Referral_ERR017;
}
declare module "@salesforce/label/c.RTL_Referral_ERR018" {
    var RTL_Referral_ERR018: string;
    export default RTL_Referral_ERR018;
}
declare module "@salesforce/label/c.RTL_Referral_ERR019" {
    var RTL_Referral_ERR019: string;
    export default RTL_Referral_ERR019;
}
declare module "@salesforce/label/c.RTL_Referral_ERR020" {
    var RTL_Referral_ERR020: string;
    export default RTL_Referral_ERR020;
}
declare module "@salesforce/label/c.RTL_Referral_ERR021" {
    var RTL_Referral_ERR021: string;
    export default RTL_Referral_ERR021;
}
declare module "@salesforce/label/c.RTL_Referral_ERR022" {
    var RTL_Referral_ERR022: string;
    export default RTL_Referral_ERR022;
}
declare module "@salesforce/label/c.RTL_Referral_NO_AUTHORIZED" {
    var RTL_Referral_NO_AUTHORIZED: string;
    export default RTL_Referral_NO_AUTHORIZED;
}
declare module "@salesforce/label/c.RTL_Referral_NO_AUTHORIZED_Update_order" {
    var RTL_Referral_NO_AUTHORIZED_Update_order: string;
    export default RTL_Referral_NO_AUTHORIZED_Update_order;
}
declare module "@salesforce/label/c.RTL_Referral_NO_LICENSE" {
    var RTL_Referral_NO_LICENSE: string;
    export default RTL_Referral_NO_LICENSE;
}
declare module "@salesforce/label/c.RTL_Referral_NO_REASSIGN" {
    var RTL_Referral_NO_REASSIGN: string;
    export default RTL_Referral_NO_REASSIGN;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_BACK" {
    var RTL_Referral_REASSIGN_BACK: string;
    export default RTL_Referral_REASSIGN_BACK;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_CONFIRM" {
    var RTL_Referral_REASSIGN_CONFIRM: string;
    export default RTL_Referral_REASSIGN_CONFIRM;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_DONE_Message" {
    var RTL_Referral_REASSIGN_DONE_Message: string;
    export default RTL_Referral_REASSIGN_DONE_Message;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_NO" {
    var RTL_Referral_REASSIGN_NO: string;
    export default RTL_Referral_REASSIGN_NO;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_TO" {
    var RTL_Referral_REASSIGN_TO: string;
    export default RTL_Referral_REASSIGN_TO;
}
declare module "@salesforce/label/c.RTL_Referral_REASSIGN_YES" {
    var RTL_Referral_REASSIGN_YES: string;
    export default RTL_Referral_REASSIGN_YES;
}
declare module "@salesforce/label/c.RTL_Referral_SALES_AMT_REQUIRED" {
    var RTL_Referral_SALES_AMT_REQUIRED: string;
    export default RTL_Referral_SALES_AMT_REQUIRED;
}
declare module "@salesforce/label/c.RTL_Reject_All" {
    var RTL_Reject_All: string;
    export default RTL_Reject_All;
}
declare module "@salesforce/label/c.RTL_Related_Record" {
    var RTL_Related_Record: string;
    export default RTL_Related_Record;
}
declare module "@salesforce/label/c.RTL_Request" {
    var RTL_Request: string;
    export default RTL_Request;
}
declare module "@salesforce/label/c.RTL_Request_Delete" {
    var RTL_Request_Delete: string;
    export default RTL_Request_Delete;
}
declare module "@salesforce/label/c.RTL_Request_Details" {
    var RTL_Request_Details: string;
    export default RTL_Request_Details;
}
declare module "@salesforce/label/c.RTL_Required_Field" {
    var RTL_Required_Field: string;
    export default RTL_Required_Field;
}
declare module "@salesforce/label/c.RTL_Result" {
    var RTL_Result: string;
    export default RTL_Result;
}
declare module "@salesforce/label/c.RTL_Sales_Label" {
    var RTL_Sales_Label: string;
    export default RTL_Sales_Label;
}
declare module "@salesforce/label/c.RTL_Select_ExAcc_ToMerge_Label" {
    var RTL_Select_ExAcc_ToMerge_Label: string;
    export default RTL_Select_ExAcc_ToMerge_Label;
}
declare module "@salesforce/label/c.RTL_Selling_Tips" {
    var RTL_Selling_Tips: string;
    export default RTL_Selling_Tips;
}
declare module "@salesforce/label/c.RTL_Send_Email" {
    var RTL_Send_Email: string;
    export default RTL_Send_Email;
}
declare module "@salesforce/label/c.RTL_Submit_for_Approval" {
    var RTL_Submit_for_Approval: string;
    export default RTL_Submit_for_Approval;
}
declare module "@salesforce/label/c.RTL_To" {
    var RTL_To: string;
    export default RTL_To;
}
declare module "@salesforce/label/c.RTL_Unselected_Account_Error_Message" {
    var RTL_Unselected_Account_Error_Message: string;
    export default RTL_Unselected_Account_Error_Message;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Email_Content" {
    var RTL_UploadCampaignMember_Email_Content: string;
    export default RTL_UploadCampaignMember_Email_Content;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Email_Header" {
    var RTL_UploadCampaignMember_Email_Header: string;
    export default RTL_UploadCampaignMember_Email_Header;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_BatchNo" {
    var RTL_UploadCampaignMember_Head_BatchNo: string;
    export default RTL_UploadCampaignMember_Head_BatchNo;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_CreateAt" {
    var RTL_UploadCampaignMember_Head_CreateAt: string;
    export default RTL_UploadCampaignMember_Head_CreateAt;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_Fail" {
    var RTL_UploadCampaignMember_Head_Fail: string;
    export default RTL_UploadCampaignMember_Head_Fail;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_Ignore" {
    var RTL_UploadCampaignMember_Head_Ignore: string;
    export default RTL_UploadCampaignMember_Head_Ignore;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_JobId" {
    var RTL_UploadCampaignMember_Head_JobId: string;
    export default RTL_UploadCampaignMember_Head_JobId;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_LogFile" {
    var RTL_UploadCampaignMember_Head_LogFile: string;
    export default RTL_UploadCampaignMember_Head_LogFile;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_Log_Name" {
    var RTL_UploadCampaignMember_Head_Log_Name: string;
    export default RTL_UploadCampaignMember_Head_Log_Name;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_Success" {
    var RTL_UploadCampaignMember_Head_Success: string;
    export default RTL_UploadCampaignMember_Head_Success;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Head_Total" {
    var RTL_UploadCampaignMember_Head_Total: string;
    export default RTL_UploadCampaignMember_Head_Total;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Log_Tab" {
    var RTL_UploadCampaignMember_Log_Tab: string;
    export default RTL_UploadCampaignMember_Log_Tab;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Log_Title" {
    var RTL_UploadCampaignMember_Log_Title: string;
    export default RTL_UploadCampaignMember_Log_Title;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_SelectCampaign" {
    var RTL_UploadCampaignMember_SelectCampaign: string;
    export default RTL_UploadCampaignMember_SelectCampaign;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_SelectCampaign_Notice" {
    var RTL_UploadCampaignMember_SelectCampaign_Notice: string;
    export default RTL_UploadCampaignMember_SelectCampaign_Notice;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_SelectFile" {
    var RTL_UploadCampaignMember_SelectFile: string;
    export default RTL_UploadCampaignMember_SelectFile;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Subtitle" {
    var RTL_UploadCampaignMember_Subtitle: string;
    export default RTL_UploadCampaignMember_Subtitle;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Title" {
    var RTL_UploadCampaignMember_Title: string;
    export default RTL_UploadCampaignMember_Title;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Upload_Btn" {
    var RTL_UploadCampaignMember_Upload_Btn: string;
    export default RTL_UploadCampaignMember_Upload_Btn;
}
declare module "@salesforce/label/c.RTL_UploadCampaignMember_Upload_Tab" {
    var RTL_UploadCampaignMember_Upload_Tab: string;
    export default RTL_UploadCampaignMember_Upload_Tab;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ACCOUNTID" {
    var RTL_ViewCampaignMember_ACCOUNTID: string;
    export default RTL_ViewCampaignMember_ACCOUNTID;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_AMOUNT" {
    var RTL_ViewCampaignMember_AMOUNT: string;
    export default RTL_ViewCampaignMember_AMOUNT;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CAMPAIGNCODE" {
    var RTL_ViewCampaignMember_CAMPAIGNCODE: string;
    export default RTL_ViewCampaignMember_CAMPAIGNCODE;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CAMPAIGNNAME" {
    var RTL_ViewCampaignMember_CAMPAIGNNAME: string;
    export default RTL_ViewCampaignMember_CAMPAIGNNAME;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CARDNO" {
    var RTL_ViewCampaignMember_CARDNO: string;
    export default RTL_ViewCampaignMember_CARDNO;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CampaignNameTH" {
    var RTL_ViewCampaignMember_CampaignNameTH: string;
    export default RTL_ViewCampaignMember_CampaignNameTH;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CampaignProduct" {
    var RTL_ViewCampaignMember_CampaignProduct: string;
    export default RTL_ViewCampaignMember_CampaignProduct;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ClickDateTime" {
    var RTL_ViewCampaignMember_ClickDateTime: string;
    export default RTL_ViewCampaignMember_ClickDateTime;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ClickFlag" {
    var RTL_ViewCampaignMember_ClickFlag: string;
    export default RTL_ViewCampaignMember_ClickFlag;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ContactResult" {
    var RTL_ViewCampaignMember_ContactResult: string;
    export default RTL_ViewCampaignMember_ContactResult;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CreatedDateRegisterDate" {
    var RTL_ViewCampaignMember_CreatedDateRegisterDate: string;
    export default RTL_ViewCampaignMember_CreatedDateRegisterDate;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_CustomerName" {
    var RTL_ViewCampaignMember_CustomerName: string;
    export default RTL_ViewCampaignMember_CustomerName;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_DESCRIPTION" {
    var RTL_ViewCampaignMember_DESCRIPTION: string;
    export default RTL_ViewCampaignMember_DESCRIPTION;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_DisplayDateTime" {
    var RTL_ViewCampaignMember_DisplayDateTime: string;
    export default RTL_ViewCampaignMember_DisplayDateTime;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_DisplayFlag" {
    var RTL_ViewCampaignMember_DisplayFlag: string;
    export default RTL_ViewCampaignMember_DisplayFlag;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_EmailResponse" {
    var RTL_ViewCampaignMember_EmailResponse: string;
    export default RTL_ViewCampaignMember_EmailResponse;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_FULFILLMENTTYPE" {
    var RTL_ViewCampaignMember_FULFILLMENTTYPE: string;
    export default RTL_ViewCampaignMember_FULFILLMENTTYPE;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_FulfillmentInfo" {
    var RTL_ViewCampaignMember_FulfillmentInfo: string;
    export default RTL_ViewCampaignMember_FulfillmentInfo;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_MIBResponse" {
    var RTL_ViewCampaignMember_MIBResponse: string;
    export default RTL_ViewCampaignMember_MIBResponse;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_OpenDateTime" {
    var RTL_ViewCampaignMember_OpenDateTime: string;
    export default RTL_ViewCampaignMember_OpenDateTime;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_OpenFlag" {
    var RTL_ViewCampaignMember_OpenFlag: string;
    export default RTL_ViewCampaignMember_OpenFlag;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_OutboundContactDateTime" {
    var RTL_ViewCampaignMember_OutboundContactDateTime: string;
    export default RTL_ViewCampaignMember_OutboundContactDateTime;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_OutboundResult" {
    var RTL_ViewCampaignMember_OutboundResult: string;
    export default RTL_ViewCampaignMember_OutboundResult;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_Outbound_Contact_Staff" {
    var RTL_ViewCampaignMember_Outbound_Contact_Staff: string;
    export default RTL_ViewCampaignMember_Outbound_Contact_Staff;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_PROCESSDATE" {
    var RTL_ViewCampaignMember_PROCESSDATE: string;
    export default RTL_ViewCampaignMember_PROCESSDATE;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_RegisterDateTime" {
    var RTL_ViewCampaignMember_RegisterDateTime: string;
    export default RTL_ViewCampaignMember_RegisterDateTime;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_RejectDate" {
    var RTL_ViewCampaignMember_RejectDate: string;
    export default RTL_ViewCampaignMember_RejectDate;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ResponseInfo" {
    var RTL_ViewCampaignMember_ResponseInfo: string;
    export default RTL_ViewCampaignMember_ResponseInfo;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ResultBusiness" {
    var RTL_ViewCampaignMember_ResultBusiness: string;
    export default RTL_ViewCampaignMember_ResultBusiness;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_ResultDetail" {
    var RTL_ViewCampaignMember_ResultDetail: string;
    export default RTL_ViewCampaignMember_ResultDetail;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_SMSContent" {
    var RTL_ViewCampaignMember_SMSContent: string;
    export default RTL_ViewCampaignMember_SMSContent;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_SMSMessage" {
    var RTL_ViewCampaignMember_SMSMessage: string;
    export default RTL_ViewCampaignMember_SMSMessage;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_SMSRegistration" {
    var RTL_ViewCampaignMember_SMSRegistration: string;
    export default RTL_ViewCampaignMember_SMSRegistration;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_STATUS" {
    var RTL_ViewCampaignMember_STATUS: string;
    export default RTL_ViewCampaignMember_STATUS;
}
declare module "@salesforce/label/c.RTL_ViewCampaignMember_SentDate" {
    var RTL_ViewCampaignMember_SentDate: string;
    export default RTL_ViewCampaignMember_SentDate;
}
declare module "@salesforce/label/c.RTL_Wealth" {
    var RTL_Wealth: string;
    export default RTL_Wealth;
}
declare module "@salesforce/label/c.RTL_WebDropLead_HLCAL_Remark" {
    var RTL_WebDropLead_HLCAL_Remark: string;
    export default RTL_WebDropLead_HLCAL_Remark;
}
declare module "@salesforce/label/c.RTL_WebDropLead_Product_Features" {
    var RTL_WebDropLead_Product_Features: string;
    export default RTL_WebDropLead_Product_Features;
}
declare module "@salesforce/label/c.RTL_WebDropLead_Remark" {
    var RTL_WebDropLead_Remark: string;
    export default RTL_WebDropLead_Remark;
}
declare module "@salesforce/label/c.Rate_interest_header" {
    var Rate_interest_header: string;
    export default Rate_interest_header;
}
declare module "@salesforce/label/c.Realized_G_L" {
    var Realized_G_L: string;
    export default Realized_G_L;
}
declare module "@salesforce/label/c.Reason" {
    var Reason: string;
    export default Reason;
}
declare module "@salesforce/label/c.Recommended_Action" {
    var Recommended_Action: string;
    export default Recommended_Action;
}
declare module "@salesforce/label/c.RecordType" {
    var RecordType: string;
    export default RecordType;
}
declare module "@salesforce/label/c.Rejected_Stage" {
    var Rejected_Stage: string;
    export default Rejected_Stage;
}
declare module "@salesforce/label/c.Related_Bot_Customer_ID_header" {
    var Related_Bot_Customer_ID_header: string;
    export default Related_Bot_Customer_ID_header;
}
declare module "@salesforce/label/c.Related_Insurance_Info" {
    var Related_Insurance_Info: string;
    export default Related_Insurance_Info;
}
declare module "@salesforce/label/c.Related_To" {
    var Related_To: string;
    export default Related_To;
}
declare module "@salesforce/label/c.Relationship" {
    var Relationship: string;
    export default Relationship;
}
declare module "@salesforce/label/c.Request_Amount" {
    var Request_Amount: string;
    export default Request_Amount;
}
declare module "@salesforce/label/c.Request_Amt_Vol_THB" {
    var Request_Amt_Vol_THB: string;
    export default Request_Amt_Vol_THB;
}
declare module "@salesforce/label/c.Request_Date" {
    var Request_Date: string;
    export default Request_Date;
}
declare module "@salesforce/label/c.Retention_Date" {
    var Retention_Date: string;
    export default Retention_Date;
}
declare module "@salesforce/label/c.Reward_Points" {
    var Reward_Points: string;
    export default Reward_Points;
}
declare module "@salesforce/label/c.Risk_information_title_Mini_CSV" {
    var Risk_information_title_Mini_CSV: string;
    export default Risk_information_title_Mini_CSV;
}
declare module "@salesforce/label/c.SMS_Alert_Service" {
    var SMS_Alert_Service: string;
    export default SMS_Alert_Service;
}
declare module "@salesforce/label/c.Sales" {
    var Sales: string;
    export default Sales;
}
declare module "@salesforce/label/c.Sales_Stage" {
    var Sales_Stage: string;
    export default Sales_Stage;
}
declare module "@salesforce/label/c.Sales_Support_Information" {
    var Sales_Support_Information: string;
    export default Sales_Support_Information;
}
declare module "@salesforce/label/c.Save" {
    var Save: string;
    export default Save;
}
declare module "@salesforce/label/c.Save_New" {
    var Save_New: string;
    export default Save_New;
}
declare module "@salesforce/label/c.Saving_account" {
    var Saving_account: string;
    export default Saving_account;
}
declare module "@salesforce/label/c.Saving_account_bundling" {
    var Saving_account_bundling: string;
    export default Saving_account_bundling;
}
declare module "@salesforce/label/c.Send_an_Email" {
    var Send_an_Email: string;
    export default Send_an_Email;
}
declare module "@salesforce/label/c.Settlement_Date" {
    var Settlement_Date: string;
    export default Settlement_Date;
}
declare module "@salesforce/label/c.So_Good_Any_Pay_Plan_Experience" {
    var So_Good_Any_Pay_Plan_Experience: string;
    export default So_Good_Any_Pay_Plan_Experience;
}
declare module "@salesforce/label/c.So_Good_Pay_Plan_Record_In_Last_Statement" {
    var So_Good_Pay_Plan_Record_In_Last_Statement: string;
    export default So_Good_Pay_Plan_Record_In_Last_Statement;
}
declare module "@salesforce/label/c.Source_Contact_Result" {
    var Source_Contact_Result: string;
    export default Source_Contact_Result;
}
declare module "@salesforce/label/c.Stage" {
    var Stage: string;
    export default Stage;
}
declare module "@salesforce/label/c.Standing_Order" {
    var Standing_Order: string;
    export default Standing_Order;
}
declare module "@salesforce/label/c.Start_date" {
    var Start_date: string;
    export default Start_date;
}
declare module "@salesforce/label/c.Status" {
    var Status: string;
    export default Status;
}
declare module "@salesforce/label/c.Sum_Insure" {
    var Sum_Insure: string;
    export default Sum_Insure;
}
declare module "@salesforce/label/c.Sum_Insured" {
    var Sum_Insured: string;
    export default Sum_Insured;
}
declare module "@salesforce/label/c.Supplementary_Card_Number" {
    var Supplementary_Card_Number: string;
    export default Supplementary_Card_Number;
}
declare module "@salesforce/label/c.Supplementary_Cardholder_Name_1" {
    var Supplementary_Cardholder_Name_1: string;
    export default Supplementary_Cardholder_Name_1;
}
declare module "@salesforce/label/c.Supplementary_Info" {
    var Supplementary_Info: string;
    export default Supplementary_Info;
}
declare module "@salesforce/label/c.SurveyChoiceInputComponent_WhenPatternMismatch_Type_Email_Msg" {
    var SurveyChoiceInputComponent_WhenPatternMismatch_Type_Email_Msg: string;
    export default SurveyChoiceInputComponent_WhenPatternMismatch_Type_Email_Msg;
}
declare module "@salesforce/label/c.SurveyChoiceInputComponent_WhenPatternMismatch_Type_Telephone_Msg" {
    var SurveyChoiceInputComponent_WhenPatternMismatch_Type_Telephone_Msg: string;
    export default SurveyChoiceInputComponent_WhenPatternMismatch_Type_Telephone_Msg;
}
declare module "@salesforce/label/c.SurveyChoiceInputComponent_WhenTypeMismatch_Type_Email_Msg" {
    var SurveyChoiceInputComponent_WhenTypeMismatch_Type_Email_Msg: string;
    export default SurveyChoiceInputComponent_WhenTypeMismatch_Type_Email_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_ChoiceValidate_Failed_Msg" {
    var SurveyManagerRender_ChoiceValidate_Failed_Msg: string;
    export default SurveyManagerRender_ChoiceValidate_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_DependentQuestionValidate_Failed_Msg" {
    var SurveyManagerRender_DependentQuestionValidate_Failed_Msg: string;
    export default SurveyManagerRender_DependentQuestionValidate_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_QuestionValidate_Failed_Msg" {
    var SurveyManagerRender_QuestionValidate_Failed_Msg: string;
    export default SurveyManagerRender_QuestionValidate_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_SaveAnswer_Failed_Msg" {
    var SurveyManagerRender_SaveAnswer_Failed_Msg: string;
    export default SurveyManagerRender_SaveAnswer_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_SaveAnswer_Success_Msg" {
    var SurveyManagerRender_SaveAnswer_Success_Msg: string;
    export default SurveyManagerRender_SaveAnswer_Success_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_SaveDraft_Failed_Msg" {
    var SurveyManagerRender_SaveDraft_Failed_Msg: string;
    export default SurveyManagerRender_SaveDraft_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_SaveDraft_Success_Msg" {
    var SurveyManagerRender_SaveDraft_Success_Msg: string;
    export default SurveyManagerRender_SaveDraft_Success_Msg;
}
declare module "@salesforce/label/c.SurveyManagerRender_Validate_Failed_Msg" {
    var SurveyManagerRender_Validate_Failed_Msg: string;
    export default SurveyManagerRender_Validate_Failed_Msg;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Percentage" {
    var SurveyManager_Choice_Validate_Percentage: string;
    export default SurveyManager_Choice_Validate_Percentage;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_All" {
    var SurveyManager_Choice_Validate_Require_All: string;
    export default SurveyManager_Choice_Validate_Require_All;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_All_Or_None" {
    var SurveyManager_Choice_Validate_Require_All_Or_None: string;
    export default SurveyManager_Choice_Validate_Require_All_Or_None;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_One" {
    var SurveyManager_Choice_Validate_Require_One: string;
    export default SurveyManager_Choice_Validate_Require_One;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_One_Or_More" {
    var SurveyManager_Choice_Validate_Require_One_Or_More: string;
    export default SurveyManager_Choice_Validate_Require_One_Or_More;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_One_Or_More_Sub" {
    var SurveyManager_Choice_Validate_Require_One_Or_More_Sub: string;
    export default SurveyManager_Choice_Validate_Require_One_Or_More_Sub;
}
declare module "@salesforce/label/c.SurveyManager_Choice_Validate_Require_One_Sub" {
    var SurveyManager_Choice_Validate_Require_One_Sub: string;
    export default SurveyManager_Choice_Validate_Require_One_Sub;
}
declare module "@salesforce/label/c.System_Information" {
    var System_Information: string;
    export default System_Information;
}
declare module "@salesforce/label/c.TMB_Account_ID_header" {
    var TMB_Account_ID_header: string;
    export default TMB_Account_ID_header;
}
declare module "@salesforce/label/c.TMB_Customer_ID_PE" {
    var TMB_Customer_ID_PE: string;
    export default TMB_Customer_ID_PE;
}
declare module "@salesforce/label/c.TMB_Smart_title" {
    var TMB_Smart_title: string;
    export default TMB_Smart_title;
}
declare module "@salesforce/label/c.TaskEvent_RecordType" {
    var TaskEvent_RecordType: string;
    export default TaskEvent_RecordType;
}
declare module "@salesforce/label/c.Task_DueDate" {
    var Task_DueDate: string;
    export default Task_DueDate;
}
declare module "@salesforce/label/c.Temporary_Line" {
    var Temporary_Line: string;
    export default Temporary_Line;
}
declare module "@salesforce/label/c.Temporary_Line_Period" {
    var Temporary_Line_Period: string;
    export default Temporary_Line_Period;
}
declare module "@salesforce/label/c.Tenor_Month" {
    var Tenor_Month: string;
    export default Tenor_Month;
}
declare module "@salesforce/label/c.Title_Note" {
    var Title_Note: string;
    export default Title_Note;
}
declare module "@salesforce/label/c.To" {
    var To: string;
    export default To;
}
declare module "@salesforce/label/c.Total_Amount" {
    var Total_Amount: string;
    export default Total_Amount;
}
declare module "@salesforce/label/c.Total_Cash_Back_Paid" {
    var Total_Cash_Back_Paid: string;
    export default Total_Cash_Back_Paid;
}
declare module "@salesforce/label/c.Total_Premium_Paid" {
    var Total_Premium_Paid: string;
    export default Total_Premium_Paid;
}
declare module "@salesforce/label/c.Total_Txn" {
    var Total_Txn: string;
    export default Total_Txn;
}
declare module "@salesforce/label/c.Touch_IB_Prom" {
    var Touch_IB_Prom: string;
    export default Touch_IB_Prom;
}
declare module "@salesforce/label/c.Trn_Sts" {
    var Trn_Sts: string;
    export default Trn_Sts;
}
declare module "@salesforce/label/c.Trn_Type" {
    var Trn_Type: string;
    export default Trn_Type;
}
declare module "@salesforce/label/c.Trn_Value" {
    var Trn_Value: string;
    export default Trn_Value;
}
declare module "@salesforce/label/c.Txn_of_Interest_Charged" {
    var Txn_of_Interest_Charged: string;
    export default Txn_of_Interest_Charged;
}
declare module "@salesforce/label/c.Txn_of_Non_Interest_Charged" {
    var Txn_of_Non_Interest_Charged: string;
    export default Txn_of_Non_Interest_Charged;
}
declare module "@salesforce/label/c.Type" {
    var Type: string;
    export default Type;
}
declare module "@salesforce/label/c.Type_Note" {
    var Type_Note: string;
    export default Type_Note;
}
declare module "@salesforce/label/c.Unit_Holder_No" {
    var Unit_Holder_No: string;
    export default Unit_Holder_No;
}
declare module "@salesforce/label/c.Unit_Movement" {
    var Unit_Movement: string;
    export default Unit_Movement;
}
declare module "@salesforce/label/c.Unit_O_S" {
    var Unit_O_S: string;
    export default Unit_O_S;
}
declare module "@salesforce/label/c.Unitholder_No" {
    var Unitholder_No: string;
    export default Unitholder_No;
}
declare module "@salesforce/label/c.Units" {
    var Units: string;
    export default Units;
}
declare module "@salesforce/label/c.Unrealized_G_L" {
    var Unrealized_G_L: string;
    export default Unrealized_G_L;
}
declare module "@salesforce/label/c.Update_All_Order_Status_Success" {
    var Update_All_Order_Status_Success: string;
    export default Update_All_Order_Status_Success;
}
declare module "@salesforce/label/c.User_History" {
    var User_History: string;
    export default User_History;
}
declare module "@salesforce/label/c.Value_Per_Unit" {
    var Value_Per_Unit: string;
    export default Value_Per_Unit;
}
declare module "@salesforce/label/c.View" {
    var View: string;
    export default View;
}
declare module "@salesforce/label/c.View_Activity_History" {
    var View_Activity_History: string;
    export default View_Activity_History;
}
declare module "@salesforce/label/c.View_All" {
    var View_All: string;
    export default View_All;
}
declare module "@salesforce/label/c.View_All_Notes_Attachments" {
    var View_All_Notes_Attachments: string;
    export default View_All_Notes_Attachments;
}
declare module "@salesforce/label/c.Warning_text_EN" {
    var Warning_text_EN: string;
    export default Warning_text_EN;
}
declare module "@salesforce/label/c.Warning_text_TH" {
    var Warning_text_TH: string;
    export default Warning_text_TH;
}
declare module "@salesforce/label/c.Year_of_payment" {
    var Year_of_payment: string;
    export default Year_of_payment;
}
declare module "@salesforce/label/c.Yr_of_Manufacture" {
    var Yr_of_Manufacture: string;
    export default Yr_of_Manufacture;
}
declare module "@salesforce/label/c.of_Bill_Payment" {
    var of_Bill_Payment: string;
    export default of_Bill_Payment;
}
declare module "@salesforce/label/c.of_Deposit" {
    var of_Deposit: string;
    export default of_Deposit;
}
declare module "@salesforce/label/c.of_Loan_Payment" {
    var of_Loan_Payment: string;
    export default of_Loan_Payment;
}
declare module "@salesforce/label/c.of_Transfer_Out_Other_Banks" {
    var of_Transfer_Out_Other_Banks: string;
    export default of_Transfer_Out_Other_Banks;
}
declare module "@salesforce/label/c.of_Transfer_Out_Within_TMB" {
    var of_Transfer_Out_Within_TMB: string;
    export default of_Transfer_Out_Within_TMB;
}
declare module "@salesforce/label/c.of_Withdrawal" {
    var of_Withdrawal: string;
    export default of_Withdrawal;
}
declare module "@salesforce/label/c.osc01_sc1col1" {
    var osc01_sc1col1: string;
    export default osc01_sc1col1;
}
declare module "@salesforce/label/c.osc01_sc1col2" {
    var osc01_sc1col2: string;
    export default osc01_sc1col2;
}
declare module "@salesforce/label/c.percent_of_Unrealized_G_L" {
    var percent_of_Unrealized_G_L: string;
    export default percent_of_Unrealized_G_L;
}
declare module "@salesforce/label/c.remove_product_item_modal_title" {
    var remove_product_item_modal_title: string;
    export default remove_product_item_modal_title;
}
