declare module "@salesforce/resourceUrl/AccountPlanConfigTestData" {
    var AccountPlanConfigTestData: string;
    export default AccountPlanConfigTestData;
}
declare module "@salesforce/resourceUrl/AccountPlanEditTable" {
    var AccountPlanEditTable: string;
    export default AccountPlanEditTable;
}
declare module "@salesforce/resourceUrl/AccountPlanMaster" {
    var AccountPlanMaster: string;
    export default AccountPlanMaster;
}
declare module "@salesforce/resourceUrl/AccountPlanPDF" {
    var AccountPlanPDF: string;
    export default AccountPlanPDF;
}
declare module "@salesforce/resourceUrl/AccountPlan_Bootstrap" {
    var AccountPlan_Bootstrap: string;
    export default AccountPlan_Bootstrap;
}
declare module "@salesforce/resourceUrl/AccountPlan_HTML5shiv" {
    var AccountPlan_HTML5shiv: string;
    export default AccountPlan_HTML5shiv;
}
declare module "@salesforce/resourceUrl/AccountPlan_JqueryScript" {
    var AccountPlan_JqueryScript: string;
    export default AccountPlan_JqueryScript;
}
declare module "@salesforce/resourceUrl/AccountPlan_Mustache" {
    var AccountPlan_Mustache: string;
    export default AccountPlan_Mustache;
}
declare module "@salesforce/resourceUrl/AccountPlan_Respond" {
    var AccountPlan_Respond: string;
    export default AccountPlan_Respond;
}
declare module "@salesforce/resourceUrl/AcctPlanQuestionnaireTemplate" {
    var AcctPlanQuestionnaireTemplate: string;
    export default AcctPlanQuestionnaireTemplate;
}
declare module "@salesforce/resourceUrl/Activity_pipeline" {
    var Activity_pipeline: string;
    export default Activity_pipeline;
}
declare module "@salesforce/resourceUrl/ChangeCustomerOwner" {
    var ChangeCustomerOwner: string;
    export default ChangeCustomerOwner;
}
declare module "@salesforce/resourceUrl/CloseSystemMessage" {
    var CloseSystemMessage: string;
    export default CloseSystemMessage;
}
declare module "@salesforce/resourceUrl/Customer_IncomeEstimation" {
    var Customer_IncomeEstimation: string;
    export default Customer_IncomeEstimation;
}
declare module "@salesforce/resourceUrl/Customer_wallet" {
    var Customer_wallet: string;
    export default Customer_wallet;
}
declare module "@salesforce/resourceUrl/DepositeSection" {
    var DepositeSection: string;
    export default DepositeSection;
}
declare module "@salesforce/resourceUrl/FNA_SharpCalendar" {
    var FNA_SharpCalendar: string;
    export default FNA_SharpCalendar;
}
declare module "@salesforce/resourceUrl/FullBodyPhoto" {
    var FullBodyPhoto: string;
    export default FullBodyPhoto;
}
declare module "@salesforce/resourceUrl/FullPhotoUser" {
    var FullPhotoUser: string;
    export default FullPhotoUser;
}
declare module "@salesforce/resourceUrl/Global_TMB_CSS" {
    var Global_TMB_CSS: string;
    export default Global_TMB_CSS;
}
declare module "@salesforce/resourceUrl/HelpOrange13" {
    var HelpOrange13: string;
    export default HelpOrange13;
}
declare module "@salesforce/resourceUrl/HotMessagePic1" {
    var HotMessagePic1: string;
    export default HotMessagePic1;
}
declare module "@salesforce/resourceUrl/HotMessagePic2" {
    var HotMessagePic2: string;
    export default HotMessagePic2;
}
declare module "@salesforce/resourceUrl/HotMessagePic3" {
    var HotMessagePic3: string;
    export default HotMessagePic3;
}
declare module "@salesforce/resourceUrl/HotMessagePic4" {
    var HotMessagePic4: string;
    export default HotMessagePic4;
}
declare module "@salesforce/resourceUrl/InternetError" {
    var InternetError: string;
    export default InternetError;
}
declare module "@salesforce/resourceUrl/MaintenanceSiteResource" {
    var MaintenanceSiteResource: string;
    export default MaintenanceSiteResource;
}
declare module "@salesforce/resourceUrl/Mobile_Design_Templates" {
    var Mobile_Design_Templates: string;
    export default Mobile_Design_Templates;
}
declare module "@salesforce/resourceUrl/Opp_icon" {
    var Opp_icon: string;
    export default Opp_icon;
}
declare module "@salesforce/resourceUrl/Overdue" {
    var Overdue: string;
    export default Overdue;
}
declare module "@salesforce/resourceUrl/Performance_report" {
    var Performance_report: string;
    export default Performance_report;
}
declare module "@salesforce/resourceUrl/ProductMaster" {
    var ProductMaster: string;
    export default ProductMaster;
}
declare module "@salesforce/resourceUrl/RTLProductMaster" {
    var RTLProductMaster: string;
    export default RTLProductMaster;
}
declare module "@salesforce/resourceUrl/RTL_Calllist_Logo" {
    var RTL_Calllist_Logo: string;
    export default RTL_Calllist_Logo;
}
declare module "@salesforce/resourceUrl/RTL_Campaign_Logo" {
    var RTL_Campaign_Logo: string;
    export default RTL_Campaign_Logo;
}
declare module "@salesforce/resourceUrl/RTL_Campaignmember_Logo" {
    var RTL_Campaignmember_Logo: string;
    export default RTL_Campaignmember_Logo;
}
declare module "@salesforce/resourceUrl/RTL_Household_Logo" {
    var RTL_Household_Logo: string;
    export default RTL_Household_Logo;
}
declare module "@salesforce/resourceUrl/RTL_NBO_Logo" {
    var RTL_NBO_Logo: string;
    export default RTL_NBO_Logo;
}
declare module "@salesforce/resourceUrl/RTL_NBO_Logo_Invert" {
    var RTL_NBO_Logo_Invert: string;
    export default RTL_NBO_Logo_Invert;
}
declare module "@salesforce/resourceUrl/RTL_Reallocate_Campaign_Member_Logo" {
    var RTL_Reallocate_Campaign_Member_Logo: string;
    export default RTL_Reallocate_Campaign_Member_Logo;
}
declare module "@salesforce/resourceUrl/Risk" {
    var Risk: string;
    export default Risk;
}
declare module "@salesforce/resourceUrl/SFScript" {
    var SFScript: string;
    export default SFScript;
}
declare module "@salesforce/resourceUrl/SLOS" {
    var SLOS: string;
    export default SLOS;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/TMBLogo" {
    var TMBLogo: string;
    export default TMBLogo;
}
declare module "@salesforce/resourceUrl/TMB_All_Blue_Logo" {
    var TMB_All_Blue_Logo: string;
    export default TMB_All_Blue_Logo;
}
declare module "@salesforce/resourceUrl/TMBjquery" {
    var TMBjquery: string;
    export default TMBjquery;
}
declare module "@salesforce/resourceUrl/ToastMessage" {
    var ToastMessage: string;
    export default ToastMessage;
}
declare module "@salesforce/resourceUrl/Wallet1" {
    var Wallet1: string;
    export default Wallet1;
}
declare module "@salesforce/resourceUrl/Wallet2" {
    var Wallet2: string;
    export default Wallet2;
}
declare module "@salesforce/resourceUrl/Wallet3" {
    var Wallet3: string;
    export default Wallet3;
}
declare module "@salesforce/resourceUrl/Wallet4" {
    var Wallet4: string;
    export default Wallet4;
}
declare module "@salesforce/resourceUrl/approvals" {
    var approvals: string;
    export default approvals;
}
declare module "@salesforce/resourceUrl/approvals_jQuery_2_0_2" {
    var approvals_jQuery_2_0_2: string;
    export default approvals_jQuery_2_0_2;
}
declare module "@salesforce/resourceUrl/approvals_underscore_1_5_1" {
    var approvals_underscore_1_5_1: string;
    export default approvals_underscore_1_5_1;
}
declare module "@salesforce/resourceUrl/blueMarker" {
    var blueMarker: string;
    export default blueMarker;
}
declare module "@salesforce/resourceUrl/boostrapex" {
    var boostrapex: string;
    export default boostrapex;
}
declare module "@salesforce/resourceUrl/boostrapsf1" {
    var boostrapsf1: string;
    export default boostrapsf1;
}
declare module "@salesforce/resourceUrl/bootstrapsf1" {
    var bootstrapsf1: string;
    export default bootstrapsf1;
}
declare module "@salesforce/resourceUrl/campaign_icon" {
    var campaign_icon: string;
    export default campaign_icon;
}
declare module "@salesforce/resourceUrl/collateral" {
    var collateral: string;
    export default collateral;
}
declare module "@salesforce/resourceUrl/cometd_3_1_4" {
    var cometd_3_1_4: string;
    export default cometd_3_1_4;
}
declare module "@salesforce/resourceUrl/contact_icon" {
    var contact_icon: string;
    export default contact_icon;
}
declare module "@salesforce/resourceUrl/customer_logo_icon" {
    var customer_logo_icon: string;
    export default customer_logo_icon;
}
declare module "@salesforce/resourceUrl/datatable" {
    var datatable: string;
    export default datatable;
}
declare module "@salesforce/resourceUrl/fonts" {
    var fonts: string;
    export default fonts;
}
declare module "@salesforce/resourceUrl/fullCalendarCSS" {
    var fullCalendarCSS: string;
    export default fullCalendarCSS;
}
declare module "@salesforce/resourceUrl/fullCalendarMinJS" {
    var fullCalendarMinJS: string;
    export default fullCalendarMinJS;
}
declare module "@salesforce/resourceUrl/jquery_ui1114" {
    var jquery_ui1114: string;
    export default jquery_ui1114;
}
declare module "@salesforce/resourceUrl/medal_bronze_transparent" {
    var medal_bronze_transparent: string;
    export default medal_bronze_transparent;
}
declare module "@salesforce/resourceUrl/medal_gold_transparent" {
    var medal_gold_transparent: string;
    export default medal_gold_transparent;
}
declare module "@salesforce/resourceUrl/medal_platinum_transparent" {
    var medal_platinum_transparent: string;
    export default medal_platinum_transparent;
}
declare module "@salesforce/resourceUrl/medal_silver_transparent" {
    var medal_silver_transparent: string;
    export default medal_silver_transparent;
}
declare module "@salesforce/resourceUrl/momentJS" {
    var momentJS: string;
    export default momentJS;
}
declare module "@salesforce/resourceUrl/opportunity_tab_icon" {
    var opportunity_tab_icon: string;
    export default opportunity_tab_icon;
}
declare module "@salesforce/resourceUrl/sfLightningVF" {
    var sfLightningVF: string;
    export default sfLightningVF;
}
declare module "@salesforce/resourceUrl/transparent" {
    var transparent: string;
    export default transparent;
}
declare module "@salesforce/resourceUrl/visitReport_icon" {
    var visitReport_icon: string;
    export default visitReport_icon;
}
