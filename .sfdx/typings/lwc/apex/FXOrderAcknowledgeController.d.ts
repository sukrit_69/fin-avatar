declare module "@salesforce/apex/FXOrderAcknowledgeController.acknowledgeFromQuickAction" {
  export default function acknowledgeFromQuickAction(param: {fxo: any}): Promise<any>;
}
declare module "@salesforce/apex/FXOrderAcknowledgeController.getFXO" {
  export default function getFXO(param: {fxoId: any}): Promise<any>;
}
