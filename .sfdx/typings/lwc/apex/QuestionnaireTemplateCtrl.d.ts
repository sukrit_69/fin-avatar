declare module "@salesforce/apex/QuestionnaireTemplateCtrl.getQuestion" {
  export default function getQuestion(param: {thisQuestion: any, thisForm: any}): Promise<any>;
}
declare module "@salesforce/apex/QuestionnaireTemplateCtrl.saveQuestionInformation" {
  export default function saveQuestionInformation(param: {thisQuestion: any, answerQuestion: any}): Promise<any>;
}
declare module "@salesforce/apex/QuestionnaireTemplateCtrl.previousQuestionInformation" {
  export default function previousQuestionInformation(param: {idForm: any, idQuestion: any}): Promise<any>;
}
declare module "@salesforce/apex/QuestionnaireTemplateCtrl.Questionnaire" {
  export default function Questionnaire(): Promise<any>;
}
