declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_recId" {
  export default function __sfdc_recId(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_recId" {
  export default function __sfdc_recId(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_IsAllowToConvert" {
  export default function __sfdc_IsAllowToConvert(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_IsAllowToConvert" {
  export default function __sfdc_IsAllowToConvert(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_IsownerAllowed" {
  export default function __sfdc_IsownerAllowed(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_IsownerAllowed" {
  export default function __sfdc_IsownerAllowed(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_originalAccount" {
  export default function __sfdc_originalAccount(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_originalAccount" {
  export default function __sfdc_originalAccount(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_AccountPermissionSet" {
  export default function __sfdc_AccountPermissionSet(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_AccountPermissionSet" {
  export default function __sfdc_AccountPermissionSet(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_expected_submit_date" {
  export default function __sfdc_expected_submit_date(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_expected_submit_date" {
  export default function __sfdc_expected_submit_date(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_expected_complete_date" {
  export default function __sfdc_expected_complete_date(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_expected_complete_date" {
  export default function __sfdc_expected_complete_date(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_isDuplicateWithAccount" {
  export default function __sfdc_isDuplicateWithAccount(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_isDuplicateWithAccount" {
  export default function __sfdc_isDuplicateWithAccount(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_stdConvertOppty" {
  export default function __sfdc_stdConvertOppty(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_stdConvertOppty" {
  export default function __sfdc_stdConvertOppty(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_currentUser" {
  export default function __sfdc_currentUser(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_currentUser" {
  export default function __sfdc_currentUser(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_PricebookAccess" {
  export default function __sfdc_PricebookAccess(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_PricebookAccess" {
  export default function __sfdc_PricebookAccess(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_mapMonth" {
  export default function __sfdc_mapMonth(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_mapMonth" {
  export default function __sfdc_mapMonth(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_creditSEOpptyRecordType" {
  export default function __sfdc_creditSEOpptyRecordType(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_creditSEOpptyRecordType" {
  export default function __sfdc_creditSEOpptyRecordType(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_CommercialLeadRecordType" {
  export default function __sfdc_CommercialLeadRecordType(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_CommercialLeadRecordType" {
  export default function __sfdc_CommercialLeadRecordType(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_CompletedRecordType" {
  export default function __sfdc_CompletedRecordType(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_CompletedRecordType" {
  export default function __sfdc_CompletedRecordType(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_AccountRecordType" {
  export default function __sfdc_AccountRecordType(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_AccountRecordType" {
  export default function __sfdc_AccountRecordType(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_ProductList" {
  export default function __sfdc_ProductList(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_ProductList" {
  export default function __sfdc_ProductList(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_hostproductGroup" {
  export default function __sfdc_hostproductGroup(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_hostproductGroup" {
  export default function __sfdc_hostproductGroup(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_leadObj" {
  export default function __sfdc_leadObj(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.__sfdc_leadObj" {
  export default function __sfdc_leadObj(): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.InitData" {
  export default function InitData(param: {objleadid: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.getStdConvertOpptyId" {
  export default function getStdConvertOpptyId(param: {leadId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_ConvertLeadController.convertLead" {
  export default function convertLead(param: {objleadid: any, submit_date: any, complete_date: any}): Promise<any>;
}
