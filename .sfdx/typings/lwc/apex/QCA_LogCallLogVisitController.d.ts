declare module "@salesforce/apex/QCA_LogCallLogVisitController.getMyTaskLastedInforamtion" {
  export default function getMyTaskLastedInforamtion(param: {leadObjId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.getMyVisitPlanReportLastedInforamtion" {
  export default function getMyVisitPlanReportLastedInforamtion(param: {leadObjId: any, flowType: any, opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.getMyVisitReportOpportunityLasted" {
  export default function getMyVisitReportOpportunityLasted(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.UpdateVisitPlanReport" {
  export default function UpdateVisitPlanReport(param: {callReportObj: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.UpdateVisitPlanReportOppty" {
  export default function UpdateVisitPlanReportOppty(param: {callReportObj: any, opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.UpdateTaskRecord" {
  export default function UpdateTaskRecord(param: {taskObj: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.getTaskValues" {
  export default function getTaskValues(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LogCallLogVisitController.getVisitPlanReportDependencyFieldValues" {
  export default function getVisitPlanReportDependencyFieldValues(param: {FieldAPIName1st: any, FieldAPIName2nd: any}): Promise<any>;
}
