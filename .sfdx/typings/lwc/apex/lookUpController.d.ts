declare module "@salesforce/apex/lookUpController.fetchAccount" {
  export default function fetchAccount(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/lookUpController.getOwnerName" {
  export default function getOwnerName(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/lookUpController.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
declare module "@salesforce/apex/lookUpController.fetchAccountOwner" {
  export default function fetchAccountOwner(param: {accountId: any}): Promise<any>;
}
