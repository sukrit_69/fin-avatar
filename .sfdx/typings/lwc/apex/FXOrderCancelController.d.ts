declare module "@salesforce/apex/FXOrderCancelController.getUserData" {
  export default function getUserData(): Promise<any>;
}
declare module "@salesforce/apex/FXOrderCancelController.cancelFromQuickAction" {
  export default function cancelFromQuickAction(param: {fxo: any}): Promise<any>;
}
declare module "@salesforce/apex/FXOrderCancelController.getFXO" {
  export default function getFXO(param: {fxoId: any}): Promise<any>;
}
