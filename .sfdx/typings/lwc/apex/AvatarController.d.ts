declare module "@salesforce/apex/AvatarController.getAvatar" {
  export default function getAvatar(param: {thisAvatar: any}): Promise<any>;
}
