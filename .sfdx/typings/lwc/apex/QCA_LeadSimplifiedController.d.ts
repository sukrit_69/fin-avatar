declare module "@salesforce/apex/QCA_LeadSimplifiedController.__sfdc_LeadRecordObj" {
  export default function __sfdc_LeadRecordObj(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.__sfdc_LeadRecordObj" {
  export default function __sfdc_LeadRecordObj(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.__sfdc_AccountRecordObj" {
  export default function __sfdc_AccountRecordObj(param: {value: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.__sfdc_AccountRecordObj" {
  export default function __sfdc_AccountRecordObj(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.getRecordTypeSimplifiedLead" {
  export default function getRecordTypeSimplifiedLead(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.getDeepLink" {
  export default function getDeepLink(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.getMyLeadInforamtion" {
  export default function getMyLeadInforamtion(param: {leadObjId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.getMyOpportunityId" {
  export default function getMyOpportunityId(param: {leadObjId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.getUserCurrent" {
  export default function getUserCurrent(): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.updateLeadDetail" {
  export default function updateLeadDetail(param: {leadObj: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.validateDuplicateLeadAndAccount" {
  export default function validateDuplicateLeadAndAccount(param: {leadObj: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_LeadSimplifiedController.updateTrackStatus" {
  export default function updateTrackStatus(param: {leadId: any, stage: any}): Promise<any>;
}
