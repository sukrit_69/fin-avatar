declare module "@salesforce/apex/SmartBDMPreviewQuickCAController.getProductionList" {
  export default function getProductionList(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/SmartBDMPreviewQuickCAController.getOppty" {
  export default function getOppty(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/SmartBDMPreviewQuickCAController.getHost" {
  export default function getHost(): Promise<any>;
}
declare module "@salesforce/apex/SmartBDMPreviewQuickCAController.submitToHost" {
  export default function submitToHost(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/SmartBDMPreviewQuickCAController.getDeepLink" {
  export default function getDeepLink(): Promise<any>;
}
