declare module "@salesforce/apex/FXOrderSubmitToTraderController.submitFromQuickAction" {
  export default function submitFromQuickAction(param: {fxo: any}): Promise<any>;
}
declare module "@salesforce/apex/FXOrderSubmitToTraderController.getFXO" {
  export default function getFXO(param: {fxoId: any}): Promise<any>;
}
