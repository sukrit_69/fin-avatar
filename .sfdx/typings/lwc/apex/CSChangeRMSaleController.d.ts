declare module "@salesforce/apex/CSChangeRMSaleController.getCurrentEclient" {
  export default function getCurrentEclient(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSChangeRMSaleController.confirmEclientListView" {
  export default function confirmEclientListView(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSChangeRMSaleController.insertSurveySheet" {
  export default function insertSurveySheet(param: {sm: any, eCustomerId: any, ownerNameId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSChangeRMSaleController.updateCurrentEclient" {
  export default function updateCurrentEclient(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSChangeRMSaleController.getCurrentUser" {
  export default function getCurrentUser(): Promise<any>;
}
declare module "@salesforce/apex/CSChangeRMSaleController.setManualShareRead" {
  export default function setManualShareRead(param: {eClient: any}): Promise<any>;
}
