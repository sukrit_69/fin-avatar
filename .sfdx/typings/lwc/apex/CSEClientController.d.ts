declare module "@salesforce/apex/CSEClientController.getCurrentEclient" {
  export default function getCurrentEclient(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.updateCurrentEclientToBasic" {
  export default function updateCurrentEclientToBasic(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.updateEclient" {
  export default function updateEclient(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.saveEClientRecordId" {
  export default function saveEClientRecordId(param: {recordIdEClient: any, surveySheetId: any, formType: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.getEClientSuit" {
  export default function getEClientSuit(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.getSurveySheet" {
  export default function getSurveySheet(): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.confirmEclient" {
  export default function confirmEclient(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.insertSurveySheet" {
  export default function insertSurveySheet(param: {sm: any, eCustomerId: any, ownerNameId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientController.setManualShareRead" {
  export default function setManualShareRead(param: {recId: any, userId: any}): Promise<any>;
}
