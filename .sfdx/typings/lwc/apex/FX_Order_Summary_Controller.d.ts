declare module "@salesforce/apex/FX_Order_Summary_Controller.getItemPerPage" {
  export default function getItemPerPage(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getFXSItemAmount" {
  export default function getFXSItemAmount(param: {searchKey: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getFXSOrder" {
  export default function getFXSOrder(param: {searchKey: any, Buypage: any, Sellpage: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getbuyOrder" {
  export default function getbuyOrder(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getSellOrder" {
  export default function getSellOrder(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getSubmittedOrderByFXS" {
  export default function getSubmittedOrderByFXS(param: {FXSId: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getFXO" {
  export default function getFXO(param: {fxoId: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getFXS" {
  export default function getFXS(param: {FXSId: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.adjustOrderSummary" {
  export default function adjustOrderSummary(param: {newFXS: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.updateOrderSummary" {
  export default function updateOrderSummary(param: {newFXS: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getUserData" {
  export default function getUserData(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.spreadOrder" {
  export default function spreadOrder(param: {orderList: any, newfxs: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getBlotterProfile" {
  export default function getBlotterProfile(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.getSessionId" {
  export default function getSessionId(): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.stampCancelRemain" {
  export default function stampCancelRemain(param: {FXOrderItem: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.cancelOrderSummaryFromPartialAndFull" {
  export default function cancelOrderSummaryFromPartialAndFull(param: {newFXS: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.validateAndUpdateFXS" {
  export default function validateAndUpdateFXS(param: {newFXS: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.createFXOrder" {
  export default function createFXOrder(param: {FXOrderItem: any}): Promise<any>;
}
declare module "@salesforce/apex/FX_Order_Summary_Controller.validateFXSTotalRequestAmount" {
  export default function validateFXSTotalRequestAmount(param: {newFXS: any}): Promise<any>;
}
