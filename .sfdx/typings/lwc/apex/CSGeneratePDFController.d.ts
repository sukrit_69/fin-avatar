declare module "@salesforce/apex/CSGeneratePDFController.getEclient" {
  export default function getEclient(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSGeneratePDFController.generatePDFfile" {
  export default function generatePDFfile(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSGeneratePDFController.createPDFfile" {
  export default function createPDFfile(param: {eclientId: any}): Promise<any>;
}
