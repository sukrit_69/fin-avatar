declare module "@salesforce/apex/QCA_OpportunitySimplifiedController.getDeepLink" {
  export default function getDeepLink(): Promise<any>;
}
declare module "@salesforce/apex/QCA_OpportunitySimplifiedController.getRecordTypeSimplifiedOpportunity" {
  export default function getRecordTypeSimplifiedOpportunity(): Promise<any>;
}
declare module "@salesforce/apex/QCA_OpportunitySimplifiedController.updateTrackStatus" {
  export default function updateTrackStatus(param: {opptyObjId: any, stage: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_OpportunitySimplifiedController.getOpportuniyObjById" {
  export default function getOpportuniyObjById(param: {opptyId: any}): Promise<any>;
}
