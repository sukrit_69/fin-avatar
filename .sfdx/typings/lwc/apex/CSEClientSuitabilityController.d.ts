declare module "@salesforce/apex/CSEClientSuitabilityController.saveEClientRecordId" {
  export default function saveEClientRecordId(param: {recordIdEClient: any, surveySheetId: any, formType: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getFormList" {
  export default function getFormList(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.SurveyEClient" {
  export default function SurveyEClient(param: {recordId: any, formType: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getSurveyAnswer" {
  export default function getSurveyAnswer(param: {surveySheetId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getUserName" {
  export default function getUserName(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getSurveySheet" {
  export default function getSurveySheet(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getUserRecordAccess" {
  export default function getUserRecordAccess(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.updateSurveyStatus" {
  export default function updateSurveyStatus(param: {idSurveySheet: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.updateEClientCSLevel" {
  export default function updateEClientCSLevel(param: {eClientRecoredId: any, surveySheetId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSEClientSuitabilityController.getEClientId" {
  export default function getEClientId(param: {surveySheetId: any}): Promise<any>;
}
