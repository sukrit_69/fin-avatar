declare module "@salesforce/apex/CSUploadPDFToECMController.getEclient" {
  export default function getEclient(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSUploadPDFToECMController.getOnlineLog" {
  export default function getOnlineLog(param: {eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSUploadPDFToECMController.getApexJob" {
  export default function getApexJob(): Promise<any>;
}
declare module "@salesforce/apex/CSUploadPDFToECMController.saveFile" {
  export default function saveFile(param: {fileName: any, base64Data: any, contentType: any, eclientId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSUploadPDFToECMController.ecmTimeOut" {
  export default function ecmTimeOut(): Promise<any>;
}
declare module "@salesforce/apex/CSUploadPDFToECMController.getCurrentUser" {
  export default function getCurrentUser(): Promise<any>;
}
