declare module "@salesforce/apex/QCA_SLSProductController.onInitQCASLSProduct" {
  export default function onInitQCASLSProduct(param: {relatedRecordId: any, flowType: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_SLSProductController.getHostProductMappingFieldValues" {
  export default function getHostProductMappingFieldValues(): Promise<any>;
}
declare module "@salesforce/apex/QCA_SLSProductController.getMyRecommendedProduct" {
  export default function getMyRecommendedProduct(param: {leadObjId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_SLSProductController.getMyOpportunityLineItem" {
  export default function getMyOpportunityLineItem(param: {opptyId: any}): Promise<any>;
}
declare module "@salesforce/apex/QCA_SLSProductController.saveSLSProduct" {
  export default function saveSLSProduct(param: {SLSProductList: any, relatedRecordId: any, isNewLead: any}): Promise<any>;
}
