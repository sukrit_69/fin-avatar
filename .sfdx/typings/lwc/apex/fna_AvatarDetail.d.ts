declare module "@salesforce/apex/fna_AvatarDetail.getAvatarMasterDetail" {
  export default function getAvatarMasterDetail(param: {idForm: any}): Promise<any>;
}
