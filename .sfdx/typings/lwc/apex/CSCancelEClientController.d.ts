declare module "@salesforce/apex/CSCancelEClientController.getEclient" {
  export default function getEclient(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSCancelEClientController.updateCurrentEclient" {
  export default function updateCurrentEclient(param: {eClient: any}): Promise<any>;
}
declare module "@salesforce/apex/CSCancelEClientController.updateSurveyStatus" {
  export default function updateSurveyStatus(param: {idSurveySheet: any}): Promise<any>;
}
