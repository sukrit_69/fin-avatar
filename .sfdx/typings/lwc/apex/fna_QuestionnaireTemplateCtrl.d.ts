declare module "@salesforce/apex/fna_QuestionnaireTemplateCtrl.getQuestion" {
  export default function getQuestion(param: {thisQuestion: any, thisForm: any, preQuestion: any}): Promise<any>;
}
declare module "@salesforce/apex/fna_QuestionnaireTemplateCtrl.saveQuestionInformation" {
  export default function saveQuestionInformation(param: {thisQuestion: any, answerQuestion: any}): Promise<any>;
}
declare module "@salesforce/apex/fna_QuestionnaireTemplateCtrl.previousQuestionInformation" {
  export default function previousQuestionInformation(param: {idForm: any, idQuestion: any}): Promise<any>;
}
