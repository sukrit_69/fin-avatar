declare module "@salesforce/apex/CSRevertFormAController.getEclient" {
  export default function getEclient(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CSRevertFormAController.revertFormA" {
  export default function revertFormA(param: {ecId: any, comment: any}): Promise<any>;
}
