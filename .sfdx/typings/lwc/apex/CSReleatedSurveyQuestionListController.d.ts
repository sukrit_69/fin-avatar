declare module "@salesforce/apex/CSReleatedSurveyQuestionListController.getQuestionList" {
  export default function getQuestionList(param: {recordId: any}): Promise<any>;
}
