declare module "@salesforce/apex/SmartBDM_Utility.updateLeadStatus" {
  export default function updateLeadStatus(param: {leadId: any, stage: any}): Promise<any>;
}
declare module "@salesforce/apex/SmartBDM_Utility.updateOpptyStatus" {
  export default function updateOpptyStatus(param: {opptyId: any, stage: any}): Promise<any>;
}
