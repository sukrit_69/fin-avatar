declare module "@salesforce/apex/SurveyManagerController.getSurvey" {
  export default function getSurvey(param: {SurveyName: any, SurveyVersionId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.saveDraftSurveySheet" {
  export default function saveDraftSurveySheet(param: {surveyAwnser: any, survey: any, surveySheetId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.saveSurveySheet" {
  export default function saveSurveySheet(param: {surveyAwnser: any, survey: any, surveySheetId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.getSurveySheet" {
  export default function getSurveySheet(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.getSurveyAnswer" {
  export default function getSurveyAnswer(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.SurveySheetAnswer" {
  export default function SurveySheetAnswer(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.getEClientSuit" {
  export default function getEClientSuit(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.SurveyEClient" {
  export default function SurveyEClient(param: {recordId: any, formType: any}): Promise<any>;
}
declare module "@salesforce/apex/SurveyManagerController.getEClientId" {
  export default function getEClientId(param: {surveySheetId: any}): Promise<any>;
}
