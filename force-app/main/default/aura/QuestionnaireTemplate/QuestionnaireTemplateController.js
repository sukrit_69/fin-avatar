({
	doInit : function(component, event, helper) {
		// get value   
        /*var defaultChoice = ['ตัวเลือกที่1','ตัวเลือกที่2','ตัวเลือกที่3','ตัวเลือกที่4','ตัวเลือกที่5'];
        var twoChoice = ['ตัวเลือกที่1','ตัวเลือกที่2'];
        component.set("v.question", 'คำถามอะไรเอ๋ย');
        component.set("v.choice", defaultChoice);
        component.set("v.twoChoice", twoChoice);*/
        
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
		//console.log('555555555555555555555555 ' + sPageURL);
        var mapParameter = helper.getparameter(sPageURL);
        //console.log('mapParameter ' , mapParameter);
        var act = component.get("c.getQuestion");
        act.setParams({
            thisQuestion: mapParameter.get('questionId'),
        });
        act.setCallback(this, function(response){
            var state = response.getState();
            console.log('state : ' + state);
            if (state === "SUCCESS") {
                var rtnValue = response.getReturnValue();
                console.log('response' , rtnValue);
                console.log('response' , rtnValue.question);
                console.log('response' , rtnValue.template);
                console.log('response next' , rtnValue.nextQuestion);
                var choiceList = [];
                for (var key in rtnValue.nextQuestion) {
                	choiceList.push(key);
                }
                
                if(rtnValue.haveValue){
                    component.set("v.questionTemplate", rtnValue);
                    component.set("v.choice", choiceList);
                }else{
                    /*console.log('response' , rtnValue.nextPage);
                    var url = "/";
        			helper.gotoURL(url);*/
                }
            }
        });       
        $A.enqueueAction(act);

	},
    
    changeAge : function(component, event, helper) {
        console.log('TESTTT' + event.getSource().get("v.value"));
        component.set("v.questionObj", event.getSource().get("v.value"));
	},
    
    backPage : function(component, event, helper){
        
        var url = "/";
        helper.gotoURL(url);
    },
    
    nextPage : function(component, event, helper){
        var url = "/";
        console.log('TESTTT' + event.getSource().get("v.value"));
        var answer = event.getSource().get("v.value");
        
        var act = component.get("c.saveQuestionInformation");
        act.setParams({
            thisQuestion: component.get("v.questionTemplate"),
            answerQuestion : answer
        });
        act.setCallback(this, function(response){
            var state = response.getState();
            console.log('state : ' + state);
            if (state === "SUCCESS") {
                var rtnValue = response.getReturnValue();
                console.log('response' , rtnValue);
                console.log('response' , rtnValue.success);
                console.log('response' , rtnValue.nextPage);
                if(rtnValue.success){
                    console.log('response' , rtnValue.nextPage);
                    if(rtnValue.nextPage != "HOME"){
                        url += 'questionnaire?questionId=' + rtnValue.nextPage;
                    }
                    helper.gotoURL(url);
                }
            }
        });       
        $A.enqueueAction(act);
    }
})