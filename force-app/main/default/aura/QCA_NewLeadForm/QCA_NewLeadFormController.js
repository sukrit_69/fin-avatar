({
    onLoad: function (component, event, helper) {
        // console.log('Run on Loading Lead form');
        console.log('Lead Id :: ', component.get('v.leadObjId'));
        var leadObj = component.get('v.leadObj');

        // if (component.find('varPrimary_Campaign')) component.find('varPrimary_Campaign').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varParent_Company')) component.find('varParent_Company').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varGroup')) component.find('varGroup').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varIndustry')) component.find('varIndustry').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varPreferred_Branch')) component.find('varPreferred_Branch').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varBranch_Referred')) component.find('varBranch_Referred').set('v.value', leadObj.Primary_Campaign__c);
        // if (component.find('varRTL_Referral')) component.find('varRTL_Referral').set('v.value', leadObj.Primary_Campaign__c);

        component.set('v.showOnloading', true);
        component.set('v.isDisabledFeild', component.get('v.leadRecordTypeId') != component.get('v.leadObj.RecordTypeId') && component.get('v.leadObj.RecordTypeId') ? true : false);
        component.set('v.isEditedFieldPrimaryCampaign', leadObj.Primary_Campaign__c ? true || component.get('v.isDisabledFeild') : false);
        component.set('v.isEditedFieldContact', (leadObj.Mobile_No__c ? true : false || leadObj.Office_No__c ? true : false) && component.get('v.isDisabledFeild'));


        // var recordUI = event.getParam("recordUi");
        // console.log(helper.parseObj(recordUI));
        // console.log(component.get('v.leadRecordTypeId'));
        // helper.stopSpinner(component);
    },
    onSubmitValidateCustomeField: function (component, event, helper) {
        var leadObj = helper.mapFields(component.find('LeadFormInput'));
        helper.validateField(component, helper, leadObj);
    },
    onSubmit: function (component, event, helper) {
        // console.log('onSubmit :: running!!');
        event.preventDefault();
        helper.displayErrorMeassge(component, event, false);
        var leadObjId = component.get('v.leadObjId');
        var eventFields = helper.setFieldLeadForm(component, event, helper);

        var leadObj = eventFields;
        leadObj.Id = leadObjId;
        // Case isDisabledFeild be true is Recordtype is Commercial account
        leadObj.RecordTypeId = component.get('v.isDisabledFeild') ? component.get('v.leadObj.RecordTypeId') : component.get('v.leadRecordTypeId');

        component.set('v.isValidInputField', helper.validateField(component, helper, leadObj));
        if (component.get('v.isValidInputField')) {
            helper.startSpinner(component);
            var action = component.get("c.validateDuplicateLeadAndAccount");
            action.setParams({
                "leadObj": leadObj
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var leadUpdateRecordWrapper = response.getReturnValue();
                    // console.log("From server: ", leadUpdateRecordWrapper);

                    // if (leadUpdateRecordWrapper.isOwner) {
                    eventFields = leadUpdateRecordWrapper.leadObj;
                    component.set('v.leadObj', helper.parseObj(eventFields));
                    delete eventFields['RecordTypeId'];
                    // }
                    component.set('v.leadObj.Name', component.get('v.leadObj.FirstName') + ' ' + component.get('v.leadObj.LastName'));
                    component.find('LeadForm').submit(eventFields);
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                errors[0].message);
                            helper.displayToast(component, 'Error', errors[0].message);
                            // helper.displayErrorMeassge(component, errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                    helper.stopSpinner(component);
                }
            });
            $A.enqueueAction(action);
        }
    },
    onSuccess: function (component, event, helper) {
        // console.log('new lead form response :: ', helper.parseObj(event.getParam("response")));
        helper.updateLeadDetail(component, event, helper);

        var leadObj = event.getParam("response");
        if (leadObj.fields.Status.value == "Unqualified") {
            helper.displayToast(component, 'Error', $A.get("$Label.c.Lead_is_disqualified_error_message"));
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": '/lightning/n/My_Lead_Quick_CA',
                // "url": '/one/one.app#/alohaRedirect/apex/SmartBDM_MyLead_QuickCA',
                "isredirect": true
            });
            urlEvent.fire();
        } else {
            helper.displayToast(component, 'Success', 'Record updated successfully!');
            var compEvent = component.getEvent("varSimplifiedLeadProcessStatus");
            compEvent.setParams({
                "leadObjId": leadObj.id,
                "simplifiedLeadProcessStage": 1,
                "isAllowSimplifiedLeadProcessStage": true
            });
            compEvent.fire();
        }
        helper.stopSpinner(component);
    },
    onError: function (component, event, helper) {
        // console.log('onError :: running!!');
        helper.stopSpinner(component);
        helper.displayErrorMeassge(component, event, true);
    },
    onChanngRemoveInvalidInputField: function (component, event, helper) {
        if (component.get('v.showOnloading'))
            helper.removeInvalidInputField(component, event, helper);
    }
})