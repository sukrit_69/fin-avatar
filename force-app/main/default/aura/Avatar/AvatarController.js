({
	doinit : function(component, event, helper) {
		  
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        
        var mapParameter = helper.getparameter(sPageURL);
        
        var act = component.get("c.getAvatarEN");
        act.setParams({
            thisAvataEN: mapParameter.get('Avatar EN'),
       });
         act.setCallback(this, function(response){
            var state = response.getState();
            console.log('state : ' + state);
            if (state === "SUCCESS") {
                var rtnValue = response.getReturnValue();
                console.log('response' , rtnValue);
                console.log('response' , rtnValue.question);
                console.log('response' , rtnValue.template);
                console.log('response next' , rtnValue.nextQuestion);
                var choiceList = [];
                for (var key in rtnValue.nextQuestion) {
                	choiceList.push(key);
                }
                
                if(rtnValue.haveValue){
                    component.set("v.questionTemplate", rtnValue);
                    component.set("v.choice", choiceList);
                }else{
                    /*console.log('response' , rtnValue.nextPage);
                    var url = "/";
        			helper.gotoURL(url);*/
                }
            }
        });       
        $A.enqueueAction(act);

	}
    
})