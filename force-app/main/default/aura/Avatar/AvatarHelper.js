({
	gotoURL : function(urlPath) {
		console.log('urlPath' + urlPath);
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": urlPath
        });
        
        urlEvent.fire();
	},
	 getparameter : function (para){
        
        var sURLVariables = para.split('&');
        var myMap = new Map();

        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('='); //to split the key from the value.
			myMap.set(sParameterName[0], sParameterName[1]);
        }
        console.log('my map ' , myMap);
        return myMap;
    }
})