/* eslint-disable no-undef */
/* eslint-disable guard-for-in */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
import { LightningElement, track, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
//import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import fna_SharpCalendar from '@salesforce/resourceUrl/fna_SharpCalendar';
import getquestion from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.getQuestion'; 
import savequestion from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.saveQuestionInformation'; 
import previousQuestion from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.previousQuestionInformation';

export default class Fna_Questionnaire extends LightningElement {
    buttonClicked;
    @track cssClass = 'slds-button slds-button_neutral fullbutton';
    //@track ageValueRetrun;
    @track temp = '';
    @track choice = [];
    @track template = [];
    /// For multiple choice
    @track answer = '';
    @track loadingDisplay = 'hideLoading';  

    // get cssClass(){
    //     return this.buttonClicked ? 'slds-button slds-button_neutral fullbutton active' : 'slds-button slds-button_neutral fullbutton';
    // }

    //scrollInitialized = false;
    /*@track yearValue;
    @track ageValue;

    InitializeScroll() {
        
        console.log("include static resource complete");
        var generatePicker = this.template.querySelector('.js-inline-picker');
        var pickerArgument = this.template.querySelector('.year');
        var picker = new Picker(pickerArgument, {
            container: generatePicker,
            controls: true,
            inline: true,
            format: 'YYYY',
            rows: 9,
        });
        console.log("This Year = "+picker.getDate(true));       
        this.yearValue = picker.getDate(true);

        
    }

    CalculateAge(event) {
        this.yearChange = event.target.value;
        console.log("Calculate Age");
        var d = new Date();
        console.log("This Year Picker = "+this.yearChange); 
        console.log("This Year = "+d.getFullYear()); 
        var calAge = d.getFullYear() - this.yearChange;
        if(calAge >=0){
            this.ageValue = calAge;
        }else{
            this.ageValue = 0;
        }
        
        console.log("Age = "+this.value);

    }*/

    constructor() {
        //// syntact js
        
        super();
        const sPageURL = decodeURIComponent(window.location.search.substring(1));
        const parameter = this.getparameter(sPageURL);
        // console.log('has parameter ' + parameter.has('qId'));
        if(parameter.has('qId')){
            this.getQuestionTemplate(parameter.get('qId'), parameter.get('Id'));
        }else{
            this.getQuestionTemplate('');
        }
    }

    handleEventChangeYear(event) {
        //event.preventDefault();
        this.answer = event.detail;
        console.log("Main Year : " + this.answer);
    }
    /*renderedCallback() {
        if (this.scrollInitialized) {
            return;
        }
        this.scrollInitialized = true;
    
        Promise.all([
            loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.js'),
            //loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/jQuery.js'),
            loadStyle(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.css'),
        ])
            .then(() => {
                this.InitializeScroll();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading FNA_SharpCalendar',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }*/

    getparameter(param){
        var myMap = new Map();
        if(param !== ''){
            var sURLVariables = param.split('&');
        
            var i;
            var sParameterName;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                myMap.set(sParameterName[0], sParameterName[1]);
            }
        }
        return myMap;
    }

    getQuestionTemplate(quesId, formId){
        console.log('quesId' + quesId);
        getquestion({thisQuestion: quesId, thisForm: formId })
        .then(result => {
            this.temp = result;
            this.template = result.template;
            this.choice = result.choiceObj;
            // var conts = result.choice;
            /// creating the array format to show on UI.
            // for(var key in conts){
            //     this.choice.push({key:key, value:conts[key]});
            // }
            console.log('then');
            console.log(this.temp);
            console.log('this.choice ' , this.choice);
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }

    nextPage(event){
        this.loadingDisplay = 'showLoading';
        console.log('Event ' + event.target.value);
        var ansQuestion = event.target.value;
        if(ansQuestion === 'multipleChoice' || ansQuestion === 'singleValue'){
            console.log('This answer ' + this.answer);
            ansQuestion = this.answer;
        }
        
        savequestion({thisQuestion: this.temp, answerQuestion: ansQuestion})
        .then(result => {
            if(result.success){
                
                var url = "/fin/s/";
                console.log('response' , result.nextPage);
                if(result.nextPage !== "" && result.nextPage !== undefined){
                    url += 'questionnairejj?qId=' + result.nextPage + '&Id=' + result.idForm;
                    //this.getQuestionTemplate(result.nextPage, result.idForm);
                }else{
                    url += 'avatardetail?Id=' + result.idForm;
                }

                
                this.gotoURL(url);
            }
            console.log('then');
            console.log(result);
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }

    backPage(){
        this.loadingDisplay = 'showLoading';
        previousQuestion({idForm : this.temp.idForm,idQuestion : this.temp.questionId})
        .then(result => {
            console.log('then');
            console.log(result);
            var url = "/fin/s/questionnairejj?qId=" + result + '&Id=' + this.temp.idForm;
            this.gotoURL(url);
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        })
    }

    gotoURL(urlpage){
         // Navigate to a URL
        // this[NavigationMixin.Navigate]({
        //         attributes: {
        //             url: 'http://salesforce.com'
        //         }
        //     },
        //     true // Replaces the current page in your browser history with the URL
        // );
        // // window.location.href(urlpage);
        window.location.assign(urlpage);
    }

    addAnswer(event){
        console.log('Event' + event.target.value);
        console.log('Before Answer ' + this.answer);
        var checkValue = event.target.value;
        if(this.answer.search(checkValue) !== -1){
            console.log('-1');
            this.answer = this.answer.replace(checkValue + ';', '');
        }else{
            this.answer += event.target.value + ';';
        }
        console.log('After Answer ' + this.answer);
        
        // const evt = event.target.dataset.item;
        const evt = event.currentTarget;
        // console.log(event.target.dataset.item)
        // this.buttonClicked = !this.buttonClicked; //set to true if false, false if true.
        if(evt.classList.contains('active') === true){
            evt.classList.remove('active');
        }
        else{
            evt.classList.add('active');
        }
        // evt.classList.remove('active');
        // this.cssClass = this.buttonClicked ? 'slds-button slds-button_neutral fullbutton active' : 'slds-button slds-button_neutral fullbutton';
        // console.log(this.buttonClicked)
    }
    
    changeHandler(event){
        this.answer = event.target.value;

    }
    /*InitializeScroll() {
        
        console.log("include static resource complete");
        var generatePicker = this.template.querySelector('.js-inline-picker');
        var pickerArgument = this.template.querySelector('.year');
        var picker = new Picker(pickerArgument, {
            container: generatePicker,
            controls: true,
            inline: true,
            format: 'YYYY',
            rows: 9,
        });
        console.log("This Year = "+picker.getDate(true));       
        this.yearValue = picker.getDate(true);

        
    }

    CalculateAge(event) {
        this.yearChange = event.target.value;
        console.log("Calculate Age");
        var d = new Date();
        console.log("This Year Picker = "+this.yearChange); 
        console.log("This Year = "+d.getFullYear()); 
        var calAge = d.getFullYear() - this.yearChange;
        if(calAge >=0){
            this.ageValue = calAge;
        }else{
            this.ageValue = 0;
        }
        
        console.log("Age = "+this.value);

    }*/
    
    // questionId = 'a3U0l000000qlukEAA';
    // @wire(getquestion, { thisQuestion: '$questionId' }) questionTemplate({data}){
    //     if(data){
    //         // this.accounts = JSON.parse(JSON.stringify(data));
    //         // this.temp = JSON.parse(JSON.stringify(data));
    //         //this.temp =  data;
           
            

    //         //console.log('testst ++++' , JSON.parse(JSON.stringify(data)).question);
    //         // console.log('recordId ' + this.recordId);
            
    //     }
    // }

}