import { LightningElement, track } from 'lwc';
import getAvatarDetail from '@salesforce/apex/QuestionnaireTemplateCtrl.getQuestion';

export default class Fna_avatarTest extends LightningElement {

    @track temp = '';


    constructor(){
        super();
        this.getAvatarDetail();
        console.log('123456' , '123456');
    }

    getAvatarDetail(){

        getAvatarDetail({thisQuestion: 'a3U0l000000qluhEAA'})
        .then(result => {
            this.temp = result;

            console.log('then');
            console.log(this.temp);
        })
        .catch(error => {
            console.log('catch');
            console.log(error);
        });
    }


}