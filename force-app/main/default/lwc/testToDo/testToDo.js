/* eslint-disable no-console */
import { LightningElement, track } from 'lwc';
import saveQuestionInformation from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.saveQuestionInformation';
import previousQuestionInformation from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.previousQuestionInformation';
import getQuestion from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.getQuestion';
export default class TestToDo extends LightningElement {

    @track question = {};
    @track answer = 'พนักงานเอกชน';

    @track responAns;

    @track valueTest = null;
    constructor() {
        super();
        this.ready();
    }

    // clickNext(){
    //     saveQuestionInformation({ thisQuestion: this.question, answerQuestion: this.answer })
    //     .then(result => {
    //         this.responAns = result
    //         console.log(result)
    //     })
    //     .catch(result => {
    //         console.log(result)
    //     })
    // }

    clickBack(){
        previousQuestionInformation({ thisQuestion: this.question.questionId })
        .then(result => {
            console.log(result)
        })
        .catch(result => {
            console.log(result)
        })
    }

    handleChange(event){
        this.property = event.target.value
        console.log(this.property)
        this.valueTest = this.property
    }

    clickSubmit(){
        saveQuestionInformation({ thisQuestion: this.question, answerQuestion: this.valueTest })
        .then(result => {
            console.log('then')            
            // if(this.question === undefined){
                this.responAns = result
                // this.question.idForm = result.idForm
                // console.log(this.responAns.idForm)
                // console.log(this.question.idForm)
                // var x = this.responAns.idForm
                // this.question.idForm.put(x)
                // console.log(this.question)
                // this.question.idForm = result.idForm
                // console.log('this.responAns (if) : ' + this.responAns)
            // }
            // else {
            //     this.responAns = result
            //     this.question.idForm = result.idForm
            //     console.log('this.responAns (else) : ' + this.responAns)
            //     console.log('this.question.idForm : '+this.question.idForm)
            // // }
            // console.log('this.responAns.idForm (show) : ' + this.responAns.idForm)
            // console.log('this.question.idForm (show) : '+this.question.idForm)
            // this.question.questionId = this.responAns.idForm
            // console.log(result)
        })
        .catch(result => {
            console.log('catch')
            console.log(result)
        })
    }

    ready(){
        getQuestion({thisQuestion: 'a3U0l000000qlunEAA' }) //fix คำถามแรก
        .then(result => {
            this.question = result
            console.log('then')
            console.log(this.question)
        })
        .catch(error => {
            console.log('catch')
            console.log(error)
        });
    }

}