/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
import { LightningElement, track, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import fna_SharpCalendar from '@salesforce/resourceUrl/fna_SharpCalendar';

export default class fna_ScrollList extends LightningElement {

    @api getAge;
    scrollInitialized = false;
    @track yearValue;
    @track ageValue;

    @api ageProperty;

    /*constructor() {
        super()
        this.setvalue()
    }

    setvalue(event){
        if(this.yearValue != null){
            // this.yearValue = '1997'
            console.log(event)
            this.yearValue = event.value
            console.log('ageProperty='+this.ageProperty);
            this.yearValue = ageProperty;
        }
    }*/
    
    renderedCallback() {
        if (this.scrollInitialized) {
            return;
        }
        this.scrollInitialized = true;
    
        Promise.all([
            loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.js'),
            //loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/jQuery.js'),
            loadStyle(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.css'),
        ])
            .then(() => {
                this.InitializeScroll();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading FNA_SharpCalendar',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }
    InitializeScroll() {
        
        
        //console.log("include static resource complete");
        var generatePicker = this.template.querySelector('.js-inline-picker');
        var pickerArgument = this.template.querySelector('.year');
        console.log("ageProperty="+this.ageProperty);  
        var d = new Date();
        var calAge = d.getFullYear();
        //console.log("calAge="+calAge); 
        if(this.ageProperty>0){
            calAge = d.getFullYear() - this.ageProperty;
        }
        console.log("calAge="+calAge); 
        var picker = new Picker(pickerArgument, {
            container: generatePicker,
            controls: true,
            inline: true,
            format: 'YYYY',
            rows: 5,
            date: new Date(calAge, 1, 1, 0, 0),
        });   
        //console.log("This Year = "+picker.getDate(true));           
        //this.yearValue = picker.setDate(calAge);
        //console.log("yearAge="+yearAge);  

    }
    
    CalculateAge(event) {
        this.yearChange = event.target.value;
        console.log('this.yearChange: '+ this.yearChange );
        var d = new Date();
        //console.log("This Year Picker = "+this.yearChange); 
        //console.log("This Year = "+d.getFullYear()); 
        var calAge = d.getFullYear() - this.yearChange;
        if(calAge >=0){
            this.ageValue = calAge;
        }else{
            this.ageValue = 0;
        }
        this.getAge = this.ageValue;
        console.log("Age = "+this.ageValue);
        //event.preventDefault();
        /*const returnValueEvent = new CustomEvent('valuechanged',{
            detail: {
                ageValue: this.ageValue,
            }
        });*/
        this.dispatchEvent(new CustomEvent( 'valuechanged', { detail: this.ageValue } ) );
        //this.ageValueRetrun = this.ageValue;
        
    }


    //const sentAgeToScroll = this.template.querySelector('c-fna-scroll-list').getYearFromScroll('50'); 
    
}