import { LightningElement, track } from 'lwc';
// import apexTestClass from '@salesforce/apex/QuestionnaireTemplateCtrl.testGET';
import getQuestion from '@salesforce/apex/fna_QuestionnaireTemplateCtrl.getQuestion';

export default class TestPageA extends LightningElement {

    
    // @wire(apexTestClass)apexTest;
    @track temp2 = '';
    @track temp = '';
    @track myValue = '';

    handleSubmit() {
        // console.log('Current value of the input: ' + evt.target.value);
        getQuestion({thisQuestion: this.myValue })
        .then(result => {
            this.temp = result;
            
            console.log('then')
            console.log(this.temp)
        })
        .catch(error => {
            console.log('catch')
            console.log(error)
        });
    }

    //document ready
    constructor() {
        super();
        this.ready();
    }

    ready(){
        getQuestion({thisQuestion: 'a3U0l000000qlukEAA' })
        .then(result => {
            this.temp2 = result;
            console.log('then')
            console.log(this.temp)
        })
        .catch(error => {
            console.log('catch')
            console.log(error)
        });
    }
    
    
}