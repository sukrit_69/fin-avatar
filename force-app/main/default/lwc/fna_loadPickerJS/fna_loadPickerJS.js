/* eslint-disable guard-for-in */
/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
/*import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import fna_SharpCalendar from '@salesforce/resourceUrl/FNA_SharpCalendar';

export default class fna_ScrollList extends LightningElement {
    
    scrollInitialized = false;
    @track yearValue;
    @track ageValue;
    renderedCallback() {
        if (this.scrollInitialized) {
            return;
        }
        this.scrollInitialized = true;

        Promise.all([
            loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.js'),
            loadScript(this, fna_SharpCalendar + '/pickerjs-master/dist/jQuery.js'),
            loadStyle(this, fna_SharpCalendar + '/pickerjs-master/dist/picker.css'),
        ])
            .then(() => {
                this.InitializeScroll();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading FNA_SharpCalendar',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }
    InitializeScroll() {
        
        console.log("include static resource complete");
        var generatePicker = this.template.querySelector('.js-inline-picker');
        var pickerArgument = this.template.querySelector('.year');
        var picker = new Picker(pickerArgument, {
            container: generatePicker,
            controls: true,
            inline: true,
            format: 'YYYY',
            rows: 9,
        });
        console.log("This Year = "+picker.getDate(true));       
        this.yearValue = picker.getDate(true);

        
    }

    CalculateAge(event) {
        this.yearChange = event.target.value;
        console.log("Calculate Age");
        var d = new Date();
        console.log("This Year Picker = "+this.yearChange); 
        console.log("This Year = "+d.getFullYear()); 
        var calAge = d.getFullYear() - this.yearChange;
        if(calAge >=0){
            this.ageValue = calAge;
        }else{
            this.ageValue = 0;
        }
        
        console.log("Age = "+this.value);

    }
}*/
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';