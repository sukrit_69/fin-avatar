/* eslint-disable vars-on-top */
/* eslint-disable no-console */
import { LightningElement, track } from 'lwc';
import getAvatarDetail from '@salesforce/apex/fna_AvatarDetail.getAvatarMasterDetail'; 

export default class Fna_AvatarDetail extends LightningElement {

    @track temp = '';
    @track b64 = '';
    @track name1 = '';
    @track loading;
     
    handleClick() {
        window.location.assign('/fin/s/avatar-phone');
        this.loading = true;
    }

    handleProduct() {

        window.location.assign('/fin/s/avatar-phone-input');
        this.loading = true;
    }

    homePage(){
        window.location.assign('/fin/s/');
        this.loading = true;
    }

    exitPage(){
        var urlHome = "/fin/s/";
        this.gotoURL(urlHome);
    }

    gotoURL(urlpage){
        // Navigate to a URL
       window.location.assign(urlpage);
   }

    constructor(){
        super();
        const sPageURL = decodeURIComponent(window.location.search.substring(1));
        const parameter = this.getparameter(sPageURL);
        // console.log('has parameter ' + parameter.has('qId'));
        if(parameter.has('Id')){
            this.getGiftboxDetail(parameter.get('Id'));
        }else{
            this.getGiftboxDetail();
        }
    }
 
    getGiftboxDetail(formId){
        console.log('Form ID ' + formId);
        //'a3b0l0000003r1EAAQ'
        getAvatarDetail({idForm: formId })
        .then(result => {
            this.temp = result.avatar;
            this.b64 = 'data:image/jpeg;base64, ' + result.urlImage;
            this.name1 = result.avatarName;
            console.log('then');
            console.log(this.temp);
        })
        .catch(error => { 
            console.log('catch');
            console.log(error);
        });

    }

    
    getparameter(param){
        var myMap = new Map();
        if(param !== ''){
            var sURLVariables = param.split('&');
        
            var i;
            var sParameterName;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                myMap.set(sParameterName[0], sParameterName[1]);
            }
        }
        return myMap;
    }
    
}