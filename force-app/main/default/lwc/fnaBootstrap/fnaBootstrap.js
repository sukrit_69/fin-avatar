/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
import { LightningElement, track, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import fna_Bootstrap from '@salesforce/resourceUrl/fna_Bootstrap';

export default class fnaBootstrap extends LightningElement {

    fnaBootstrap = false;
    
    renderedCallback() {
        if (this.fnaBootstrap) {
            return;
        }
        this.fnaBootstrap = true;
    
        Promise.all([
            //loadScript(this, fna_Bootstrap + '/fna_Bootstrap/js/bootstrap.min.js'),
            //loadStyle(this, fna_Bootstrap + '/fna_Bootstrap/css/bootstrap.min.css'),
            loadStyle(this, fna_Bootstrap + '/fna_Bootstrap/fonts/fonts.css'),
        ])
            .then(() => {
                this.InitializeBootstrap();
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error loading FNA_Bootstrap',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }

    InitializeBootstrap(){
        console.log("Initialize Bootstrap");
    }
}