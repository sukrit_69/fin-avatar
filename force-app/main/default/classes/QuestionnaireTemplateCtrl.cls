public without sharing class QuestionnaireTemplateCtrl {
    
    public class QuestionResponse{
        @AuraEnabled
        public boolean success {get;set;}
        @AuraEnabled
        public String nextPage {get;set;} //Id question_template
        @AuraEnabled
        public String idForm {get;set;} // Id question_form
        @AuraEnabled
        public String errMsg {get;set;}
    }
    
    public class Template {
        @AuraEnabled
        public Boolean template1 {get;set;}
        @AuraEnabled
        public Boolean template2 {get;set;}
        @AuraEnabled
        public Boolean template3 {get;set;}
        @AuraEnabled
        public Boolean template4 {get;set;}
        @AuraEnabled
        public Boolean template5 {get;set;}
        @AuraEnabled
        public Boolean template6 {get;set;}
        
    }

    public class Choice{
        @AuraEnabled
        public Integer id {get;set;}
        @AuraEnabled
        public String choice {get;set;}
        @AuraEnabled
        public String urlImage {get;set;}
    }

    public class QuestionTemplate{
        @AuraEnabled
        public String questionId {get;set;}
        @AuraEnabled
        public String question {get;set;}
        @AuraEnabled
        public Template template {get;set;}
        @AuraEnabled
        public List<Choice> choiceObj {get;set;}
        @AuraEnabled
        public Map<String, String> nextQuestion {get;set;}
        @AuraEnabled
        public String idForm {get;set;}
        @AuraEnabled
        public String answer {get;set;}
        @AuraEnabled
        public Map<String, String> answerAvatar {get;set;}
    }

    @AuraEnabled
    public Static QuestionTemplate getQuestion(String thisQuestion, String thisForm){
        String questionId = thisQuestion;
        system.debug('Question Id' + questionId);
        system.debug('thisForm' + thisForm);
        QuestionTemplate res = new QuestionTemplate();
        List<Questionnaire_Template__c> questionnaireList = new List<Questionnaire_Template__c>();
        if(questionId != ''){
            questionnaireList = [SELECT Id, Template__c, Template__r.name, Project__c, Question__c, Version__c, 
                                                                 (SELECT Id, Name, Answer__c, Questionnaire_Template__c, Next_Question__c, Answer_Avatar__c
                                                                  From Choice_Templates__r) FROM Questionnaire_Template__c WHERE id =:questionId];
        }
        else{
            questionnaireList = [SELECT Id, Template__c, Template__r.name, Project__c, Question__c, Version__c, 
                                                                 (SELECT Id, Name, Answer__c, Questionnaire_Template__c, Next_Question__c, Answer_Avatar__c
                                                                  From Choice_Templates__r) FROM Questionnaire_Template__c WHERE default__c = true];
        }
        system.debug('questionnaireList ' + questionnaireList);
        if(questionnaireList.size() > 0){
            
            Questionnaire_Template__c question = questionnaireList[0];
            res.questionId = question.id;
            res.question = question.Question__c;
            res.template = defineTemplate(question.Template__r.name);
            res.choiceObj = new List<Choice>();
            res.nextQuestion = new Map<String, String>();
            res.answerAvatar = new Map<String, String>();            
            res.idForm = thisForm == null ? '' : thisForm;
            Integer x = 0;
            
            for(Choice_Template__c choice : question.Choice_Templates__r){
                system.debug('choice ' + choice);
                x++;
                Choice tmpChoice = new Choice();
                tmpChoice.id = x;
                tmpChoice.choice = choice.Answer__c;
                res.choiceObj.add(tmpChoice);

                //res.choice.put(x,choice.Answer__c);
                if(choice.Answer__c == null || choice.Answer__c == ''){
                    res.nextQuestion.put('fill input', choice.Next_Question__c);
                }else{
                    res.nextQuestion.put(choice.Answer__c, choice.Next_Question__c);
                }

                res.answerAvatar.put(choice.Answer__c, choice.Answer_Avatar__c);
                
            }


            Map<String, String> titleImage = new Map<String, String>();

            if(res.questionId == 'a3U0l000000qluhEAA'){
                titleImage.put('ชาย' , 'male');
                titleImage.put('หญิง' , 'female');

            }else if(res.questionId == 'a3U0l000000qmKkEAI' ){
                List<Questionnaire__c> questionList = [SELECT Id, Question__c, Answer__c, Questionnaire_Form__c, Questionnaire_Template__c FROM Questionnaire__c WHERE Questionnaire_Form__c =:thisForm AND Questionnaire_Template__c= 'a3U0l000000qluhEAA'];
                system.debug('questionList : ' + questionList);
                     
                if(questionList.size() > 0){
                    if(questionList[0].Answer__c == 'ชาย'){
                        titleImage.put(questionList[0].Answer__c , 'male_circle');
                    }else{
                        titleImage.put(questionList[0].Answer__c , 'female_circle');
                    }
                    res.choiceObj[0].choice = questionList[0].Answer__c;
                }
            }

            if(titleImage.size() > 0){
                Map<String, String> temp = getImageUrl(titleImage);
                for(Choice tempChoice : res.choiceObj){
                    for(String imageKey : temp.keySet()){
                        if(tempChoice.choice == imageKey){
                            tempChoice.urlImage = temp.get(imageKey);
                        }
                    }
                }
                
                ///res.titleImage = getImageUrl(titleImage);
            }

            
            system.debug('res choice ' + res.choiceObj);
        } 

        if(thisForm != ''){
            List<Questionnaire_Form__c> questionList = [SELECT Id, Avatar_Master__c, Drop_Off_Page__c, (Select Id, Question__c, Answer__c FROM Questionnaires__r WHERE Questionnaire_Template__c=:questionId) FROM Questionnaire_Form__c WHERE id=:thisForm];
            //List<Questionnaire__c> questionList = [SELECT Id, Question__c, Answer__c, Questionnaire_Form__c, Questionnaire_Template__c FROM Questionnaire__c WHERE Questionnaire_Form__c =:thisForm AND Questionnaire_Template__c=:questionId];
            if(questionList.size() > 0){
                system.debug('questionList[0].Questionnaires__r' + questionList[0].Questionnaires__r );
                if(questionList[0].Questionnaires__r != null && questionList[0].Questionnaires__r.size() > 0){
                    res.answer = questionList[0].Questionnaires__r[0].Answer__c;
                }
                questionList[0].Drop_Off_Page__c = '/fin/s/questionnaire?qid=' + questionId + '&id=' + thisForm;
                update questionList;
            }
        }
        return res;
    }

    public Static Map<String, String> getImageUrl(Map<String, String> titleImage){

        List<ContentVersion> cvsList = [SELECT Id, Title, VersionData, ContentDocumentId FROM ContentVersion where Title in :titleImage.values()];    
        Map<String, String> b64 = new Map<String, String>();
        for(String keyTiltle : titleImage.keySet()){
            for(ContentVersion cvs : cvsList){
                if(cvs.title == titleImage.get(keyTiltle)){
                    b64.put(keyTiltle, 'data:image/png;base64, ' + EncodingUtil.base64Encode(cvs.VersionData));
                }
            }
        }
        
        return b64;

    }

    public Static Template defineTemplate(String idTemplate){
        Template resTemplate = new Template();
        switch on idTemplate {
            when 'Template#1' {
                resTemplate.template1 = true;
            }
            when 'Template#2' {
                resTemplate.template2 = true;
            }
            when 'Template#3' {
                resTemplate.template3 = true;
            }
            when 'Template#4' {
                resTemplate.template4 = true;
            }
            when 'Template#5' {
                resTemplate.template5 = true;
            }
            when 'Template#6' {
                resTemplate.template6 = true;
            }
        }
        return resTemplate;
    }

	/*public Static Map<String, String> getNavigateFinAvatar(String thisQuestion){
        Map<String, String> finAvatar = new Map<String, String>();
        if(thisQuestion != ''){
            List<FIN_AVATAR__mdt> finAvatarList = [Select id, Test_Giftbox__Navigate__c, DeveloperName  From FIN_AVATAR__mdt Where DeveloperName = :thisQuestion];
            if(finAvatarList.size() > 0){
                system.debug('finAvatarList' + finAvatarList);
                //finAvatar = finAvatarList[0].Test_Giftbox__Navigate__c;
                for(FIN_AVATAR__mdt fin : finAvatarList){
                    String fullText = fin.Test_Giftbox__Navigate__c;
                    system.debug('fullText' + fullText);
                    if(fullText != ''){
                        List<String> splitLine = fullText.split(',');
                        system.debug('splitLine' + splitLine);
                        for(String line : splitLine){
                            system.debug('line' + line);
                            List<String> question = line.trim().split(':');
                            system.debug('question' + question);
                            if(question.size() > 1){
                                finAvatar.put(question[0], question[1]);
                            }
                            
                        }
                    }
                }
            }
        }
        return finAvatar;
    }*/

   
    
    @AuraEnabled
    public static QuestionResponse saveQuestionInformation(QuestionTemplate thisQuestion, String answerQuestion){
        system.debug('saveQuestionInformation');
        system.debug('thisQuestion : ' + thisQuestion);
        system.debug('answerQuestion : ' + answerQuestion);

        // system.debug('thisQuestion.idForm : ' + thisQuestion.idForm);
        QuestionResponse questionRes = new QuestionResponse();
        //List<Questionnaire_Form__c> questionnaireForm_DB = new List<Questionnaire_Form__c>();
        Questionnaire_Form__c objquestionnaireForm = new Questionnaire_Form__c();  
        if(thisQuestion.idForm == '' || thisQuestion.idForm == null){
            // objquestionnaireForm.Avatar_Master__c = 'a3b0l0000003r1EAAQ';
            upsert objquestionnaireForm;
            thisQuestion.idForm = objquestionnaireForm.id;
          
        }
        /*else{
            questionnaireForm_DB = [SELECT Id, Name, Answer_Flow__c FROM Questionnaire_Form__c WHERE Id =: thisQuestion.idForm];
        }*/
        
        system.debug('thisQuestion.questionId : ' + thisQuestion.questionId);
        system.debug('thisQuestion.idForm : ' + thisQuestion.idForm);
        List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c FROM Questionnaire__c WHERE Questionnaire_Template__c =: thisQuestion.questionId AND Questionnaire_Form__c =: thisQuestion.idForm];
        // List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c FROM Questionnaire__c WHERE id=: 'a3W0l0000005y93EAA'];// id of questiionnaire_id
        system.debug('questionnaire_DB : ' + questionnaire_DB);

        //  questionnaire_DB <<
        if(questionnaire_DB.isEmpty()){
            Questionnaire__c objQuestionnaire = new Questionnaire__c();
            objQuestionnaire.Question__c = thisQuestion.question;
            objQuestionnaire.Answer__c = answerQuestion;
            objQuestionnaire.Questionnaire_Template__c = thisQuestion.questionId;
            objQuestionnaire.Questionnaire_Form__c = thisQuestion.idForm;
            system.debug('questionnaire (if) : ' + objQuestionnaire);
            upsert objQuestionnaire;
        }
        else{
            questionnaire_DB[0].Question__c = thisQuestion.question;
            questionnaire_DB[0].Answer__c = answerQuestion;
            system.debug('questionnaire (else) : ' + questionnaire_DB);
            upsert questionnaire_DB;            
        }


        //a
        Boolean haveAnswer = thisQuestion.nextQuestion.containsKey(answerQuestion);
        if(thisQuestion.questionId != 'a3U0l000000qlupEAA'){
            if(haveAnswer){
                questionRes.nextPage = thisQuestion.nextQuestion.get(answerQuestion);
            }else{
                if(thisQuestion.nextQuestion.containsKey('fill input')){
                    questionRes.nextPage = thisQuestion.nextQuestion.get('fill input');
                }else{
                    answerQuestion = changeAgetoChoice(answerQuestion);
                    questionRes.nextPage = thisQuestion.nextQuestion.get(answerQuestion);
                }
            }
        }else{
             questionRes.nextPage = '';
        }

        List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id, Answer_Flow__c FROM Questionnaire_Form__c WHERE Id =: thisQuestion.idForm];
        system.debug('Questionnaire_Form__c : ' + questionnaireForm_DB);
        // Questionnaire_Form__c objQuestionnaireForm = new Questionnaire_Form__c();
        
        if(questionnaireForm_DB[0].Answer_Flow__c == null){
            questionnaireForm_DB[0].Answer_Flow__c = thisQuestion.answerAvatar.get(answerQuestion);
        }
        else{
            if(questionnaire_DB[0].Id != 'a3U0l000000qlurEAA' && thisQuestion.answerAvatar.get(answerQuestion) == null){
                questionnaireForm_DB[0].Answer_Flow__c += 'X';
            }   
            else{
                questionnaireForm_DB[0].Answer_Flow__c += thisQuestion.answerAvatar.get(answerQuestion);
            }
        }
        
        system.debug('questionnaireForm_DB[0].Answer_Flow__c : ' + questionnaireForm_DB[0].Answer_Flow__c);

        questionnaireForm_DB[0].Avatar_Master__c = saveAvatarMaster(thisQuestion, answerQuestion, questionnaireForm_DB);
        
        update questionnaireForm_DB;





        // query lastest questionnaire [delete query lastest and save on db]
        // List<Questionnaire__c> questionnaireSave_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c, Questionnaire_Form__c, CreatedDate FROM Questionnaire__c WHERE Questionnaire_Form__c =: thisQuestion.idForm ORDER BY CreatedDate DESC]; 
        // List<Questionnaire__c> questionnaireSave_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c, Questionnaire_Form__c FROM Questionnaire__c WHERE Questionnaire_Form__c =: 'a3W0l0000005y93EAA' ORDER BY CreatedDate DESC];    
        // system.debug('questionnaire_DB : ' + questionnaireSave_DB);

        // Integer i = 0;
        // boolean status = true;
        // for(Questionnaire__c questionLoop : questionnaireSave_DB){
        //     system.debug('questionLoop.Questionnaire_Template__c : ' + questionLoop.Question__c);

        //     if(status == true){
        //         if (thisQuestion.questionId == questionLoop.Questionnaire_Template__c && i == (questionnaireSave_DB.size()-1) ) {
        //             Questionnaire__c questionnaire = new Questionnaire__c();
        //             questionnaire.Question__c = questionLoop.Question__c;
        //             questionnaire.Answer__c = questionLoop.Answer__c;
        //             //upsert questionnaire;
        //             status = false;
        //             break;
        //         }
        //         else if (thisQuestion.questionId == questionLoop.Questionnaire_Template__c){
        //             Questionnaire__c questionnaire = new Questionnaire__c();
        //             questionnaire.Question__c = questionLoop.Question__c;
        //             questionnaire.Answer__c = questionLoop.Answer__c;
        //             //upsert questionnaire;
        //             status = false;
        //         }
        //     }
        //     else{
        //          questionnaireSave_DB.remove(i);
        //          //delete questionnaireSave_DB;
        //     }
        //     i++;
        // }
        system.debug('thisQuestion.nextQuestion ' + thisQuestion.nextQuestion);
        system.debug('thisQuestion.haveAnswer ' + haveAnswer);
        system.debug('thisQuestion.nextPage ' + questionRes.nextPage);

        questionRes.success = true;
        questionRes.errMsg = '';
        questionRes.idForm = thisQuestion.idForm;
        /// save Question

        return questionRes;
    }

    public static String saveAvatarMaster(QuestionTemplate thisQuestion, String answerQuestion, List<Questionnaire_Form__c> qForm_DB){
        String temp = thisQuestion.nextQuestion.get(answerQuestion);
        String tempDB;
        if(temp == null || temp == ''){    
            List<AvatarMappingResults__c> avatarMap_DB = [SELECT Id, Avatar__c, Answer2__c, AvatarMaster__c FROM AvatarMappingResults__c WHERE Answer2__c =: qForm_DB[0].Answer_Flow__c];
            if(avatarMap_DB.isEmpty()){//default avatar
                List<Avatar_Master__c> avatarMap_DB_Default = [SELECT Id, Avatar_Image__c, Segment__c, Avatar_EN__c, Avatar_TH__c, Avatar_Description__c FROM Avatar_Master__c WHERE Segment__c =: 'Default Avatar'];
                system.debug('avatarMap_DB_Default : ' + avatarMap_DB_Default);       
                // qForm_DB[0].Avatar_Master__c = avatarMap_DB_Default[0].Id;
                tempDB = avatarMap_DB_Default[0].Id;
            }
            else {// out of  default avatar
                // List<Avatar_Master__c> avatarMap_DB_OutOfDefault = [SELECT Id, Avatar_Image__c, Segment__c, Avatar_EN__c, Avatar_TH__c, Avatar_Description__c  FROM Avatar_Master__c WHERE Id =: avatarMap_DB[0].AvatarMaster__c];
                // system.debug('avatarMap_DB : ' + avatarMap_DBZ[0].Id);      
                // system.debug('avatarMap_DB_OutOfDefault : ' + avatarMap_DB_OutOfDefault); 
                system.debug('avatarMap_DB : ' + avatarMap_DB); 
                // qForm_DB[0].Avatar_Master__c = avatarMap_DB[0].AvatarMaster__c;
                tempDB = avatarMap_DB[0].AvatarMaster__c;
            }
        }
        return tempDB;
    }

    public static String changeAgetoChoice(String age){
        integer ageNumber = integer.valueof(age);
        String choiceText = '';
        if(ageNumber < 22){
            choiceText = 'น้อยกว่า 22 ปี';
        }else if(ageNumber >= 22 && ageNumber <= 27){
            choiceText = '22-27 ปี';
        }else if(ageNumber >= 28 && ageNumber <= 40){
            choiceText = '28-40 ปี';
        }else if(ageNumber >= 41 && ageNumber <= 50){
            choiceText = '41-50 ปี';
        }else if(ageNumber >= 51 && ageNumber <= 65){
            choiceText = '51-65 ปี';
        }else{
            choiceText = 'มากกว่า 65 ปี';
        }
        return choiceText;
    }

    @AuraEnabled
    public static String previousQuestionInformation(String idForm, String idQuestion){
        String previousQuestionId;
        system.debug('previousQuestionInformation');
        system.debug('idForm' + idForm);
        system.debug('idQuestion' + idQuestion);

        // query lastest questionnaire [delete query lastest and save on db]
        List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c, Questionnaire_Form__c FROM Questionnaire__c WHERE Questionnaire_Form__c =: idForm ORDER BY CreatedDate ASC]; 
        // List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c, Questionnaire_Form__c FROM Questionnaire__c WHERE Questionnaire_Form__c =: 'a3W0l0000005y93EAA' ORDER BY CreatedDate DESC];    
        system.debug('questionnaire_DB[0] : ' + questionnaire_DB[0]);
        // system.debug('questionnaire_DB[1] : ' + questionnaire_DB[1]);

        // Integer i = 0;

        if(!questionnaire_DB.isEmpty()){
            system.debug('questionnaire_DB.size() : ' + questionnaire_DB.size());
            // for(Questionnaire__c questionDBLoop : questionnaire_DB) {
            //     system.debug('questionDBLoop : ' + questionDBLoop);
            //     if(questionDBLoop.Questionnaire_Template__c == idQuestion){
            //         previousQuestionId = questionnaire_DB[1].Questionnaire_Template__c;
            //     }
            //     else{
            //         previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;
            //     }
            //     i++;
            // }

            // previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;

            // if(questionnaire_DB.size()==1){
            //     previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;
            // }
            // else {
            //     if(questionnaire_DB[0].Questionnaire_Template__c != idQuestion){
            //         previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;
            //     }
            //     else {
            //         previousQuestionId = questionnaire_DB[1].Questionnaire_Template__c;
            //     }
            // }

            // if((questionnaire_DB.Questionnaire_Template__c).indexOf(idQuestion) == null){

            // }

            for(Integer i = 0;i<questionnaire_DB.size();i++){
                if((questionnaire_DB.size()-1) != 0 ){
                    system.debug('if back');
                    system.debug('idQuestion : ' + idQuestion);
                    system.debug(i-1);
                    previousQuestionId = questionnaire_DB[i-1].Questionnaire_Template__c;
                    system.debug('previousQuestionId : ' + previousQuestionId);
                    break;
                }
                else {
                    system.debug('else back');
                    system.debug(i);
                    previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;
                    system.debug('previousQuestionId : ' + previousQuestionId);
                    break;
                }
            }
        }
        else{
            system.debug('null impossible');
        }
        return previousQuestionId;

        // if(!questionnaire_DB.isEmpty()){
        //     system.debug('questionnaire_DB.size() : ' + questionnaire_DB.size());
        //     previousQuestionId = questionnaire_DB[0].Questionnaire_Template__c;
        //     system.debug('questionnaire_DB[0].Questionnaire_Template__c : ' + questionnaire_DB[0].Questionnaire_Template__c);
        //     system.debug('questionnaire_DB[0].Questionnaire_Form__c : ' + questionnaire_DB[0].Questionnaire_Template__c);


            // if(questionnaire_DB.size()<3){
            //     if (thisQuestion.questionId == questionnaire_DB[0].Questionnaire_Template__c) {        
            //         thisQuestion.questionId = questionnaire_DB[1].Questionnaire_Template__c;
            //         system.debug('1-0thisQuestion.questionId : ' + thisQuestion.questionId);
            //     }
            //     else if (thisQuestion.questionId == questionnaire_DB[1].Questionnaire_Template__c){
            //         thisQuestion.questionId = questionnaire_DB[2].Questionnaire_Template__c;
            //         system.debug('2-1thisQuestion.questionId : ' + thisQuestion.questionId);
            //     } 
            //     else if (thisQuestion.questionId == questionnaire_DB[2].Questionnaire_Template__c){
            //         thisQuestion.questionId = questionnaire_DB[3].Questionnaire_Template__c;
            //     } 
            //     else if (thisQuestion.questionId == questionnaire_DB[3].Questionnaire_Template__c){
            //         thisQuestion.questionId = questionnaire_DB[4].Questionnaire_Template__c;
            //     } 
            //     else {
            //         // impossible
            //         system.debug('else bug');
            //     }
            // }
            // else {
            //     system.debug('else bug');
            // }
        // }

        // system.debug('thisQuestion.questionId : ' + thisQuestion.questionId);
        // return previousQuestionId;

        // List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id, Name, Answer_Flow__c FROM Questionnaire_Form__c WHERE Id =: thisQuestion.idForm];
        // system.debug('questionnaireForm_DB : ' + questionnaireForm_DB);

        // if(!questionnaireForm_DB.isEmpty()){
        //     system.debug('previousQuestion.idForm : ' + previousQuestion.idForm);
        //     List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c FROM Questionnaire__c WHERE Questionnaire_Template__c =: thisQuestion.questionId];
        //     // List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c FROM Questionnaire__c WHERE id=: 'a3W0l0000005y93EAA'];// id of questiionnaire_id
        //     system.debug('questionnaire_DB : ' + questionnaire_DB);
        // }

        //query from db
        // List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id,Name,Questionnaire_Flow__c FROM Questionnaire_Form__c WHERE id =: thisQuestion.questionId];
        // List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id,Name,Questionnaire_Flow__c FROM Questionnaire_Form__c WHERE id =: 'a3W0l0000005y93EAA'];
        // system.debug('questionnaireFormDB : ' + questionnaireForm_DB);

        // if(!questionnaireForm_DB.isEmpty()){
        //     String questionnaireFlowDB_toString = questionnaireForm_DB[0].Questionnaire_Flow__c;
        //     system.debug('questionnaireFlowDB_toString : ' + questionnaireFlowDB_toString);

        //     //split '|' toString -> array
        //     List<String> questionnaireFlowDB_array = questionnaireFlowDB_toString.split('\\|');
        //     // for (String str : questionnaireFlowDB_array){
        //     //     system.debug(str); 
        //     // }
        //     system.debug('questionnaireFlowDB_array : ' + questionnaireFlowDB_array);

        //     //remove last value in array
        //     questionnaireFlowDB_array.remove(questionnaireFlowDB_array.size()-1);

        //     system.debug('questionnaireFlowDB_array : ' + questionnaireFlowDB_array);

        //     //rray -> toString
        //     String questionnaireFlow_toString = null;
        //     for(String temp : questionnaireFlowDB_array){
        //         if (questionnaireFlow_toString == null){
        //             questionnaireFlow_toString = temp;
        //         }
        //         else{
        //             questionnaireFlow_toString += '|' + temp;
        //         }
        //     }
        //     system.debug('questionnaireFlow_toString : ' + questionnaireFlow_toString);

        //     //upsert to db
        //     system.debug('questionnaireFlow_toString before store to db : ' + questionnaireFlow_toString);
        //     questionnaireForm_DB[0].Questionnaire_Flow__c = questionnaireFlow_toString;
        //     system.debug('questionnaireForm_DB[0].Questionnaire_Flow__c after store to db : ' + questionnaireForm_DB[0].Questionnaire_Flow__c);
        //     upsert questionnaireForm_DB;
        // }

        // return previousQuestion;
    }

    @AuraEnabled(cacheable=true)
    public static List<Questionnaire_Template__c> Questionnaire() {
        return [SELECT Id, Template__c, Project__c, Question__c, Version__c, 
                                                                 (SELECT Id, Name, Answer__c, Questionnaire_Template__c, Next_Question__c
                                                                  From Choice_Templates__r) FROM Questionnaire_Template__c LIMIT 3];
    }
}