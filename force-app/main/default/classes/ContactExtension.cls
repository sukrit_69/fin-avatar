public with sharing class ContactExtension {}
//public with sharing class ContactExtension implements Database.AllowsCallouts{
    //**********************************************************//
    // Modify log
    //**********************************************************//
    // Change No. : CH01
    // Change By : Uttaporn L.
    // Change Date : 2015.06.22
    // Change Detail : Edit When select Sub-District to --None-- Return Error
    // Case No. : 00002582
    //**********************************************************//   
    /*public Contact contact;
    
    public string accid {get;set;}
    public static final String DML_EXCEPTION_CODE = '1000';
    public static final String CALLOUT_EXCEPTION_CODE = '1002';
    public static final String QUERY_EXCEPTION_CODE = '1001'; 
    public boolean isCalloutAllow {get;set;}
    public boolean isCreateSuccess {get;set;}
    public boolean isIDValid {get;set;}
    public boolean IsCountryAsTH {get;set;}
    public boolean IsOtherCountry {get;set;}
    public String selected {get;set;}
    public Integer ContactMobileNoTemp {get;set;}
    public Province__C province;
    public District__c district;
    public Sub_District__c subdis;
    public Map<id, List<SelectOption>> provinceMap {get;set;}
    public static Map<String,id> districtMap {get;set;}
    public Map<String,id> subdisMap {get;set;}
    public Map<id,string> postcodes {get;set;}
    public String selectedProvince {get;set;}
    public String selectedDistrict {get;set;}
    public String selectedSubDistrict {get;set;}
    public String selectedPostcode {get;set;}
    public List<Province__c> ProvinceList{get;set;}
    public List<District__c> DistrictList{get;set;}
    public List<Sub_District__c> subdisList {get;set;}
    public Set<String> postcodeSet {get;set;}
    public List<SelectOption> ProvinceOptionList {get;set;}
    public List<SelectOption> DistrictOptionList {get;set;}
    public List<SelectOption> SubDistrictOptionList {get;set;}
    public List<SelectOption> PostcodeOptionList {get;set;}
    public string selectedCountry {get;set;}
    public Map<String,String> CountryMap {get;set;}
    public  List<SelectOption> CountriesOptionList {get;set;}
    public  List<Country__c> CountriesList {get;set;}
    
    //constructor
    public ContactExtension(ApexPages.StandardController controller) {
        
       
        
        ProvinceList = new List<Province__c>();
        DistrictList = new List<District__C>();
        subdisList = new List<Sub_District__c>();
        provinceMap = new Map<id, List<SelectOption>>();
        districtMap = new Map<String,id>();
        subdisMap = new Map<String,id>();
        postcodes = new Map<id,string>();
        postcodeSet = new Set<String>();            
        ProvinceOptionList = new List<SelectOption>();
        ProvinceOptionList.add(new SelectOption('','--None--'));
        ProvinceList = new List<Province__c>( [SELECT Id,Name,Code__c FROM Province__C WHERE id != null ORDER BY Name]);
        for(Province__C pro : ProvinceList){
            ProvinceOptionList.add(new SelectOption(pro.id,pro.Name));
        }
        
        DistrictOptionList = new List<SelectOption>();
        SubDistrictOptionList = new List<SelectOption>();
        PostcodeOptionList = new List<SelectOption>();
        contact = (Contact)controller.getRecord();      
        
         if(ApexPages.currentPage().getParameters().containsKey('accid'))
        {
            contact.accountId = ApexPages.currentPage().getParameters().get('accid');
        }
        
        CountryMap = new Map<String,String>();
        CountriesOptionList = new List<SelectOption>();
        DistrictOptionList.add(new SelectOption('','--None--'));
        SubDistrictOptionList.add(new SelectOption('','--None--'));
        PostcodeOptionList.add(new SelectOption('','--None--'));            
        CountriesList = new List<Country__c>( [SELECT ID,Name,Code__c FROM Country__c WHERE Name != '' AND Code__c != null ORDER BY Name]);
        for(Country__c coun : CountriesList){
            CountriesOptionList.add(new SelectOption(coun.Code__c,coun.Name));
            CountryMap.put(coun.Code__c, coun.Name);
        }
        selectedcountry = 'TH';
        IsCountryAsTH =true;
    }
    
    public PageReference save(){ 
        isCalloutAllow = false;
        if(contact.ID_Type_Temp__c!=null&&contact.ID_Number_Temp__c==null){         
           ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3004').Status_Message__c));
           return null;
        }
        if(contact.First_Name_Temp__c==null||contact.First_Name_Temp__c==''){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3009').Status_Message__c));
            return null;
        }
        
        if(!isIDValid&&contact.ID_Type_Temp__c=='Citizen ID'){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('3001').Status_Message__c));
            return null;
        }

        try{
            System.debug('Contact ' + contact);
            contact.LastName = 'N/A';
            insert contact;
            isCalloutAllow = true;
        }catch(DMLException e){
            if(contact.Date_of_Birth__c !=null){
                if(contact.Date_of_Birth__c > System.today()){
                 ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Status_Code__c.GetValues('3019').Status_Message__c) );
            }
            }else{
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Status_Code__c.GetValues('1000').Status_Message__c));
            }
            return null;
        }

        return null;
    }
    
    public PageReference insertContactCallout(){
        
        isCreateSuccess = true;
        contact.Country_Temp__c = selectedCountry;
        System.debug('Country : '+contact.Country_Temp__c +' : '+selectedCountry);
        Date_Of_Birth__c dateOfBirth = Date_Of_Birth__c.getValues('Default');
        String custAge;
        if(contact.Date_Of_Birth__c !=null){
          custAge = calculateAge(contact.Date_of_Birth__c)+'';  
        }else{
            custAge = '0';
        }
        
        System.debug(contact.Date_Of_Birth__c);
        DateTime d = (contact.Date_of_Birth__c==null)?dateOfBirth.Value__c:contact.Date_Of_Birth__c;
        contact.Date_of_Birth__c = date.newinstance(d.year(), d.month(), d.day());
        String myDate =  d.format('yyyy-MM-dd') ;
        //String custAge = calculateAge(contact.Date_of_Birth__c)+'';
        System.debug(myDate);
        System.debug(custAge);
        Type_of_ID__c typeid;
        if(contact.ID_Type_Temp__c !=null){
             typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:contact.ID_Type_Temp__c LIMIT 1];
        }else{
             typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name ='--None--' LIMIT 1];
        }
        
        try{
            TMBServiceProxy.TMBServiceProxySoap tmbService = new TMBServiceProxy.TMBServiceProxySoap();
            tmbService.timeout_x = 120000;
             // ktc add for rsa soap service call
            tmbService = SoapRsa.setSecureHeader(tmbService);
          /*  System.debug('CREATE CONTACT : '+contact.id);
            System.debug('CREATE CONTACT : '+contact.AccountId);
            System.debug('CREATE CONTACT : '+contact.First_Name_Temp__c);
            System.debug('CREATE CONTACT : '+contact.Last_Name_Temp__c);
            System.debug('CREATE CONTACT : '+typeid.Value__c);
            System.debug('CREATE CONTACT : '+contact.ID_number_Temp__c);
            System.debug('CREATE CONTACT : '+mydate);
            System.debug('CREATE CONTACT : '+contact.Position__c);
            System.debug('CREATE CONTACT : '+contact.Mobile_No_Temp__c);
            System.debug('CREATE CONTACT : '+contact.Office_No_Temp__c);
            System.debug('CREATE CONTACT : '+contact.Fax_No_Temp__c);
            System.debug('CREATE CONTACT : '+contact.Email_Temp__c);
            System.debug('CREATE CONTACT : '+custAge);
            System.debug('CREATE CONTACT : '+contact.Decision_Map__c);
            System.debug('CREATE CONTACT : '+contact.Product_Decision__c);
            System.debug('CREATE CONTACT : '+contact.Value_Style__c);
            System.debug('CREATE CONTACT : '+contact.Difficult_to_deal__c);
            System.debug('CREATE CONTACT : '+contact.Description__c);
            System.debug('CREATE CONTACT : '+contact.Salutation);
            System.debug('CREATE CONTACT : '+contact.Address_No_Temp__c);                                                                       
            System.debug('CREATE CONTACT : '+contact.Sub_District_Temp__c);                                                                         
            System.debug('CREATE CONTACT : '+contact.District_Temp__c);                                                                          
            System.debug('CREATE CONTACT : '+contact.Province_Temp__c);                                                                                              
            System.debug('CREATE CONTACT : '+contact.Zip_code_Temp__c);    
            System.debug('CREATE CONTACT : '+contact.Country_Temp__c);             
            System.debug('CREATE CONTACT : '+contact.Authorized_person_of_signature__c); */
            
           // TMBServiceProxy.ContactResultDTO InsertContactresult = tmbService.InsertContact(  /*  1   */    contact.Id
                                                                                              /*  2   */  //  ,contact.AccountId
                                                                                              /*  3   */  //  ,(contact.First_Name_Temp__c==null)?'':contact.First_Name_Temp__c
                                                                                              /*  4   */  // ,(contact.Last_Name_Temp__c==null)?'':contact.Last_Name_Temp__c
                                                                                              /*  5   */  //  ,(contact.ID_Type_Temp__c==null)?'':typeid.Value__c
                                                                                              /*  6   */  //  ,(contact.ID_number_Temp__c==null)?'':contact.ID_number_Temp__c
                                                                                              /*  7   */  //  ,myDate
                                                                                              /*  8   */  // ,(contact.Position__c==null)?'':contact.Position__c
                                                                                              /*  9   */  //  ,(contact.Mobile_No_Temp__c==null)?'':contact.Mobile_No_Temp__c
                                                                                              /*  10  */  //  ,(contact.Office_No_Temp__c==null)?'':contact.Office_No_Temp__c
                                                                                              /*  11  */  //  ,(contact.Fax_No_Temp__c==null)?'':contact.Fax_No_Temp__c
                                                                                              /*  12  */  //  ,(contact.Email_Temp__c==null)?'':contact.Email_Temp__c
                                                                                              /*  13  */  //  ,custAge
                                                                                              /*  14  */  //  ,(contact.Decision_Map__c==null)?'':contact.Decision_Map__c
                                                                                              /*  15  */  //  ,(contact.Product_Decision__c==null)?'':contact.Product_Decision__c
                                                                                              /*  16  */  //  ,(contact.Value_Style__c==null)?'':contact.Value_Style__c
                                                                                              /*  17  */  //  ,(contact.Difficult_to_deal__c==null)?'':contact.Difficult_to_deal__c,
                                                                                              /*  18  */  //  (contact.Description__c==null)?'':contact.Description__c,                                                                                                 
                                                                                              /*  19  */  //  (contact.Salutation==null)?'':contact.Salutation,
                                                                                              /*  20  */  //  (contact.Address_No_Temp__c==null)?'':contact.Address_No_Temp__c,
                                                                                              /*  21  */  //  (contact.Street_Temp__c==null)?'':contact.Street_Temp__c,
                                                                                              /*  22  */  // (contact.Soi_Temp__c==null)?'':contact.Soi_Temp__c,                                                                        
                                                                                              /*  23  */  //  (contact.Sub_District_Temp__c==null)?'':contact.Sub_District_Temp__c,                                                                         
                                                                                              /*  24  */  //  (contact.District_Temp__c==null)?'':contact.District_Temp__c,                                                                          
                                                                                              /*  25  */  //  (contact.Province_Temp__c==null)?'':contact.Province_Temp__c,                                                                                              
                                                                                              /*  26  */  // (contact.Zip_code_Temp__c==null)?'':contact.Zip_code_Temp__c,                                                                           
                                                                                              /*  27  */  //  (contact.Country_Temp__c==null)?'':contact.Country_Temp__c,
                                                                                              /*  28  */  //  contact.Authorized_person_of_signature__c==true?'Y':'N',
                                                                                              /*  29  */  //  UserInfo.getUserId(), 
                                                                                              /*  30  */  //  UserInfo.getUserName());
           /* System.debug('status : '+InsertContactresult.status);
            System.debug('total record : '+InsertContactresult.totalrecord);
            System.debug('message : '+InsertContactresult.massage); 
        
            if(InsertContactresult.status == '0000' ){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO,Status_Code__c.GetValues('3010').Status_Message__c));
                //contact.ID_Type_Temp__c = null;
                contact.ID_Number_Temp__c = null;
                contact.First_Name_Temp__c = null;
                contact.Last_Name_Temp__c = null;
                contact.Mobile_No_Temp__c = null;
                contact.Fax_No_Temp__c = null;
                contact.Office_No_Temp__c = null;
                    DateTime defaultdate = Date_Of_Birth__c.getValues('Default').Value__c;
                    DateTime birthdate = contact.Date_of_Birth__c;
                    //Change Default Birth Date equal null
                    if(defaultdate == birthdate){
                       contact.Date_of_Birth__c =null;
                    }
                
                contact.LastName = 'Contact Info';
                update contact;
            }else{
                isCreateSuccess = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,InsertContactresult.massage+'    '+contact));
                delete contact;
            }
        
       }catch(CalloutException e){
            Disqualified_Reason__c reason = Disqualified_Reason__c.getValues('Webservice_Timeout');
            contact.isDisqualified__c = true;
            contact.Disqualified_Reason__c = reason.Value__c;    
            update contact;
            ApexPages.addmessage(ErrorHandler.Messagehandler(CALLOUT_EXCEPTION_CODE, ''));
            System.debug(logginglevel.ERROR,'Callout Error: '+e.getMessage());
            return null;
       }catch(DMLException e){
            ApexPages.addMessage(ErrorHandler.Messagehandler(DML_EXCEPTION_CODE, ''));
            System.debug(logginglevel.ERROR,'DML Exception: '+e.getMessage());
            return null;
       }
       return null;
          
    }
    
    public PageReference viewContact(){
        PageReference ContactPage;
        ContactPage = new ApexPages.StandardController(contact).view();
        ContactPage.setRedirect(true);          
        return ContactPage; 
    }
    
    private String provinceholder;
    
        public void Provinceselected(){
            boolean provincechange =false;
             selectedSubDistrict =null;
        SubDistrictOptionList = new List<SelectOption>();
        selectedPostcode = null;
        contact.Province_Temp__c = null;
        PostcodeOptionList =  new List<SelectOption>();
            if(provinceholder ==null){
                provinceholder = selectedProvince;
            }else{
                if(selectedProvince !=null && selectedprovince !='' && provinceholder != selectedProvince){
                    provincechange = true;
                }
            }
          
            if((selectedProvince !=null && selectedProvince != '' && selectedProvince !='null')||provincechange){
               
              DistrictOptionList = new List<SelectOption>();
                contact.Province_Temp__c = [SELECT ID,NAME FROM Province__C WHERE ID =:selectedProvince LIMIT 1].Name;
            
             DistrictList = new List<District__c>([SELECT ID,Name,Code__c FROM District__c WHERE Province__c =:selectedProvince]);
             DistrictOptionList.add(new SelectOption('','--None--'));
               for(District__C dis : DistrictList){
                  DistrictOptionList.add(new SelectOption(dis.id,dis.Name)); 
                    }
                provinceholder = selectedprovince;
      }else{
        selectedDistrict = null;
        selectedSubDistrict = null;
        selectedpostcode = null;
        DistrictOptionList = new List<SelectOption>();
        contact.District_Temp__c =null;
        contact.Sub_District_Temp__c =null;  
        contact.Zip_Code_Temp__c = null;  
        postcodeSet = new Set<String>(); 
        SubDistrictOptionList = new List<SelectOption>();
         PostcodeOptionList = new List<SelectOption>();
      }  
           
      
    }
    
    public void DistrictSelected(){
       
         if(selectedDistrict !=null && selectedDistrict != '' && selectedDistrict !='null' ){
    SubDistrictOptionList = new List<SelectOption>();
        PostcodeOptionList = new List<SelectOption>();
    PostcodeOptionList.add(new SelectOption('','--None--'));
        selectedSubDistrict = null;
    postcodeSet = new Set<String>();
    contact.District_Temp__c = [SELECT ID,NAME FROM District__c WHERE ID =:selectedDistrict LIMIT 1].Name;
    System.debug('DISTRICT : '+contact.District_Temp__c);
    subdisList = new List<Sub_District__c>([SELECT ID,Name,Code__c,Zip_code__c,Location_Code__c FROM Sub_District__c WHERE District__c =:selectedDistrict]);
    subDistrictOptionList.add(new SelectOption('','--None--'));
    for(Sub_District__c subdis : subdisList){
      SubDistrictOptionList.add(new SelectOption(subdis.id,subdis.Name));
      postcodeSet.add(subdis.Zip_code__c);
    }  
         }else{
             selectedSubDistrict = null;
             selectedSubDistrict = null;
             selectedPostcode = null;
             contact.District_Temp__c = null;
             contact.Sub_District_Temp__c =null;  
             contact.Zip_Code_Temp__c = null;  
             SubDistrictOptionList = new List<SelectOption>();
             PostcodeOptionList = new List<SelectOption>();
             postcodeSet = new Set<String>(); 
           
         }
    }
    
     public void SubDistrictSelected(){
        
        if(selectedSubDistrict !=null && selectedSubDistrict != '' && selectedSubDistrict !='null' ){
    Sub_District__c sub = [SELECT ID,Name,Zip_code__c FROM Sub_District__c WHERE ID=: selectedSubDistrict LIMIT 1];
    contact.Sub_District_Temp__c = sub.Name;
    contact.Zip_Code_Temp__c = sub.Zip_code__c;
    selectedpostcode =null;
    PostcodeOptionList = new List<SelectOption>();
        PostcodeOptionList.add(new SelectOption('','--None--'));
    for(String Postcode : postcodeSet){
            
      PostcodeOptionList.add(new SelectOption(Postcode,Postcode)); 
            System.debug(Postcode);
    }
        }else{
           PostcodeOptionList = new List<SelectOption>();
           selectedpostcode =null;
            
             contact.Zip_Code_Temp__c = null;
        }
    }
   
        public static Integer calculateAge(Date birthDate){
        Integer Age;

        Date day = Date.today();
        
        if(day >= birthDate)
        {
          Age =   day.year() - birthDate.year();
        }
        else{
            Age =   day.year() - (birthDate.year() - 1);
        }  
        return Age;
    }
    
    
    public void CheckCountry(){
        System.debug('SELECT : '+selectedCountry);
        if(selectedCountry =='TH'){
            IsCountryAsTH =true;
            IsOtherCountry = false;
        }else{
            IsCountryAsTH =false;
            IsOtherCountry = true;
            
        }        
    }       
}*/