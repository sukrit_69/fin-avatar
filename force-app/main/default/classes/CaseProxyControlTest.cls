@isTest
public class CaseProxyControlTest {    

    public static testmethod void Unittest(){
        TestUtils.createAppConfig();
        Account acct = TestUtils.createAccounts(1,'TESTACCOUNT','Individual', true).get(0);
        Contact con = TestUtils.createContacts(1,acct.id,true ).get(0);
        Opportunity opp = TestUtils.createOpportunity(1,acct.id,true ).get(0);
        RecordType SECaseRecordType = [SELECT id,Name FROM RecordType 
                                 WHERE sObjectType='Case' AND Name = 'SE Call Center' limit 1];
        RecordType RetailCaseRecordType = [SELECT id,Name FROM RecordType 
                                 WHERE sObjectType='Case' AND Name = 'Retail Service' limit 1];
        
        Case caseRecord1 = new Case();
        caseRecord1.Subject = 'Test Subject';
        caseRecord1.Status = 'LeadExtension';
        caseRecord1.PTA_Segment__c = 'SBG';
        caseRecord1.Category__c = 'Service Level1';
        caseRecord1.Sub_Category__c = 'Service level2';
        caseRecord1.Product_Category__c = 'Service level3';
        caseRecord1.Issue__c = 'Service level4';
        caseRecord1.Status = 'New';
        caseRecord1.Description = 'Test create Case';
        caseRecord1.recordtypeID = SECaseRecordType.id;
        caseRecord1.AccountId = acct.id;
        
        Test.setCurrentPageReference(new PageReference('Page.CaseProxyPage')); 
        System.currentPageReference().getParameters().put('RecordType', SECaseRecordType.id);
        System.currentPageReference().getParameters().put('def_account_id', acct.id);
        System.currentPageReference().getParameters().put('def_opportunity_id', opp.id);
        
        PageReference proxy1 = Page.CaseProxyPage;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(caseRecord1);
        CaseProxyControl proxyExCre = new CaseProxyControl(controller1);
        proxyExCre.redirectPage(); 
        Test.setCurrentPage(proxy1);
        
        insert caseRecord1;
        
        PageReference proxy2 = Page.CaseProxyPage;
        ApexPages.StandardController controller = new ApexPages.StandardController(caseRecord1);
        CaseProxyControl proxyEx = new CaseProxyControl(controller);
        proxyEx.redirectPage();
        Test.setCurrentPage(proxy2); 
                
        Case caseRecord2 = new Case();
        PageReference proxy6 = Page.CaseProxyPage;
        ApexPages.StandardController controller6 = new ApexPages.StandardController(caseRecord2);
        CaseProxyControl proxyExCre6 = new CaseProxyControl(controller6);
        proxyExCre6.redirectPage(); 
        Test.setCurrentPage(proxy6);        
        
        
        Test.setCurrentPageReference(new PageReference('Page.CaseProxyPage')); 
        System.currentPageReference().getParameters().put('RecordType', SECaseRecordType.id);
        System.currentPageReference().getParameters().put('def_contact_id', con.id); 
         System.currentPageReference().getParameters().put('def_opportunity_id', opp.id);
        PageReference proxy3 = Page.CaseProxyPage;
        ApexPages.StandardController controller2 = new ApexPages.StandardController(caseRecord2);
        CaseProxyControl proxyExCre1 = new CaseProxyControl(controller2);
        proxyExCre1.redirectPage(); 
        Test.setCurrentPage(proxy3);        
        
        Test.setCurrentPageReference(new PageReference('Page.CaseProxyPage')); 
        System.currentPageReference().getParameters().put('RecordType', SECaseRecordType.id);
        System.currentPageReference().getParameters().put('def_contact_id', con.id); 
        System.currentPageReference().getParameters().put('def_account_id', acct.id);
        System.currentPageReference().getParameters().put('def_opportunity_id', opp.id);
        
        PageReference proxy4 = Page.CaseProxyPage;
        ApexPages.StandardController controller3 = new ApexPages.StandardController(caseRecord2);
        CaseProxyControl proxyExCre2 = new CaseProxyControl(controller3);
        proxyExCre2.redirectPage(); 
        Test.setCurrentPage(proxy4);
        
        
        Test.setCurrentPageReference(new PageReference('Page.CaseProxyPage')); 
        System.currentPageReference().getParameters().put('RecordType', RetailCaseRecordType.id);
        System.currentPageReference().getParameters().put('def_contact_id', con.id); 
        System.currentPageReference().getParameters().put('def_account_id', acct.id);
        System.currentPageReference().getParameters().put('def_opportunity_id', opp.id);
        
        PageReference proxy5 = Page.CaseProxyPage;
        ApexPages.StandardController controller4 = new ApexPages.StandardController(caseRecord2);
        CaseProxyControl proxyExCre3 = new CaseProxyControl(controller4);
        proxyExCre3.redirectPage(); 
        Test.setCurrentPage(proxy5);
        
    }
    
}