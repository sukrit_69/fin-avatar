@isTest
global  class SearchControllerExV4Test {}
/*
global  class SearchControllerExV4Test {
    // This method will Run First
    static{
        // Initial Data Custom config
        TestUtils.createAppConfig();
        TestUtils.createStatusCode();        
        TestUtils.createIdType();
    }  
    static testmethod void SearchbyID(){
        // Initial Data
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
         Account acct =  TestUtils.createAccounts(1,'webserviceTESTACCOUNT','Individual', false).get(0);
        
        acct.ID_Type_PE__c ='Alien ID';
        acct.ID_Number_PE__c ='1510100100181965';
        insert acct;
        Id [] fixedSearchResults= new Id[1];
       fixedSearchResults[0] = acct.id;
       Test.setFixedSearchResults(fixedSearchResults);

        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search Individual By ID
        searchEx.hdCusType ='Individual';
        searchEx.hdIdType = 'info';
        searchEx.account.ID_Type__c ='Alien ID';
        searchEx.account.ID_Number_Temp__c ='1510100100181965'; 
        
        Test.startTest();            
        searchEx.search();
        searchEx.next();
        searchEx.viewOwner();
        searchEx.viewProspect(); 
        searchEx.resetNextButton();
        searchEx.nextbutton();
        integer currentPage = searchEx.currentPage;
        integer maxSize =searchEx.maxSize;
        boolean hasNext =searchEx.hasNext;
        boolean hasPrevious =searchEx.hasPrevious;
        integer pageNumber =searchEx.pageNumber;
        searchEx.first();
        searchEx.last();
        searchEx.next();
        searchEx.previous();
       
        Test.stopTest();  
        
        
        
        
    }
    
    
    static testmethod void SearchbyName(){
        // Initial Data
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search Individual By Full Name
        searchEx.hdCusType ='Individual';
        searchEx.hdIdType = 'info';
        searchEx.account.First_Name__c ='Tin';         
        searchEx.search();
        searchEx.account.First_Name__c ='Tinnakrit';
        searchEx.account.Last_Name__c ='Kidmai';           
        searchEx.search();  
        
        
        
        
    }
    
    static testmethod void SearchbyPhone(){
        // Initial Data
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search Individual By Phone
        searchEx.hdCusType ='Individual';
        searchEx.hdIdType = 'info';
        searchEx.account.Office_Number_Temp__c ='053532198';
        searchEx.account.Mobile_Number_Temp__c ='053532198'; 

        
        Test.startTest();            
        searchEx.search();    
        Test.stopTest();  
        
        
        
        
    }
    
    
    static testmethod void SearchbyCUSTID(){
        // Initial Data
        Search__c acc = new Search__c();  
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search Individual By Cust ID
        searchEx.hdCusType ='Individual';
        searchEx.hdIdType = 'info';
        searchEx.account.TMB_Customer_ID_Temp__c ='053532198';

        
        Test.startTest();            
        searchEx.search();    
        Test.stopTest();  
    }
    
    
    static testmethod void InvalidPrerequisite(){
        // Initial Data
        Search__c acc = new Search__c(); 
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search 
        searchEx.search();  
        searchEx.hdCusType = 'Individual';
        searchEx.search(); 
    }
    
    static testmethod void InvalidCitizen(){
        // Initial Data
        Search__c acc = new Search__c();         
        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search 
        Test.startTest();
        searchEx.hdCusType = 'Individual';
        searchEx.hdIdType = 'Citizen ID';
        searchEx.search();  
        searchEx.hdIdType = 'Alien ID';
        searchEx.hdCheckBoxMode = 'info';
        searchEx.search();
        searchEx.hdCheckBoxMode = 'cust';
        searchEx.search(); 
        searchEx.hdIdType = 'Citizen ID';
        searchEx.account.ID_Number_Temp__c ='151515151';
        searchEx.isIDValid = false;
        searchEx.search(); 
        searchEx.hdCheckBoxMode = 'info';
        searchEx.hdIdType = null;
        searchEx.search();
        Test.stopTest();
    }
 
     static testmethod void SearchDuplicated(){
        // Initial Data
        Search__c acc = new Search__c();  
        Account acct = TestUtils.createAccounts(1, 'CitizenID', 'Individual', false).get(0);
         acct.ID_Type_Temp__c ='Citizen ID'; 
         acct.ID_Number_Temp__c ='1510100100181965'; 
         acct.Customer_Type__c = 'Individual';
         insert acct;
         
        Id [] fixedSearchResults = new Id[]{acct.Id};
        Test.setFixedSearchResults(fixedSearchResults);

        PageReference searchPage = Page.SearchProspectExV4;
        Test.setCurrentPage(searchPage);
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        SearchControllerExV4 searchEx = new SearchControllerExV4(sc);   
        // Search Individual By ID
        searchEx.hdCusType ='Individual';
        searchEx.hdIdType = 'Citizen ID';
        searchEx.account.ID_Number_Temp__c ='1510100100181965'; 
                    
        searchEx.search();  
        acct.OwnerId = Userinfo.getUserId();
         update acct;
         searchEx.search();
        
        
        
    }
    

}*/