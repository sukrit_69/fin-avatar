public without sharing class GroupPerformanceRefreshEntranceBatch {}
/*****************************************************************************************

*  NOTE : GroupPerformanceRefreshEntranceBatch 
*         สำหรับ Refresh เคสที่ยังไม่มี Account Plan
*
******************************************************************************************/

/*Comment Cleansing Code global without sharing class GroupPerformanceRefreshEntranceBatch implements Database.Batchable<sObject> ,Database.AllowsCallouts{
    
    // Account Plan Year
    /*Comment Cleansing Code public string m_year {get;set;}
    //  Support
    public Set<Id> m_accountWithAccountPlan    {get;set;}
    public Set<Id> m_accountWithoutAccountPlan {get;set;} 
    
    public id m_groupId {get;set;} 
    public id m_groupProfileId {get;set;}
    
    global GroupPerformanceRefreshEntranceBatch (Set<Id> accountWithAccountPlan,Set<Id> accountWithoutAccountPlan,Id groupId,string year,id groupProfileId){
        
        m_accountWithAccountPlan    =  accountWithAccountPlan ;
        m_accountWithoutAccountPlan =  accountWithoutAccountPlan;
        m_year = year;
        m_groupId = groupId;
        m_groupProfileId = groupProfileId;
        
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){  
        
        //Get Account to Process
        Set<Id> accountIds = m_accountWithoutAccountPlan ;
        string year = m_year ;
        return Database.getQueryLocator([
            SELECT Id, Name ,Account_Plan_Flag__c,  Group__c,Group__r.Name ,Owner.Segment__c   
            FROM Account 
            WHERE   Id IN : accountIds 
        ]);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<id> accountIds = (new Map<Id,SObject>(scope)).keySet();
        // Logic to be Executed batch wise      
        AccountPlanRefreshService.RefreshProductStrategyPort(accountIds,this.m_year);
    }
    
    global void finish(Database.BatchableContext BC){
        if(m_accountWithAccountPlan.size() > 0){
        	 GroupPerformanceRefreshSecondBatch batch =     
        	 new GroupPerformanceRefreshSecondBatch( m_accountWithAccountPlan , m_accountWithoutAccountPlan ,m_groupId, m_year,m_groupProfileId); 
             Database.executeBatch(batch ,25);
        }
        else{  
    		 GroupPerformanceRefreshThirdBatch batch =  new GroupPerformanceRefreshThirdBatch( m_accountWithAccountPlan, m_accountWithoutAccountPlan,m_groupId, m_year,m_groupProfileId); 
             Database.executeBatch(batch ,25);
	     
		}
        
    }
}*/