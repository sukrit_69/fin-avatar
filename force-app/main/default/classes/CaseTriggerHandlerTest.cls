//Test Class
@isTest 
public class CaseTriggerHandlerTest {

    static List<Service_Type_Matrix__c> serviceTypeList;
    static List<Case> caseList;

    static User adminUser{
        get
        {
            if(adminUser == null){
                adminUser = [SELECT Id,Name FROM User WHERE IsActive = true AND ProfileId =: TestUtils.SYSADMIN_PROFILE_ID LIMIT 1];
            }
            return adminUser;
        }
        set;
    }

    static User branchUser{
        get
        {
            if(branchUser == null){
                branchUser = [SELECT Id,Name,RTL_Branch_Code__c,UserRole.Name FROM User WHERE IsActive = true AND ProfileId =: RTL_TestUtility.RTL_BRANCH_SALES_PROFILE_ID AND RTL_Branch_Code__c = '001' LIMIT 1 ];
            }
            return branchUser;
        }
        set;
    }

    static void setupData(){

        System.runAs(adminUser){
            Group queue = new Group();
            queue.Name = 'test queue';
            queue.DeveloperName = 'test_queue';
            queue.type = 'Queue';
            insert queue;
            
            QueuesObject qsObject = new QueueSObject();
            qsObject.QueueId = queue.Id;
            qsObject.SobjectType = 'Case';
            insert qsObject;

            Group rmcGroup = [SELECT Id,Name,DeveloperName,Type FROM Group WHERE Type='Role' AND DeveloperName = 'RMC'];
            List<GroupMember> groupMemberList = new List<GroupMember>();
            GroupMember member1 = new GroupMember();
            member1.UserOrGroupId = adminUser.id;
            member1.GroupId = queue.Id;
            groupMemberList.add(member1);

            GroupMember member2 = new GroupMember();
            member2.UserOrGroupId = rmcGroup.id;
            member2.GroupId = queue.Id;
            groupMemberList.add(member2);

            insert groupMemberList;

            TestUtils.createAppConfig();
        } 

        Account acct = TestUtils.createAccounts(1,'TESTACCOUNT','Individual', true).get(0);

        serviceTypeList = new List<Service_Type_Matrix__c>();
        caseList = new List<Case>();

        Service_Type_Matrix__c serviceType = New Service_Type_Matrix__c();
        serviceType.Segment__c = 'SBG';
        serviceType.Service_Level1__c = 'Service Level1';
        serviceType.Service_Level2__c = 'Service level2';
        serviceType.Service_Level3__c = 'Service level3';
        serviceType.Service_Level4__c = 'Service level4';
        serviceType.SLA__c = 7;
        serviceType.Severity__c = '1';
        serviceType.Responsible_BU__c = 'Test_Queue_SE';
        serviceType.active__c = true;
        serviceTypeList.add(serviceType);

        Case caseObj = New Case(); 
        //case.recordtypeID = SErecordType.id;
        caseObj.Subject = 'TestCase';
        caseObj.PTA_Segment__c = 'SBG';
        caseObj.Category__c = 'Service Level1';
        caseObj.Sub_Category__c = 'Service level2';
        caseObj.Product_Category__c = 'Service level3';
        caseObj.Issue__c = 'Service level4';
        caseObj.Status = 'New';
        caseObj.Description = 'Test create Case';
        caseObj.AccountId = acct.id;
        caseObj.Root_Cause_List__c = 'TMB_ระบบขัดข้อง';
        caseObj.Resolution_LIst__c = 'IT ดำเนินการแก้ไขระบบ';
        caseList.add(caseObj);

    }
    
    static testMethod void runTestClass() {
         Test.startTest();
         	//List<User> adminuser = [Select ID, Name from user where isActive = true  and profileID in: [Select ID from profile where name = 'System Administrator'] limit 2];
         	system.runAs(adminUser){
            //Create Customer
            TestUtils.createAppConfig();
            Account acct = TestUtils.createAccounts(1,'TESTACCOUNT','Individual', true).get(0);

             //Create Entitlement
             Recordtype   typeID = [Select ID, Name from Recordtype where SobjectType = 'Entitlement' and Name = 'With Business Hours' limit 1];   
             Entitlement ent = new Entitlement(Name='Testing', AccountId= acct.Id, StartDate=Date.valueof(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)), SLA_Day__c = 7, recordtypeID = typeID.id);
             insert ent;
             
             //Create Service Type matrix
             Service_Type_Matrix__c SeMatrix = New Service_Type_Matrix__c();
             SeMatrix.Segment__c = 'SBG';
             SeMatrix.Service_Level1__c = 'Service Level1';
             SeMatrix.Service_Level2__c = 'Service level2';
             SeMatrix.Service_Level3__c = 'Service level3';
             SeMatrix.Service_Level4__c = 'Service level4';
             SeMatrix.SLA__c = 7;
             SeMatrix.Severity__c = '1';
             SeMatrix.Responsible_BU__c = 'Test Queue SE';
             SeMatrix.active__c = true;
             SeMatrix.Closed_By_BU__c = 'Test Queue SE';
             insert SeMatrix;
             
             //Create Queue         
             Group que = new Group(Name='Test Queue SE', DeveloperName = 'Test_Queue_SE', type='Queue');
             insert que;
             QueuesObject q1 = new QueueSObject(QueueID = que.id, SobjectType = 'Case');
             insert q1;
             
             //Create Case
             Recordtype SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center']; 
             List<Case> list_case = New list<Case>();
             Case caseNew1 = New Case(); 
             caseNew1.recordtypeID = SErecordType.id;
             caseNew1.Subject = 'TestCase';
             caseNew1.PTA_Segment__c = 'SBG';
             caseNew1.Category__c = 'Service Level1';
             caseNew1.Sub_Category__c = 'Service level2';
             caseNew1.Product_Category__c = 'Service level3';
             caseNew1.Issue__c = 'Service level4';
             caseNew1.Status = 'New';
             caseNew1.Description = 'Test create Case';
             caseNew1.AccountId = acct.id;
             caseNew1.CCRP_Number__c = '11-99770-9';
             list_case.add(caseNew1);
             insert list_case;
             
             Case caseRecord = [Select ID, OwnerID, EntitlementId, Case_Severity__c from Case where id =: caseNew1.id];
             system.assertEquals(que.id, caseRecord.ownerID);
             system.assertEquals('1', caseRecord.Case_Severity__c);
             system.assertEquals(ent.ID, caseRecord.EntitlementId);
             
             /*caseNew1.Category__c = 'Test Service Level1';
             caseNew1.OwnerID = adminUser.id;
             Update caseNew1;
             */
             List<Customer_Complaint__c> list_ccrp = New List<Customer_Complaint__c>();
             Customer_Complaint__c ccrp1 = New Customer_Complaint__c();
             ccrp1.Account__c = acct.id;
             ccrp1.TMB_Care__c = '11-99770-9';
             ccrp1.Name = 'testCCRP1';
             ccrp1.Cust_Complaint_Key__c = '001000000066666-11-99770-9';
             ccrp1.Category__c = 'Service Level1';
             ccrp1.Sub_Category__c = 'Service Level2';
             ccrp1.Product_Category__c = 'Service Level2';
             ccrp1.Issue__c = 'Service Level2';
             ccrp1.Issue_Description__c = 'Test comments';
             ccrp1.Status__c = 'CLOSED';
             list_ccrp.add(ccrp1);
            
             
             Customer_Complaint__c ccrp2 = New Customer_Complaint__c();
             ccrp2.Account__c = acct.id;
             ccrp2.TMB_Care__c = '11-99770-8';
             ccrp2.Name = 'testCCRP2';
             ccrp2.Cust_Complaint_Key__c = '001000000066666-11-99770-8';
             ccrp2.Category__c = 'Service Level1';
             ccrp2.Sub_Category__c = 'Service Level2';
             ccrp2.Product_Category__c = 'Service Level2';
             ccrp2.Issue__c = 'Service Level2';
             ccrp2.Issue_Description__c = 'Test comments';
             ccrp2.Status__c = 'Open';
             list_ccrp.add(ccrp2);
             insert list_ccrp;
             
            /* 
             Case case_compare1 = [Select ID, OwnerID, IsClosed from Case where id =: caseNew1.id];
             system.assertEquals(True, case_compare1.IsClosed); */
             
             ccrp2.Status__c = 'CANCEL';
             update ccrp2;
             
             /*
             Case case_compare2 = [Select ID, OwnerID,IsClosed from Case where id =: caseNew2.id];
             system.assertEquals(True, case_compare2.IsClosed); */

             Test.stopTest();
         }
     }

    static testMethod void testWithClosedByBURegister() {
        setupData();
        serviceTypeList[0].Closed_By_BU__c = 'REGISTER';
        insert serviceTypeList;
        insert caseList;

        Test.startTest();
            caseList[0].Status = 'Resolved';
            update caseList;

            System.runAs(adminUser){
                try{
                    caseList[0].Status = 'Completed';
                    update caseList;
                }catch(Exception e){
                    System.debug(e.getMessage());
                    System.assertEquals(e.getMessage().contains('Case close failed, Logged in User is not match to Close by BU'),true);
                }
            }
            
        Test.stopTest();
    }

    static testMethod void testWithClosedByBU() {
        setupData();
        serviceTypeList[0].Closed_By_BU__c = 'test queue';
        insert serviceTypeList;
        insert caseList;

        Test.startTest();
            caseList[0].Status = 'Resolved';
            update caseList;

            Case caseObj = [SELECT Id,Subject,Owner.Name FROM Case WHERE Id=: caseList[0].id];
            System.assertEquals('test queue',caseObj.Owner.Name);

            caseList[0].Status = 'Completed';
            update caseList;
        Test.stopTest();
    }

    static testMethod void testWithWrongClosedByBU() {
        setupData();
        serviceTypeList[0].Closed_By_BU__c = 'testqueue';
        insert serviceTypeList;
        insert caseList;

        Test.startTest();
            caseList[0].Status = 'Resolved';
            update caseList;

            Case caseObj = [SELECT Id,Subject,Owner.Name FROM Case WHERE Id=: caseList[0].id];
            System.assertEquals(UserInfo.getUserId(),caseObj.OwnerId);

        Test.stopTest();
    }

    static testMethod void testWithoutClosedByBU() {
        setupData();

        insert serviceTypeList;
        insert caseList;

        Test.startTest();
            caseList[0].Status = 'Resolved';
            update caseList;
        Test.stopTest();
    }

}