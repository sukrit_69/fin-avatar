public without sharing class fna_QuestionnaireTemplateCtrl {
    
    public class QuestionResponse{
        @AuraEnabled
        public boolean success {get;set;}
        @AuraEnabled
        public String nextPage {get;set;} //Id question_template
        @AuraEnabled
        public String idForm {get;set;} // Id question_form
        @AuraEnabled
        public String errMsg {get;set;}
    }
    
    public class Template {
        @AuraEnabled
        public Boolean template1 {get;set;}
        @AuraEnabled
        public Boolean template2 {get;set;}
        @AuraEnabled
        public Boolean template3 {get;set;}
        @AuraEnabled
        public Boolean template4 {get;set;}
        @AuraEnabled
        public Boolean template5 {get;set;}
        @AuraEnabled
        public Boolean template6 {get;set;}
        
    }

    public class Choice{
        @AuraEnabled
        public Integer id {get;set;}
        @AuraEnabled
        public String choice {get;set;}
        @AuraEnabled
        public String urlImage {get;set;}
        @AuraEnabled
        public String btnClass {get;set;}
    }

    public class QuestionTemplate{
        @AuraEnabled
        public String questionId {get;set;}
        @AuraEnabled
        public String question {get;set;}
        @AuraEnabled
        public Template template {get;set;}
        @AuraEnabled
        public List<Choice> choiceObj {get;set;}
        @AuraEnabled
        public Map<String, String> nextQuestion {get;set;}
        @AuraEnabled
        public String idForm {get;set;}
        @AuraEnabled
        public String answer {get;set;}
        @AuraEnabled
        public Map<String, String> answerAvatar {get;set;}
        @AuraEnabled
        public String previousQuestion {get;set;}
    }

    @AuraEnabled
    public static QuestionTemplate getQuestion(String thisQuestion, String thisForm, String preQuestion){
        String questionId = thisQuestion;
        String questionName = '';
        system.debug('Question Id' + questionId);
        system.debug('thisForm' + thisForm);
        QuestionTemplate res = new QuestionTemplate();
        List<Questionnaire_Template__c> questionnaireList = new List<Questionnaire_Template__c>();
        if(questionId != ''){
            questionnaireList = [SELECT Id, Name, Template__c, Dependent__c, Template__r.name, Project__c, Question__c, Version__c, 
                                                                 (SELECT Id, Name, Answer__c, Image_Title__c, Questionnaire_Template__c, Next_Question__c, Answer_Avatar__c
                                                                  From Choice_Templates__r) FROM Questionnaire_Template__c WHERE id =:questionId];
        }
        else{
            questionnaireList = [SELECT Id, Name, Template__c, Dependent__c, Template__r.name, Project__c, Question__c, Version__c, 
                                                                 (SELECT Id, Name, Answer__c, Image_Title__c, Questionnaire_Template__c, Next_Question__c, Answer_Avatar__c
                                                                  From Choice_Templates__r) FROM Questionnaire_Template__c WHERE default__c = true];
        }
        system.debug('questionnaireList ' + questionnaireList);
        if(questionnaireList.size() > 0){
            
            Questionnaire_Template__c question = questionnaireList[0];
            Map<String, String> titleImage = new Map<String, String>();
            questionName = question.Name;
            res.questionId = question.id;
            res.question = question.Question__c;
            res.template = defineTemplate(question.Template__r.name);
            res.choiceObj = new List<Choice>();
            res.nextQuestion = new Map<String, String>();
            res.answerAvatar = new Map<String, String>();            
            res.idForm = thisForm == null ? '' : thisForm;
            Integer x = 0;
            
            for(Choice_Template__c choice : question.Choice_Templates__r){
                system.debug('choice ' + choice);
                x++;
                Choice tmpChoice = new Choice();
                tmpChoice.id = x;
                tmpChoice.choice = choice.Answer__c;
                res.choiceObj.add(tmpChoice);

                //res.choice.put(x,choice.Answer__c);
                if(choice.Answer__c == null || choice.Answer__c == ''){
                    res.nextQuestion.put('fill input', choice.Next_Question__c);
                }else{
                    res.nextQuestion.put(choice.Answer__c, choice.Next_Question__c);
                }

                res.answerAvatar.put(choice.Answer__c, choice.Answer_Avatar__c);

                if(choice.Image_Title__c != '' && choice.Image_Title__c != null){
                    titleImage.put(choice.Answer__c , choice.Image_Title__c);
                }
                tmpChoice.btnClass= 'slds-button slds-button_neutral fullbutton';
                
            }

            if(question.Dependent__c != null){
                List<Questionnaire__c> questionList = [SELECT Id, Question__c, Answer__c, Questionnaire_Form__c, Questionnaire_Template__c FROM Questionnaire__c WHERE Questionnaire_Form__c =:thisForm AND Questionnaire_Template__c= :question.Dependent__c];
                system.debug('questionList : ' + questionList);
                     
                if(questionList.size() > 0){
                    if(questionList[0].Answer__c == 'ชาย'){
                        titleImage.put(questionList[0].Answer__c , 'male_circle');
                    }else{
                        titleImage.put(questionList[0].Answer__c , 'female_circle');
                    }
                    res.choiceObj[0].choice = questionList[0].Answer__c;
                }
            }

            if(titleImage.size() > 0){
                Map<String, String> temp = getImageUrl(titleImage);
                for(Choice tempChoice : res.choiceObj){
                    for(String imageKey : temp.keySet()){
                        if(tempChoice.choice == imageKey){
                            tempChoice.urlImage = temp.get(imageKey);
                        }
                    }
                }
            }

            
            system.debug('res choice ' + res.choiceObj);
        } 

        if(thisForm != ''){
            List<Questionnaire_Form__c> questionList = [SELECT Id, Avatar_Master__c, Drop_Off_URL__c , Drop_Off_Page__c, (Select Id, Question__c, Answer__c FROM Questionnaires__r WHERE Questionnaire_Template__c=:questionId) FROM Questionnaire_Form__c WHERE id=:thisForm];
            if(questionList.size() > 0){
                system.debug('questionList[0].Questionnaires__r' + questionList[0].Questionnaires__r );
                if(questionList[0].Questionnaires__r != null && questionList[0].Questionnaires__r.size() > 0){
                    res.answer = questionList[0].Questionnaires__r[0].Answer__c;
                }
                questionList[0].Drop_Off_Page__c = 'questionnaire=' + questionName;
                questionList[0].Drop_Off_URL__c = '/fin/s/questionnaire?qid=' + questionId + '&id=' + thisForm;
                update questionList;
            }
        }

        res.previousQuestion = preQuestion;
        return res;
    }

    public Static Map<String, String> getImageUrl(Map<String, String> titleImage){

        List<ContentVersion> cvsList = [SELECT Id, Title, VersionData, ContentDocumentId FROM ContentVersion where Title in :titleImage.values()];    
        Map<String, String> b64 = new Map<String, String>();
        for(String keyTiltle : titleImage.keySet()){
            for(ContentVersion cvs : cvsList){
                if(cvs.title == titleImage.get(keyTiltle)){
                    b64.put(keyTiltle, 'data:image/png;base64, ' + EncodingUtil.base64Encode(cvs.VersionData));
                }
            }
        }
        
        return b64;

    }

    public Static Template defineTemplate(String idTemplate){
        Template resTemplate = new Template();
        switch on idTemplate {
            when 'Template#1' {
                resTemplate.template1 = true;
            }
            when 'Template#2' {
                resTemplate.template2 = true;
            }
            when 'Template#3' {
                resTemplate.template3 = true;
            }
            when 'Template#4' {
                resTemplate.template4 = true;
            }
            when 'Template#5' {
                resTemplate.template5 = true;
            }
            when 'Template#6' {
                resTemplate.template6 = true;
            }
        }
        return resTemplate;
    }   
    
    @AuraEnabled
    public static QuestionResponse saveQuestionInformation(QuestionTemplate thisQuestion, String answerQuestion){
        system.debug('saveQuestionInformation');
        system.debug('thisQuestion : ' + thisQuestion);
        system.debug('answerQuestion : ' + answerQuestion);

        String originalAnswer = '';
        Boolean isInput = false;
        // system.debug('thisQuestion.idForm : ' + thisQuestion.idForm);
        QuestionResponse questionRes = new QuestionResponse();
        Questionnaire_Form__c objquestionnaireForm = new Questionnaire_Form__c();  
        if(thisQuestion.idForm == '' || thisQuestion.idForm == null){
            upsert objquestionnaireForm;
            thisQuestion.idForm = objquestionnaireForm.id;
          
        }
        
        system.debug('thisQuestion.questionId : ' + thisQuestion.questionId);
        system.debug('thisQuestion.idForm : ' + thisQuestion.idForm);
        List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c,Questionnaire_Template__r.IsInput__c FROM Questionnaire__c WHERE Questionnaire_Template__c =: thisQuestion.questionId AND Questionnaire_Form__c =: thisQuestion.idForm];
        system.debug('questionnaire_DB : ' + questionnaire_DB);

        //  questionnaire_DB <<
        if(questionnaire_DB.isEmpty()){
            Questionnaire__c objQuestionnaire = new Questionnaire__c();
            objQuestionnaire.Question__c = thisQuestion.question;
            objQuestionnaire.Answer__c = answerQuestion;
            objQuestionnaire.Questionnaire_Template__c = thisQuestion.questionId;
            objQuestionnaire.Questionnaire_Form__c = thisQuestion.idForm;
            system.debug('questionnaire (if) : ' + objQuestionnaire);
            upsert objQuestionnaire;
        }
        else{
            originalAnswer = questionnaire_DB[0].Answer__c;
            isInput = questionnaire_DB[0].Questionnaire_Template__r.IsInput__c;
            questionnaire_DB[0].Question__c = thisQuestion.question;
            questionnaire_DB[0].Answer__c = answerQuestion;
            system.debug('questionnaire (else) : ' + questionnaire_DB);
            upsert questionnaire_DB;            
            
        }


        //a
        Boolean haveAnswer = thisQuestion.nextQuestion.containsKey(answerQuestion);
        if(haveAnswer){
            questionRes.nextPage = thisQuestion.nextQuestion.get(answerQuestion);
        }else if(thisQuestion.nextQuestion.containsKey('fill input')){
            questionRes.nextPage = thisQuestion.nextQuestion.get('fill input');
        }else if(answerQuestion.isNumeric()){
            //answerQuestion = changeAgetoChoice(answerQuestion);
            questionRes.nextPage = thisQuestion.nextQuestion.get(changeAgetoChoice(answerQuestion));
            
        }else{
            questionRes.nextPage = '';
        }

        List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id, Answer_Flow__c FROM Questionnaire_Form__c WHERE Id =: thisQuestion.idForm];
        system.debug('Questionnaire_Form__c : ' + questionnaireForm_DB);

        if(questionnaireForm_DB[0].Answer_Flow__c == null){
            questionnaireForm_DB[0].Answer_Flow__c = thisQuestion.answerAvatar.get(answerQuestion);
        }
        else{
            String answerQuestionSet = answerQuestion;
            if(answerQuestion.isNumeric()){
                answerQuestionSet = changeAgetoChoice(answerQuestion);
            }
            
                /*if(thisQuestion.answerAvatar.get(answerQuestionSet) == null && thisQuestion.questionId != 'a3U0l000000qlurEAA'){
                    questionnaireForm_DB[0].Answer_Flow__c += 'X';
                }   
                else{
                    if(questionnaireForm_DB[0].Answer_Flow__c.length() == 1 ){
                    	questionnaireForm_DB[0].Answer_Flow__c += '';
                    }else{
                        questionnaireForm_DB[0].Answer_Flow__c += thisQuestion.answerAvatar.get(answerQuestionSet);
                    }
                }*/
                if(thisQuestion.questionId == 'a3U0l000000qluhEAA'){//Q13
                    	questionnaireForm_DB[0].Answer_Flow__c = thisQuestion.answerAvatar.get(answerQuestionSet);
                    }else if(thisQuestion.questionId == 'a3U0l000000qmKkEAI' && questionnaireForm_DB[0].Answer_Flow__c.length() == 2){//Q0
        				questionnaireForm_DB[0].Answer_Flow__c += '';
                    }else if(thisQuestion.questionId == 'a3U0l000000qluiEAA' && questionnaireForm_DB[0].Answer_Flow__c.length() == 3){//Q1
        				questionnaireForm_DB[0].Answer_Flow__c += '';
                    }else if(thisQuestion.answerAvatar.get(answerQuestionSet) == null && thisQuestion.questionId != 'a3U0l000000qlurEAA'){//Q12
                        questionnaireForm_DB[0].Answer_Flow__c += 'X';
                    } else{
                        questionnaireForm_DB[0].Answer_Flow__c += thisQuestion.answerAvatar.get(answerQuestionSet);
                    }
            
        }

        system.debug('thisQuestion.questionId : ' + thisQuestion.questionId);
        system.debug('questionnaireForm_DB[0].Answer_Flow__c : ' + questionnaireForm_DB[0].Answer_Flow__c);
    

        questionnaireForm_DB[0].Avatar_Master__c = saveAvatarMaster(thisQuestion, answerQuestion, questionnaireForm_DB);
        
        update questionnaireForm_DB;
        
        //if(!questionnaire_DB.isEmpty()){
            system.debug('answerQuestion : ' + answerQuestion);
            system.debug('originalAnswer : ' + originalAnswer);
             system.debug('isInput : ' + isInput);
            if(!isInput){
                if(answerQuestion !=  originalAnswer){
                    changeFlow(thisQuestion.idForm, thisQuestion.questionId, thisQuestion.previousQuestion);
                }
            }
        //}
        
        system.debug('thisQuestion.nextQuestion ' + thisQuestion.nextQuestion);
        system.debug('thisQuestion.haveAnswer ' + haveAnswer);
        system.debug('thisQuestion.nextPage ' + questionRes.nextPage);

        questionRes.success = true;
        questionRes.errMsg = '';
        questionRes.idForm = thisQuestion.idForm;
        /// save Question

        return questionRes;
    }

    public static void changeFlow(String idForm, String thisQuestion, String previousQuestion){

        // String idForm =  'a3W0l00000060etEAA';
        // String thisQuestion = 'a3U0l000000qlurEAA';
        // String previousQuestion = 'a3U0l000000qlukEAA';
        system.debug('previousQuestion ' + previousQuestion);
        List<Questionnaire__c> deleteQuestionnaire = new List<Questionnaire__c>();
        if(previousQuestion != '' && previousQuestion != null){
            List<Questionnaire__c> questionnaire_DB = [SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c, Questionnaire_Form__c  FROM Questionnaire__c WHERE Questionnaire_Form__c =: idForm ORDER BY CreatedDate DESC]; 
            for(Questionnaire__c ques : questionnaire_DB){
            
                if(ques.Questionnaire_Template__c == previousQuestion){
                    break;
                }
                system.debug('ques.Questionnaire_Template__c ' + ques.Questionnaire_Template__c);
                system.debug('thisQuestion >> ' + thisQuestion);

                if(ques.Questionnaire_Template__c != thisQuestion ){
                    system.debug('ques >> ' + ques);
                    deleteQuestionnaire.add(ques);
                }
                system.debug('temp ' + deleteQuestionnaire);
            }
        }
        
        if(deleteQuestionnaire.size() > 0){
            system.debug('deleteQuestionnaire ' + deleteQuestionnaire);
            delete deleteQuestionnaire;
        }
    }

    public static String saveAvatarMaster(QuestionTemplate thisQuestion, String answerQuestion, List<Questionnaire_Form__c> qForm_DB){
        String temp = thisQuestion.nextQuestion.get(answerQuestion);
        String tempDB;
        if(temp == null || temp == ''){    
            List<AvatarMappingResults__c> avatarMap_DB = [SELECT Id, Avatar__c, Answer2__c, AvatarMaster__c FROM AvatarMappingResults__c WHERE Answer2__c =: qForm_DB[0].Answer_Flow__c];
            if(avatarMap_DB.isEmpty()){//default avatar
                List<Avatar_Master__c> avatarMap_DB_Default = [SELECT Id, Avatar_Image__c, Segment__c, Avatar_EN__c, Avatar_TH__c, Avatar_Description__c FROM Avatar_Master__c WHERE Segment__c =: 'Default Avatar'];
                system.debug('avatarMap_DB_Default : ' + avatarMap_DB_Default);       
                // qForm_DB[0].Avatar_Master__c = avatarMap_DB_Default[0].Id;
                tempDB = avatarMap_DB_Default[0].Id;
            }
            else {// out of  default avatar
                // List<Avatar_Master__c> avatarMap_DB_OutOfDefault = [SELECT Id, Avatar_Image__c, Segment__c, Avatar_EN__c, Avatar_TH__c, Avatar_Description__c  FROM Avatar_Master__c WHERE Id =: avatarMap_DB[0].AvatarMaster__c];
                // system.debug('avatarMap_DB : ' + avatarMap_DBZ[0].Id);      
                // system.debug('avatarMap_DB_OutOfDefault : ' + avatarMap_DB_OutOfDefault); 
                system.debug('avatarMap_DB : ' + avatarMap_DB); 
                // qForm_DB[0].Avatar_Master__c = avatarMap_DB[0].AvatarMaster__c;
                tempDB = avatarMap_DB[0].AvatarMaster__c;
            }
        }
        return tempDB;
    }

    public static String changeAgetoChoice(String age){
        integer ageNumber = integer.valueof(age);
        String choiceText = '';
        if(ageNumber < 22){
            choiceText = 'น้อยกว่า 22 ปี';
        }else if(ageNumber >= 22 && ageNumber <= 27){
            choiceText = '22-27 ปี';
        }else if(ageNumber >= 28 && ageNumber <= 40){
            choiceText = '28-40 ปี';
        }else if(ageNumber >= 41 && ageNumber <= 50){
            choiceText = '41-50 ปี';
        }else if(ageNumber >= 51 && ageNumber <= 65){
            choiceText = '51-65 ปี';
        }else{
            choiceText = 'มากกว่า 65 ปี';
        }
        return choiceText;
    }

    @AuraEnabled
    public static String previousQuestionInformation(String idForm, String idQuestion){
        String previousQuestionId = '';
        system.debug('previousQuestionInformation');
        system.debug('idForm' + idForm);
        system.debug('idQuestion' + idQuestion);

        //Delete Answer from Answer_Flow__c when click Back
        List<Questionnaire_Form__c> questionnaireForm_DB = [SELECT Id, Answer_Flow__c, (SELECT Id, Name, Question__c, Answer__c, Questionnaire_Template__c,Questionnaire_Template__r.Name, Questionnaire_Form__c FROM Questionnaires__r ORDER BY CreatedDate DESC) FROM Questionnaire_Form__c WHERE Id =: idForm];
        if(questionnaireForm_DB.size() > 0){
            questionnaireForm_DB[0].Answer_Flow__c = questionnaireForm_DB[0].Answer_Flow__c.substring(0,questionnaireForm_DB[0].Answer_Flow__c.length()-1);
            
            
            system.debug('After back : questionnaireForm_DB[0].Answer_Flow__c : ' + questionnaireForm_DB[0].Answer_Flow__c);
            /*if(questionnaireForm_DB[0].Answer_Flow__c.length() == 0){
                questionnaireForm_DB[0].Answer_Flow__c = '';
            }*/
            upsert questionnaireForm_DB;
            
            //Redirect when click back
            if(questionnaireForm_DB[0].Questionnaires__r != null && questionnaireForm_DB[0].Questionnaires__r.size() > 0){
                List<String> temp = new List<String>();
                for(Questionnaire__c ques : questionnaireForm_DB[0].Questionnaires__r){
                    temp.add(ques.Questionnaire_Template__c);
                    system.debug('temp ' + temp);
                }

                if(temp.indexOf(idQuestion) != questionnaireForm_DB[0].Questionnaires__r.size() - 1){
                    previousQuestionId = temp[temp.indexOf(idQuestion) + 1];
                }
            }

        }
        
        system.debug('previousQuestionId : ' + previousQuestionId);
        return previousQuestionId;
    }

}