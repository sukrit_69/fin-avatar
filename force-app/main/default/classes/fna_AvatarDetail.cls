public without sharing class fna_AvatarDetail {
     
   public class AvatarObj{
       @AuraEnabled
       public Avatar_Master__c avatar {get;set;}
       @AuraEnabled
       public String urlImage {get;set;}
       @AuraEnabled
       public String avatarName {get;set;}
   }
 
    @AuraEnabled
    public static AvatarObj getAvatarMasterDetail(String idForm){

        system.debug('idForm' + idForm);
        String avtarMasterId = '';
        List<Questionnaire_Form__c> questionFormList = [SELECT Id, Referral__c, Drop_Off_URL__c, Avatar_Master__c, Drop_Off_Page__c, Answer_Flow__c, CreatedDate FROM Questionnaire_Form__c WHERE id=:idForm];
        if(questionFormList.size() > 0 ){
            avtarMasterId = questionFormList[0].Avatar_Master__c;
            questionFormList[0].Drop_Off_Page__c = 'avatardetail';
            questionFormList[0].Drop_Off_URL__c = '/fin/s/avatardetail?Id=' + idForm;
            update questionFormList;
        }

        AvatarObj avatar = new AvatarObj();
        //System.debug('idAvatar ' + avtarMasterId)
        List<Avatar_Master__c> avtarList = [SELECT Id, Segment__c, Avatar_EN__c, Avatar_TH__c, Avatar_Description__c FROM Avatar_Master__c where id =: avtarMasterId];
        if(avtarList.size()> 0){

            avatar.avatar = avtarList[0];
            Map<String, String> name1 = getAvatarName(idForm);
            avatar.avatarName = name1.get('name');
            avatar.urlImage = getAvatarImageUrl(avtarList[0].Segment__c, name1.get('gender'));
        }
        return avatar;
    }

    public static String getAvatarImageUrl(String segment , String gender){

        if (gender=='ชาย'){
            gender ='_male';
        }else {
            gender ='_female';
        }

        if(segment != 'M2' && segment != 'UAF4' && segment != 'SE2' && segment != 'MAF1' ){
            segment = 'Default Avatar';
        }
        

        String avatartitle = segment + gender;

        system.debug('Avatar Title' + avatartitle);

        List<ContentVersion> cvs = [SELECT Id, Title, VersionData, ContentDocumentId FROM ContentVersion where Title = :avatartitle];    
        system.debug('CVS ' + cvs);
        String b64 = '';
        if(!cvs.isEmpty()) {
            b64 = EncodingUtil.base64Encode(cvs[0].VersionData);
        }
        return b64;
    
    }

    public static Map<String, String> getAvatarName(String idForm){

        List<Questionnaire__c> questionwho =[SELECT Id, Question__c, Answer__c, Questionnaire_Form__c, Questionnaire_Template__c FROM Questionnaire__c Where Questionnaire_Form__c =:idForm];
        system.debug('Avatarname ' + questionwho);
        Map<String, String> name1 = new Map<String, String>(); 


        for (Questionnaire__c item : questionwho) {
            if (item.Question__c == 'Avatar ของคุณ') {
                
                name1.put('gender', item.Answer__c);

            }else if (item.Question__c =='ตั้งชื่อให้กับ AVATAR ของคุณ'){
                name1.put('name',item.Answer__c);

            }
            

        }

        return name1;
    }
     
    /*public static String getAvatarImage(String segment){
        String resAvatar ='';
        segment = segment + '_male';
        switch on segment {
            when 'M2_female' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvm&operationContext=CHATTER&contentId=05T0l000005lvZg';
            }
            when 'M2_male' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvr&operationContext=CHATTER&contentId=05T0l000005lvZl';
            }
            when 'MAF1_female' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvs&operationContext=CHATTER&contentId=05T0l000005lvZq';
            }
            when 'MAF1_male' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvw&operationContext=CHATTER&contentId=05T0l000005lvZv';
            }
            when 'SE2_female' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvt&operationContext=CHATTER&contentId=05T0l000005lva0';
            }
            when 'SE2_male' {
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001Wyvn&operationContext=CHATTER&contentId=05T0l000005lvZh';
            }
            when 'UAF4_female'{
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001WywB&operationContext=CHATTER&contentId=05T0l000005lvaj';
            }
            when 'UAF4_male'{
                resAvatar = 'https://tmbbank--fna--c.cs58.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680l000001WywG&operationContext=CHATTER&contentId=05T0l000005lvat';
            }
        }
        return resAvatar; 

    }*/

}