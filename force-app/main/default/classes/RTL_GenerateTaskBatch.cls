global class RTL_GenerateTaskBatch implements Database.Batchable<sObject>, Database.stateful {
	Datetime batchDate = datetime.now();
	Datetime batchStartTime = datetime.now();
	Datetime batchEndTime = null;

	final String SEMICO = ',';
	final String jobDailyName = 'Generate Wealth Tasks';
	final String sObjectName = 'Task';

	String query;
	Integer countAll;
	Integer countAccError;
	Integer countTskError;

	List<Id> accIdErrorList;

	Map<Id, Task> tskErrorMap;
	Map<Id, String> tskErrorMsgMap;
	Map<Id, Account> accErrorMap;

	@TestVisible
	Map<String, Operating_Model__c> OPModelMap{
		get{
			if(OPModelMap != null) {
				return OPModelMap;
			}
			OPModelMap = new Map<String, Operating_Model__c>();
			
			Map<ID, Operating_Model__c> m = new Map<ID, Operating_Model__c>([SELECT Operating_Model_Name__c, Duration__c, Days_Month__c, Day_of_the_month__c, Day_of_due_date__c FROM Operating_Model__c]);
			
			for(Operating_Model__c OPModel : m.values()){
				OPModelMap.put(OPModel.Operating_Model_Name__c, OPModel);
			}
			return OPModelMap;
		}
		set;
	}
	
	global RTL_GenerateTaskBatch(String q) {
		System.debug(':::::::::::::::CONSTRUCTOR:::::::::::::::');
		accIdErrorList = new List<Id>();

		tskErrorMap = new Map<Id, Task>();
		tskErrorMsgMap = new Map<Id, String>();
		accErrorMap = new Map<Id, Account>();

		countAll = 0;
		countAccError = 0;
		countTskError = 0;
		query = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug(':::::::::::::::start:::::::::::::::');
		System.debug('query');
		System.debug(query);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account> scope) {
		System.debug(':::::::::::::::execute:::::::::::::::');
		date d = System.today();
		date NextGenDate;
		date DueDate;

		List<Task> tskList = new List<Task>();
		Map<Id, Account> AccMap = new Map<Id, Account>();

		String key = '';
		Operating_Model__c opm = new Operating_Model__c();
		countAll += scope.size();
		for (Account acc : scope) {
			if(acc.Operating_Model__c != null){
				key = acc.Operating_Model__c;
				opm = OPModelMap.get(key);
				
				if(opm != null){
					System.debug('UPDATE ACCOUNT : ' + acc);
					acc.Last_Generate_task_date__c = acc.Next_Generate_task_date__c;
					NextGenDate = date.newInstance(d.year(), d.month(), d.day());

					//UPDATE ACCOUNT NEXT GEN DATE 
					if(opm.Days_Month__c == 'Month'){
						//ADD MONTH
						NextGenDate = date.newInstance(d.year(), d.month(), 1);
						NextGenDate = NextGenDate.addMonths(Integer.valueOf(opm.Duration__c));

						//CHANGE DAY
						Integer genDay = d.day();
						
						if(opm.Day_of_the_month__c != null){
							genDay = Integer.valueOf(opm.Day_of_the_month__c);
						}

						if(genDay > Date.daysInMonth(NextGenDate.year(), NextGenDate.month())){
							NextGenDate = date.newInstance(NextGenDate.year(), NextGenDate.month(), Date.daysInMonth(NextGenDate.year(), NextGenDate.month()));
						}else{
							NextGenDate = date.newInstance(NextGenDate.year(), NextGenDate.month(), genDay);
						}

					}else if(opm.Days_Month__c == 'Days'){
						//CHANGE DAY
						NextGenDate = NextGenDate.addDays(Integer.valueOf(opm.Duration__c));
					}

					acc.Next_Generate_task_date__c = NextGenDate;
					
					System.debug('UPDATE ACCOUNT FINISH: ' + acc);
					AccMap.put(acc.id, acc);

					Task tsk = new Task();
					Id TaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Retail Task').getRecordTypeId();

					if(opm.Day_of_due_date__c != null){
						DueDate	= date.newInstance(d.year(), d.month(), d.day()).addDays(Integer.valueOf(opm.Day_of_due_date__c));
						tsk.ActivityDate = DueDate;
					}


					tsk.RTL_Task_Subject__c = 'Portfolio Review';
					tsk.RTL_Channel__c  = 'Wealth Management';
					tsk.Type = 'Visit';
					tsk.WhatId = acc.Id;
					tsk.OwnerId = acc.RTL_Wealth_RM__c;
					tsk.Priority = 'High';
					tsk.Description  = '[Operating Model:' + opm.Operating_Model_Name__c + ']';
					tsk.RecordTypeId = TaskRecordTypeId;
					tsk.ReminderDateTime = datetime.now();

					System.debug('Generate Task: ' + tsk);
					tskList.add(tsk);
				}
			}
		}
		
		if(tskList != null && tskList.size()>0){
			List<Database.SaveResult> srList = Database.insert(tskList, false);

			for (Integer i = 0 ; i < srList.size() ; i++){
				Database.SaveResult sr = srList.get(i);
				
				if (!sr.isSuccess()) {
					countTskError += 1;
					System.debug('sr : ' + sr);
					accIdErrorList.add(tskList.get(i).WhatId);
					tskErrorMap.put(tskList.get(i).WhatId, tskList.get(i));
					String errormsg = '';

					for(Database.Error err : sr.getErrors()) {
						errormsg += err.getStatusCode() + ':' + err.getMessage();
					}

					tskErrorMsgMap.put(tskList.get(i).WhatId, errormsg);
				}
			}
			// Database.insert(tskList, false);
			// insert tskList;
			System.debug('INSERT TASK: ' + tskList);
		}

		for(Id id : accIdErrorList){
			if(AccMap.get(Id) != null){
				accErrorMap.put(id, AccMap.get(Id));
				AccMap.remove(Id);
			}
		}

		if(AccMap != null && !AccMap.isEmpty()){
			Database.SaveResult[] srList = Database.update(AccMap.values(), false);

			for (Database.SaveResult sr : srList) {
				if (!sr.isSuccess()) {
					// countAccError += 1;
				}
			}
			System.debug('UPDATE ACCOUT : ' + AccMap.values());
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug(':::::::::::::::finish:::::::::::::::');
		batchEndTime = datetime.now();

		Integer recordsSuccessful = countAll - countTskError;
		Integer recordsFailed = countTskError;

		String csvHeaderStr = 'AccountId, TMB Customer ID, TaskOwnerId, Wealth RM Name, Description, DueDate, ErrorMessage, StatusCode';
		String CSV_BODY  = '';

		String[] errorMsg;

		System.debug('countAll : ' + countAll);
		System.debug('countAccError : ' + countAccError);
		System.debug('countTskError : ' + countTskError);
		
		System.debug('accIdErrorList : ' + accIdErrorList);
		
		for(Task tsk : tskErrorMap.values()){
			System.debug(tsk);
		}

		for(Id ID : tskErrorMsgMap.keySet()){
			System.debug(ID + ' : ' + tskErrorMap.get(ID) + ' : ' + tskErrorMsgMap.get(ID));

			errorMsg = tskErrorMsgMap.get(ID).split(':', 2); 

			System.debug(errorMsg[0] + ' : ' + errorMsg[1]);

			Task tsk = tskErrorMap.get(ID);
			Account acc = accErrorMap.get(ID);

			CSV_BODY = CSV_BODY + 
					tsk.WhatId + SEMICO +
					acc.TMB_Customer_ID_PE__c + SEMICO +
					tsk.OwnerId + SEMICO + 
					acc.RTL_Wealth_RM__r.Name + SEMICO +
					tsk.Description + SEMICO +
					tsk.ActivityDate + SEMICO +
					errorMsg[1] + SEMICO +
					errorMsg[0] + SEMICO + '\n';
		}
		

		System.debug(csvHeaderStr);
		System.debug(CSV_BODY);

		List<String> sendToEmail = new List<String>();
		sendToEmail.add('CRM Admin 2');
		// sendToEmail.add('BU');

		if(countAll > 0){
			//SEND MAIL
			RTL_BatchEmailService.SendFinishBatchSummaryEmail(batchDate
                                                        , batchStartTime
                                                        , batchEndTime
                                                        , recordsSuccessful
                                                        , recordsFailed
														, csvHeaderStr
                                                        , jobDailyName
                                                        , sObjectName
                                                        , sendToEmail
                                                        , CSV_BODY);
		}
	}
	
}