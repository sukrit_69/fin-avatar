@isTest
public with sharing class RTL_CampaignMemberTriggerHandlerTest {

    @testSetup static void setupData(){
        TestUtils.createAppConfig();

        Id recordTypeId  = [select Id from RecordType where SobjectType = 'Campaign' and DeveloperName=: 'Dummy_Campaign' and IsActive=true ].Id;
        
        RTL_Campaign_Running_No__c rn = new RTL_Campaign_Running_No__c( 
            Name ='Local Exclusive Campaign Running No.' , 
            DateValue__c='170717',
            Day__c='17',
            Month__c='07',
            Year__c='17',
            Running_No__c = '01' );
        insert rn;
        
        RTL_CampaignMember_Running_No__c cmrn = new RTL_CampaignMember_Running_No__c(
            Name = 'Campaign Member Running No',
            Running_No__c = '000000'
        );
        
        insert cmrn;
        
        Branch_and_zone__c bz = new Branch_and_zone__c();
        bz.Branch_Code__c = '611';
        bz.isActive__c = true;
        insert bz;

         List<Lead> leads = new List<Lead>();
        
        List<User> retailUsers = RTL_TestUtility.createRetailTestUserOppt(true);
        User firstUser = retailUsers[0];//outbound channel
        
        List<Account> accList = RTL_TestUtility.createAccounts(1,true);
        List<Opportunity> oppList = new List<Opportunity>();
        
        User retailUser = RTL_TestUtility.createRetailTestUser(true);
        System.runAs(retailUser) {
            RTL_TestUtility.createRetailMasterProducts(true);
            leads = RTL_TestUtility.createLeads(2,true);
            RTL_TestUtility.createInterestedProducts(leads, true);
            
            RTL_Interested_products_c__c nonPrimaryProduct = [select Is_Primary__c from RTL_Interested_products_c__c where Lead__c = :leads.get(0).Id and Is_Primary__c = false];
            nonPrimaryProduct.Is_Primary__c = true;
            //check result: the update should be failed because no more than one primary interested product is allowed
            boolean errMsgMatch = false;
            try {
                update nonPrimaryProduct;
            } catch (Exception ex) {
                
            }   
        }
        
        System.runAs(firstUser) {
            oppList = RTL_TestUtility.createOpportunity(accList,true);
        }
        Lead l = new Lead(Company = 'JohnMiller', LastName = 'Mike', Status = 'Open');
        l.RTL_Branch_Code_Rpt__c = '611';
        insert l;
        
        Campaign camp = new Campaign();
        camp.Name = 'Campaign_';
        camp.Segment__c = 'MB';
        camp.OwnerId  = UserInfo.getUserId();
        camp.RecordTypeId = Recordtypeid;
        insert camp;
        

        List<CampaignMember> cmList = new List<CampaignMember>();
        CampaignMember cm1 = new CampaignMember();
        cm1.LeadId = l.id;
        cm1.CampaignId = camp.id;
        cm1.RTL_TMB_Campaign_Source__c = 'Web';     
        cm1.RTL_Product_Name__c = 'product';
        cm1.RTL_Lead_Group__c =  'group';
        cm1.RTL_OfferResult_Product_1__c = 'N/A';
        cmList.add(cm1);
        
        Contact ct = RTL_TestUtility.createContacts(accList.get(0));
        ct.OwnerId = firstUser.id;
        insert ct;

        CampaignMember cm2 = new CampaignMember();
        cm2.ContactId = ct.id;
        cm2.CampaignId = camp.id;
        cm2.RTL_TMB_Campaign_Source__c = 'Web';     
        cm2.RTL_Product_Name__c = 'product';
        cm2.RTL_Lead_Group__c =  'group';
        cm2.RTL_OfferResult_Product_1__c = 'N/A';
        cmList.add(cm2);

        insert cmList;

        for( CampaignMember cm : cmList )
        {
            cm.RTL_Lead_Group__c =  'grouplead';
        }
        update cmList;




        
                        
        RTL_Campaign_Assignment_Rule__c obj = new RTL_Campaign_Assignment_Rule__c();
        obj.RTL_Campaign_Lead_Source__c = 'Web';
        obj.RTL_Start_Date__c = System.today()-10;
        obj.RTL_End_Date__c = System.today()+10;
        obj.RTL_Active__c = true;
        insert obj;

    }
    
    public static testMethod void positive() {     

        Test.startTest();
 		
        Lead l = [SELECT id,Company,LastName,Status,RTL_W2L_Calculated_Amount__c,RTL_INTERESTS__c,RTL_W2L_Calculated_Loan_Period__c,
                RTL_W2L_Calculated_No_of_Installments__c,RTL_W2L_Has_Co_Borrower__c
                FROM LEAD WHERE  LastName = 'Mike' ];

        RTL_CampaignMemberTriggerHandler.settingValueTranformation('xxxxxxxxxxxxxxx');
        RTL_CampaignMemberTriggerHandler.concatProductFeatures(l);
        RTL_CampaignMemberTriggerHandler.checkNBOStatusWithCampaignMember(null);
        RTL_CampaignMemberTriggerHandler.checkNBOStatusWithCampaignMember('Interested');
        RTL_CampaignMemberTriggerHandler.checkNBOStatusWithCampaignMember('Not Interested');
        RTL_CampaignMemberTriggerHandler.checkNBOStatusWithCampaignMember('N/A');
        
        Map<String, String> fieldLabelMap = RTL_CampaignMemberTriggerHandler.fieldLabelMap;
        //Map<ID, String> contactNameMap = RTL_CampaignMemberTriggerHandler.contactNameMap;

        List<RTL_Channel_Code_Mapping__mdt> channelCodeMapping = [SELECT Id, RTL_Lead_Group__c, 
                                  RTL_Campaign_Lead_Source__c, 
                                  RTL_Channel_Code__c, 
                                  RTL_Campaign_Reference__c, 
                                  DeveloperName, 
                                  MasterLabel, Label 
                                  FROM RTL_Channel_Code_Mapping__mdt];

        List<CampaignMember> cmList = new List<CampaignMember>();
        for( CampaignMember campaignMemberObj : [SELECT id,RTL_Register_Date__c,RTL_Product_Name__c,
                                        RTL_Lead_Group__c,RTL_TMB_Campaign_Source__c,RTL_Product_Feature__c,RTL_Reason_1__c
                                        FROM CampaignMember] 
        ) {
            campaignMemberObj.RTL_OfferResult_Product_1__c = 'Interested';
            cmList.add(campaignMemberObj);
            RTL_CampaignMemberTriggerHandler.concatRemark(channelCodeMapping,campaignMemberObj);
        }

        update cmList; 
        //RTL_Reason_1__c

        Test.stopTest();  
    }          

    public static testMethod void afterUpdate() {     
        Test.startTest();
            
            CampaignMember cm1 = [SELECT id,RTL_Contact_Status__c,RTL_Assigned_Agent__c,RTL_Assigned_Branch__c
                FROM CampaignMember LIMIT 1];

            User firstUser = [SELECT id FROM User WHERE Email='standarduser-ii@tmbbank.com' ];
            Branch_and_zone__c bz = [SELECT id from Branch_and_zone__c WHERE Branch_Code__c = '611'];

            cm1.RTL_Contact_Status__c = 'Contact';
            cm1.RTL_Assigned_Agent__c = firstUser.id;
            cm1.RTL_Assigned_Branch__c = bz.id;
            update cm1;

            cm1.RTL_Contact_Staff_Name__c = firstUser.id;
            update cm1;

        Test.stopTest();
        
    }

    public static testMethod void afterInsertDroplead() {    

        Campaign camp = [SELECT id FROM Campaign WHERE name = 'Campaign_'];
        User firstUser = [SELECT id FROM User WHERE Email='standarduser-ii@tmbbank.com' ];

        Test.startTest();

            Lead l = new Lead(Company = 'JohnMiller2', LastName = 'Mike2', Status = 'Open');
            l.RTL_Branch_Code_Rpt__c = '611';
            l.RTL_TMB_Campaign_Source__c = 'web';
            l.OwnerId = firstUser.id;
            insert l;

            CampaignMember cm2 = new CampaignMember();
            cm2.LeadId = l.id;
            cm2.CampaignId = camp.id;
            cm2.RTL_TMB_Campaign_Source__c = 'Web';     
            cm2.RTL_Product_Name__c = 'product';
            cm2.RTL_Lead_Group__c =  'group';
            cm2.RTL_Product_Name__c = '1:MM';
            insert cm2;
        
                    
        Test.stopTest();
    }
 
}