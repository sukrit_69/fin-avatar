public class CaseProxyControl {
 
    private final ApexPages.StandardController std ;
    private final Case Caserec;
    // private RecordType recTypes;
    private string recordTypeId;
    private Id defAccountId = null;
    private Id defContactId = null;
    private Id defOptyId = null;

    public CaseProxyControl(ApexPages.StandardController controller){
        std = controller;  
        Caserec = (Case)std.getRecord();
        
        recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');  
        defAccountId = ApexPages.currentpage().getparameters().get('def_account_id');
        defContactId = ApexPages.currentpage().getparameters().get('def_contact_id');
        defOptyId = ApexPages.currentPage().getParameters().get('def_opportunity_id');
    }

    public PageReference redirectPage(){         
        String url ='/';
        String isclone = apexpages.currentpage().getparameters().get('clone');
        String params = '';
        Schema.RecordTypeInfo currentRecordType;
        //if Edit Mode
        if(Caserec.Id != null){
            System.debug('Edit Mode');
            currentRecordType = Schema.Sobjecttype.Case.getRecordTypeInfosById().get(Caserec.RecordTypeId);
            // recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
            //         where Id  =:Caserec.RecordTypeId
            //        LIMIT 1];             
            //  //SE Call Center
            //  if(recTypes.DeveloperName.containsIgnoreCase('SE_Call_Center')){
            //      if(isClone == '1'){
            //          url = '/apex/CaseUpdate?id='+ Caserec.Id+'&clone='+isClone;
            //      }else{
            //          url = '/apex/CaseUpdate?id='+ Caserec.Id;
            //      }  
            //  }else{ //Other record type
            //     url = '/apex/CommonCaseUpdate?id='+Caserec.Id+'&clone='+isClone;

            //  }
            params = '?id='+ Caserec.Id;
            if(isClone == '1')
            {
                params += '&clone='+isClone;
            }

            //SE Call Center
            if(currentRecordType.getDeveloperName().containsIgnoreCase('SE_Call_Center'))
            {
                url = '/apex/CaseUpdate';
            }
            else //Other record type
            { 
                url = '/apex/CommonCaseUpdate';
            }

            url += params;
             
         }else{ //Create Mode
            Schema.DescribeSObjectResult dsr = Case.SObjectType.getDescribe();
            Schema.RecordTypeInfo defaultRecordType;            
               
            for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
                if(rti.isDefaultRecordTypeMapping()) {
                    defaultRecordType = rti;
                }
            }

            currentRecordType = defaultRecordType;
            if(null != recordTypeId){
                currentRecordType = Schema.Sobjecttype.Case.getRecordTypeInfosById().get(recordTypeId);
            }
            
            //if()
            
            //  if(null == recordTypeId){
            //      recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
            //              where Id  =: defaultRecordType.getRecordTypeId()
            //              LIMIT 1];
            //  }else{
            //      recTypes = [select Id, Name, sobjectType,DeveloperName from RecordType   
            //              where Id  =:recordTypeId
            //              LIMIT 1];
            //  }

            if(defAccountId != null){
                params += '&def_account_id='+defAccountId;
            }
            if(defContactId != null){
                params += '&def_contact_id='+defContactId;
            }
            if(defOptyId != null){
                params += '&def_opportunity_id='+defOptyId;
            }

             //SE Call Center
            if(currentRecordType.getDeveloperName().containsIgnoreCase('SE_Call_Center')){
                // if(defAccountId != null && defContactId != null){
                //     url = '/apex/CaseUpdate?recordType='+recordTypeId+'&def_account_id='+defAccountId+'&def_contact_id='+defContactId;
                //     if(defOptyId != null){
                //         url += '&def_opportunity_id=' + defOptyId;
                //     }
                // }else if(defAccountId != null && defContactId == null){
                //     url = '/apex/CaseUpdate?recordType='+recordTypeId+'&def_account_id='+defAccountId;
                //     if(defOptyId != null){
                //         url += '&def_opportunity_id=' + defOptyId;
                //     } 
                // }else if(defAccountId == null && defContactId != null){
                //     url = '/apex/CaseUpdate?recordType='+recordTypeId+'&def_contact_id='+defContactId;
                //     if(defOptyId != null){
                //         url += '&def_opportunity_id=' + defOptyId;
                //     }
                // }else {
                //     url = '/apex/CaseUpdate?recordType='+recordTypeId;
                //     if(defOptyId != null){
                //         url += '&def_opportunity_id=' + defOptyId;
                //     }
                // } 
                url = '/apex/CaseUpdate?recordType='+currentRecordType.getRecordTypeId();

            }else{ //Other record type
                url = '/apex/CommonCaseUpdate?recordType='+currentRecordType.getRecordTypeId();  
            }   
            url += params;     
        }
        if(url != null){
            PageReference page = new PageReference(url);
            page.getParameters().put('nooverride','1');
            page.setRedirect(true); 
            return page;  
        }else{
            PageReference page = new PageReference('/500/e?retURL=%2F500%2Fo&RecordType='+currentRecordType.getRecordTypeId()+'&ent=Case'); 
            page.getParameters().put('nooverride','1');
            return page;
        }
            
    }

}