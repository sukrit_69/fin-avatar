public class SmartBDM_Utility {

    private final static Map<String, String> QuickCA_Lead_Stage = new Map<String, String> { 
        '0' => 'NewLead', 
        '1' => 'Contact', 
        '2' => 'AddProduct', 
        '3' => 'Convert', 
        '4' => 'SubmitSLS' 
    };

    private final static Map<String, String> QuickCA_Opportunity_Stage = new Map<String, String> { 
        '0' => 'NewOpportunity', 
        '1' => 'Contact', 
        '2' => 'AddProduct', 
        '3' => 'SubmitSLS'
    };

    // public static void doBusinessTypeBatch(){
    //     SmartBDM_BusinessTypeBatch BusinessType = new SmartBDM_BusinessTypeBatch();
    //     DataBase.executeBatch(BusinessType);
    // }
    
    @AuraEnabled
    public static void updateLeadStatus(String leadId, String stage)
    {
        // System.debug('leadId:'+leadId);
        // System.debug('stage:'+stage);
        try
        {
            Lead ld = new Lead(Id = leadId);
            ld.SmartBDM_QuickCA_ProcessStatus__c = QuickCA_Lead_Stage.get(stage);

            // if(stage == '0')
            // {
            //     ld.SmartBDM_QuickCA_ProcessStatus__c = 'NewLead';
            // }
            // else if(stage == '1')
            // {
            //     ld.SmartBDM_QuickCA_ProcessStatus__c = 'Contact';
            // }
            // else if(stage == '2')
            // {
            //     ld.SmartBDM_QuickCA_ProcessStatus__c = 'AddProduct';
            // }
            // else if(stage == '3')
            // {
            //     ld.SmartBDM_QuickCA_ProcessStatus__c = 'Convert';
            // }
            // else if(stage == '4')
            // {
            //     ld.SmartBDM_QuickCA_ProcessStatus__c = 'SubmitSLS';
            // }
            
            update ld;
        }
        catch(Exception e) 
        {
            System.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());           
        }   
    }
    
    @AuraEnabled
    public static void updateOpptyStatus(String opptyId,String stage)
    {
        try
        {
            // System.debug('opptyId:'+opptyId);
            // System.debug('stage:'+stage);
            Opportunity oppty = new Opportunity(Id = opptyId);
            oppty.SmartBDM_QuickCA_ProcessStatus__c = QuickCA_Opportunity_Stage.get(stage);
            // if(stage == '0')
            // {
            //     oppty.SmartBDM_QuickCA_ProcessStatus__c = 'NewOpportunity';
            // }
            // else if(stage == '1')
            // {
            //     oppty.SmartBDM_QuickCA_ProcessStatus__c = 'Contact';
            // }
            // else if(stage == '2')
            // {
            //     oppty.SmartBDM_QuickCA_ProcessStatus__c = 'AddProduct';
            // }
            // else if(stage == '3')
            // {
            //     oppty.SmartBDM_QuickCA_ProcessStatus__c = 'SubmitSLS';
            // }
            
            update oppty;
        }
        catch(Exception e) 
        {
            System.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());           
        }   
    }
}