global without sharing class RestfulServiceUtil implements Database.Batchable<sObject>
                                            , Database.Stateful
                                            , Database.AllowsCallouts {
	private final String query;
    private String JOB_NAME;
    
    private Integration_Info__c intInfo;
    private List<Sobject> FORUPDATE_SOBJLIST = new List<Sobject>();
   
    private Integer RETRY_COUNT     = 0;
    private Integer MAX_RETRY_COUNT = 3;
    private String  IntegrationName;
    private Datetime startDatetime = Datetime.now();

    private EIMManager manager = null;
    private String RETRY_INTEGRATION_LOG = 'RETRYBATCH_EXTWS_LOG';
    private String ERROR_INTEGRATION_LOG = 'ERROR_EXTWS_LOG';
    private String SUCCESS_INTEGRATION_LOG = 'SUCCESS_EXTWS_LOG';
    private String NO_CALLOUT_LOG = 'NO_CALLOUT_LOG';
    private Boolean isRetryCallout = false;
    private String ACCOUNT_ID_FIELD = null;
    private String REF_NAME_FIELD   = null;


    private List<RTL_Online_Service_Log__c> LogList = new List<RTL_Online_Service_Log__c>();
    private Map<Id,List<Attachment>> bodyAttMap     = new Map<Id,List<Attachment>>();
    private List<Sobject> SobjList = new List<SObject>();
    private RestfulServiceUtil.ValueMapBase valueExecuter;

    public void setValueMapImpl(RestfulServiceUtil.ValueMapBase valueExecImpl) {
        this.valueExecuter = valueExecImpl;
    }

    public virtual class ValueMapBase {
        public virtual 
            Map<String,String> prepareValueMap(SObject sobj,EIMManager manager){ return new Map<String, String>();}


        public virtual 
            Boolean isErrorNotCallout(Map<String,String> valueMap){ return false;}
    } 
        
    private void init_process(Sobject sobj,Map<String,String> valueMap){
        setIntegrationInfo(this.IntegrationName);
        this.manager.getBodyRequest(valueMap,intInfo.Request_Format__c);

        Boolean ret = this.valueExecuter.isErrorNotCallout(valueMap);

        if (!ret) {
            this.callout(sobj); 
        }else {
            //insert log
            String requestBody = JSON.serialize(sobj);
            addIntegrationLog(startDatetime
                                ,DateTime.now()
                                //,this.objectId
                                ,sobj
                                ,NO_CALLOUT_LOG
                                ,ERROR_INTEGRATION_LOG
                                ,false
                                ,requestBody
                                ,''
                                ,'');
        }
    }

    private void init_process(Sobject sobj){
        setIntegrationInfo(this.IntegrationName);
        this.manager.getBodyRequest(sobj,intInfo.Request_Format__c);
        this.callout(sobj);
    }

    global RestfulServiceUtil(String q,String integrationName){
         if (q.toLowerCase().contains('where')) query = q+' AND Is_Exectued__c = false';
        else query = q+' WHERE Is_Exectued__c = false';

        this.IntegrationName = integrationName;
        //this.manager = new EIMManager(this.IntegrationName);
    }

	global RestfulServiceUtil(String q,String integrationName,String accountField,String refField){
        if (q.toLowerCase().contains('where')) query = q+' AND Is_Exectued__c = false';
        else query = q+' WHERE Is_Exectued__c = false';

        this.IntegrationName = integrationName;
        //this.manager = new EIMManager(this.IntegrationName);
        this.ACCOUNT_ID_FIELD = accountField;
        this.REF_NAME_FIELD  = refField;
    }

    //global RestfulServiceUtil(){}

    private void setIntegrationInfo(String integrationName) {
        //Query Integration_Info__c
        intInfo = [SELECT Id,Name,Endpoint__c,Timeout__c,Request_Format__c,Response_Format__c 
                    FROM Integration_Info__c 
                   WHERE Name =: integrationName]; 
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<Sobject> scope){
        this.manager = new EIMManager(this.IntegrationName);
        for (Sobject sobj : scope) {
            try {
                 Map<String,String> valueMap = this.valueExecuter.prepareValueMap(sobj,this.manager);
            
                if (valueMap.isEmpty()) {
                    init_process(sobj);
                }else {
                    init_process(sobj,valueMap);
                }
            }catch(Exception e) {
                addIntegrationLog(startDatetime
                                ,DateTime.now()
                                //,this.objectId
                                ,sobj
                                ,e.getMessage()+ ' '+e.getStackTraceString()
                                ,ERROR_INTEGRATION_LOG
                                ,false
                                ,''
                                ,''
                                ,'');
            }
        }
    }    

    global void finish(Database.BatchableContext bc){
        this.finish_job();
    }

    public void finish_job() {
        
        if (!this.FORUPDATE_SOBJLIST.isEmpty()) {
            Integer runningIndex = 0;
            Database.SaveResult[]  lsr = Database.update(this.FORUPDATE_SOBJLIST
                                                        , false);
            
            // Iterate through each returned result
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully Update '
                                +' with ID: ' 
                                + sr.getId());
                }else {
                    // Operation failed, so get all errors                
                    //Id recordId = this.FORUPDATE_SOBJLIST.get(runningIndex).Id;
                    Sobject sobj = this.FORUPDATE_SOBJLIST.get(runningIndex);
                    for(Database.Error err : sr.getErrors()) {
                            String detail =  err.getMessage() + ','
                                                +String.join(err.getFields(),',') + ','
                                                +err.getStatusCode()+'\n';

                            addIntegrationLog(startDatetime
                                              ,DateTime.now()
                                              ,sobj
                                              ,detail
                                              ,ERROR_INTEGRATION_LOG
                                              ,false
                                              ,''
                                              ,''
                                              ,'');
                    }
                }
              runningIndex++;
            } 
        }

        if (!this.LogList.isEmpty()) {
            insert this.LogList;

            List<Attachment> allAttList = new List<Attachment>();
            for (RTL_Online_Service_Log__c log : this.LogList) {
                List<Attachment> attList = bodyAttMap.get(log.RTL_RM_ID__c);

                for (Attachment att : attList) {
                    att.ParentId = log.Id;
                }

                allAttList.addAll(attList);
            }

            if (allAttList.size() > 0) insert allAttList;
        }
    }
    
    private Boolean isRetry(SObject sobj) {
        if (this.RETRY_COUNT < this.MAX_RETRY_COUNT) {
            this.RETRY_COUNT = this.RETRY_COUNT + 1;
            this.callout(sobj);
            return true;
        }else {
            return false;
        }
    }

    public Boolean retryCallout(SObject sobj,SObject retailLog,String requestBody) {
        this.IntegrationName = (String)retailLog.get('RTL_Customer_Name__c');
        setIntegrationInfo(this.IntegrationName);
        isRetryCallout = true;
        return this.callout(sobj,requestBody);
    } 

    public Boolean callout(SObject sobj) {
        String requestBody = this.manager.getBodyRequestMap().get(sobj.Id);
        return this.callout(sobj,requestBody);
    }

    public Boolean callout(SObject sobj,String requestBody) {
        try{
            Integer TIMEOUT_INT_MILLISECS = Integer.valueOf(intInfo.Timeout__c)*1000;
            //Integer TIMEOUT_INT_MILLISECS = Integer.valueOf(intInfo.Timeout__c);
            HttpRequest req = new HttpRequest();
            String endpoint = intInfo.Endpoint__c;
            
            req.setEndpoint(endpoint);
            req.setTimeout(TIMEOUT_INT_MILLISECS);
            req.setMethod('POST');
            req.setHeader('Content-Type',intInfo.Request_Format__c);

            String egLog = sobj.Id +'_'+startDatetime.getTime();
            req.setHeader('From',egLog);

            req.setBody(requestBody);

            Http http = new Http();
            HTTPResponse res = http.send(req);
            String responseBody = res.getBody();

            Map<String,Object>  RET_MAP = this.manager.getResponseInfo(responseBody, intInfo.Response_Format__c);
            
            if (sobj != null && !RET_MAP.isEmpty()) {
                System.debug(RET_MAP.keySet());
                
                for (String key : RET_MAP.keySet()) {
                    sobj.put(key, RET_MAP.get(key));
                }

                sobj.put('Is_Exectued__c', true);

                this.FORUPDATE_SOBJLIST.add(sobj);

                if (this.manager.isIntegrationSuccess) {
                    addIntegrationLog(startDatetime
                              ,DateTime.now()
                              //,this.objectId
                              ,sobj
                              ,SUCCESS_INTEGRATION_LOG
                              ,SUCCESS_INTEGRATION_LOG
                              ,this.manager.isIntegrationSuccess
                              ,requestBody
                              ,res.getBody()
                              ,egLog);

                    return this.manager.isIntegrationSuccess;
                }        
            }

            addIntegrationLog(startDatetime
                              ,DateTime.now()
                              //,this.objectId
                              ,sobj
                              ,ERROR_INTEGRATION_LOG
                              ,ERROR_INTEGRATION_LOG
                              ,false
                              ,requestBody
                              ,res.getBody()
                              ,egLog);

            return false;
        }catch(Exception e) {
           if (e.getMessage().equalsIgnoreCase('Read timed out')) {
                if (!isRetry(sobj)) {
                     if (!isRetryCallout) 
                        addIntegrationLog(startDatetime
                                ,DateTime.now()
                                //,this.objectId
                                ,sobj
                                ,requestBody
                                ,RETRY_INTEGRATION_LOG
                                ,false
                                ,requestBody
                                ,''
                                ,sobj.Id +'_'+startDatetime.getTime());
                }
            }else {
                addIntegrationLog(startDatetime
                                ,DateTime.now()
                                //,this.objectId
                                ,sobj
                                ,e.getMessage()+ ' '+e.getStackTraceString()
                                ,ERROR_INTEGRATION_LOG
                                ,false
                                ,requestBody
                                ,''
                                ,sobj.Id +'_'+startDatetime.getTime());
            }
            return false;
        }
    }

    public void runBatch() {
        Id BatchProcessIdForInt = Database.ExecuteBatch(this,10);
    }
    
    private void addIntegrationLog(DateTime startDatetime
                                  ,DateTime endDatetime
                                  ,Sobject sobj
                                  ,String errorMessages
                                  ,String logType
                                  ,Boolean isRet
                                  ,String requestBody
                                  ,String responseBody
                                  ,String egLog
                                  ) {
        RTL_Online_Service_Log__c inlog = new RTL_Online_Service_Log__c ();
        inlog.RTL_Name_of_User__c = UserInfo.getName();
        inlog.RTL_RM_ID__c = sobj.Id;
        inlog.Online_Batch_Log__c = logType;
        inlog.RTL_Error_Message__c = errorMessages;
        inlog.RTL_Start_DateTime__c = startDatetime;      
        inlog.RTL_End_DateTime__c   = endDatetime;
        inlog.RTL_Service_Name__c   = this.IntegrationName;
        inlog.RTL_Is_Success__c     = isRet;
        inlog.RTL_Mulesoft_Log_Id__c = egLog;


        if (this.ACCOUNT_ID_FIELD != null 
                && this.ACCOUNT_ID_FIELD.length() > 0 
                && sobj.get(this.ACCOUNT_ID_FIELD) != null) {
            inlog.RTL_Account__c = Id.valueOf((String)sobj.get(this.ACCOUNT_ID_FIELD));
        }

        if (this.REF_NAME_FIELD != null && this.REF_NAME_FIELD.length() > 0) {
            List<String> sffieldList = this.REF_NAME_FIELD.split('\\.');
            SObject tempSobj = null;

            for (Integer i=0;i<sffieldList.size()-1;i++) {
                tempSobj =  sobj.getSobject(sffieldList.get(i));
            }         

            if (tempSobj != null) {
                String value = (String)tempSobj.get(sffieldList.get(sffieldList.size()-1));
                inlog.RTL_Customer_Name__c = value;
            }
        }

        LogList.add(inlog);

        List<Attachment> importattachmentfileList = new List<Attachment>();

        if(!String.isBlank(requestBody)) { 
            Attachment requestAttachment = new Attachment();
            requestAttachment.name = 'requestBody.txt';
            requestAttachment.IsPrivate = true;
            requestAttachment.body = Blob.valueOf(requestBody);
            importattachmentfileList.add(requestAttachment);
        }

        if(!String.isBlank(responseBody)) { 
            Attachment responseAttachment = new Attachment();
            responseAttachment.name = 'responseBody.txt';
            responseAttachment.IsPrivate = true;
            responseAttachment.body = Blob.valueOf(responseBody);
            importattachmentfileList.add(responseAttachment);
        }

        bodyAttMap.put(sobj.Id,importattachmentfileList);
    }


}