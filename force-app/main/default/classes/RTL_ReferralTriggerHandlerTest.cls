@isTest
public class RTL_ReferralTriggerHandlerTest {
    static List<sObject> ls;
    static List<Account> acctList;
    static List<Lead> leadList;
    static Map<String, Group> queueMap;
    static List<Branch_And_Zone__c> branchAndZoneList;
    static List<RTL_Product_Master__c> retailProductMasterList;
    static List<RTL_Referral__c> referralList;
    static List<Opportunity> opportunityList;
    static User retailWithBranchUser;
    static User rmcUser;
    static User bdmUser;
    static User adminUser;

    static {
        TestUtils.createAppConfig();
        TestUtility_Referral.createAppConfigReferralAssignment();
        TestUtility_Referral.createReferralAssignment();
        TestUtility_Referral.createAppConfigCallMeNowBH();
        branchAndZoneList = RTL_TestUtility.createBranchZone(9,true);
        retailProductMasterList = RTL_TestUtility.createRetailProducts(true);
        retailWithBranchUser = RTL_TestUtility.createRetailTestUserWithBranch(true);
        adminUser = TestUtils.createUsers(1,'USERADMIN','TESTCLASS','test_TMB@tmbbank.com',true)[0];
        acctList = RTL_TestUtility.createAccounts(2,true);
        // update acct[0] as wealth account
        acctList[0].RTL_RM_Name__c = '00001';
        update acctList[0];
        
        RTL_Branch_Manager_Title__c branchTitle = new RTL_Branch_Manager_Title__c(Name='Branch Manager',RTL_Value__c='Branch Manager');
        insert branchTitle;
        //ls = Test.loadData(Branch_And_Zone__c.sObjectType, 'BranchAndZoneMaster');
        ID RMC_ROLE_ID = [SELECT Id FROM UserRole WHERE Name = 'RMC' LIMIT 1].id;
        System.debug('RMC ROLE ID '+RMC_ROLE_ID);
        
        queueMap = new Map<String, Group>();
        for(Group queue : [Select Id, Name, DeveloperName from Group where Type = 'Queue'])
            queueMap.put(queue.DeveloperName, queue);
        
        System.runAs(adminUser){
            //create rmc user for test
            rmcUser = TestUtility_Referral.createRMCTestUser(true);
            //create bdm user for test 
            bdmUser = TestUtility_Referral.createBDMTestUser(true);
        }
        
    }
    
    static testmethod void testInsertNewReferral(){
        
        Test.startTest();
        System.runAs(retailWithBranchUser){
            //Insert referral without account
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            //Insert referral cross channel with account
            referralList = TestUtility_Referral.createReferralsCrossChannelWithAccount(1,acctList[0].id,branchAndZoneList[0].id,retailProductMasterList[0].id,true);
            
            referralList = queryReferral();
            System.assertEquals(2,referralList.size());
        }
        Test.stopTest();
    }
    
    static testmethod void testInsertReferralWithAccount(){
        //assert employee information , refered info , last assign info ,owner info , reporing product for different record type
        Test.startTest();
        System.runAs(retailWithBranchUser){
            //Referral with wealth account
            referralList = TestUtility_Referral.createReferralsWithAccount(1,acctList[0].id,branchAndZoneList[0].id,true);
            
            //Referral with non-wealth account
            referralList = TestUtility_Referral.createReferralsWithAccount(1,acctList[1].id,branchAndZoneList[0].id,true);
        }
        Test.stopTest();
    }
    
    static testmethod void testDefaultPreferredBranch(){
        //for refer to commercial
        Test.startTest();
        System.runAs(retailWithBranchUser){
            //Referral without preferred branch
            referralList = TestUtility_Referral.createReferralsWithoutBranch(1,true);
            referralList = queryReferral();
            System.assertNotEquals(null,referralList[0].RTL_Preferred_Branch__c);
        }
        Test.stopTest();
    }
    
    
    static testmethod void testBulkInsertReferralWithAccount(){
         //for refer to commercial
        Test.startTest();
        System.runAs(retailWithBranchUser){
            //Referral without preferred branch
            referralList = TestUtility_Referral.createReferralsWithAccount(200,acctList[0].id,branchAndZoneList[0].id,true);
            referralList = queryReferral();
            //assert insert bulk insert success
            System.assertEquals(200,referralList.size());
        }
        Test.stopTest();
    }
    
    static testmethod void testCreateOpportunityWithReferral(){
        //assert referral status changed to closed (interested)
        System.runAs(retailWithBranchUser){
            //Referral without preferred branch
            referralList = TestUtility_Referral.createReferralsWithAccount(1,acctList[0].id,branchAndZoneList[0].id,true);
            opportunityList = RTL_TestUtility.createOpportunities(acctList[0],1,true);
            
            List<Opportunity> opptyList = queryOpportunity();
            System.debug('BEFORE OPPORTUNITY   :: '+opptyList);
            
            opptyList[0].RTL_Referral__c = referralList[0].id;
            update opptyList;
            
            opptyList = queryOpportunity();
            List<RTL_Referral__c> refList = queryReferral();
            System.debug('AFTER OPPORTUNITY   :: '+opptyList);
            //Assert referral attached with opportunity
            System.assertequals(referralList[0].id,opptyList[0].RTL_Referral__c);
            //Assert referral stage is Closed (Interested)
            System.assertequals('Closed (Interested)',refList[0].RTL_Stage__c);
        }
        
       
    }
    
    static testmethod void testUpdateReferralContacted(){
        //assert first contacted date
        //
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            referralList[0].RTL_Stage__c = 'In progress_Contacted';
            update referralList;
            
            referralList = queryReferral();
            System.assertNotEquals(null,referralList[0].RTL_First_Contacted_Date__c);
            
            
        }
        Test.stopTest();
    }
    
    static testmethod void testCloseReferral(){
        //assert first contacted , closed date
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            referralList[0].RTL_Stage__c = 'Closed (Unable to contact)';
            referralList[0].RTL_Reason__c = 'Busy Line';
            update referralList;
            
            referralList = queryReferral();
            System.assertNotEquals(null,referralList[0].RTL_First_Contacted_Date__c);
            System.assertNotEquals(null,referralList[0].RTL_Closed_Date_Time__c);
            
        }
        Test.stopTest();
    }
    
    static testmethod void testHasPermissionAcceptReferral(){
        //assert change owner success , assert last assign owner
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            
            //change owner to RMC Queue
            referralList[0].OwnerId = queueMap.get('RMC_Queue').id;
            update referralList;
            
            referralList = queryReferral();
            System.assertEquals(queueMap.get('RMC_Queue').id,referralList[0].OwnerId);
                        
            
        }
        
        
        
        System.runAs(rmcUser){
            RTL_ReferralTriggerHandler.currentUser = null;
            //rmc user accept referral
            String result = RTL_ReferralChangeOwner.acceptReferral(referralList[0].id);   
            System.assertequals('Success',result);
        }
        Test.stopTest();
        
    }
    
    static testmethod void testNoPermissionAcceptReferral(){
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            
            //change owner to RMC Queue
            referralList[0].OwnerId = queueMap.get('RMC_Queue').id;
            update referralList;
            
            //Branch try to accept referral
            String result = RTL_ReferralChangeOwner.acceptReferral(referralList[0].id);   
            System.assertequals('You are not authorized to accept this referral.',result);
            
            
        }
      
        Test.stopTest();
    }
    
    static testmethod void testHasPermissionEditReferral(){
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            
            //change owner to RMC Queue
            referralList[0].RTL_Account_Name__c = acctList[0].id;
            referralList[0].RTL_Interested_Product__c = 'Payroll';
            update referralList;
            
            //query referral to check result
            referralList = queryReferral();
            
            //System.assertEquals(acctList[0].id,referralList[0].RTL_Account_Name__c );
            //System.assertEquals('EDC',referralList[0].RTL_Interested_Product__c );
            //System.assertEquals('EDC',referralList[0].RTL_Reporting_Product__c );

            
            
        }
      
        Test.stopTest();
    }
    static testmethod void testReferralChangeOwner(){
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell (เพื่อนำเสนอขาย)',true);
            
            //change owner to Branch Queue
            referralList[0].OwnerId = queueMap.get('RTLQ_001').id;
            update referralList;
            
            referralList = queryReferral();
            //assert change owner success
            System.assertEquals(queueMap.get('RTLQ_001').id, referralList[0].OwnerId);
            
            
        }
      
        Test.stopTest();
    }
    
    static testmethod void testManualCloseInterestedReferral(){
        //assert error message
        Test.startTest();
        System.runAs(retailWithBranchUser){
            
            try{
                
                referralList = TestUtility_Referral.createReferralsWithoutAccount(1,branchAndZoneList[0].id,'Payroll','Sell',true);
                referralList[0].RTL_Stage__c = 'Closed (Interested)';
                update referralList;
                // 1. If we get to this line it means an error was not added and the test class should throw an exception here.
                throw new MyException('An exception should have been thrown by the trigger but was not.'); 
            }catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('Stage Closed (Interested) can be set by system only') ? true : false;
                //System.AssertEquals(expectedExceptionThrown, true);
            }
            
        }
        Test.stopTest();
    }
    
    public class MyException extends Exception {}
    
    public static List<Opportunity> queryOpportunity(){
        List<Opportunity> oppList = [SELECT Id,Name,RTL_Referral__c FROM Opportunity ];
        return oppList;
    }
    
    public static List<RTL_Referral__c> queryReferral(){
        List<RTL_Referral__c> referralList = [SELECT Id,Name,RTL_Stage__c,RTL_Owner_Branch__c,OwnerId,
                                              RTL_First_Contacted_Date__c,RTL_Closed_Date_Time__c,
                                              RTL_Preferred_Branch__c,RTL_Account_Name__c,RTL_Interested_Product__c,
                                              RTL_Reporting_Product__c,Owner.Name,RTL_Is_Owner__c
                                              FROM RTL_Referral__c];
        return referralList;
    }
    
    
    
}