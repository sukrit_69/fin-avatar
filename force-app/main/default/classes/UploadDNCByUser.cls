public with sharing class UploadDNCByUser  implements Messaging.InboundEmailHandler {
     public Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env){
        
     	Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();


     	try {
     		String SendersEmail = env.fromAddress;
            
            //get Attachment
            Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;
            String jobName = 'CXM_BulkUpload_Do_Not_Contact';
            if (bAttachments == null || bAttachments.size() != 1) {
                result.message = RTL_ReportUtility.INVALID_ATTACHMENT_NUMBERS;
                result.success = false;
                return result;
            }

            Messaging.InboundEmail.BinaryAttachment btt = bAttachments.get(0);

            List<String> emailOpsList = new List<String>();
            emailOpsList.add(SendersEmail);
            emailOpsList.add('CRM Admin 2');

            EmailServiceUtil emailQueue = new EmailServiceUtil(jobName
                                                              ,btt.body.toString()
                                                              ,'MK_CODE_EXT_KEY__c'
                                                              ,EmailServiceUtil.UPSERT_OPERATION
                                                              ,emailOpsList
                                                              ,env.fromAddress
                                                              ,env.toAddress
                                                              ,true
                                                              );

            Map<String,String> ret = emailQueue.validateAndProcess(bAttachments);

            if (ret.get(emailQueue.IS_SUCCESS) == 'false') {
                result.message = ret.get(emailQueue.MESSAGE);
                result.success = false;
                return result;
            }

            result.message = ret.get(emailQueue.MESSAGE);
            result.success = true;
     	}catch(Exception e){
     		result.message = RTL_ReportUtility.INTERNAL_SERVER_ERROR+'\n'+e.getMessage() 
                                                + '\n'
                                                +e.getStackTraceString();
        result.success = false;
     	}

        return result;                                                  
     }


}