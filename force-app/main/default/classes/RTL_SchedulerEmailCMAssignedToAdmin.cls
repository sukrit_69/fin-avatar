global class RTL_SchedulerEmailCMAssignedToAdmin implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		//String query = 'SELECT Id, Name FROM CampaignMember WHERE RTL_Assigned_Channel__c != \'Outbound\' AND RTL_Assigned_Branch__c = null AND RTL_Assigned_Agent__c = null AND RTL_TMB_Campaign_Source__c IN (\'Web\',\'ATM Request\',\'Call me now\')';
        
		String query = 'SELECT Id, Name FROM CampaignMember WHERE RTL_Assigned_Channel__c != \'Outbound\' AND RTL_Assigned_Branch__c = null AND RTL_Assigned_Agent__c = null AND RTL_TMB_Campaign_Source__c IN (\'Web\',\'ATM Request\',\'Call me now\')';
		query = query + ' AND Campaign.IsActive = true AND RTL_Offer_Result__c = \'Pending\'';
		
        RTL_BatchEmailCMAssignAdmin emailBatch = new RTL_BatchEmailCMAssignAdmin(query);
        Id BatchProcessId = Database.ExecuteBatch(emailBatch);	
	}

	
}