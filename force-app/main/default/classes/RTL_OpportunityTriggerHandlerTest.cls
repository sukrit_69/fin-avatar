@IsTest

public class RTL_OpportunityTriggerHandlerTest {  
    static List<Account> acctList;
    static List<Branch_And_Zone__c> branchAndZoneList;
    static {
        TestUtils.createAppConfig();
    }
    
    public static testmethod void RunPositiveTestOpportunityForChannelReferral(){
        System.debug(':::: RunPositiveTestOpportunityForChannelReferral Start ::::');
        
        TEST.startTest();
        List<User> retailUsers = RTL_TestUtility.createRetailTestUserOppt(true);
        User firstUser = retailUsers[0];//outbound channel
        User secondUser = retailUsers[1];//outbound channel
        User thirdUser = retailUsers[2];//branch channel
        TestInit.createUser(false);
        User adminUser = TestInit.us;  
        List<Account> accountList = null;
        List<Opportunity> opportunityList = null;
        String opptId = null;
        Opportunity updatedOppt = null;
        

        System.runAs(adminUser) {
        	//only system admin can create account
            accountList = RTL_TestUtility.createAccounts(1,true);
            //change the account ownership to retail user for opportunity creation later
            Account acct = accountList[0];
            acct.OwnerId = firstUser.Id;
            update acct;
        }
        System.runAs(firstUser) {
        	//create oppportunity with retail user so that the retail record type is set
            
            opportunityList = RTL_TestUtility.createOpportunity(accountList,true);
            opptId = opportunityList.get(0).Id;
            system.debug('Test_opptId : '+opptId);
            Opportunity oppt = [select OwnerId from Opportunity where Id = :opptId];
            //1st test case: the default channel referral is created
            List<RTL_Channel_Referral__c> defaultOpptReferrals = [Select RTL_Owner__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId];
            //System.assertEquals(1, defaultOpptReferrals.size());
            //check if the opportunity owner is first user
            System.assertEquals(firstUser.Id, oppt.OwnerId);
            //check if the opportunity owner is set as channel referral owner by default
            //System.assertEquals(firstUser.Id, defaultOpptReferrals[0].RTL_Owner__c);
            //2nd test case: change ownership, but the channel is the same (Outbound)                            
            oppt.OwnerId = secondUser.Id;
            //3rd test case: check first entry of first channel referral to be true
            //System.assertEquals(true, defaultOpptReferrals[0].RTL_First_Entry__c);
            update oppt;            
        }
        System.runAs(secondUser) {
            //check if the opportunity owner is changed
            updatedOppt = [select OwnerId from Opportunity where Id = :opptId];
            System.assertEquals(secondUser.Id, updatedOppt.OwnerId);
            //check if no additional oppportunity referral is created
            List<RTL_Channel_Referral__c> newOpptReferrals = [Select RTL_Owner__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId];
            //System.assertEquals(1, newOpptReferrals.size());
            //check if the oppportunity referral is not changed
            //System.assertEquals(firstUser.Id, newOpptReferrals[0].RTL_Owner__c);  
            //check first entry of first channel referral to be true
            //System.assertEquals(true, newOpptReferrals[0].RTL_First_Entry__c);
        }
        System.runAs(adminUser) {
            updatedOppt.OwnerId = thirdUser.Id;
            update updatedOppt;
        }
        System.runAs(thirdUser) {
            //check if the opportunity owner is changed
            Opportunity updatedOppt2 = [select OwnerId from Opportunity where Id = :opptId];
            System.assertEquals(thirdUser.Id, updatedOppt2.OwnerId);             
            List<RTL_Channel_Referral__c> updatedOpptReferrals = [Select RTL_Owner__c, RTL_End_Date__c, RTL_First_Entry__c from RTL_Channel_Referral__c where RTL_Opportunity__c = :opptId order by CreatedDate asc];
            //System.assertEquals(2, updatedOpptReferrals.size());
            //check if additional channel refferal is created, the old referral has end date updated
            //System.assertEquals(firstUser.Id, updatedOpptReferrals[0].RTL_Owner__c);  
            //System.assert(updatedOpptReferrals[0].RTL_End_Date__c!=null);
            //check first entry of first channel referral to be true
            //System.assertEquals(true, updatedOpptReferrals[0].RTL_First_Entry__c);            
            //System.assertEquals(thirdUser.Id, updatedOpptReferrals[1].RTL_Owner__c);
            //check 2nd entry of first channel referral to be false
            //System.assertEquals(false, updatedOpptReferrals[1].RTL_First_Entry__c);
        }
        TEST.stopTest();
        System.debug(':::: RunPositiveTestOpportunityForChannelReferral End ::::');
    }
    
    public static testmethod void TestBulkDefaultOpportunityName(){
        
        TEST.startTest();
        
        List<Account> accountList = null;
        List<Opportunity> opportunityList = null;
        String opptId = null;
        Opportunity updatedOppt = null;
        List<User> retailUsers = RTL_TestUtility.createRetailTestUserOppt(true);
        User user = retailUsers[0];//outbound channel
        accountList = RTL_TestUtility.createAccounts(1,true);
////////////////// test mock///////////
        RTL_TestUtility.createRetailMasterProducts(true);

        Contact ct = RTL_TestUtility.createContacts(accountList.get(0));
            ct.OwnerId = user.id;
            ct.TMB_Customer_ID__c = '001p000000CKy8AAD1';
            insert ct;
        system.debug('Opportunity : test '+ct);

        RecordType rt = [SELECT id FROM recordType WHERE DeveloperName   = 'Mass_Campaign_Active' ];
        Campaign masterC = RTL_TestUtility.createMasterCampaign('MasterCam1',
            'M20171001',
            'Mass',
            'MB',user
            );

        insert masterC;

        Campaign childC = RTL_TestUtility.createChildCampaign('ChildCam1','1', masterC , rt) ;
        insert childC;

        List<RTL_product_master__c> rpm = [select id,name,Product_Sub_group__c,Product_Group__c from RTL_product_master__c LIMIT 1];

        Retail_Campaign_Products__c rcp = new Retail_Campaign_Products__c();
        rcp.RTL_Campaign__c = childC.id;
        rcp.RTL_Product_Group__c = rpm.get(0).Product_Group__c;

        insert rcp;
////////////////// test mock///////////

        System.runAs(user){
        //create oppportunity with retail user so that the retail record type is set
            try{
                Campaign c = [SELECT ID FROM Campaign where RecordTypeId =: rt.id LIMIT 1];
                    opportunityList = RTL_TestUtility.createOpportunity(accountList,false);
                    opportunityList.get(0).CampaignId = c.Id;
                    opportunityList.get(0).RTL_Product_Name__c = rpm.get(0).id;
                    //opportunityList.get(0).RTL_TMB_Customer_ID_PE__c = '001100000000000000000011279374';
                    insert opportunityList;

                	opportunityList = RTL_TestUtility.createOpportunities(accountList[0],4,true);
                    opportunityList = RTL_TestUtility.createOpportunities(accountList[0],4,true);
            }catch( Exception e ){
                
            }
        }
      
        TEST.stopTest();
    }
    
    public static testmethod void testAutoCreateOrderFromOpty(){
        System.debug(':::: testAutoCreateOrderFromOpty Start ::::');
        TestUtility_Referral.createAppConfigReferralAssignment();
        TestUtility_Referral.createReferralAssignment();
        branchAndZoneList = RTL_TestUtility.createBranchZone(9,true);
        acctList = RTL_TestUtility.createAccounts(2,true);
        RTL_Branch_Manager_Title__c branchTitle = new RTL_Branch_Manager_Title__c(Name='Branch Manager',RTL_Value__c='Branch Manager');
        insert branchTitle;
        
        AppConfig__c Aconfig = AppConfig__c.getValues('runtrigger');
        Aconfig.Value__c = 'true';    
        update Aconfig;
            

        User currentUser = [Select id,RTL_License_No_Paper_1__c FROM User Where id =: UserInfo.getUserId() ];
        currentUser.RTL_License_No_Paper_1__c = '4325325426';
        update currentUser;

        Id retailOrderRecType = Schema.Sobjecttype.RTL_Referral__c.getRecordTypeInfosByName().get('Retail Order Transaction').getRecordTypeId();
        RTL_Referral__c referral = new RTL_Referral__c();
        referral.RecordTypeId = retailOrderRecType;
        referral.RTL_Account_Name__c = acctList[0].id;
        referral.RTL_Stage__c = 'New';
        referral.RTL_License_No__c = '4325325426';
        referral.RTL_Preferred_Branch__c = branchAndZoneList[0].id;
        insert referral;
        
        TEST.startTest();
        	Id retailInvestment = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Retail Investment').getRecordTypeId();
        	Opportunity opp = new Opportunity();
        	opp.Name = 'Test Opp';
        	opp.recordTypeId = retailInvestment;
        	opp.CloseDate = Date.today().addDays(30);
        	opp.Amount = 1;
            opp.StageName = 'Sales (Investment)';
        	opp.AccountId = acctList[0].id;
        	opp.RTL_Referral__c = referral.id;
        	insert opp;
        TEST.stopTest();
        System.debug(':::: testAutoCreateOrderFromOpty End ::::');
    }

}