global class RTL_ReferralChangeOwner {
    webservice static String acceptReferral(String referralId){
        String result;
        try{
            RTL_Referral__c referral = [SELECT Id,Name,OwnerId FROM RTL_Referral__c WHERE Id=:referralId];
        	referral.OwnerId = System.UserInfo.getUserId();
            referral.RTL_Is_Accept_Button__c = true;
        	update referral;
            result = 'Success';
        }catch(DMLException e){
            result = e.getDmlMessage(0);
        }catch(Exception e){
            result = e.getMessage();
        }
        return result;
        
    }
}