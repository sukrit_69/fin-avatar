@isTest
private class MiniCSV_Controller_Test {
	//public static final Id BDM_PROFILE_ID =   
	// public static final Id SE_SALES_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB SE Sales Management Profile' LIMIT 1].Id;
	public static final Id Commercial_Account_ID = Schema.Sobjecttype.Lead.getRecordTypeInfosByName().get('Commercial Account').getRecordTypeId();
    
    public static Id BDM_PROFILE_ID {
        get{
            if(BDM_PROFILE_ID == null){
                BDM_PROFILE_ID = [SELECT Id FROM Profile WHERE Name = 'TMB BDM Profile' LIMIT 1].Id;
            }
            return BDM_PROFILE_ID;
        }
        set;
	}
	public static RecordType CommercialLeadRecordType {
        get{
            if(CommercialLeadRecordType == null){
                CommercialLeadRecordType = [SELECT id,Name FROM RecordType 
                                        WHERE Name='Commercial Lead' 
                                        AND sObjectType='Lead' LIMIT 1];
            }
            return CommercialLeadRecordType;
        }
        set;
	}

	public static Lead CreateValidLead(Id OwnerID, Id accId){
        Lead leadRec = new lead();
        leadRec.Phone = '0877874871';
        leadRec.Company = 'LeadExtension';
        leadRec.ID_Type__c = 'Passport ID';
        leadRec.ID_Number__c = '13255555';
        leadRec.Mobile_No__c = '053532198';
        leadRec.Customer_Type__c = 'Individual';
        leadRec.FirstName =  'LeadExFirst';
        leadRec.LastName = 'LeadExLast';
        leadRec.RecordTypeId = CommercialLeadRecordType.id;
        leadRec.Interest_result__c = 'Yes';
        leadRec.OwnerID = OwnerID;
        leadRec.Account__c = accId;
        return leadRec;
    }

	@testSetup static void setupData() {
		TestUtils.createAppConfig();

		List<AppConfig__c> mc = new List<AppConfig__c> ();
        mc.Add(new AppConfig__c(Name = 'runtrigger', Value__c = 'false'));
        insert mc;

		RTL_Branch_Manager_Title__c branchTitle = new RTL_Branch_Manager_Title__c(Name='Branch Manager',RTL_Value__c='Branch Manager');
		insert branchTitle;
		
		Branch_and_zone__c bz = new Branch_and_zone__c();
        bz.Branch_Code__c = '611';
        insert bz;

		User User01 = RTL_TestUtility.createUsers(1, 'TestMock', 'SmartBDM', 'smartbdm@tmb.com.test', false).get(0);
		User01.ProfileId = BDM_PROFILE_ID;
		User01.isActive = true;
		User01.title = 'Branch Manager';
		User01.RTL_Branch_Code__c = bz.Branch_Code__c;
		User01.Employee_ID__c = '123456';
		insert User01;

		RecordType Existing_Customer = [SELECT Id FROM RecordType WHERE developername='Existing_Customer' AND SobjectType = 'Account' and IsActive = true LIMIT 1];
		// Account acc1 = RTL_TestUtility.createAccounts(1 , false).get(0);
		Account acc1 = TestUtils.createAccounts(1, 'test', 'CType', false).get(0);
		acc1.Customer_Type__c = 'Individual';
		acc1.OwnerID = User01.id;
		acc1.RecordType = Existing_Customer;
		acc1.RecordTypeId = Existing_Customer.Id;
		acc1.BIZ_FLAG__c = 'Y';
		acc1.WOW_FLAG__c = 'Y';
		acc1.Total_Revenue_Baht__c = 100000;
		acc1.Warning_Code__c = 'white';
		acc1.Business_Type_Code__c = 'C1023';
		insert acc1;

		IndustryMaster__c im = new IndustryMaster__c();
		im.TMBCode__c = 'C1023';
		im.SubIndustry__c ='asd';
		insert im;
		// Create Contact
		// Contact cont1 = new Contact();
		// cont1.FirstName = 'SmartBDM';
		// cont1.LastName = 'II';
		// cont1.Accountid = acc1.id;
		// insert cont1;

		Campaign masterC = RTL_TestUtility.createMasterCampaign('MasterCam1',
        	'D20171001',
        	'Exclusive',
        	'MB', User01
        	);
		insert masterC;

		RecordType rt = [SELECT id FROM recordType WHERE DeveloperName   = 'Exclusive_Campaign_Active' ];
		Campaign childC = RTL_TestUtility.createChildCampaign('ChildCam1','1', masterC , rt) ;
		insert childC;

		RecordType Commercial_Account = [SELECT Id FROM RecordType WHERE developername='Commercial_Account' AND SobjectType = 'Lead' and IsActive = true LIMIT 1];
		// Lead Lead1 = CreateValidLead(User01.id, acc1.id);
		// Lead Lead1 = RTL_TestUtility.createLeads(1, false).get(0);
		// Lead1.RecordType = Commercial_Account;
		// Lead1.RecordTypeId = CommercialLeadRecordType.id;
        // Lead1.Interest_result__c = 'Yes';
        // Lead1.OwnerID = User01.id;
        // Lead1.Account__c = acc1.id;
		// insert Lead1;
		List<Lead> lead_lst = RTL_TestUtility.createLeads(5, false);
		for (Lead l : lead_lst) {
			l.RecordType = Commercial_Account;
			l.RecordTypeId = CommercialLeadRecordType.id;
			l.Interest_result__c = 'Yes';
			l.OwnerID = User01.id;
			l.Account__c = acc1.id;
			l.Primary_Campaign__c = childC.id;
		}
		insert lead_lst;

		Collateral__c colla = new Collateral__c();
		// colla.Appraisal_Value__c = 123;
		// colla.Pledge_Value__c = 234;
		colla.Collateral_Group_Type__c = 'ASDFG';
		colla.Collateral_Group_Type_Key__c  = 'Test1234';
		colla.Account__c = acc1.id;
		insert colla;
		
		Collateral_Detail__c colladetail = new Collateral_Detail__c();
		colladetail.Appraisal_Date__c = system.today();
		colladetail.Collateral__c = colla.id;
		insert colladetail;

		Product2 prod1 = new Product2(Name = 'Funding', Product_Domain__c ='Funding & Borrowing', Family = 'Hardware', IsActive=true);
		insert prod1;
		Product_Information_On_Hand__c onhand1 = new Product_Information_On_Hand__c();
        onhand1.Account__c = acc1.Id;
        onhand1.Product_Hierachy_Code__c = String.valueOf(prod1.id);
        insert onhand1;
		Product_Information_Detail__c detail1 = new Product_Information_Detail__c();
        detail1.Product_Information__c = onhand1.id;
		// detail1.ACC_S1B__c = 'Y';
		// detail1.EDC_FLAG__c = 'Y';
		// detail1.LATEST_MTH_TXN__c = 59;
		// detail1.LATEST_SIXMTH_TXN__c = 49;
        insert detail1;

		Product2 prod2 = new Product2(Name = 'Deposit', Product_Domain__c ='Deposit & Investment', Family = 'Hardware', IsActive=true);
        insert prod2;
		Product_Information_On_Hand__c onhand2 = new Product_Information_On_Hand__c();
        onhand2.Account__c = acc1.Id;
        onhand2.Product_Hierachy_Code__c = String.valueOf(prod2.id);
        insert onhand2;
		Product_Information_Detail__c detail2 = new Product_Information_Detail__c();
        detail2.Product_Information__c = onhand2.id;
		detail2.Customer__c = acc1.Id;
		detail2.ACC_S1B__c = 'Y';
		// detail2.EDC_FLAG__c = 'Y';
		detail2.LATEST_MTH_TXN__c = 8;
		detail2.LATEST_SIXMTH_TXN__c = 8;
        insert detail2;
		Product_Information_Detail__c detail3 = new Product_Information_Detail__c();
        detail3.Product_Information__c = onhand2.id;
		detail3.Customer__c = acc1.Id;
		detail3.ACC_S1B__c = 'N';
		// detail3.EDC_FLAG__c = 'Y';
		detail3.LATEST_MTH_TXN__c = 9;
		detail3.LATEST_SIXMTH_TXN__c = 9;
        insert detail3;



	}

	@isTest static void test_method_MiniCSV_Controller_Customer() {
		Test.startTest();	

		User user = [SELECT Id, Name FROM User WHERE Email = 'smartbdm@tmb.com.test' AND ProfileId =: BDM_PROFILE_ID LIMIT 1];
		System.runAs(user){
			Id Id = [SELECT Id FROM Account WHERE Customer_Type__c = 'Individual' AND OwnerId =: user.id LIMIT 1].Id;
			MiniCSV_Controller ext;

			Test.setCurrentPageReference(new PageReference('Page.Mini_CSV'));
			System.currentPageReference().getParameters().put('Id', Id);
			System.currentPageReference().getParameters().put('refType', 'Accountlist');
			System.currentPageReference().getParameters().put('retURL', ApexPages.currentPage().getUrl());
			
			ext = new MiniCSV_Controller();
			Boolean b = ext.isSF1;	
			ext.getAccount();
			ext.getLeads();
			ext.getCreditInfoes();
			ext.getCollateralDetails();	
			ext.getDepositInfoes();	

			// xxxMTH_TXN = Medium
			List<Product_Information_Detail__c> tmp_detailProd = [SELECT Id, LATEST_MTH_TXN__c, LATEST_SIXMTH_TXN__c
				FROM Product_Information_Detail__c 
				WHERE Customer__c =: ext.account.id and Product_Domain__c = 'Deposit & Investment'
			];
			tmp_detailProd.get(1).LATEST_MTH_TXN__c = 35;
			tmp_detailProd.get(1).LATEST_SIXMTH_TXN__c = 35;
			update tmp_detailProd;
			ext.CheckingProductHoldingInfo();
		}

        Test.stopTest();
	}

	
	
	@isTest static void test_method_MiniCSV_Controller_Lead() {
		Test.startTest();
		User user = [SELECT Id, Name FROM User WHERE Email = 'smartbdm@tmb.com.test' AND ProfileId =: BDM_PROFILE_ID LIMIT 1];
		System.runAs(user){
			Id Id = [SELECT Id FROM Account WHERE Customer_Type__c = 'Individual' AND OwnerId =: user.id LIMIT 1].Id;
			MiniCSV_Controller ext;

			Test.setCurrentPageReference(new PageReference('Page.Mini_CSV'));
			System.currentPageReference().getParameters().put('Id', Id);
			System.currentPageReference().getParameters().put('refType', 'Leadlist');
			System.currentPageReference().getParameters().put('retURL', ApexPages.currentPage().getUrl());
			System.currentPageReference().getParameters().put('isdtp', 'p1');
			
			ext = new MiniCSV_Controller();
			Boolean b = ext.isSF1;	
			ext.getAccount();
			ext.getLeads();
			ext.getCreditInfoes();
			ext.getCollateralDetails();		
			ext.getDepositInfoes();

			// xxxMTH_TXN = High
			List<Product_Information_Detail__c> tmp_detailProd = [SELECT Id, LATEST_MTH_TXN__c, LATEST_SIXMTH_TXN__c
				FROM Product_Information_Detail__c 
				WHERE Customer__c =: ext.account.id and Product_Domain__c = 'Deposit & Investment'
			];
			tmp_detailProd.get(1).LATEST_MTH_TXN__c = 80;
			tmp_detailProd.get(1).LATEST_SIXMTH_TXN__c = 80;
			update tmp_detailProd;
			ext.CheckingProductHoldingInfo();

		}
        Test.stopTest();
	}

	@isTest static void test_method_MiniCSV_Controller_getFullNameContactLead() {
		Test.startTest();
		User user = [SELECT Id, Name FROM User WHERE Email = 'smartbdm@tmb.com.test' AND ProfileId =: BDM_PROFILE_ID LIMIT 1];
		System.runAs(user){
			Id Id = [SELECT Id FROM Account WHERE Customer_Type__c = 'Individual' AND OwnerId =: user.id LIMIT 1].Id;
			MiniCSV_Controller ext;

			Test.setCurrentPageReference(new PageReference('Page.Mini_CSV'));
			System.currentPageReference().getParameters().put('Id', Id);
			System.currentPageReference().getParameters().put('refType', 'Leadlist');
			System.currentPageReference().getParameters().put('retURL', ApexPages.currentPage().getUrl());

			Campaign cam = [SELECT Id, Name FROM Campaign WHERE Name = 'ChildCam1' LIMIT 1];
			Lead Lead1 = CreateValidLead(user.id, Id);
			Lead1.Primary_Campaign__c = cam.Id;
			insert Lead1;
			ext = new MiniCSV_Controller();
			ext.getFullNameContactLead();


			// Create Contact
			Contact cont1 = new Contact();
			cont1.FirstName = 'SmartBDM';
			cont1.LastName = 'II';
			cont1.Accountid = Id;
			insert cont1;
			ext = new MiniCSV_Controller();
			ext.getFullNameContactLead();


			//Negative throw Exception - Not availiable
			ext.contact = null;
			ext.leads = null;
			ext.getFullNameContactLead();
		}	
		Test.stopTest();
	}

	@isTest static void test_method_MiniCSV_Controller_Negative() {
		Test.startTest();
		User user = [SELECT Id, Name FROM User WHERE Email = 'smartbdm@tmb.com.test' AND ProfileId =: BDM_PROFILE_ID LIMIT 1];
		System.runAs(user){
			Id Id = [SELECT Id FROM Account WHERE Customer_Type__c = 'Individual' AND OwnerId =: user.id LIMIT 1].Id;
			MiniCSV_Controller ext;

			Test.setCurrentPageReference(new PageReference('Page.Mini_CSV'));
			System.currentPageReference().getParameters().put('Id', '001p0000008XXXS');
			System.currentPageReference().getParameters().put('refType', 'Leadlist');
			System.currentPageReference().getParameters().put('retURL', ApexPages.currentPage().getUrl());

			ext = new MiniCSV_Controller();
			ext.collateral_details = null;
			ext.credit_infoes = null;
			ext.product_holding_info = null;
			ext.BDMLatestMTHTXN = null;
			ext.BDMLatestSIXMTHTXN = null;
			ext.account = null;
			List<Collateral_Detail__c> collateral_details = ext.collateral_details;
			List<Product_Information_Detail__c> credit_infoes = ext.credit_infoes;
			List<Product_Information_Detail__c> product_holding_info = ext.product_holding_info;
			String BDMLatestMTHTXN = ext.BDMLatestMTHTXN;
			String BDMLatestSIXMTHTXN = ext.BDMLatestSIXMTHTXN;
			

		}	
		Test.stopTest();
	}

}