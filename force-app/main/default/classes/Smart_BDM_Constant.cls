public class Smart_BDM_Constant {

    public static Map<string , Smart_BDM_Constant__mdt> Smart_BDM_Constants {
        get{
            if (null == Smart_BDM_Constants) {
                Smart_BDM_Constants = new Map<string , Smart_BDM_Constant__mdt>();
                for (List<Smart_BDM_Constant__mdt> cons : [Select Id, MasterLabel , DeveloperName , Value__c From Smart_BDM_Constant__mdt]) {
                    for (Smart_BDM_Constant__mdt con : cons) {
                        Smart_BDM_Constants.put(con.DeveloperName , con);
                    }
                }
            }
            return Smart_BDM_Constants;
        }
        set ;
    }
    public static final string  MAX_TOTAL_REVENUS   =  Smart_BDM_Constants.containsKey('MAX_TOTAL_REVENUS') ? Smart_BDM_Constants.get('MAX_TOTAL_REVENUS').value__c :  '100000000' ;
    public static final string  RED_COLOR   =  Smart_BDM_Constants.containsKey('RED_COLOR') ? Smart_BDM_Constants.get('RED_COLOR').value__c :  'red' ;
    public static final string  GREEN_COLOR   =  Smart_BDM_Constants.containsKey('GREEN_COLOR') ? Smart_BDM_Constants.get('GREEN_COLOR').value__c :  'green' ;
    public static final string  WHITE_COLOR   =  Smart_BDM_Constants.containsKey('WHITE_COLOR') ? Smart_BDM_Constants.get('WHITE_COLOR').value__c :  'white' ;
    public static final string  CREDIT_INFO_WARNING_MESSAGE   =  Smart_BDM_Constants.containsKey('CREDIT_INFO_WARNING_MESSAGE') ? Smart_BDM_Constants.get('CREDIT_INFO_WARNING_MESSAGE').value__c :  'End O/S > Limit' ;
    public static final string  TOTAL_REVENUS_WARNING_MESSAGE   =  Smart_BDM_Constants.containsKey('TOTAL_REVENUS_WARNING_MESSAGE') ? Smart_BDM_Constants.get('TOTAL_REVENUS_WARNING_MESSAGE').value__c :  'รายได้เกิน 100 ล้านบาท' ;
    public static final string  TOTAL_REVENUE_EMPTY_WARNING_MESSAGE   =  Smart_BDM_Constants.containsKey('TOTAL_REVENUE_EMPTY_WARNING_MESSAGE') ? Smart_BDM_Constants.get('TOTAL_REVENUE_EMPTY_WARNING_MESSAGE').value__c :  'Total revenue is empty' ;
    public static final string  BUSINESS_CODE_WARNING_MESSAGE   =  Smart_BDM_Constants.containsKey('BUSINESS_CODE_WARNING_MESSAGE') ? Smart_BDM_Constants.get('BUSINESS_CODE_WARNING_MESSAGE').value__c :  'ประเภทธุรกิจไม่ผ่านเกณฑ์ธนาคาร' ;
    public static final string  SUCCESS_MESSAGE   =  Smart_BDM_Constants.containsKey('SUCCESS_MESSAGE') ? Smart_BDM_Constants.get('SUCCESS_MESSAGE').value__c :  'Pass basic pre-screen' ;
    public static final string  HOST_SYSTEM   =  Smart_BDM_Constants.containsKey('HOST_SYSTEM') ? Smart_BDM_Constants.get('HOST_SYSTEM').value__c :  'SLS' ;
    public static final string  DEEP_LINK_TO_KONY_TELL_LANDING   =  Smart_BDM_Constants.containsKey('DEEP_LINK_TO_KONY_TELL_LANDING') ? Smart_BDM_Constants.get('DEEP_LINK_TO_KONY_TELL_LANDING').value__c :  'tmbsmart://?action=quickCATellMeNowLandingPage' ;
    public static final string  DEEP_LINK_TO_KONY_LANDING   =  Smart_BDM_Constants.containsKey('DEEP_LINK_TO_KONY_LANDING') ? Smart_BDM_Constants.get('DEEP_LINK_TO_KONY_LANDING').value__c :  'tmbsmart://?action=quickCALandingPage' ;
    public static final string SE_CREDIT_PRODUCT_RECORDTYPE_ID_OPPTY = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('SE Credit Product').getRecordTypeId();
}