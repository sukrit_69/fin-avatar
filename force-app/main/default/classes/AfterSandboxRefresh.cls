global class AfterSandboxRefresh implements SandboxPostCopy{
	 private String FOR_SANDBOX = 'FOR_SANDBOX';

     global void runApexClass(SandboxContext context) {
     	//clear custom setting
     	clearCustomSetting();
     }

     public AfterSandboxRefresh(){}

     public void runExecute() {
     	/**
     		AfterSandboxRefresh refresh = new AfterSandboxRefresh();
			refresh.runExecute();
		**/
     	clearCustomSetting();
     }

     private void clearCustomSetting() {
     	String splitFieldName = '0__o';

     	List<For_After_Refresh_Sandbox__c> afterSBList 
     								= [SELECT Id
										     	,Field_Name__c
										     	,Name__c
										     	,Value__c 
										FROM For_After_Refresh_Sandbox__c
									   ];

		if (afterSBList != null && afterSBList.size() > 0) {
			MAP<String,List<String>> mapResetCS = new MAP<String,List<String>>();
			Map<String,List<SObject>> sObjMap   = new Map<String,List<SObject>>();

			for (For_After_Refresh_Sandbox__c afterSB: afterSBList) {
				String name  = afterSB.Name__c;
				String fieldName  = afterSB.Field_Name__c;

				List<String> valList = afterSB.Value__c.split(',');

				if (mapResetCS.containsKey(name+splitFieldName+fieldName)) {
					List<String> existValList = mapResetCS.get(name+splitFieldName+fieldName);
					existValList.addAll(valList);
				}else {
					mapResetCS.put(name+splitFieldName+fieldName,valList);
				}
			}

			for (String sobjNameField : mapResetCS.KeySet()) {
				List<String> valList = mapResetCS.get(sobjNameField);
				List<String> sobjNameList = sobjNameField.split(splitFieldName);
				String sobjName  = sobjNameList[0];
				String fieldName = sobjNameList[1];

				String joinList = String.join( valList, '\' , \'' );

				String creteriaIn = '(\''+joinList+'\')';
				
				String sql = 'SELECT '+fieldName+' FROM '+sobjName+' WHERE NAME IN '+creteriaIn;
				System.debug(sql);
				List<SObject> sobjList = Database.query(sql);

				for (SObject sobj : sobjList) {
		     		sobj.put(fieldName, FOR_SANDBOX);
		     	}


				sObjMap.put(sobjName,sobjList);
			}

			for (String key : sObjMap.KeySet()) {
				List<SObject> sObjList = sObjMap.get(key);
				update sObjList;
			}
		}
     }
}