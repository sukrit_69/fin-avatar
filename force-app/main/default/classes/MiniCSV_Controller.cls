public class MiniCSV_Controller {
	public static final String ISHAVING = 'มี';
	public static final String ISNOTHAVING = 'ไม่มี';

    public String RED_COLOR = Smart_BDM_Constant.RED_COLOR;
    public String GREEN_COLOR = Smart_BDM_Constant.GREEN_COLOR;
	public String WHITE_COLOR = Smart_BDM_Constant.WHITE_COLOR;
    public String MAX_TOTAL_REVENUS = Smart_BDM_Constant.MAX_TOTAL_REVENUS;
	public String SUCCESS_MESSAGE= Smart_BDM_Constant.SUCCESS_MESSAGE;
	// public String BUSINESS_CODE_WARNING_MESSAGE = Smart_BDM_Constant.BUSINESS_CODE_WARNING_MESSAGE;
	public Id id { get; set; }
	public String retURL { get; set; }
	public String refType { get; set; }
	public String navTitle { get; set; }
	public String successMsg { get; set; }
	public Account account {
		get {
			if(account == null){
				account = new Account();
			}
			return account;
		}
		set;
	}
    
    public class colorCodeStatus{
        public string creditInfo { 
            get { if(creditInfo == null)creditInfo = 'green';
                    return creditInfo;
                }
            set; }
        public string riskInfo { 
            get { if(riskInfo == null)riskInfo = 'red';
                    return riskInfo;
                }
            set; }
        public string businessInfo { 
            get { if(businessInfo == null)businessInfo = 'red';
                    return businessInfo;
                }
            set; }
        public string summaryInfo { 
            get { if(summaryInfo == null)summaryInfo = 'red';
                    return summaryInfo;
                }
            set; }
    }
    // public class WarningMessage{
    //      public string brlWarning { 
    //         get { if(brlWarning == null)brlWarning = '';
    //                 return brlWarning;
    //             }
    //         set; }
    //      public string csiWarning { 
    //         get { if(csiWarning == null)csiWarning = '';
    //                 return csiWarning;
    //             }
    //         set; }
    // }
    public List<String> warningMsg {
        	get{
                if(warningMsg == null){
                    warningMsg = new List<String>();
			}
				return warningMsg;
			}
        	set;}
    
	public Contact contact { get; set; }
	public String FullNameContactInfo { get; set; }
	// public Lead lead { get; set; }
	public List<Lead> leads { 
		get{
			if(leads == null){
				leads = new List<Lead>();
			}
			return leads;
		}
		set;
	}
	public List<Collateral_Detail__c> collateral_details { 
		get{
			if(collateral_details == null){
				collateral_details = new List<Collateral_Detail__c>();
			}
			return collateral_details;
		}
		set;
	}
	public List<Product_Information_Detail__c> credit_infoes { 
		get{
			if(credit_infoes == null){
				credit_infoes = new List<Product_Information_Detail__c>();
			}
			return credit_infoes;
		}
		set;
	}
	public List<Product_Information_Detail__c> product_holding_info {
		get{
			if(product_holding_info == null){
				product_holding_info = new List<Product_Information_Detail__c>();
			}
			return product_holding_info;
		}
		set;
	}
	public String Deeplink_iOS { get; set; }
    public String Deeplink_Android { get; set; }
	public String Deeplink_KnowledgeCenter { get; set; }

	
	public Boolean isDepositAccount { get { if(isDepositAccount == null) isDepositAccount = false; return isDepositAccount;} set; }
	public Boolean isS1B { get { if(isS1B == null) isS1B = false; return isS1B;} set; }
	public Boolean isBIZTOUCH { get { if(isBIZTOUCH == null) isBIZTOUCH = false; return isBIZTOUCH;} set; }
	public Boolean isBIZWOW { get { if(isBIZWOW == null) isBIZWOW = false; return isBIZWOW;} set; }
	public Boolean isEDC { get { if(isEDC == null) isEDC = false; return isEDC;} set; }
    
	public String BDMLatestMTHTXN { 
		get {
			if(BDMLatestMTHTXN == null){
				BDMLatestMTHTXN = 'Low';
			}
			return BDMLatestMTHTXN;
		}
		set; 
	}
	public String BDMLatestSIXMTHTXN { 
		get {
			if(BDMLatestSIXMTHTXN == null){
				BDMLatestSIXMTHTXN = 'Low';
			}
			return BDMLatestSIXMTHTXN;
		}
		set; 
	}
	public String ActionRecommand { get; set; }
	
	public colorCodeStatus codeList { get; set; }
    // public WarningMessage warningList { get; set; }
    
    public string brl_color ;
    public string csi_color ;
    public string seg_color ;
    public string total_reven_color ;
    public string busi_code_color ;
	public string warning_code_account {get;set;}
    
    
    public String getBRLColor() {
		try {
            string  color;
            List<Smart_BDM_BRL_Code__mdt> bcList = [SELECT Color_Code__c, Warning_Message__c 
                            FROM Smart_BDM_BRL_Code__mdt 
                            WHERE BRL__c=:account.NAMBRL__c ];
            if(bcList.size() > 0){
                color = bcList[0].Color_Code__c;
                if(color == RED_COLOR){
                	warningMsg.add(bcList[0].Warning_Message__c);
                }
            }else{
                color = WHITE_COLOR;
            }
            
            return color;
		} catch (Exception e) {
		   	System.debug('getBRLColor error : ' + e.getMessage());
            return null;
		}
		
	}
    
    public String getCSIColor() {
		string color;
		
		try {
			List<String> CSIList = new List<String>();
			if(account.Warning_Code__c != null){
				CSIList = account.Warning_Code__c.split(',');
				System.debug('CSIList size : '+CSIList.size());
				List<Smart_BDM_CSI_Warning__mdt> smartCSIList =[SELECT Color_Code__c,CSI_Code__c , Warning_Message__c 
																FROM Smart_BDM_CSI_Warning__mdt  
																WHERE CSI_Code__c IN:CSIList ];
				Map<String, String> warningMap = new Map<String,String>();
				for(Smart_BDM_CSI_Warning__mdt csiMsg : smartCSIList)
				{
					warningMap.put(csiMsg.CSI_Code__c, csiMsg.Warning_Message__c);
				}
				System.debug('warningMap : '+warningMap);
				if(smartCSIList.size() > 0){
					color = RED_COLOR;
					for(String csi : CSIList){
						warningMsg.add(warningMap.get(csi));
					}
					System.debug('warningMsg : '+warningMsg);
				}else{
					color = GREEN_COLOR;
				}
			}else{
				 color = WHITE_COLOR;
			}
            return color;
		} catch (Exception e) {
		   	System.debug('getCSIColor error : ' + e.getMessage());
            return null;
		}
		
	}
    
    public String getSEGColor() {
		try {
            string color;
            List<Smart_BDM_Segment__mdt>  bdm_segment	=	[SELECT Color_Code__c , Warning_Message__c
                                                            FROM Smart_BDM_Segment__mdt  
                                                            WHERE Segment_Code__c =:account.Core_Banking_Suggested_Segment__c ];
            if(bdm_segment.size()>0){
				color = bdm_segment[0].Color_Code__c;        
				warningMsg.add(bdm_segment[0].Warning_Message__c);    
            }else{
                color = WHITE_COLOR;
            }
            return color;
		} catch (Exception e) {
		   	System.debug('getSEGColor error : ' + e.getMessage());
            return null;
		}
		
	}
    
    public String getTOTALREVENColor() {
		try {
            string color ;
			if(account.Total_Revenue_Baht__c != null && account.Total_Revenue_Baht__c >= 0){
				if(account.Total_Revenue_Baht__c > Integer.valueOf(MAX_TOTAL_REVENUS) ){
					color = RED_COLOR;
					string msg = Smart_BDM_Constant.TOTAL_REVENUS_WARNING_MESSAGE;
					warningMsg.add(msg);
				}else{
					color = GREEN_COLOR;
				}
			}else{

				color = WHITE_COLOR;  
			}
            
            return color;
		} catch (Exception e) {
		   	System.debug('getTOTALREVENColor error : ' + e.getMessage());
            return null;
		}
		
	}
    
    public String getBusinessCodeColor() {
		try {
            string color;
			if(account.Business_Type_Code__c != null && account.Business_Type_Code__c != ''){
				List<IndustryMaster__c> industMaster = [SELECT TMBCode__c ,ColorCode__c
                                            FROM IndustryMaster__c 
                                            WHERE TMBCode__c=:account.Business_Type_Code__c ];
				if(industMaster.size() > 0){
					color= industMaster[0].ColorCode__c;
					if(color == RED_COLOR){
						warningMsg.add(Smart_BDM_Constant.BUSINESS_CODE_WARNING_MESSAGE);   
					}
				}else{
					color = WHITE_COLOR;
				}
			}else{
				color = WHITE_COLOR;   
			}
			return color;
            
		} catch (Exception e) {
		   	System.debug('getBusinessCodeColor error : ' + e.getMessage());
            return null;
		}
		
	}
    
    public colorCodeStatus getRiskInfoColor(string brlCode,string csiCode,colorCodeStatus codeList) {
		
        try {
			 
            List<Smart_BDM_Risk_Info__mdt>  riskInfoList = [SELECT BRL_Code__c  , CSI_Code__c ,Color_Code__c
                            FROM Smart_BDM_Risk_Info__mdt  
                            WHERE BRL_Code__c=:brlCode  AND CSI_Code__c=:csiCode  ];
			if(riskInfoList.size() > 0 ){
				codeList.riskInfo  = riskInfoList[0].Color_Code__c;
			}else{
				codeList.riskInfo  = WHITE_COLOR;
			}
		} catch (Exception e) {
            codeList.riskInfo  = WHITE_COLOR;
		   	System.debug('getRiskInfoColor error : ' + e.getMessage());
            return codeList;
		}
        return codeList;
	}
    
    public colorCodeStatus getBusinessColor(string busiCode ,string segCode,string revenueCode,colorCodeStatus codeList) {
		
        try {

            List<Smart_BDM_Business_Info_Display__mdt>  busiInfoList = [SELECT Business_Code__c   , Segment_Code__c  , Status_Total_Revenue__c ,Color_Code__c
                            FROM Smart_BDM_Business_Info_Display__mdt   
                            WHERE Business_Code__c=:busiCode  AND Segment_Code__c=:segCode AND Status_Total_Revenue__c  =:revenueCode  ];
			if(busiInfoList.size() > 0 ){
				codeList.businessInfo  = busiInfoList[0].Color_Code__c;
			}else{
				codeList.businessInfo  = WHITE_COLOR;
			}
		
		} catch (Exception e) {
            codeList.businessInfo  = WHITE_COLOR;
		   	System.debug('getBusinessColor error : ' + e.getMessage());
            return codeList;
		}
        return codeList;
	}
    
    public colorCodeStatus getSummaryInfoColor(colorCodeStatus codeList) {
		
        try {
             List<Smart_BDM_Summary__mdt>  sumInfoList = [SELECT Business_Info_Code__c   , Risk_Info_Code__c ,Color_Code__c 
								FROM Smart_BDM_Summary__mdt   
								WHERE Business_Info_Code__c=:codeList.riskInfo  
								AND Risk_Info_Code__c=:codeList.businessInfo];
			if(sumInfoList.size() > 0 ){
				codeList.summaryInfo  = sumInfoList[0].Color_Code__c;
			}else{
				codeList.summaryInfo  = WHITE_COLOR;
			}
		} catch (Exception e) {
            codeList.summaryInfo  = WHITE_COLOR;
		   	System.debug('getSummaryInfoColor error : ' + e.getMessage());
            return codeList;
		}
        return codeList;
	}
    
    // public colorCodeStatus getCreditInfoColor(List<Product_Information_Detail__c> productList,colorCodeStatus codeList) {
	// 	codeList.creditInfo = GREEN_COLOR;
    //     for(Product_Information_Detail__c pid : productList ){
    //         if(pid.Ending_out_Ending_Balance__c > pid.Limit_Balance__c){
    //             codeList.creditInfo = RED_COLOR;
	// 			string msg = Smart_BDM_Constant.CREDIT_INFO_WARNING_MESSAGE;
	// 			warningMsg.add(msg);
    //             return codeList;
    //         }
    //     }
    //     return codeList;
        
	// }
    
	public MiniCSV_Controller() {
		this.Id = ApexPages.currentPage().getParameters().get('id');
		this.retURL = ApexPages.currentPage().getParameters().get('retURL');
		this.refType = ApexPages.currentPage().getParameters().get('refType');
		successMsg =  SUCCESS_MESSAGE;
		
		// this.Id = '001p000000WGeGVAA1';
		// this.Id = '001p000000WGh3dAAD';
		// this.Id = '001p000000WGgPeAAL';
		// this.Id = '001p000000WGeGVAA1'; // Check Product holding info
		// this.Id = '001p000000cJ8ydAAC'; // Check Full name Contact info
		
		if( navTitle == null){
			if(refType == 'Accountlist'){
				this.navTitle = Label.My_Customers;
			}
			else if(refType == 'Leadlist'){
				this.navTitle = Label.My_Leads;
			}
		}
		
		Smart_BDM_Setting__mdt settings = [SELECT Id, Number_of_Records__c, Deep_link_iOS__c, Deep_link_Android__c, Deep_link_Knowledge_Center__c, Deep_link_Product_Details__c
				FROM Smart_BDM_Setting__mdt 
				WHERE DeveloperName = 'Smart_BDM_Setting'];
		this.Deeplink_iOS = settings.Deep_link_iOS__c;
		this.Deeplink_Android = settings.Deep_link_Android__c;
		this.Deeplink_KnowledgeCenter = settings.Deep_link_Android__c;

		account = getAccount();
		warning_code_account = '';
		if(account.Warning_Code__c != null){
			warning_code_account = account.Warning_Code__c.replace(',',', ');
		}
		leads = getLeads();
		contact = getContact();
		FullNameContactInfo = getFullNameContactLead();
		credit_infoes = getCreditInfoes();
		collateral_details = getCollateralDetails();
		product_holding_info = getProductHoldingInfo();
        
		
		// Start Smart BDM II
		CheckingProductHoldingInfo();
		System.debug('isDepositAccount: ' + isDepositAccount + '| isS1B: ' + isS1B + '| isBIZTOUCH: ' + isBIZTOUCH + '| BDMLatestSIXMTHTXN: ' + BDMLatestSIXMTHTXN);
		ActionRecommand = AccountUtility.CalculateRecommendedAction(isDepositAccount, isS1B, isBIZTOUCH, BDMLatestSIXMTHTXN);
		System.debug('ActionRecommand ::: ' + ActionRecommand);
        
        //Traffic Light       
        codeList = new colorCodeStatus();
        // warningList = new WarningMessage();
        // system.debug('warningList : '+warningList);
        brl_color = getBRLColor();
        csi_color = getCSIColor();
        seg_color = getSEGColor();
        total_reven_color = getTOTALREVENColor() ;
        busi_code_color = getBusinessCodeColor() ;
        //system.debug('warningList- : '+warningList);
        system.debug('brl_color : '+brl_color);
        system.debug('csi_color : '+csi_color);
        system.debug('seg_color : '+seg_color);
        system.debug('total_reven_color : '+total_reven_color);
        system.debug('busi_code_color : '+busi_code_color);
        codeList = getRiskInfoColor(brl_color,csi_color,codeList);
        codeList = getBusinessColor(busi_code_color,seg_color,total_reven_color,codeList);
        // codeList = getCreditInfoColor(credit_infoes,codeList);
        codeList = getSummaryInfoColor(codeList);
		system.debug('warningMsg : '+warningMsg);
        system.debug('codeList : '+codeList);
        
		
	}


	public Account getAccount(){
		try{
			Account acct = [SELECT Id, Name, TMB_Customer_ID_PE__c, ID_Number_PE__c, NAMBRL__c, Warning_Code__c,
					CSI_Waring_Message__c, Core_Banking_Suggested_Segment__c, Total_Revenue_Baht__c,
					Business_Type_Code__c, Business_Type_Description__c, RTL_Registered_Address__c,
					Registered_Address_Line_1_PE__c, Registered_Address_Line_2_PE__c, Registered_Address_Line_3_PE__c,
					Province_Registered_PE__c, Zip_Code_Registered_PE__c,
					Mobile_Number_PE__c, RTL_Office_Phone_Number__c, Phone,
					BIZ_Touch__c, BIZ_Wow__c
					FROM Account 
					WHERE Id =: Id
					LIMIT 1];
			return acct;
		}catch(Exception e){
			System.debug('getAccount error: ' + e.getMessage());
			return null;
		}
	}

	public String getFullNameContactLead() {
		try {
			System.debug('contact ::: ' + contact);
			System.debug('leads ::: ' + leads);
			if(contact != null) { return contact.Name; }
			else if(leads.size() > 0) { return leads.get(0).Name; }
		} catch (Exception e) {
		   	System.debug('getFullNameContactLead error : ' + e.getMessage());
		}
		return null;
	}

	public Contact getContact(){
		try{
			Contact tmpContact = [SELECT Id, Name 
						FROM Contact 
						WHERE AccountId =: account.Id 
						AND RecordType.Name IN ('Core Bank', 'Salesforce')
						ORDER BY LastModifiedDate DESC
						LIMIT 1];
			return tmpContact;
		}catch(Exception e){
			System.debug('getContact error: ' + e.getMessage());
			return null;
		}
	}
	
	public List<Lead> getLeads(){
		try{
			List<Lead> tmp_leads = [SELECT Id, Name, Primary_Campaign__c, CreatedDate, Remark__c,
						Primary_Campaign__r.Name, Account__c
						FROM Lead 
						WHERE Account__c  =: account.Id
						ORDER BY LastModifiedDate DESC
						];
			return tmp_leads;
		}catch(Exception e){
			System.debug('getLeads error: ' + e.getMessage());
			return new List<Lead>();
		}
	}

	public List<Product_Information_Detail__c> getCreditInfoes(){
		try{
			List<Product_Information_Detail__c> tmp_creditInfoes = [SELECT Id, Name, Product_Information__r.Product_Hierachy_Code__r.Name, 
						Product_Hierachy_Code__c, Product_Description__c, TMB_Account_ID__c,
						Limit_Balance__c, Ending_out_Ending_Balance__c, Rate__c
						FROM Product_Information_Detail__c 
						WHERE Customer__c =: account.Id and Product_Domain__c = 'Funding & Borrowing'
						];
			return tmp_creditInfoes;
		}catch(Exception e){
			System.debug('getCreditInfoes error: ' + e.getMessage());
			return new List<Product_Information_Detail__c>();
		}
	}

	public List<Product_Information_Detail__c> getDepositInfoes(){
		try{
			List<Product_Information_Detail__c> tmp_creditInfoes = [SELECT Id, Account_Name__c,TMB_Account_ID__c, TMB_Suffix__c, Product_Performance_Name__c, Ending_out_Ending_Balance__c, Rate__c
					FROM Product_Information_Detail__c 
					WHERE Customer__c =: account.Id and Product_Domain__c = 'Deposit & Investment'
					];
			return tmp_creditInfoes;
		}catch(Exception e){
			System.debug('getDepositInfoes error: ' + e.getMessage());
			return new List<Product_Information_Detail__c>();
		}
	}

	public List<Collateral_Detail__c> getCollateralDetails(){
		try{
			list<string> collateralID = new list<string>();
			list<Collateral__c> listCollateral = new list<Collateral__c>();
			list<Collateral_Detail__c> listCollateralDetail = new list<Collateral_Detail__c>();

			list<Collateral_Detail__c> tmp_listColDtl = new list<Collateral_Detail__c>();

			for (Collateral__c co : [SELECT Id, Account__c, Appraisal_Value__c, Pledge_Value__c, Collateral_Group_Type__c
									FROM Collateral__c WHERE Account__c =: account.id])
			{
				collateralID.add(co.id);
				listCollateral.add(co);
			}
			
			for (Collateral_Detail__c cod : [SELECT Id, Collateral_Type__c, Collateral_Owner__c, 
											Appraisal_Date__c, Appraisal_Value__c, Pledge_Value__c,
											Collateral_Joint_BOT__c, Collateral__c
											FROM Collateral_Detail__c WHERE Collateral__c in: collateralID])
			{
				listCollateralDetail.add(cod);
			}

			for (Collateral__c co : listCollateral)
			{
				
				for (Collateral_Detail__c cod : listCollateralDetail)
				{
					if (cod.Collateral__c == co.Id)
					{
						tmp_listColDtl.add(cod);
					}
				}
				
			}
			return tmp_listColDtl;
		}catch(Exception e){
			System.debug('getCollateralDetails error: ' + e.getMessage());
			return new List<Collateral_Detail__c>();
		}
	}

	public List<Product_Information_Detail__c> getProductHoldingInfo(){
		try{
			List<Product_Information_Detail__c> tmp_Product_Info = [SELECT Id, Deposit_Account__c, S1B__c, BDM_Latest_SIXMTH_TXN__c, BDM_Latest_MTH_TXN__c
				FROM Product_Information_Detail__c 
				WHERE Customer__c =: account.Id and Product_Domain__c = 'Deposit & Investment'
				];
			return tmp_Product_Info;
		}catch(Exception e){
			System.debug('getCollateralDetails error: ' + e.getMessage());
			return new List<Product_Information_Detail__c>();
		}
	}

	public void CheckingProductHoldingInfo(){
		Map<String, List<Product_Information_Detail__c>> mapSIXMonthTXN = new Map<String, List<Product_Information_Detail__c>>{
			'Low' => new List<Product_Information_Detail__c>(),
			'Medium' => new List<Product_Information_Detail__c>(),
			'High' => new List<Product_Information_Detail__c>()
		};

		Map<String, List<Product_Information_Detail__c>> mapLASTMonthTXN = new Map<String, List<Product_Information_Detail__c>>{
			'Low' => new List<Product_Information_Detail__c>(),
			'Medium' => new List<Product_Information_Detail__c>(),
			'High' => new List<Product_Information_Detail__c>()
		};

		for(Product_Information_Detail__c p : product_holding_info)
		{
			if(p.Deposit_Account__c == ISHAVING){
				isDepositAccount = true;
				// System.debug('isDepositAccount ::: ' + isDepositAccount + ' | ' + p.Deposit_Account__c );
			}

			if(p.S1B__c == ISHAVING){
				isS1B = true;
				// System.debug('isS1B ::: ' + isS1B + ' | ' + p.S1B__c );
			}
		
			// if(p.EDC__c == ISHAVING){
			// 	isEDC = true;
			// }

			mapSIXMonthTXN.get(p.BDM_Latest_SIXMTH_TXN__c).add(p);
			mapLASTMonthTXN.get(p.BDM_Latest_MTH_TXN__c).add(p);
		}

		if(account.BIZ_Touch__c == ISHAVING){
			isBIZTOUCH = true;
			// System.debug('isBIZTOUCH ::: ' + isBIZTOUCH + ' | ' + account.BIZ_Touch__c );
		}

		if(account.BIZ_Wow__c == ISHAVING){
			isBIZWOW = true;
			// System.debug('isBIZWOW ::: ' + isBIZWOW + ' | ' + account.BIZ_Wow__c );
		}

		if(isDepositAccount == false && isS1B == false)
        {
            BDMLatestMTHTXN = '-';
            BDMLatestSIXMTHTXN = '-';
        }        
        else 
        {
            if(mapLASTMonthTXN.get('High').size() > 0){
                BDMLatestMTHTXN = 'High';
            }else if(mapLASTMonthTXN.get('Medium').size() > 0){
                BDMLatestMTHTXN = 'Medium';
            }else{
                BDMLatestMTHTXN = 'Low';
            }
            // System.debug('BDMLatestMTHTXN ::: ' + BDMLatestMTHTXN + ' | ' + mapLASTMonthTXN );
    
            if(mapSIXMonthTXN.get('High').size() > 0){
                    BDMLatestSIXMTHTXN = 'High';
            }else if(mapSIXMonthTXN.get('Medium').size() > 0){
                BDMLatestSIXMTHTXN = 'Medium';
            }else{
                BDMLatestSIXMTHTXN = 'Low';
            }   
            // System.debug('BDMLatestSIXMTHTXN ::: ' + BDMLatestSIXMTHTXN + ' | ' + mapSIXMonthTXN );                              
        }      
	}

	/* check if the VF page is display by SF1 */
    public Boolean isSF1 {
        get {                   
            if(String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameHost')) ||
                String.isNotBlank(ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')) ||
                ApexPages.currentPage().getParameters().get('isdtp') == 'p1' ||
                (ApexPages.currentPage().getParameters().get('retURL') != null && ApexPages.currentPage().getParameters().get('retURL').contains('projectone') )
            ) {
                return true;
            }else{
                return false;
            }
        }
    }
}