public without sharing class AvatarController {

    // public class Avatar{
    //     @AuraEnabled
    //     public String avatar {get;set;}
    //     @AuraEnabled
    //     public String avataren {get;set;}
    //     @AuraEnabled
    //     public String avatarId {get;set;}
    //     @AuraEnabled
    //     public String avatardescription {get;set;}
    //      @AuraEnabled
    //     public String avatarth {get;set;}
    //      @AuraEnabled
    //     public String avatartitle {get;set;}
    //     @AuraEnabled
    //     public boolean haveValue {get;set;}

    // }

    @AuraEnabled(cacheable=true)
    public Static Avatar_Master__c getAvatar(String thisAvatar){

        Avatar_Master__c objavatar = new Avatar_Master__c();
        String avatarId  = 'a3b0l0000003r1DAAQ';
        system.debug('Avatar Id' + avatarId);
        if(avatarId != ''){
            List<Avatar_Master__c> avatarList = [SELECT Id, Avatar_EN__c, Avatar_Description__c, Segment__c, Avatar_TH__c FROM Avatar_Master__c WHERE id =:avatarId];
            system.debug('AvatarList ' + avatarList);

            if(avatarList.size() > 0){
                objavatar = avatarList[0];
                //objavatar.avatar = avatarList[0];
                //return avatarList[0];
                //objavatar.url = getURLAvatarImage(avatarList[0].Segment__c, 'male');
            }  
        }
        
        return objavatar;
    }
    
    public Static String getURLAvatarImage(String segment, String gender){

        String avatartitle = segment + '_male';

        system.debug('Avatar Title' + avatartitle);

        List<ContentVersion> cvs = [SELECT Id, Title, VersionData, ContentDocumentId FROM ContentVersion where Title = :avatartitle];    
        system.debug('CVS ' + cvs);
        String b64 = '';
        if(!cvs.isEmpty()) {
            b64 = EncodingUtil.base64Encode(cvs[0].VersionData);
        }
        return b64;
    }
}