public class CaseTriggerHandler {

    static List<Service_Type_Matrix__c> serviceMatrix = New List<Service_Type_Matrix__c>();
    static List<Case> CaseForUpdate = New List<Case>();
    static Map<String,Service_Type_Matrix__c> ServiceMatrixMap = new Map<String, Service_Type_Matrix__c>();
    static List<Service_Type_Matrix__c> allMatrix;
    static Map<String,ID> mapTeamName = New Map<String,ID>();
    static List<Group> teamName;
    //static Recordtype SErecordType;
    public static Recordtype SErecordType {get{
        if(SErecordType==null){
            SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center'];

        }
        return SErecordType;

    }set;}
    
    static List<Entitlement> EntitlementID;
    static Map<String,ID> EntitlementMap = New Map<String, ID>();
    static Boolean FlagePrepareData = true;
    static Map<ID,String> queueMapName = New Map<ID,String>();
    static Map<ID,String> userMapName = New Map<ID,String>();
    static Boolean checkLoop = true;
    static Boolean isUpdateCS_Service = true; //Check if Change Category = Condition_CS or Service Request _CS, not update case owner

    Static List<Case_Assignment_Round_Robin__c> list_Round_Robin = New List<Case_Assignment_Round_Robin__c>();
    Static Map<String, String> map_UW_Queue = New Map<String,String>();
    Static Map<String, Case_Assignment_Round_Robin__c> map_Round_Robin = New Map<String,Case_Assignment_Round_Robin__c>();

    public static BusinessHours standardHoursId {get{
        if(standardHoursId==null){
            standardHoursId = [Select ID, Name from BusinessHours where name = 'Service' limit 1];

        }
        return standardHoursId;

    }set;}

    public static void prepareData(){

        //SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center'];
        allMatrix = [Select ID, Segment__c,Service_Level1__c,Service_Level2__c,Service_Level3__c,Service_Level4__c,
                                                 SLA__c,Responsible_BU__c,Severity__c, Closed_By_BU__c from Service_Type_Matrix__c where Active__c = true limit 10000];
        EntitlementID = [Select ID, Status, SLA_Day__c, recordtype.name from Entitlement where Status = 'Active' and recordtype.name = 'With Business Hours' ];

        //Mapping key service
        for(Service_Type_Matrix__c issueMatrix : allMatrix){
            String keyMap = issueMatrix.Segment__c  + issueMatrix.Service_Level1__c+ issueMatrix.Service_Level2__c + issueMatrix.Service_Level3__c + issueMatrix.Service_Level4__c;
            keyMap = keyMap.replaceAll('/', '');
            keyMap = keyMap.replaceAll( ' ', '');
            keyMap = keyMap.replaceAll( '\\s+', ''); //Remove all space
            ServiceMatrixMap.put(keyMap,issueMatrix);
        }

        List<User> userBu = [select ID, Employee_ID__c, Name from user where IsActive = true and Employee_ID__c != Null];

        //Query only queue that support object Case
        List<QueueSobject> QuSType = [SELECT Id,QueueId,SobjectType FROM QueueSobject where SobjectType  = 'Case'];
        Set<ID> QueueID = New Set<ID>();
        if(QuSType.size() > 0){
            for(QueueSobject qType : QuSType){
                 QueueID.add(qType.QueueId);
            }
        }
        teamName = [Select ID,DeveloperName, Name  from Group where Type = 'Queue' and ID IN: QueueID];
        //Mapping Queue in Responsible BU
        if(teamName.size() > 0){
            for(Group g: teamName){
                mapTeamName.put(g.Name, g.ID);
                queueMapName.put(g.ID, g.Name);
            }
        }

        //Mapping user in Responsible BU
        if(userBu.size() > 0){
            for(User user_loop : userBu){
                mapTeamName.put(user_loop.Employee_ID__c, user_loop.ID);
                userMapName.put(user_loop.ID, user_loop.Name);
            }
        }

        //Mapping Entitlement for assign SLA
        if(EntitlementID.size() > 0){
            for(Entitlement en_loop : EntitlementID){
                if(en_loop.SLA_Day__c != null){
                  String keyMap = en_loop.SLA_Day__c + '';
                  EntitlementMap.put(keyMap, en_loop.ID);
                }
            }
        }

        //Manage round robin assignment for UW team
        list_Round_Robin = [Select ID,Main_Queue__c, List_Queue__c from  Case_Assignment_Round_Robin__c];
        if(list_Round_Robin != null){
            for(Case_Assignment_Round_Robin__c uw_Round_Robin :list_Round_Robin){
                map_UW_Queue.put(uw_Round_Robin.Main_Queue__c, uw_Round_Robin.List_Queue__c);
                map_Round_Robin.put(uw_Round_Robin.Main_Queue__c,uw_Round_Robin);
            }
        }


        FlagePrepareData = false;
    }

    public static void handleBeforeInsert(List<Case> caseList){
        if(FlagePrepareData){
           prepareData();
        }

        for(Case caseNew : caseList){
            // Check only record Not FCR Case
            if(caseNew.FCR__c == false){
                autoAssign(caseNew);
            }else if(caseNew.FCR__c == true){
                caseNew.EntitlementId = null;
                caseNew.Commit_Date__c = null;
            }

       }

       //Auto Assign Owner for resolve status and close by bu stamp
       updateCloseByBUOwner( null,caseList );
    }

    public static void handleAfterInsert(List<Case> caseList){

        if(FlagePrepareData){
           prepareData();
        }

        //Stamp owner for count SLA
        List<SLA_Per_Owner__c> list_slaPerOwner = New List<SLA_Per_Owner__c>();
        for(Case newCaseInsert : caseList){
               SLA_Per_Owner__c slaPerUser = New SLA_Per_Owner__c();
               slaPerUser.Case__c = newCaseInsert.ID;
               slaPerUser.Case_Status__c = newCaseInsert.Status;

               //Check if case owner is User
               String OwnerID = newCaseInsert.OwnerId;
               if(OwnerID.startsWith('005')){
                  slaPerUser.Link_User__c = newCaseInsert.OwnerId;
                  slaPerUser.Name = userMapName.get(newCaseInsert.ownerID);
               }else{
                   slaPerUser.Queue_ID__c = newCaseInsert.ownerID;
                   slaPerUser.Name = queueMapName.get(newCaseInsert.ownerID);
               }
               slaPerUser.Start_Date_Time__c = system.now();
               list_slaPerOwner.add(slaPerUser);

         }
        if(list_slaPerOwner.size() > 0){
            Database.insert(list_slaPerOwner,false);

        }

        //Share case to creator
        sharingRecord(caseList);

    }

    public static void handleBeforeUpdate(Map<Id,Case> oldMap,Map<Id,Case> newMap){
         if(FlagePrepareData){
                prepareData();
         }

        List<Case_Assignment_Round_Robin__c> list_assign_round_robin_update = New List<Case_Assignment_Round_Robin__c>();
        String list_main_queue = '';
        String list_Round_Robin = '';
        Case_Assignment_Round_Robin__c new_Round_Robin = New Case_Assignment_Round_Robin__c();
        Map<String,Case_Assignment_Round_Robin__c> map_Round_Robin_Update = New Map<String,Case_Assignment_Round_Robin__c>();
        //Map<Id,UserRecordAccess> recordAccessMap = RTL_Utility.queryRecordAccess(newMap.keySet(),System.UserInfo.getUserId());

        for(Id caseID : newMap.keySet()){
            Case oldCase = oldMap.get(caseID);
            Case newCase = newMap.get(caseID);
            isUpdateCS_Service = true;
            //Set auto assign if service issue type changed
            if((newCase.FCR__c == false && newCase.IsClosed == false) &&
               (oldCase.PTA_Segment__c != newCase.PTA_Segment__c  || oldCase.Category__c != newCase.Category__c
                || oldCase.Sub_Category__c != newCase.Sub_Category__c || oldCase.Product_Category__c != newCase.Product_Category__c
                || oldCase.Issue__c != newCase.Issue__c || newCase.Isclone())){
                    String oldCS_Category = '';
                    String newCS_Category = '';
                    if(oldCase.Category__c != null){
                        oldCS_Category = oldCase.Category__c.replaceAll('\\s+', '');
                    }
                    if(newCase.Category__c != null){
                        newCS_Category = newCase.Category__c.replaceAll('\\s+', '');
                    }
                    //Not update CS owner if not change Category.
                    //And Category is Condition_CS or Service Request_CS
                    if(newCase.Isclone() == False && (oldCS_Category.containsIgnoreCase('ChangeCondition_CS') || oldCS_Category.containsIgnoreCase('ServiceRequest_CS')) &&
                       (newCS_Category.containsIgnoreCase('ChangeCondition_CS') || newCS_Category.containsIgnoreCase('ServiceRequest_CS'))){
                        isUpdateCS_Service = false;
                    }
                  autoAssign(newCase);
            }

            //Auto assign round robin for UW team
            if(newCase.OwnerId != oldCase.OwnerId){
                String newOwnerName = null;
                String newOwnerID = newCase.OwnerId;
                if(newOwnerID.startsWith('005')){
                    newOwnerName = userMapName.get(newCase.OwnerId);
                }else{
                    newOwnerName = queueMapName.get(newCase.OwnerId);
                }
                if(map_UW_Queue.containsKey(newOwnerName)){
                    if(!list_main_queue.containsIgnoreCase(newOwnerName)){
                        list_Round_Robin = map_UW_Queue.get(newOwnerName);
                        new_Round_Robin = map_Round_Robin.get(newOwnerName); //get custom setting for update
                        list_main_queue = list_main_queue + newOwnerName;
                    }
                    list_Round_Robin = list_Round_Robin.replaceAll('\\s+', ''); //Remove all space
                    String[] first_Owner = list_Round_Robin.split(',');
                    if(mapTeamName.get(first_Owner[0]) != null){
                        newCase.OwnerId = mapTeamName.get(first_Owner[0]);
                        newCase.UW_Auto_Assign_Flag__c = true;
                    }
                    list_Round_Robin = list_Round_Robin.remove(first_Owner[0]+',');
                    list_Round_Robin = list_Round_Robin + ','+first_Owner[0];
                    new_Round_Robin.List_Queue__c = list_Round_Robin;
                    map_Round_Robin_Update.put(newOwnerName,new_Round_Robin);
                }
            }
        }

        if(map_Round_Robin_Update.size() > 0){
            for(String keyMap : map_Round_Robin_Update.keySet()){
                list_assign_round_robin_update.add(map_Round_Robin_Update.get(keyMap));
            }
        }
        if(list_assign_round_robin_update.size() > 0){
           update list_assign_round_robin_update;
        }

        //Auto Assign Owner for resolve status and close by bu stamp
        updateCloseByBUOwner(oldMap, newMap.values() );
    }



    public static void handleAfterUpdate(Map<Id,Case> oldMap,Map<Id,Case> newMap){

        //SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center'];
        List<CaseMilestone> casMileUpdate = New List<CaseMilestone>();
        Set<ID> setCaseID = New Set<ID>();
        List<SLA_Per_Owner__c> sla_Owner_insertList = New List<SLA_Per_Owner__c>();
        List<SLA_Per_Owner__c> sla_Owner_updateList = New List<SLA_Per_Owner__c>();
        Map<String, SLA_Per_Owner__c> map_slaPerOwner = New Map<String, SLA_Per_Owner__c>();
        List<SLA_Per_Owner__c> list_previous_slaPerOwner = [Select ID, Name, Link_User__c, Case__c, Start_Date_Time__c, End_Date_Time__c, Queue_ID__c from SLA_Per_Owner__c where End_Date_Time__c = null and Case__c in: newMap.keySet()];
        List<Case> unShareCaseList = New List<case>();

        //Mapping previous owner for caculate SLA per user
        if(list_previous_slaPerOwner.size() > 0){
            for(SLA_Per_Owner__c slaPerOwner : list_previous_slaPerOwner){
                //Check if previous owner is user
                if(slaPerOwner.Link_User__c != null){
                    map_slaPerOwner.put(string.valueof(slaPerOwner.Link_User__c) + string.valueof(slaPerOwner.Case__c), slaPerOwner);
                }else{
                    //Previous owner is queue
                    map_slaPerOwner.put(slaPerOwner.Queue_ID__c + string.valueof(slaPerOwner.Case__c), slaPerOwner);
                }
            }
        }

        for(ID caseID : newMap.keySet()){
            Case newCase = newMap.get(caseID);
            Case oldCase = oldMap.get(caseID);
            //Check case if closed, add list to update Milestone for auto completed only for recordtype 'SE Call Center'
            if(newCase.isClosed == true && oldCase.IsClosed == False){
               setCaseID.add(newCase.ID);
            }

            //Check for update SLA per Owner
            if(newCase.OwnerId != oldCase.OwnerId && newCase.IsClosed == false
                 && checkLoop ){

                   //Stamp new owner to monitor SLA
                   SLA_Per_Owner__c slaPerUser = New SLA_Per_Owner__c();
                   slaPerUser.Case__c = newCase.ID;

                   //Check if case owner is User
                   String OwnerID = newCase.OwnerId;
                   if(OwnerID.startsWith('005')){
                      slaPerUser.Link_User__c = newCase.OwnerId;
                      slaPerUser.Name = userMapName.get(newCase.OwnerId);
                   }else{
                      slaPerUser.Queue_ID__c = newCase.OwnerId;
                      slaPerUser.Name = queueMapName.get(newCase.OwnerId);
                   }
                   slaPerUser.Start_Date_Time__c = system.now();
                   sla_Owner_insertList.add(slaPerUser);

                    //Query previous owner to update end date
                    SLA_Per_Owner__c  old_slaPerOwner = map_slaPerOwner.get(string.valueOf(oldCase.OwnerId) + string.valueOf(oldCase.ID));
                     if(old_slaPerOwner != null){
                        old_slaPerOwner.End_Date_Time__c = system.Now();
                        old_slaPerOwner.Case_Status__c = newCase.Status;
                        sla_Owner_updateList.add(old_slaPerOwner);
                     }
                }else if(newCase.IsClosed == true && oldCase.IsClosed == false && checkLoop){

                    //Query previous owner to update end date
                    SLA_Per_Owner__c  old_slaPerOwner = map_slaPerOwner.get(string.valueOf(oldCase.OwnerId) + string.valueOf(oldCase.ID));
                    if(old_slaPerOwner != null){
                        old_slaPerOwner.End_Date_Time__c = system.Now();
                        old_slaPerOwner.Case_Status__c = newCase.Status;
                        sla_Owner_updateList.add(old_slaPerOwner);
                    }
                }

            //Remove sharing case to creator when new owner change case status from 'New' to 'In progress'
            if(oldCase.Status == 'New' && NewCase.Status != 'New' && NewCase.CreatedById != NewCase.OwnerId){
                unShareCaseList.add(NewCase);
            }


        }



        //Check all milestore that related to Closed Case and update Milestore to completed
        if(setCaseID.size() > 0){
            List<CaseMilestone> casMileList = [Select ID, CompletionDate from CaseMilestone where caseId  in: setCaseID];
            if(casMileList.size() > 0){
                for(CaseMilestone caseM : casMileList){
                    caseM.CompletionDate = system.now();
                    casMileUpdate.add(caseM);
                }
            }
        }

        //Update Case Milestone to completed
        if(casMileUpdate.size() > 0){
            try{
                update casMileUpdate;
            }catch(exception e){

            }
        }

        //Insert SLA per Case owner user
        if(!Test.isRunningTest()){
            checkLoop = false;
        }

        if(sla_Owner_insertList.size() > 0){
            Database.insert(sla_Owner_insertList,false);
        }

        if(sla_Owner_updateList.size() > 0){
           update sla_Owner_updateList;
        }

        //Remove share case to creator
        if(unShareCaseList.size() > 0){
            removeSharingRecord(unShareCaseList);
        }
    }

    public static void handleBeforeDelete(List<Case> caseList){
       //Define method for future function
    }

    //This method for update auto assign CaseOwner, SLA, Severity, Closed by BU
    public static void autoAssign(Case caseNew){
        String keyMap = caseNew.PTA_Segment__c + caseNew.Category__c + caseNew.Sub_Category__c + caseNew.Product_Category__c + caseNew.Issue__c;
        keyMap = keyMap.replaceAll('/', '');
        keyMap = keyMap.replaceAll( ' ', '');
        keyMap = keyMap.replaceAll( '\\s+', ''); //Remove all space
        if(ServiceMatrixMap.containsKey(keyMap)){
            Service_Type_Matrix__c issueMatrix = ServiceMatrixMap.get(keyMap);
            //Auto assign Case Owner
            if(mapTeamName.get(issueMatrix.Responsible_BU__c) != null && isUpdateCS_Service == true){
                caseNew.ownerID = mapTeamName.get(issueMatrix.Responsible_BU__c);
            }

            //Auto assign severity
            if(issueMatrix.Severity__c != null){
                caseNew.Case_Severity__c = issueMatrix.Severity__c;
            }else{
                caseNew.Case_Severity__c = '';
            }

            //Auto assign Closed By BU
            if(issueMatrix.Closed_By_BU__c != null){
                caseNew.Closed_By_BU__c = issueMatrix.Closed_By_BU__c;
            }else {
               caseNew.Closed_By_BU__c = '';
            }

            //Auto assign SLA
            string kMap = issueMatrix.SLA__c + '';
            Integer sla = Integer.valueOf(issueMatrix.SLA__c);
            if(EntitlementMap.get(kMap) != null){
                caseNew.EntitlementId = EntitlementMap.get(kMap);
                    if(caseNew.CreatedDate != null){
                        caseNew.Commit_Date__c = getCommitBusinessDays(caseNew.CreatedDate,sla);
                    }else{
                         caseNew.Commit_Date__c = getCommitBusinessDays(system.now(),sla);
                    }
                    caseNew.SLA_Day__c = issueMatrix.SLA__c;
            }
        }
    }

    //This method for calculate commit date without weekend and holiday, base on system Business Hours
    public static Datetime getCommitBusinessDays(datetime startdate, Integer target)
     {
                Datetime pointer = startdate;

                //If create case in weekend or holiday, set start datetime to next working 00.00
                Boolean NonWorkingDay = false;
                if(!BusinessHours.isWithin(standardHoursId.id, pointer)){
                 NonWorkingDay = true;
                 pointer = pointer.addDays(1);
                 while(!BusinessHours.isWithin(standardHoursId.id, pointer)){
                         pointer = pointer.addDays(1);
                 }
                     Integer year = pointer.year();
                     Integer month = pointer.month();
                     Integer day = pointer.day();
                     pointer = Datetime.newInstance(year, month, day, 0, 0, 0);
                 }

                for(integer i = 0; i < target; i++)
                {
                    pointer = pointer.addDays(1);
                    //Returns true if the specified target date occurs within business hours. Holidays are included in the calculation.
                    while(!BusinessHours.isWithin(standardHoursId.id, pointer)){
                        Integer countLoop = i+1;
                        if(countLoop == target && NonWorkingDay == true){
                            break;
                        }
                        pointer = pointer.addDays(1);
                    }
                }
                return pointer;
       }

    //Auto share case to creator
    public static void sharingRecord(List<Case> caseList){
        List<CaseShare> caseShareList = New List<CaseShare>();
        for(Case share_case : caseList){
           CaseShare share_record = new CaseShare();
           share_record.CaseId = share_case.id;
           share_record.CaseAccessLevel = 'Edit';
           share_record.UserOrGroupId = share_case.CreatedById;
           caseShareList.add(share_record);
        }

        if(caseShareList.size() > 0){
            //partially  insert
            Database.insert(caseShareList,false);
        }
    }

    //If case update status to In progress and change owner, not share case to creator.
    public static void removeSharingRecord(List<Case> caseList){
        Set<ID> userID_set = New Set<ID>();
        for(Case unShareCase : caseList){
            userID_set.add(unShareCase.CreatedById);
        }

        List<CaseShare> caseUnShareList = [Select ID, UserOrGroupId from CaseShare where UserOrGroupId in: userID_set];
        if(caseUnShareList.size() > 0){
            //partially  delete
            Database.Delete(caseUnShareList,false);
        }
    }

    private static void updateCloseByBUOwner(Map<Id,Case> oldMap,List<Case> newMap)
    {
        Set<String> queueNameSet = new Set<String>();
        Map<String,Set<Id>> queueNameAllRoleMap = new Map<String,Set<Id>> ();

        Map<Id,User> userIdRoleMap = new Map<Id,User>();
        Map<Id,Group> gMap = new Map<Id,Group>();
        Map<String,Group> queueNameGroupMap = new Map<String,Group>();
        Set<Id> caseCreatotIds = new Set<Id>();

        for(Case tmpCase : newMap )
        {
            if( tmpCase.Closed_By_BU__c != null && tmpCase.Closed_By_BU__c != '' )
            {
                queueNameSet.add(tmpCase.Closed_By_BU__c);
            }

            caseCreatotIds.add(tmpCase.createdById);

        }

        //Keep the list of the case creator as users
        Map<Id, User> userMap = new Map<Id, User>();
        for(User u: [Select Id, userroleid from User where id in :caseCreatotIds]){
            userMap.put(u.Id, u);
        }

        List<GroupMember> gmList = [SELECT group.DeveloperName,group.Name,userorgroupid
                            FROM groupmember
                            WHERE group.Name in :queueNameSet ];
        Map<String,List<GroupMember>> queueNameGroupMemberMap = new map<String,List<GroupMember>>();
        Map<Id,GroupMember> groupMemberIdMap = new Map<Id,GroupMember>();

        for( GroupMember gm : gmList )
        {
            groupMemberIdMap.put( gm.userorgroupid , gm );

            if( !queueNameGroupMemberMap.containsKey( gm.group.Name ) )
            {
                List<GroupMember> tmpGM = new List<GroupMember>();
                queueNameGroupMemberMap.put( gm.group.Name , tmpGM );
            }
            queueNameGroupMemberMap.get( gm.group.Name ).add( gm );

        }

        // Get all Users for create Map of Id to Role
        List<User> uList = [SELECT Id,UserRole.Name , UserRoleId ,RTL_Branch_Code__c
            FROM User
            WHERE id in :groupMemberIdMap.keySet() ];

        for( User u : uList )
        {
            userIdRoleMap.put(u.Id,u);
        }


        List<Group> gList = [ Select Id, Name, Type, DeveloperName , RelatedId
                    FROM Group WHERE id in :groupMemberIdMap.keySet() ];

        for( Group g : gList )
        {
            gMap.put( g.id,g );
        }

        List<Group> queueList = [SELECT Id, Name, DeveloperName, Type , RelatedId
                        FROM Group
                        WHERE type = 'queue'
                        AND name in :queueNameSet ];

        if( queueList.size() > 0 )
        {
            for( Group queue : queueList )
            {
                queueNameGroupMap.put(queue.name,queue);
            }
        }

        // Generate Map for Queue Name with all related profile under this queue

        for( String queueName : queueNameGroupMemberMap.keySet() )
        {
            queueNameAllRoleMap.put( queueName , new Set<Id>() );

            for( GroupMember gm : queueNameGroupMemberMap.get(queueName) )
            {
                // Add Role Id when group Member are Role or Role and Subordinate
                if( gMap.containsKey( gm.userorgroupid) )
                {
                    Group g = gMap.get( gm.userorgroupid );
                    if( g.Type == 'Role' || g.Type == 'RoleAndSubordinates' )
                    {
                        queueNameAllRoleMap.get(queueName).add( g.RelatedId );
                    }
                }

                // Add role Id if Group Member are User
                if( userIdRoleMap.containsKey( gm.userorgroupid ) )
                {
                    User u = userIdRoleMap.get(gm.userorgroupid);
                    queueNameAllRoleMap.get(queueName).add(u.UserRoleId);
                }

            }
        }

        // After get all role , will use to generate all upper level
        //system.debug('Gade: ' + queueNameAllRoleMap);


        User U = [SELECT id, name, email, isactive, profile.name, userrole.name, usertype , userroleId, RTL_Branch_Code__c
                    FROM user Where id =:UserInfo.getUserId() ];

        List<CaseStatus> csList = [Select Id, MasterLabel From CaseStatus Where IsClosed = true];
        Set<String> closeCaseStatusLabel = new Set<String>();
        for( CaseStatus cs : csList )
        {
            closeCaseStatusLabel.add( cs.MasterLabel );
        }

        //get all child role of current user
        Set<Id> listofChildRoleId = CaseUtil.getAllChildrenRoleId(U.userroleId);


        for(Case tmpCase : newMap )
        {
            Case oldCase = oldMap != null ? oldMap.get(tmpCase.Id) : null;
            Boolean isStatusUpdated = false;

            if(oldCase != null && oldCase.Status != tmpCase.Status){
                isStatusUpdated = true;
            }

            if( tmpCase.status == 'Resolved' && (oldCase == null || isStatusUpdated == true))
            {
                if( tmpCase.Closed_By_BU__c == '' || tmpCase.Closed_By_BU__c == null)
                {
                    tmpCase.OwnerId = UserInfo.getUserId();
                }
                // Is close by bu is Register , stamp case owner to current owner who resolve (temp logic)
                else if ( tmpCase.Closed_By_BU__c == 'REGISTER' )
                {
                    tmpCase.OwnerId = tmpCase.createdById;
                }
                // Is close by bu has balue ( Queue name ) , stamp case owner to queue
                else
                {
                    if( queueNameGroupMap.containsKey( tmpCase.Closed_By_BU__c ) )
                    {
                        Group queue = queueNameGroupMap.get(tmpCase.Closed_By_BU__c);
                        tmpCase.OwnerId = queue.Id;
                    }
                    else
                    {
                        tmpCase.OwnerId = UserInfo.getUserId();
                    }

                }
            }
            //validate closed by bu when close case and not a FCR case
            else if( closeCaseStatusLabel.contains(tmpCase.status) && tmpCase.FCR__c == false)
            {
                if( tmpCase.Closed_By_BU__c != '' && tmpCase.Closed_By_BU__c != null )
                {
                    if ( tmpCase.Closed_By_BU__c == 'REGISTER' )
                    {
                        string createdRoleId = userMap.get(tmpCase.createdById).userroleid;

                        if( UserInfo.getUserId()  != tmpCase.createdById && !listofChildRoleId.contains(createdRoleId))
                        {
                            tmpCase.addError(Label.Case_Close_Invalid_Close_By_BU);

                        }
                    }
                    else
                    {

                        //Check if User in upper Role
                        Boolean isOnUpperRole = false;

                        Set<Id> allSubRole = CaseUtil.getSubHierarchyInclusive( u.UserRoleId );
                        Set<Id> profileInQueueSet = queueNameAllRoleMap.get( tmpCase.Closed_By_BU__c );

                        if( profileInQueueSet!= null)
                        {
                            for( Id profileIdInQueue : profileInQueueSet  )
                            {
                                if( allSubRole.contains( profileIdInQueue ) )
                                {
                                    isOnUpperRole = true;
                                    break;
                                }
                            }
                        }

                        //system.debug( 'allSubRole: ' + allSubRole );
                        //system.debug( 'profileInQueueSet: ' + profileInQueueSet );
                        //system.debug( 'isOnUpperRole: ' + isOnUpperRole );

                        // If on Upper Role , allowed to save regardless of Queue Member or Branch
                        if( !isOnUpperRole )
                        {

                            Boolean isUserinCloseByBUBranch = false;
                            if( u.RTL_Branch_Code__c != null )
                            {
                                if( tmpCase.Closed_By_BU__c.contains(u.RTL_Branch_Code__c) )
                                {
                                    isUserinCloseByBUBranch = true;
                                }
                            }
                            //Check close by BU when close case
                            Group queue = queueNameGroupMap.get(tmpCase.Closed_By_BU__c);
                            Boolean isQueueMem = CaseUtil.isInGroupOrRole( queue , u , gMap , queueNameGroupMemberMap );

                            if( !isQueueMem && !isUserinCloseByBUBranch && tmpCase.Is_SystemAdmin_User__c == false)
                            {
                                tmpCase.addError(Label.Case_Close_Invalid_Close_By_BU);
                            }
                        }
                    }

                }

            }

        }

    }
}