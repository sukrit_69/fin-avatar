/****   RTL_ReferralLeadService  this class created to check logic between Referral and Lead
created by CR Referral Enhancement RQ-004 sync lead status and referral status
*****/
public class RTL_ReferralLeadService {

	public static void updateReferralStageContacted(List<sObject> leadList){
		Set<Id> referralIdSet = new Set<Id>();
        Map<Id,RTL_Referral__c> referralMap;
        List<RTL_Referral__c> referralToUpdate = new List<RTL_Referral__c>();
        
        for(sObject sObj : leadList){
        	Lead leadObj = (Lead)sObj;
            if(leadObj.RTL_Referral__c != null){
        		referralIdSet.add(leadObj.RTL_Referral__c);        
            }
        }
        
        if(referralIdSet.size() > 0){
            referralMap = new Map<Id,RTL_Referral__c>([SELECT Id,RTL_Stage__c  FROM RTL_Referral__c WHERE ID IN:referralIdSet]);
        }
        
        for(sObject sObj : leadList){
        	Lead leadObj = (Lead)sObj;
            RTL_Referral__c refObj;
            //If lead status = contacted or qualified and referral status != In progress_Contacted
            if(leadObj.RTL_Referral__c != null && referralMap.containsKey(leadObj.RTL_Referral__c) 
            	&& (leadObj.Status == 'Contacted' || leadObj.Status == 'Qualified') )
                {
            	
                refObj = referralMap.get(leadObj.RTL_Referral__c);
            	//23 May 2018 CR Refer within Commercial exclude 'Closed (Service Completed)' support type = To Product team to auto update referral stage
                if(refObj.RTL_Stage__c != 'In progress_Contacted' && refObj.RTL_Stage__c != 'Closed (Service Completed)'){
                    refObj.RTL_Stage__c = 'In progress_Contacted';
                    referralToUpdate.add(refObj);
                }
                    
            }
                
            
        }
        
        if(referralToUpdate.size() > 0){
            update referralToUpdate;
        }
	}


	public static void updateReferralInfo(Map<Id,sObject> oldMap,Map<Id,sObject> newMap){
        List<Lead> contactedLeadWithReferral = new List<Lead>();
        for(Id leadId : newMap.keySet()){
            Lead oldLead = (Lead)oldMap.get(leadId);
            Lead newLead = (Lead)newMap.get(leadId);
            //If lead status changed
            if(oldLead.Status != newLead.Status){
                contactedLeadWithReferral.add(newLead);
            }
            //If Referral Changed
            else if(oldLead.RTL_Referral__c != newLead.RTL_Referral__c){
            	contactedLeadWithReferral.add(newLead);
            }
        }
		updateReferralStageContacted(contactedLeadWithReferral);        
    }

}