public without sharing class GroupPerformanceRefreshThirdBatch {}
/*Comment Cleansing Code global without sharing class GroupPerformanceRefreshThirdBatch implements  Database.Batchable<sObject> , Database.AllowsCallouts{
        
    // Account Plan Year
    public string m_year {get;set;}
    //  Support
    public Set<Id> m_accountWithAccountPlan    {get;set;}
    public Set<Id> m_accountWithoutAccountPlan {get;set;}  
    
    public id  m_groupId {get;set;}  
    public id  m_groupProfileId {get;set;}  
   
    
    global GroupPerformanceRefreshThirdBatch (Set<Id> accountWithAccountPlan,Set<Id> accountWithoutAccountPlan,id groupId,string year,id groupProfileId){
        
        m_accountWithAccountPlan    =  accountWithAccountPlan ;
        m_accountWithoutAccountPlan =  accountWithoutAccountPlan;
        m_year = year;
        m_groupId = groupId;
        m_groupProfileId = groupProfileId;
    }
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){  
       
        
        //Get Account to Process
        Set<Id> accountIds = m_accountWithoutAccountPlan ;
        string year = m_year ;
        
       return Database.getQueryLocator([
            SELECT Id, Name ,Account_Plan_Flag__c,  Group__c,Group__r.Name ,Owner.Segment__c   
            FROM Account 
            WHERE  Id IN : accountIds 
        ]);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
         Set<id> accountWithoutAccountPlan =(new Map<Id,SObject>(scope)).keySet();
        system.debug('::: execute RefreshNameProductStrategyPort');
      	AccountPlanRefreshService.RefreshNameProductStrategyPort(accountWithoutAccountPlan,m_year);
        
        
    }
    global void finish(Database.BatchableContext BC){
        GroupPerformanceRefreshForthBatch batch =     
        new GroupPerformanceRefreshForthBatch( m_accountWithAccountPlan, m_accountWithoutAccountPlan,m_groupId, m_year,m_groupProfileId);  
        Database.executeBatch(batch ,25); 
    }
}*/