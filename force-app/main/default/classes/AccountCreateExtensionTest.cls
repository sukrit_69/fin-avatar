@isTest
global class AccountCreateExtensionTest {
 /*   
    static testmethod void WebserviceMockupTest(){
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.CreateAddress();
        List<Type_of_ID__c> typelist =TestUtils.createIdType(); 
        Account acct2 = TestUtils.createAccounts(1,'TestMockUp','AccountCreateExtension',false).get(0);
        
        Country__c bkk = new Country__c();
        bkk.Name ='Bangkok';
        bkk.Code__c ='10';
        insert bkk;
        PageReference accountCreationPage = Page.AccountCreation;
        accountCreationPage.getParameters().put('customer_type','Individual');
        accountCreationPage.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage.getParameters().put('first_name','');
        accountCreationPage.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage.getParameters().put('mobile_number',acct2.Mobile_Number_Temp__c);
        accountCreationPage.getParameters().put('office_number',acct2.Office_Number_Temp__c);
        accountCreationPage.getParameters().put('id_type','Citizen ID');
        accountCreationPage.getParameters().put('id_number','151515');
        accountCreationPage.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage);
        
        Test.setMock(WebServiceMock.class, new TMBSearchDupNotAllowCreateMock());
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx = new AccountCreateExtension(sc);
        AccCreateEx.isIDValid = false;
        AccCreateEx.oldIDType = null;
        // AccCreateEX.isDuplicate();
        //AccCreateEx.save();
        
        
        AccCreateEx.oldFirstName ='Change';
        AccCreateEx.oldFirstName = 'Change';
        // Test.startTest();    
        AccCreateEx.save();
        
        PageReference accountCreationPage2 = Page.AccountCreation;
        accountCreationPage2.getParameters().put('customer_type','Individual');
        accountCreationPage2.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage2.getParameters().put('first_name','Test');
        accountCreationPage2.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage2.getParameters().put('mobile_number',acct2.Mobile_Number_Temp__c);
        accountCreationPage2.getParameters().put('office_number',acct2.Office_Number_Temp__c);
        // accountCreationPage2.getParameters().put('id_type','Citizen ID');
        accountCreationPage2.getParameters().put('id_number','151515');
        accountCreationPage2.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage2);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx2 = new AccountCreateExtension(sc2);
        AccCreateEx2.isIDValid = false;
        AccCreateEx2.oldIDType = null;
        AccCreateEx2.save();    
        
        
        PageReference accountCreationPage3 = Page.AccountCreation;
        accountCreationPage3.getParameters().put('customer_type','Individual');
        accountCreationPage3.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage3.getParameters().put('first_name','Test');
        accountCreationPage3.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage3.getParameters().put('mobile_number',acct2.Mobile_Number_Temp__c);
        accountCreationPage3.getParameters().put('office_number',acct2.Office_Number_Temp__c);
        accountCreationPage2.getParameters().put('id_type','Citizen ID');
        accountCreationPage3.getParameters().put('id_number',null);
        accountCreationPage3.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage3);
        ApexPages.StandardController sc3 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx3 = new AccountCreateExtension(sc3);
        AccCreateEx3.isIDValid = false;
        AccCreateEx3.oldIDType = null;
        AccCreateEx3.save();    
        
        
        PageReference accountCreationPage4 = Page.AccountCreation;
        accountCreationPage4.getParameters().put('customer_type','Individual');
        accountCreationPage4.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage4.getParameters().put('first_name','Test');
        accountCreationPage4.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage4.getParameters().put('mobile_number',null);
        accountCreationPage4.getParameters().put('office_number',null);
        accountCreationPage4.getParameters().put('id_type','Citizen ID');
        accountCreationPage4.getParameters().put('id_number','15151511');
        accountCreationPage4.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage4);
        ApexPages.StandardController sc4 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx4 = new AccountCreateExtension(sc4);
        AccCreateEx4.isIDValid = false;
        AccCreateEx4.oldIDType = null;
        AccCreateEx4.save();            
        
        PageReference accountCreationPage5 = Page.AccountCreation;
        accountCreationPage5.getParameters().put('customer_type','Individual');
        accountCreationPage5.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage5.getParameters().put('first_name','Test');
        accountCreationPage5.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage5.getParameters().put('mobile_number','62623');
        accountCreationPage5.getParameters().put('office_number','45345');
        accountCreationPage5.getParameters().put('id_type','Citizen ID');
        accountCreationPage5.getParameters().put('id_number','15151511');
        accountCreationPage5.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage5);
        ApexPages.StandardController sc5 = new ApexPages.StandardController(Acct2);
        Test.startTest();
        AccountCreateExtension AccCreateEx5 = new AccountCreateExtension(sc5);
        AccCreateEx5.isIDValid = true;
        AccCreateEx5.oldIDType = null;
        AccCreateEx5.save();
        System.debug('ACCTID : '+AccCreateEx5.acctid);
        Test.setMock(WebServiceMock.class, new TMBServiceMock());
        AccCreateEx5.insertAccountCallOut();
        
        
        
        PageReference accountCreationPage6 = Page.AccountCreation;
        
        accountCreationPage6.getParameters().put('customer_type','Individual');
        //accountCreationPage6.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage6.getParameters().put('first_name','Test');
        accountCreationPage6.getParameters().put('last_name','Test123');
        accountCreationPage6.getParameters().put('mobile_number','62623');
        accountCreationPage6.getParameters().put('office_number','45345');
        // accountCreationPage6.getParameters().put('id_type','--None--');
        //accountCreationPage6.getParameters().put('id_number',null);
        accountCreationPage6.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage6);
        ApexPages.StandardController sc6 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx6 = new AccountCreateExtension(sc6);
        AccCreateEx6.isIDValid = true;
        AccCreateEx6.oldFirstName ='Change'; 
        AccCreateEx6.oldFirstName = 'Change';
        Test.setMock(WebServiceMock.class, new TMBSearchMock());
        
        acct2.ID_Number_Temp__c = null;
        acct2.ID_Type_Temp__c = null;
        AccCreateEx6.save();
        Test.setMock(WebServiceMock.class, new TMBServiceMock());
        //AccCreateEx6.insertAccountCallOut();
        AccCreateEx6.viewAccount(); 
        //Test.stopTest();
        
                PageReference accountCreationPage7 = Page.AccountCreation;
        
        accountCreationPage7.getParameters().put('customer_type','Individual');
        //accountCreationPage6.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage7.getParameters().put('first_name','Test');
        accountCreationPage7.getParameters().put('last_name','Test123');
        accountCreationPage7.getParameters().put('mobile_number','');
        accountCreationPage7.getParameters().put('office_number','');
        // accountCreationPage6.getParameters().put('id_type','--None--');
        accountCreationPage7.getParameters().put('id_number','');
        //accountCreationPage7.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage7);
        ApexPages.StandardController sc7 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx7 = new AccountCreateExtension(sc7);
        AccCreateEx7.isIDValid = true;
        AccCreateEx7.oldFirstName ='Change'; 
        AccCreateEx7.oldFirstName = 'Change';
        Test.setMock(WebServiceMock.class, new TMBSearchMock());
        
        acct2.ID_Number_Temp__c = null;
        acct2.ID_Type_Temp__c = null;
        AccCreateEx7.save();
        Test.setMock(WebServiceMock.class, new TMBServiceMock());
        //AccCreateEx6.insertAccountCallOut();
        AccCreateEx7.viewAccount(); 

        PageReference accountCreationPage8 = Page.AccountCreation;
        
        accountCreationPage8.getParameters().put('customer_type','Individual');
        //accountCreationPage6.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage8.getParameters().put('first_name','Test');
        accountCreationPage8.getParameters().put('last_name','Test123');
        accountCreationPage8.getParameters().put('mobile_number','083848341');
        accountCreationPage8.getParameters().put('office_number','023948532');
        accountCreationPage8.getParameters().put('id_type','');
        accountCreationPage8.getParameters().put('id_number','');
        //accountCreationPage7.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        Test.setCurrentPage(accountCreationPage8);
        ApexPages.StandardController sc8 = new ApexPages.StandardController(Acct2);
        AccountCreateExtension AccCreateEx8 = new AccountCreateExtension(sc8);
        AccCreateEx8.isIDValid = true;
        AccCreateEx8.oldFirstName ='Change'; 
        AccCreateEx8.oldFirstName = 'Change';
        Test.setMock(WebServiceMock.class, new TMBSearchMock());
        
        acct2.ID_Number_Temp__c = null;
        acct2.ID_Type_Temp__c = null;
        AccCreateEx8.save();
        Test.setMock(WebServiceMock.class, new TMBServiceMock());
        //AccCreateEx6.insertAccountCallOut();
        AccCreateEx8.viewAccount(); 

        Test.stopTest();
        
            
        
    }  
    
    
    // P : Positive
    static testmethod void  P_Service_Return_0000_Status()
    {
        // ########## Arrang ##########
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.CreateAddress();
        List<Type_of_ID__c> typelist =TestUtils.createIdType(); 
        Account acct2 = TestUtils.createAccounts(1,'TestPositive','AccountCreateExtension',false).get(0);        
        Country__c bkk = new Country__c();
        bkk.Name ='Bangkok';
        bkk.Code__c ='10';
        insert bkk;
        
        
        
        // ########## Act ##########
        PageReference accountCreationPage5 = Page.AccountCreation;
        accountCreationPage5.getParameters().put('customer_type','Individual');
        accountCreationPage5.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage5.getParameters().put('first_name','Test');
        accountCreationPage5.getParameters().put('last_name',acct2.Last_Name__c);
        accountCreationPage5.getParameters().put('mobile_number','62623');
        accountCreationPage5.getParameters().put('office_number','45345');
        accountCreationPage5.getParameters().put('id_type','Citizen ID');
        accountCreationPage5.getParameters().put('id_number','15151511');
        accountCreationPage5.getParameters().put('tmb_cust_id',acct2.TMB_Customer_ID_Temp__c);
        
        string n =  AccountCreateExtension.DML_EXCEPTION_CODE;
        n= AccountCreateExtension.QUERY_EXCEPTION_CODE;
        n=AccountCreateExtension.CALLOUT_EXCEPTION_CODE ;
        n=AccountCreateExtension.FOUND_DUP_NOT_ALLOW_CREATE ;
        
        
        
        
        Test.setCurrentPage(accountCreationPage5);
        ApexPages.StandardController sc5 = new ApexPages.StandardController(Acct2);
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TMBServiceMockV2());
        AccountCreateExtension AccCreateEx5 = new AccountCreateExtension(sc5);
        AccCreateEx5.isIDValid = true;
        AccCreateEx5.oldIDType = null;
        AccCreateEx5.save();
        AccCreateEx5.acctid = '';
        // Acct2.Id ='001000000000234'; // Mock Account Id 
        AccCreateEx5.insertAccountCallOut();
        Test.stopTest();
        
        // ########## Assert ##########       
        
        System.assertEquals('',  AccCreateEx5.acctid );
        
        
        
    }
    
           // P : Positive
    static testmethod void  P_Service_Return_0000_Status_indi()
    {
        // ########## Arrang ##########
        TestUtils.createAppConfig();
        TestUtils.createStatusCode(); 
        TestUtils.createDisqualifiedReason(); 
        TestUtils.CreateAddress();
        List<Type_of_ID__c> typelist =TestUtils.createIdType(); 
        Account acct3 = TestUtils.createAccounts(1,'TestReurnStatus','AccountCreateExtension',false).get(0);  
        Country__c bkk = new Country__c();
        bkk.Name ='Bangkok';
        bkk.Code__c ='10';
        insert bkk;
        
        
        
        // ########## Act ##########
        PageReference accountCreationPage8 = Page.AccountCreation;
        accountCreationPage8.getParameters().put('customer_type','Individual');
        //accountCreationPage5.getParameters().put('company_name',acct2.Company_Name_Temp__c);
        accountCreationPage8.getParameters().put('first_name','Test12');
        accountCreationPage8.getParameters().put('last_name','aa222');
        accountCreationPage8.getParameters().put('mobile_number','62623');
        accountCreationPage8.getParameters().put('office_number','45345');
        //accountCreationPage8.getParameters().put('id_type','Citizen ID');
        //accountCreationPage8.getParameters().put('id_number','15151511');
       // accountCreationPage8.getParameters().put('tmb_cust_id',acct3.TMB_Customer_ID_Temp__c);
        
        string n =  AccountCreateExtension.DML_EXCEPTION_CODE;
        n= AccountCreateExtension.QUERY_EXCEPTION_CODE;
        n=AccountCreateExtension.CALLOUT_EXCEPTION_CODE ;
        n=AccountCreateExtension.FOUND_DUP_NOT_ALLOW_CREATE ;
        
        
        
        
        Test.setCurrentPage(accountCreationPage8);
        ApexPages.StandardController sc8 = new ApexPages.StandardController(Acct3);
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TMBServiceMockV2());
        AccountCreateExtension AccCreateEx8 = new AccountCreateExtension(sc8);
        AccCreateEx8.isIDValid = true;
        AccCreateEx8.oldIDType = null;
		AccCreateEx8.isSameOwner = true;
        AccCreateEx8.save();
        AccCreateEx8.acctid = '';
        // Acct2.Id ='001000000000234'; // Mock Account Id 
        //AccCreateEx8.insertAccountCallOut();
        Test.stopTest();
        
        // ########## Assert ##########       
        
        //System.assertEquals('',  AccCreateEx8.acctid );
        
        
        
    }
   */  
    
}