@isTest
global class TMBQueryContactMock  /*implements WebServiceMock*/ {}
 /*   public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
       
        System.debug(LoggingLevel.INFO, 'TMBServiceProxyMockImpl.doInvoke() - ' +
            '\n request: ' + request +
            '\n response: ' + response +
            '\n endpoint: ' + endpoint +
            '\n soapAction: ' + soapAction +
            '\n requestName: ' + requestName +
            '\n responseNS: ' + responseNS +
            '\n responseName: ' + responseName +
            '\n responseType: ' + responseType);
       
        TMBServiceProxy.QueryContactResultDTO queryresult = new TMBServiceProxy.QueryContactResultDTO(); 
            queryresult.status ='0000';
            queryresult.totalrecord = '1';
            queryresult.massage = '';
           
                
         TMBServiceProxy.ArrayOfQueryContactData ArrayOfQueryContact = new TMBServiceProxy.ArrayOfQueryContactData();
         TMBServiceProxy.QueryContactData[] querycontact = New List<TMBServiceProxy.QueryContactData>();      
         TMBServiceProxy.QueryContactData contactdata = new TMBServiceProxy.QueryContactData();
                Contact conn = TestUtils.contactlist.get(0);
                Account acct = TestUtils.accountList.get(0);    
                Type_of_ID__c typeid = [SELECT Name,Value__c FROM Type_of_ID__c WHERE Name =:acct.ID_Type_Temp__c LIMIT 1];
                    
        contactdata.ID_TYPE= typeid.Value__c;
        contactdata.ID_NUMBER= acct.ID_Number_Temp__c;
        contactdata.CRM_ID= conn.AccountId;
        contactdata.SF_CON_ID= conn.id;
        contactdata.FNAME= conn.First_Name_Temp__c;
        contactdata.LNAME= conn.Last_Name_Temp__c;
        contactdata.CITIZEN_ID= acct.ID_Number_Temp__c;
        contactdata.PASSPORT_ID= acct.ID_Number_Temp__c;
        contactdata.BRN_ID= acct.ID_Number_Temp__c;
        contactdata.ALIEN_ID= acct.ID_Number_Temp__c;
        contactdata.WORK_PERMIT_ID= acct.ID_Number_Temp__c;
        contactdata.DATE_OF_BIRTH= System.today() ;
        contactdata.POSITION= conn.Position__c;
        //contactdata.MOBILE_PH_NBR= conn.Mobile_No_Temp__c;
        contactdata.PRI_PH_NBR= conn.MobilePhone;
        contactdata.PRI_PH_EXT= '';
        contactdata.FAX_PH_NBR= '';
        contactdata.EMAIL_ADDRESS= conn.Email_Temp__c;
        contactdata.CUSTOMER_AGE= 25;
        contactdata.DECISION_MAP_LEVEL= '';
        contactdata.PRODUCT_DICISION= '';
        contactdata.VALUE_STYLE= '';
        contactdata.DIFFICULTY_TO_DEAL= '';
        contactdata.NOTE= '';
        contactdata.GENDER= '';
        contactdata.FACEBOOK= '';
        contactdata.LINK_IN= '';
        contactdata.RM_OWNER= acct.id;
        contactdata.TMB_CUSTOMER_FLAG= '';
        contactdata.AUTHO_FLAG= '';
        contactdata.BOD_FLAG= '';
        contactdata.DATE_CREATE= System.today();
        contactdata.DATE_MODIFY= System.today();
        contactdata.PRI_CITY = 'กรุงเทพมหานคร';     
        contactdata.PRI_ADDR1 = 'Address 1';
        contactdata.PRI_ADDR2 ='Street';
        contactdata.PRI_ADDR3='Soi';
        contactdata.PRI_ADDR4 = 'ห้วยขวาง';
        contactdata.PRI_ADDR5 = 'เขตห้วยขวาง';
        contactdata.PRI_COUNTRY ='TH';
        contactdata.PRI_POSTAL_CD = '10200';

        querycontact.add(contactdata);
        ArrayOfQueryContact.QueryContactData = querycontact;
        queryresult.Datas = ArrayOfQueryContact;
        TMBServiceProxy.QueryContactResponse_element respondelement = new TMBServiceProxy.QueryContactResponse_element();
        respondelement.QueryContactResult = queryresult;

       response.put( 'response_x', respondelement);
   } 
}*/