@IsTest
public class TestCXMServiceProvider{
	static List<Service_Type_Matrix__c> serviceTypeList;
    static List<Case> caseList;

    static User adminUser{
        get
        {
            if(adminUser == null){
                adminUser = [SELECT Id,Name FROM User WHERE IsActive = true AND ProfileId =: TestUtils.SYSADMIN_PROFILE_ID LIMIT 1];
            }
            return adminUser;
        }
        set;
    }

    static User branchUser{
        get
        {
            if(branchUser == null){
                branchUser = [SELECT Id,Name,RTL_Branch_Code__c,UserRole.Name FROM User WHERE IsActive = true AND ProfileId =: RTL_TestUtility.RTL_BRANCH_SALES_PROFILE_ID AND RTL_Branch_Code__c = '001' LIMIT 1 ];
            }
            return branchUser;
        }
        set;
    }
    
	public static testmethod void testCreateCaseAndCompleted() {
		Test.startTest();
         	//List<User> adminuser = [Select ID, Name from user where isActive = true  and profileID in: [Select ID from profile where name = 'System Administrator'] limit 2];
         	system.runAs(adminUser){
            //Create Customer
            TestUtils.createAppConfig();
            Account acct = TestUtils.createAccounts(1,'TESTACCOUNT','Individual', true).get(0);

            List<AppConfig__c> apps = new  List<AppConfig__c>();
            AppConfig__c Aconfig = new AppConfig__c();
            Aconfig.Name = 'CXM_SERVICE_CONNECTOR';
            Aconfig.Value__c = 'true';        
            apps.add(Aconfig);
            insert apps;

            //for Integration_Mapping__c
            List<Integration_Mapping__c> intMappingLst = new List<Integration_Mapping__c>();

            Integration_Mapping__c intMapp = new Integration_Mapping__c();
            intMapp.Condition_1__c = 'equals';
            intMapp.Condition_2__c = 'equals';
            intMapp.Condition_3__c = 'equals';
            intMapp.Field_Condition_1__c = 'Status';
            intMapp.Field_Condition_2__c = 'PTA_Segment__c';
            intMapp.Field_Condition_3__c = 'Category__c';
            intMapp.Field_Value_1__c = 'Completed';
            intMapp.Field_Value_2__c = 'RBG;ME';
            intMapp.Field_Value_3__c = 'Complaint';
            intMapp.Variable_Field_1__c = 'Id';
            intMapp.Name = 'CXMCaseService';
            intMappingLst.add(intMapp);

            insert intMappingLst;
            
             //Create Entitlement
             Recordtype   typeID = [Select ID, Name from Recordtype where SobjectType = 'Entitlement' and Name = 'With Business Hours' limit 1];   
             Entitlement ent = new Entitlement(Name='Testing', AccountId= acct.Id, StartDate=Date.valueof(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)), SLA_Day__c = 7, recordtypeID = typeID.id);
             insert ent;
             
             //Create Service Type matrix
             Service_Type_Matrix__c SeMatrix = New Service_Type_Matrix__c();
             SeMatrix.Segment__c = 'SBG';
             SeMatrix.Service_Level1__c = 'Service Level1';
             SeMatrix.Service_Level2__c = 'Service level2';
             SeMatrix.Service_Level3__c = 'Service level3';
             SeMatrix.Service_Level4__c = 'Service level4';
             SeMatrix.SLA__c = 7;
             SeMatrix.Severity__c = '1';
             SeMatrix.Responsible_BU__c = 'Test Queue SE';
             SeMatrix.active__c = true;
             SeMatrix.Closed_By_BU__c = 'Test Queue SE';
             insert SeMatrix;
             
             //Create Queue         
             Group que = new Group(Name='Test Queue SE', DeveloperName = 'Test_Queue_SE', type='Queue');
             insert que;
             QueuesObject q1 = new QueueSObject(QueueID = que.id, SobjectType = 'Case');
             insert q1;
             

            List<EIM_Map__c> eimList = new List<EIM_Map__c>();

            EIM_Map__c eim = new EIM_Map__c();
            eim.Attribute__c = '';
            eim.Attribute_value__c = '';
            eim.Default_Value__c = '';
            eim.External_Field__c = 'ID';
            eim.Field_Order__c = Decimal.valueOf('1');
            eim.Field_Type__c = 'Request';
            eim.IsActive__c = true;
            eim.Job_Type__c = 'CXMCaseService';
            eim.Node__c = 'DataNode';
            eim.Object__c = 'Case';
            eim.ParentNode__c = 'DataNode';
            eim.SF_Field__c = 'ID';

            EIM_Map__c eim2 = new EIM_Map__c();
            eim2.Attribute__c = '';
            eim2.Attribute_value__c = '';
            eim2.Default_Value__c = '';
            eim2.External_Field__c = 'Status';
            eim2.Field_Order__c = Decimal.valueOf('2');
            eim2.Field_Type__c = 'Request';
            eim2.IsActive__c = true;
            eim2.Job_Type__c = 'CXMCaseService';
            eim2.Node__c = 'DataNode';
            eim2.Object__c = 'Case';
            eim2.ParentNode__c = '';
            eim2.SF_Field__c = 'Status';

            EIM_Map__c eim3 = new EIM_Map__c();
            eim3.Attribute__c = '';
            eim3.Attribute_value__c = '';
            eim3.Default_Value__c = '';
            eim3.External_Field__c = 'CaseNumber';
            eim3.Field_Order__c = Decimal.valueOf('3');
            eim3.Field_Type__c = 'Request';
            eim3.IsActive__c = true;
            eim3.Job_Type__c = 'CXMCaseService';
            eim3.Node__c = 'DataNode';
            eim3.Object__c = 'Case';
            eim3.ParentNode__c = '';
            eim3.SF_Field__c = 'CaseNumber';

            eimList.add(eim);
            eimList.add(eim2);
            eimList.add(eim3);

            insert eimList;

             //Create Case
             Recordtype SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center']; 
             List<Case> list_case = New list<Case>();
             Case caseNew1 = New Case(); 
             caseNew1.recordtypeID = SErecordType.id;
             caseNew1.Subject = 'TestCase';
             caseNew1.PTA_Segment__c = 'RBG';
             caseNew1.Category__c = 'Complaint';
             caseNew1.Sub_Category__c = 'Service level2';
             caseNew1.Product_Category__c = 'Service level3';
             caseNew1.Issue__c = 'Service level4';
             caseNew1.Status = 'New';
             caseNew1.Description = 'Test create Case';
             caseNew1.AccountId = acct.id;
             caseNew1.CCRP_Number__c = '11-99770-9';
             caseNew1.Journey__c     = 'Journey__c';
             caseNew1.Problem_Type__c = 'Problem_Type__c';
             list_case.add(caseNew1);

             Case caseNew2 = New Case(); 
             caseNew2.recordtypeID = SErecordType.id;
             caseNew2.Subject = 'TestCase';
             caseNew2.PTA_Segment__c = 'RBG';
             caseNew2.Category__c = 'Complaint';
             caseNew2.Sub_Category__c = 'Service level2';
             caseNew2.Product_Category__c = 'Service level3';
             caseNew2.Issue__c = 'Service level4';
             caseNew2.Status = 'New';
             caseNew2.FCR__c = true;
             caseNew2.Description = 'Test create Case';
             caseNew2.AccountId = acct.id;
             caseNew2.CCRP_Number__c = '11-99770-9';
             caseNew2.Journey__c     = 'Journey__c';
             caseNew2.Problem_Type__c = 'Problem_Type__c';
             list_case.add(caseNew2);

             insert list_case;

             Case caseUpdate1 = New Case(Id = list_case.get(0).Id); 
             caseUpdate1.Status = 'Completed';
             update caseUpdate1;

             Test.stopTest();
         }
	}

    public static testmethod void testRunBatchForRetry() {
        Test.startTest();
            //List<User> adminuser = [Select ID, Name from user where isActive = true  and profileID in: [Select ID from profile where name = 'System Administrator'] limit 2];
            system.runAs(adminUser){
            //Create Customer
            TestUtils.createAppConfig();
            Account acct = TestUtils.createAccounts(1,'TESTACCOUNT','Individual', true).get(0);

            List<AppConfig__c> apps = new  List<AppConfig__c>();
            AppConfig__c Aconfig = new AppConfig__c();
            Aconfig.Name = 'CXM_SERVICE_CONNECTOR';
            Aconfig.Value__c = 'true';        
            apps.add(Aconfig);
            insert apps;

            //for Integration_Mapping__c
            List<Integration_Mapping__c> intMappingLst = new List<Integration_Mapping__c>();

            Integration_Mapping__c intMapp = new Integration_Mapping__c();
            intMapp.Condition_1__c = 'equals';
            intMapp.Condition_2__c = 'equals';
            intMapp.Condition_3__c = 'equals';
            intMapp.Field_Condition_1__c = 'Status';
            intMapp.Field_Condition_2__c = 'PTA_Segment__c';
            intMapp.Field_Condition_3__c = 'Category__c';
            intMapp.Field_Value_1__c = 'Completed';
            intMapp.Field_Value_2__c = 'RBG;ME';
            intMapp.Field_Value_3__c = 'Complaint';
            intMapp.Variable_Field_1__c = 'Id';
            intMapp.Name = 'CXMCaseService';
            intMappingLst.add(intMapp);

            insert intMappingLst;
            
             //Create Entitlement
             Recordtype   typeID = [Select ID, Name from Recordtype where SobjectType = 'Entitlement' and Name = 'With Business Hours' limit 1];   
             Entitlement ent = new Entitlement(Name='Testing', AccountId= acct.Id, StartDate=Date.valueof(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)), SLA_Day__c = 7, recordtypeID = typeID.id);
             insert ent;
             
             //Create Service Type matrix
             Service_Type_Matrix__c SeMatrix = New Service_Type_Matrix__c();
             SeMatrix.Segment__c = 'SBG';
             SeMatrix.Service_Level1__c = 'Service Level1';
             SeMatrix.Service_Level2__c = 'Service level2';
             SeMatrix.Service_Level3__c = 'Service level3';
             SeMatrix.Service_Level4__c = 'Service level4';
             SeMatrix.SLA__c = 7;
             SeMatrix.Severity__c = '1';
             SeMatrix.Responsible_BU__c = 'Test Queue SE';
             SeMatrix.active__c = true;
             SeMatrix.Closed_By_BU__c = 'Test Queue SE';
             insert SeMatrix;
             
             //Create Queue         
             Group que = new Group(Name='Test Queue SE', DeveloperName = 'Test_Queue_SE', type='Queue');
             insert que;
             QueuesObject q1 = new QueueSObject(QueueID = que.id, SobjectType = 'Case');
             insert q1;
             

            List<EIM_Map__c> eimList = new List<EIM_Map__c>();

            EIM_Map__c eim = new EIM_Map__c();
            eim.Attribute__c = '';
            eim.Attribute_value__c = '';
            eim.Default_Value__c = '';
            eim.External_Field__c = 'ID';
            eim.Field_Order__c = Decimal.valueOf('1');
            eim.Field_Type__c = 'Request';
            eim.IsActive__c = true;
            eim.Job_Type__c = 'CXMCaseService';
            eim.Node__c = 'DataNode';
            eim.Object__c = 'Case';
            eim.ParentNode__c = 'DataNode';
            eim.SF_Field__c = 'ID';

            EIM_Map__c eim2 = new EIM_Map__c();
            eim2.Attribute__c = '';
            eim2.Attribute_value__c = '';
            eim2.Default_Value__c = '';
            eim2.External_Field__c = 'Status';
            eim2.Field_Order__c = Decimal.valueOf('2');
            eim2.Field_Type__c = 'Request';
            eim2.IsActive__c = true;
            eim2.Job_Type__c = 'CXMCaseService';
            eim2.Node__c = 'DataNode';
            eim2.Object__c = 'Case';
            eim2.ParentNode__c = '';
            eim2.SF_Field__c = 'Status';

            EIM_Map__c eim3 = new EIM_Map__c();
            eim3.Attribute__c = '';
            eim3.Attribute_value__c = '';
            eim3.Default_Value__c = '';
            eim3.External_Field__c = 'CaseNumber';
            eim3.Field_Order__c = Decimal.valueOf('3');
            eim3.Field_Type__c = 'Request';
            eim3.IsActive__c = true;
            eim3.Job_Type__c = 'CXMCaseService';
            eim3.Node__c = 'DataNode';
            eim3.Object__c = 'Case';
            eim3.ParentNode__c = '';
            eim3.SF_Field__c = 'CaseNumber';

            eimList.add(eim);
            eimList.add(eim2);
            eimList.add(eim3);

            insert eimList;

             //Create Case
             Recordtype SErecordType = [Select ID, Name from Recordtype where SobjectType = 'Case' and Name = 'SE Call Center']; 
            List<Case> list_case = New list<Case>();
            Case caseNew1 = New Case(); 
            caseNew1.recordtypeID = SErecordType.id;
            caseNew1.Subject = 'TestCase';
            caseNew1.PTA_Segment__c = 'RBG';
            caseNew1.Category__c = 'Complaint';
            caseNew1.Sub_Category__c = 'Service level2';
            caseNew1.Product_Category__c = 'Service level3';
            caseNew1.Issue__c = 'Service level4';
            caseNew1.Status = 'New';
            caseNew1.Description = 'Test create Case';
            caseNew1.AccountId = acct.id;
            caseNew1.CCRP_Number__c = '11-99770-9';
            caseNew1.Journey__c     = 'Journey__c';
            caseNew1.Problem_Type__c = 'Problem_Type__c';
            caseNew1.CXM_Latest_Response__c = 'FAIL_WAIT_FOR_RETRY';
            caseNew1.CXM_Latest_Status__c = 'FAIL_WAIT_FOR_RETRY';
            list_case.add(caseNew1);

            Case caseNew2 = New Case(); 
            caseNew2.recordtypeID = SErecordType.id;
            caseNew2.Subject = 'TestCase';
            caseNew2.PTA_Segment__c = 'RBG';
            caseNew2.Category__c = 'Complaint';
            caseNew2.Sub_Category__c = 'Service level2';
            caseNew2.Product_Category__c = 'Service level3';
            caseNew2.Issue__c = 'Service level4';
            caseNew2.Status = 'New';
            caseNew2.FCR__c = true;
            caseNew2.Description = 'Test create Case';
            caseNew2.AccountId = acct.id;
            caseNew2.CCRP_Number__c = '11-99770-9';
            caseNew2.Journey__c     = 'Journey__c';
            caseNew2.Problem_Type__c = 'Problem_Type__c';
            caseNew2.CXM_Latest_Response__c = 'OFFHOUR_RETRY';
            caseNew2.CXM_Latest_Status__c = 'OFFHOUR_RETRY';
            list_case.add(caseNew2);

            insert list_case;
            String hour = String.valueOf(Datetime.now().hour());
            String min = String.valueOf(Datetime.now().addMinutes(10).minute()); 
            String ss = String.valueOf(Datetime.now().second());

            //parse to cron expression
            String nextFireTime = ss + ' ' + min + ' ' + hour + ' * * ?';

            CXMServiceProvider s = new CXMServiceProvider(); 
            System.schedule('Job Started At ' + String.valueOf(Datetime.now()), nextFireTime, s);
        }
            Test.stopTest();
        
    }

}