global without sharing class CXMServiceProvider implements Schedulable
                                            , Database.Batchable<SObject>
                                            , Database.Stateful
                                            , Database.AllowsCallouts {
    private String query;
    private Integration_Info__c intInfo;
    private EIMManager manager = null;
    private static String FORMAT_DTM = 'dd/MM/yyyy HH:mm:ss';
    private Integer BATCHSIZE = 50;
    private String JSON  = 'application/json';
    private List<String> fieldLst = new List<String>();
    private String OBJ_STR = 'Case';

    private Datetime startDatetime = Datetime.now();

    private List<RTL_Online_Service_Log__c> inlogLst = new List<RTL_Online_Service_Log__c> ();
    private Map<Id,List<Attachment>> bodyAttMap     = new Map<Id,List<Attachment>>();

    private String INEGRATION_NAME = 'CXMCaseService';

    private String ACCOUNT_ID_FIELD = 'AccountId';
    private String REF_NAME_FIELD   = 'Account.Name';
    private String RECORDTYPE_DEV_NAME   = 'RecordType.DeveloperName';


    private String FAIL_WAIT_FOR_RETRY = 'FAIL_WAIT_FOR_RETRY';
    private String OFFHOUR_RETRY = 'OFFHOUR_RETRY';
    private String FAIL_MAX_FOR_RETRY = 'FAIL_MAX_FOR_RETRY';

    private Integer RETRY_MAX = 3;

    private String CXM_Latest_Response = 'CXM_Latest_Response__c';
    private String CXM_Latest_Status = 'CXM_Latest_Status__c';
    private String CXM_write_log_flag = 'CXM_write_log_flag__c';
    
    //Get CXM_Service_Field, CXM_Service_Start_Hour, CXM_Service_End_Hour Value
    private AppConfig__c cf = AppConfig__c.getValues('CXM_Service_Field');
    private String CXM_Service_Field = (cf == null ? 'LastModifiedDate': cf.Value__c);

    private AppConfig__c cf2 = AppConfig__c.getValues('CXM_Service_Start_Hour');
    private String CXM_Service_Start_Hour = (cf2 == null ? '08:05': cf2.Value__c);

    private AppConfig__c cf3 = AppConfig__c.getValues('CXM_Service_End_Hour');
    private String CXM_Service_End_Hour = (cf3 == null ? '19:45': cf3.Value__c);

    private String SUCCESS_CODE = '0000';
    private String FAIL_CODE = '9999';

    private String INEGRATION_ERROR_CODE = '1001';
    private String BACKEND_FAIL_CODE     = '1002';

    private Boolean isRetryBatch = false;
    private List<String> IdLstStr = new List<String>();

    private Map<Id,RecordType> mapCaseRecordType = new Map<Id,RecordType>([ SELECT Id,DeveloperName 
                                                                        FROM RecordType 
                                                                        WHERE SObjectType = 'Case']);

    private List<Integration_Mapping__c> intMappingLst = [SELECT Id
                                ,Condition_1__c,Condition_2__c
                                ,Condition_3__c,Field_Condition_1__c
                                ,Field_Condition_2__c,Field_Condition_3__c
                                ,Field_Value_1__c 
                                ,Field_Value_2__c
                                ,Field_Value_3__c
                                ,Variable_Field_1__c
                                From Integration_Mapping__c Where Name =: this.INEGRATION_NAME];

    private Map<String,RTL_CXM_Config__mdt> mapMetaConf {get{
        Map<String,RTL_CXM_Config__mdt> simList = new Map<String,RTL_CXM_Config__mdt>();
           for( RTL_CXM_Config__mdt conf : [select id,DeveloperName,Value__c from RTL_CXM_Config__mdt ] ){
                simList.put(conf.DeveloperName,conf);
            }
        
        return new Map<String,RTL_CXM_Config__mdt>(simList);
    }set;}

    private String getConfig(String confName,String defaultValue){ 
        try{
            //return CXM_Config__c.getValues(confName).Value__c;
            return mapMetaConf.get(confName).Value__c;
        }catch( Exception e ){
            return defaultValue;
        }
    }

    public CXMServiceProvider() {
        this.init_process();
    }

    global void execute(SchedulableContext sc) {
        CXMServiceProvider cxmProvider = new CXMServiceProvider();
        cxmProvider.runBatchForRetry();
    }
   
    public void integrateToCXM(Map<Id,Case> oldMap,Map<Id,Case> newMap){
        AppConfig__c cf = AppConfig__c.getValues('CXM_SERVICE_CONNECTOR');
        String isCXMServiceConnector = (cf == null ? 'false' : cf.Value__c);

        Boolean isEnable = isCXMServiceConnector.equalsIgnoreCase('true'); 

        if (!isEnable) return;
        try {
            Integration_Mapping__c intMapping = this.intMappingLst.get(0);
            String varfield1 = intMapping.Field_Condition_1__c;
            String fieldVal1 = intMapping.Field_Value_1__c;
            List<String> fieldLst1 = fieldVal1.split(';');
            String varfield2 = intMapping.Field_Condition_2__c;
            String fieldVal2 = intMapping.Field_Value_2__c;
            List<String> fieldLst2 = fieldVal2.split(';');

            String varfield3 = intMapping.Field_Condition_3__c;
            String fieldVal3 = intMapping.Field_Value_3__c;
            List<String> fieldLst3 = fieldVal3.split(';');

            for (String idVal : newMap.keyset()) {
                Case newcaseObj = newMap.get(idVal);
                String varNewVal1 = String.valueOf(newcaseObj.get(varfield1)); 
                String varNewVal2 = String.valueOf(newcaseObj.get(varfield2)); 
                String varNewVal3 = String.valueOf(newcaseObj.get(varfield3)); 

                if (oldMap == null) {
                    //do nothing
                }else {
                    //for non fcr case
                    Case oldcaseObj = oldMap.get(idVal);
                    String varOldVal1 = String.valueOf(oldcaseObj.get(varfield1)); 
                    //String varOldVal2 = String.valueOf(oldcaseObj.get(varfield2)); 
                    String recordDevName = mapCaseRecordType.get(newcaseObj.RecordTypeId).DeveloperName;

                    if (!varNewVal1.equals(varOldVal1)) {//status changed to Completed
                        //call cxm webservice
                        if (fieldLst1.contains(varNewVal1) 
                                && fieldLst2.contains(varNewVal2) 
                                && fieldLst3.contains(varNewVal3)
                                && newcaseObj.isClosed == true
                                && recordDevName.startsWithIgnoreCase('closed')
                                ) {
                            if (IdLstStr.size() == this.BATCHSIZE) {
                                    this.runBatch(IdLstStr);
                                    IdLstStr.clear();
                            }
                            IdLstStr.add(newcaseObj.Id);
                        }
                    }
                }
            }

            if (IdLstStr.size() > 0) this.runBatch(IdLstStr);
        }catch (Exception error) {
            //handble exception
            addIntegrationLog(startDatetime
                                ,DateTime.now()
                                ,null
                                ,error.getMessage()+ ' '+error.getStackTraceString()
                                ,false
                                ,''
                                ,''
                                ,'');
        }
    }

    private void init_process(){
        String fieldType = 'Request';
        List<EIM_Map__c> eimMapList = [SELECT Id
                                            ,Name
                                            ,Default_Value__c
                                            ,Attribute__c
                                            ,Attribute_value__c
                                            ,External_Field__c
                                            ,Job_Type__c
                                            ,SF_Field__c
                                            ,Node__c
                                            ,ParentNode__c
                                            ,Node_Key__c
                                            ,Object__c
                             FROM EIM_Map__c Where Job_Type__c =: this.INEGRATION_NAME 
                                  AND IsActive__c = true
                                  AND Field_Type__c =: fieldType
                             ORDER BY Field_Order__c ASC ];

        

        for (EIM_Map__c eim : eimMapList) {
            this.fieldLst.add(eim.SF_Field__c);
        }

        this.manager = new EIMManager(this.INEGRATION_NAME);
    }

    public void runBatch(List<String> IdsLst) {
        String objIds = '(\''+String.join(IdsLst,'\',\'')+'\')';

        this.query = 'SELECT '+String.join(this.fieldLst,',') 
                            + ','
                            + CXM_Service_Field
                            + ','
                            + CXM_Latest_Response
                            + ','
                            + CXM_Latest_Status
                            + ','
                            + ACCOUNT_ID_FIELD
                            + ','
                            + REF_NAME_FIELD
                        + ' FROM '+this.OBJ_STR 
                        + ' WHERE Id in '+objIds;

        Id BatchProcessIdForInt = Database.ExecuteBatch(this,this.BATCHSIZE);
    }

    public void runBatchForRetry() {
        AppConfig__c cf = AppConfig__c.getValues('CXM_SERVICE_CONNECTOR');
        String isCXMServiceConnector = (cf == null ? 'false' : cf.Value__c);

        Boolean isEnable = isCXMServiceConnector.equalsIgnoreCase('true'); 

        if (!isEnable) return;

        isRetryBatch = true;
        String retryCriteria = CXM_Latest_Status+' in (\''+OFFHOUR_RETRY+'\',\''+FAIL_WAIT_FOR_RETRY+'\')';

        this.query = 'SELECT '+String.join(this.fieldLst,',')
                            + ','
                            + CXM_Service_Field
                            + ','
                            + CXM_Latest_Response
                            + ','
                            + CXM_Latest_Status
                            + ','
                            + ACCOUNT_ID_FIELD
                            + ','
                            + REF_NAME_FIELD
                            + ','
                            + RECORDTYPE_DEV_NAME
                        + ' FROM '+this.OBJ_STR 
                        + ' WHERE '+retryCriteria
                        + ' LIMIT 50';

        Id BatchProcessIdForInt = Database.ExecuteBatch(this,this.BATCHSIZE);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(this.query);
    }

    global void execute(Database.BatchableContext bc, List<Sobject> scope){
        //Get Data
        Date today = Date.today();
        Integer d = today.day();
        Integer mo = today.month();
        Integer yr = today.year();

        DateTime startOffHour = DateTime.newInstance(yr, mo, d
                                            , Integer.valueOf(CXM_Service_Start_Hour.split(':').get(0))
                                            , Integer.valueOf(CXM_Service_Start_Hour.split(':').get(1))
                                            , 0);

        DateTime endOffHour = DateTime.newInstance(yr, mo, d
                                            , Integer.valueOf(CXM_Service_End_Hour.split(':').get(0))
                                            , Integer.valueOf(CXM_Service_End_Hour.split(':').get(1))
                                            , 0);

        for (SObject sobj : scope) {
            Datetime transactionDT = (Datetime)sobj.get(CXM_Service_Field);
            //Integer retryAmount = Integer.valueOf(sobj.get(CXM_Retry_Counter));
            sobj.put(CXM_write_log_flag,true);
            
            
            if (!this.isRetryBatch && (transactionDT < startOffHour || transactionDT > endOffHour)) {
                //stop call
                sobj.put(CXM_Latest_Response,OFFHOUR_RETRY);
                sobj.put(CXM_Latest_Status,OFFHOUR_RETRY);
                addIntegrationLog(startDatetime
                                    ,DateTime.now()
                                    ,sobj
                                    ,OFFHOUR_RETRY 
                                    +'\n TransDT '+transactionDT.format()
                                    +' startOffHour '+startOffHour.format()
                                    +' endOffHour '+endOffHour.format()
                                    ,false
                                    ,''
                                    ,String.valueOf('')
                                    ,sobj.Id);
            }else {
                connectToCXM(sobj);
            }
            
        }
        updateObjects(scope,'CXM','Id');
    }


    private void connectToCXM(SObject sobj) {
        connectToCXM(sobj,0);
    }

    private void connectToCXM(SObject sobj,Integer retryCount) {
        if (sobj == null) return;

        CXMService.TaskEventPort cxm = new CXMService.TaskEventPort();                
        CXMService.ForwardTaskEventResponse_element result 
                                = new CXMService.ForwardTaskEventResponse_element();

        String channel = 'CRM-CCRP';

        try {
            this.manager.getBodyRequest(sobj,JSON);

            String requestBody = this.manager.getBodyRequestMap().get(sobj.Id).escapeHtml4().replace('&nbsp;',' ');
            String dtm         = System.now().format(FORMAT_DTM);
            cxm.timeout_x = 1000*(Integer.valueOf(getConfig('CXM_TIMEOUT','60000'))/1000);
            //cxm.timeout_x = 10;
            result = cxm.add(sobj.Id,channel,dtm,requestBody); 

            /**
                1001    Integration error
                1002    Communication with backend failed

            **/
            String code = result.Code;
            Boolean isIntegrationSuccess = (code.equals(SUCCESS_CODE) || code.equals(FAIL_CODE));

            while(!isIntegrationSuccess && retryCount < RETRY_MAX) {
                result = cxm.add(sobj.Id,channel,dtm,requestBody);
                code = result.Code;
                isIntegrationSuccess = (code.equals(SUCCESS_CODE) || code.equals(FAIL_CODE));

                if (isIntegrationSuccess) break;
                else retryCount++;
            }

            if (isIntegrationSuccess) {
                sobj.put(CXM_Latest_Response,result.ReqId+' '+result.Description);
                sobj.put(CXM_Latest_Status,result.Code);

                addIntegrationLog(startDatetime
                                        ,DateTime.now()
                                        ,sobj
                                        ,'SUCCESS'+'\n'
                                        + result.ReqId+' '+result.Description
                                        ,true
                                        ,String.valueOf(cxm) + '\n'+requestBody
                                        ,String.valueOf(result)
                                        ,sobj.Id
                                        );
            }else {
                String errorRetry = FAIL_WAIT_FOR_RETRY;
                if (this.isRetryBatch) {
                    errorRetry = FAIL_MAX_FOR_RETRY;
                }

                sobj.put(CXM_Latest_Response,errorRetry);
                sobj.put(CXM_Latest_Status,errorRetry);
                addIntegrationLog(startDatetime
                                    ,DateTime.now()
                                    //,this.objectId
                                    ,sobj
                                    ,errorRetry+'\n'
                                    + result.ReqId+' '+result.Description
                                    ,false
                                    ,String.valueOf(cxm) + '\n'+requestBody
                                    ,String.valueOf(result)
                                    ,sobj.Id
                                    );
            }
        }catch( Exception error ){
            if (error.getMessage().contains('System.CalloutException IO Exception: Time out.') && retryCount < RETRY_MAX ) {
                retryCount++;
                connectToCXM(sobj,retryCount);
            }else {
                String errorRetry = FAIL_WAIT_FOR_RETRY;
                if (this.isRetryBatch) {
                    errorRetry = FAIL_MAX_FOR_RETRY;
                }

                sobj.put(CXM_Latest_Response,errorRetry);
                sobj.put(CXM_Latest_Status,errorRetry);
                addIntegrationLog(startDatetime
                                ,DateTime.now()
                                ,sobj
                                ,errorRetry+'\n'
                                +error.getMessage()+ ' '+error.getStackTraceString()
                                ,false
                                ,''
                                ,''
                                ,sobj.Id);
                return;
            }
        }
    }


    private void updateObjects(List<sObject> scope,String jobName,String refField) {
        Database.SaveResult[]  lsr = Database.update(scope, false);
        // Iterate through each returned result
        Integer runningIndex = 0;
        
        for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully Update '+jobName+' with ID: ' + sr.getId());
                }else {
                    String CSV_BODY = '';
                    // Operation failed, so get all errors                
                    String recordId = (String)scope[runningIndex].get(refField);
                    for(Database.Error err : sr.getErrors()) {
                        CSV_BODY = CSV_BODY +recordId + ','
                                                +err.getMessage() + ','
                                                +String.join(err.getFields(),',') + ','
                                                +err.getStatusCode()+'\n';

                        addIntegrationLog(startDatetime
                                ,DateTime.now()
                                //,this.objectId
                                ,scope[runningIndex]
                                ,CSV_BODY
                                ,false
                                ,''
                                ,''
                                ,recordId);
                }  

                runningIndex++; 
            }
        }
    }


    private void addIntegrationLog(DateTime startDatetime
                                  ,DateTime endDatetime
                                  ,Sobject sobj
                                  ,String errorMessages
                                  ,Boolean isRet
                                  ,String requestBody
                                  ,String responseBody
                                  ,String egLog
                                  ) {
            RTL_Online_Service_Log__c inlog = new RTL_Online_Service_Log__c ();
            inlog.RTL_Name_of_User__c = UserInfo.getName();

            if (sobj != null) {
                inlog.RTL_RM_ID__c = sobj.Id;
                inlog.RTL_Mulesoft_Log_Id__c = egLog 
                                                + ' ' + sobj.get('CaseNumber');
            }
            
            inlog.RTL_Error_Message__c = errorMessages;
            inlog.RTL_Start_DateTime__c = startDatetime;      
            inlog.RTL_End_DateTime__c   = endDatetime;
            inlog.RTL_Service_Name__c   = this.INEGRATION_NAME;
            inlog.RTL_Is_Success__c     = isRet;
            


            if (this.ACCOUNT_ID_FIELD != null 
                    && this.ACCOUNT_ID_FIELD.length() > 0 
                    && sobj != null
                    && sobj.get(this.ACCOUNT_ID_FIELD) != null) {
                inlog.RTL_Account__c = Id.valueOf((String)sobj.get(this.ACCOUNT_ID_FIELD));
            }

            if (this.REF_NAME_FIELD != null && this.REF_NAME_FIELD.length() > 0) {
                List<String> sffieldList = this.REF_NAME_FIELD.split('\\.');
                SObject tempSobj = null;

                for (Integer i=0;i<sffieldList.size()-1;i++) {
                    //tempSobj =  sobj.getSobject(sffieldList.get(i));
                }         

                if (tempSobj != null) {
                    String value = (String)tempSobj.get(sffieldList.get(sffieldList.size()-1));
                    inlog.RTL_Customer_Name__c = value;
                }
            }

            this.inlogLst.add(inlog);

            if (sobj != null) {
                List<Attachment> importattachmentfileList = new List<Attachment>();

                if(!String.isBlank(requestBody)) { 
                    Attachment requestAttachment = new Attachment();
                    requestAttachment.name = 'requestBody.txt';
                    requestAttachment.IsPrivate = false;
                    requestAttachment.body = Blob.valueOf(requestBody);
                    importattachmentfileList.add(requestAttachment);
                }

                if(!String.isBlank(responseBody)) { 
                    Attachment responseAttachment = new Attachment();
                    responseAttachment.name = 'responseBody.txt';
                    responseAttachment.IsPrivate = false;
                    responseAttachment.body = Blob.valueOf(responseBody);
                    importattachmentfileList.add(responseAttachment);
                }

                bodyAttMap.put(sobj.Id,importattachmentfileList);
            }
    }

    global void finish(Database.BatchableContext bc){
        if (this.inlogLst.size() > 0) {
            insert inlogLst;
            
            List<Attachment> allAttList = new List<Attachment>();
            for (RTL_Online_Service_Log__c log : this.inlogLst) {
                List<Attachment> attList = bodyAttMap.get(log.RTL_RM_ID__c);

                for (Attachment att : attList) {
                    att.ParentId = log.Id;
                }

                allAttList.addAll(attList);
            }
            if (allAttList.size() > 0) insert allAttList;
        }
    }
}