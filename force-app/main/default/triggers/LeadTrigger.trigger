trigger LeadTrigger on Lead (after insert, after update) {

    /***---------------        Check lead is not retail        ---------------***/   
    list<lead> listNewLead = new list<lead>();
    list<lead> listOldLead = new list<lead>();
    set<string> setRecordTypeId = new set<string>();
    for (recordtype r : [select id from recordtype where name like '%commercial%']){
        setRecordTypeId.add(r.id);
    }
    if (Trigger.new != null){
        for (lead l : Trigger.new){
            if (setRecordTypeId.contains(l.recordtypeId))
                listNewLead.add(l);
        }
    }
    if (Trigger.old != null){
        for (lead l : Trigger.old){
            if (setRecordTypeId.contains(l.recordtypeId))
                listOldLead.add(l);
        }
    }
    /***---------------        Check lead is not retail        ---------------***/


    Boolean RunTrigger = AppConfig__c.getValues('runtrigger').Value__c == 'true' ; 

    if(Trigger.isAfter && Trigger.isInsert)
    {
        system.debug('LeadTrigger : after insert ');
        if( RunTrigger || Test.isRunningTest() )
        {
            LeadTriggerHandler.updatePrimaryCampaign(listNewLead, new list<lead>());
            //CR Referral Enhancement RQ-004 sync lead status and referral stage
            RTL_ReferralLeadService.updateReferralStageContacted(listNewLead);
        }
    }
    
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        system.debug('LeadTrigger : after update ');
        if( RunTrigger || Test.isRunningTest() )
        {
            LeadTriggerHandler.updatePrimaryCampaign(listNewLead, listOldLead);
            LeadTriggerHandler.afterChangeOwnerLead(listNewLead, listOldLead);
            //CR Referral Enhancement RQ-004 sync lead status and referral stage
            RTL_ReferralLeadService.updateReferralInfo(new Map<Id, Lead>(listOldLead),new Map<Id, Lead>(listNewLead));

        }
    }
    
}